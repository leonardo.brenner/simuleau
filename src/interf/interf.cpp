//====================================================================================//
//                                                                                    //
//                               SIMULEAU User Interface                              //
//                                                                                    //
//====================================================================================//
//  This File:   interf.cpp                       Language: C++  (xlC and CC)         //
//  Software:    SIMULEAU                                                             //
//====================================================================================//
//  Creation:    12/apr/16                        by: Leonardo.Brenner@lsis.org       //
//  Last Change: 12/apr/16                        by: Leonardo.Brenner@lsis.org       //
//====================================================================================//
#include "simuleau.h"


 extern BPN *bpn;
 extern Schedule *schedule;
 extern Simulate *simulation;

//====================================================================================//
//   Simuleau Name Definitions                                                        //
//====================================================================================//

const simuleau_name no_name = "\0";         // Default empty value for simuleau_name

//====================================================================================//
//  Internal Function Add_Extension                                                  //
//====================================================================================//
void Add_Extension(const simuleau_name file, const file_types ft, simuleau_name tmp)
{
 strcpy(tmp, file);
 switch (ft) {
 case bpn_file : strcat(tmp, ".bpn");
                 break;
 case str_file : strcat(tmp, ".str");
                 break;
 case xct_file : strcat(tmp, ".xct");
                 break;
 case dyn_file : strcat(tmp, ".dyn");
                 break;
 case tim_file : strcat(tmp, ".tim");
                 break;
 case dbg_file : strcat(tmp, ".dbg");
                 break;
 case prf_file : strcat(tmp, ".prf");
                 break;
 case dot_file : // no extension!
                 break;
 default : strcat(tmp, ".err");
 }
}
//====================================================================================//
//  Open_File (to input)                                                              //
//====================================================================================//
void Open_File(const simuleau_name file, const file_types ft, ifstream & f_tmp)
{
 simuleau_name  n_tmp;

 Add_Extension(file, ft, n_tmp);

 // f_tmp.open(n_tmp, ios::nocreate);
 f_tmp.open(n_tmp);
 if (!f_tmp)
   cerr << "System Error - Cannot open the file '" << n_tmp << "' (#### 0001 ####)" << endl;
 else
   f_tmp.precision(simuleau_precision);
}

//====================================================================================//
//  Open_File (to output)                                                             //
//====================================================================================//
void Open_File(const simuleau_name file, const file_types ft, ofstream & f_tmp)
{
 simuleau_name  n_tmp;

 Add_Extension(file, ft, n_tmp);
 f_tmp.open(n_tmp);
 if (!f_tmp)
   cerr << "System Error - Cannot open the file '" << n_tmp << "' (#### 0002 ####)" << endl;
 else
   f_tmp.precision(simuleau_precision);
}
//====================================================================================//
//  Append_File (to output)                                                           //
//====================================================================================//
void Append_File(const simuleau_name file, const file_types ft, ofstream & f_tmp)
{
 simuleau_name  n_tmp;

 Add_Extension(file, ft, n_tmp);
 f_tmp.open(n_tmp, ios::app);
 if (!f_tmp)
   cerr << "System Error - Cannot open the file '" << n_tmp << "' (#### 0003 ####)" << endl;
 else
   f_tmp.precision(simuleau_precision);
}
//====================================================================================//
//  Close_File (to input)                                                             //
//====================================================================================//
void Close_File(const simuleau_name file, const file_types ft, ifstream & f_tmp)
{
 simuleau_name  n_tmp;

 Add_Extension(file, ft, n_tmp);
 f_tmp.close();
 if (!f_tmp)
   cerr << "System Error - Cannot close the file '" << n_tmp << "' (#### 0004 ####)" << endl;
 else
   cout << " :-) file '" << n_tmp << "' read\n";
}
//====================================================================================//
//  Close_File (to output)                                                            //
//====================================================================================//
void Close_File(const simuleau_name file, const file_types ft, ofstream & f_tmp)
{
 simuleau_name  n_tmp;

 Add_Extension(file, ft, n_tmp);
 f_tmp.close();
 if (!f_tmp)
   cerr << "System Error - Cannot close the file '" << n_tmp << "' (#### 0005 ####)" << endl;
 else
   cout << " :-) file '" << n_tmp << "' saved\n";
}
//====================================================================================//
//  Exists_File                                                                       //
//====================================================================================//
bool Exists_File(const simuleau_name file, const file_types ft)
{
 ifstream  f_tmp;
 simuleau_name n_tmp;

 Add_Extension(file, ft, n_tmp);
 // f_tmp.open(n_tmp, ios::nocreate);
 f_tmp.open(n_tmp);
 if (f_tmp) {
   return(true);
   f_tmp.close();
 }
 else
   return(false);
}

//====================================================================================//
//  Create Temporary Directories                                                      //
//====================================================================================//
void Create_Temporary_Directories()
{
 struct stat st;
 if(stat("str",&st) != 0){
   if (mkdir("str", S_IRWXU | S_IRWXG | S_IRWXO)) {
     cerr << "System Error - Cannot create directory '" << "str" << "' (#### 0006 ####)" << endl;
   }
 }
 if(stat("dbg",&st) != 0){
   if (mkdir("dbg", S_IRWXU | S_IRWXG | S_IRWXO)) {
     cerr << "System Error - Cannot create directory '" << "dbg" << "' (#### 0006 ####)" << endl;
   }
 }
  if(stat("dyn",&st) != 0){
   if (mkdir("dyn", S_IRWXU | S_IRWXG | S_IRWXO)) {
     cerr << "System Error - Cannot create directory '" << "dyn" << "' (#### 0006 ####)" << endl;
   }
 }
}
//====================================================================================//
//  Remove Temporary Directories                                                      //
//====================================================================================//
void Remove_Temporary_Directories()
{
 system("rm -rf str");
 system("rm -rf dbg");
 system("rm -rf dyn");
}
//====================================================================================//
//  Remove Probability Vectors                                                        //
//====================================================================================//
void Remove_Dynamics_Files()
{
 system("rm -f dyn/*.dyn");
}
//====================================================================================//
//  Remove Tim Files                                                                  //
//====================================================================================//
void Remove_Tim_Files()
{
 system("rm -f *.tim");
}

//====================================================================================//
//  Ask_a_File_Name                                                                   //
//====================================================================================//
void Ask_a_File_Name(simuleau_name & name, const file_types ft)
{
 cout << "\nEnter ";
 switch (ft) {
 case bpn_file : cout << "textual BPN";
                 break;
 case str_file : cout << "internal representation of BPN structure";
                 break;
 case xct_file : cout << "textual external controlled events";
                 break;
 case dyn_file : cout << "dynamics log";
                 break;
 case tim_file : cout << "time report";
                 break;
 case dbg_file : cout << "debugging";
                 break;
 case prf_file : cout << "user preferences";
                 break;
 default : cout << "unrecognized";
 }
 cout << " file name: ";
 cin >> name;
}
//====================================================================================//
//  Ask_an_Existing_File_Name                                                         //
//====================================================================================//
void Ask_an_Existing_File_Name(simuleau_name & name, const file_types ft)
{
 bool exists;

 do {
   Ask_a_File_Name(name, ft);
   exists = Exists_File(name, ft);
     if (!exists)
       cout << "This file can not be found!";
 } while (!exists);
}
//====================================================================================//
//  Permission_to_Proceed                                                             //
//====================================================================================//
bool Permission_to_Proceed(const simuleau_name name, const file_types ft)
{
 bool          answer = true;
 char          y_n;
 simuleau_name tmp;

 if (Exists_File(name, ft)) {
  Add_Extension(name, ft, tmp);
  cout << "File '" << tmp << "' already exists and will be replaced. "
       << "Procede anyway (y/n)? ";
  cin >> y_n;
  answer = ((y_n == 'y') || (y_n == 'Y'));
  cout << "\n";
 }
 return(answer);
}

//====================================================================================//
//  Get_Only_the_File_Name                                                            //
//====================================================================================//
void Get_Only_the_File_Name(const simuleau_name full_name, simuleau_name & just_the_file_name)
{
 int       last_slash_position = -1;
 int       mark = 0;

 do {
   if (full_name[mark] == '/')
     last_slash_position = mark;
   mark++;
 } while (full_name[mark] != '\0');
 strcpy(just_the_file_name, &full_name[last_slash_position+1]);
}


//====================================================================================//
//  Ask_a_Place                                                                       //
//====================================================================================//
place_id Ask_a_Place()
{
  simuleau_name name;
  place_id      i, place;

  cout << "The places are: ";
  for (i=0; i<bpn->NumberOfPlaces(); i++) {
     cout << "\n    " << i << ") " << bpn->GetPlaceName(i);
  }
  cout << "\n\nChoose a place (enter its index, -1 to show all): ";
  cin >> place;
  if (place == -1)
    return place;
  if ( (place < 0) || (place >= bpn->NumberOfPlaces()) ) {
    place = fst_place;
    cout << "Assuming first place\n";
  }
  return place;
}

//====================================================================================//
//  Ask_a_Transition                                                                       //
//====================================================================================//
trans_id Ask_a_Transition()
{
  simuleau_name name;
  trans_id      i, trans;

  cout << "The transitions are: ";
  for (i=0; i<bpn->NumberOfTransitions(); i++) {
     cout << "\n    " << i << ") " << bpn->GetTransitionName(i);
  }
  cout << "\n\nChoose a transition (enter its index, -1 to show all): ";
  cin >> trans;
  if (trans == -1)
    return trans;
  if ( (trans < 0) || (trans >= bpn->NumberOfTransitions()) ) {
    trans = fst_trans;
    cout << "Assuming first transition\n";
  }
  return trans;
}


//====================================================================================//
//  See_BPN                                                                           //
//====================================================================================//
void See_BPN()
{
  if (simulation != NULL ){
    if (!simulation->IsEmpty()) {
      cout << "\n---------------------------------------------------------------"
           << "\nInspecting the BPN model and scheduled events"
           << "\n---------------------------------------------------------------\n\n";
      simulation->Print(std::cout);
    }
  }
  else{
    cout << "There is no Batch Petri Net model (bpn) in memory.\n"
        << "You must compile a bpn using the option 1 in main menu.\n";
  }
}

//====================================================================================//
//  See_place                                                                         //
//====================================================================================//
void See_place()
{
  place_id placenumber;
  BPN *b;
  Place *p;

  if ((simulation != NULL)){
    placenumber = Ask_a_Place();
    b = simulation->GetBpn();
    if (placenumber == -1){
      cout << "\n---------------------------------------------------------------"
           << "\nInspecting all places"
           << "\n---------------------------------------------------------------\n\n";

      for (int i=0; i<b->NumberOfPlaces(); i++) {
        p = b->GetNode(b->GetPlaceName(i));
        p->Print(std::cout);
      }
      return;
    }
    cout << "\n---------------------------------------------------------------"
         << "\nInspecting place " << b->GetPlaceName(placenumber)
         << "\n---------------------------------------------------------------\n\n";

    p = b->GetNode(b->GetPlaceName(placenumber));
    p->Print(std::cout);
  }
}

//====================================================================================//
//  See_trans                                                                         //
//====================================================================================//
void See_trans()
{
  trans_id transnumber;
  BPN *b;
  Transition *t;

  if ((simulation != NULL)){
    transnumber = Ask_a_Transition();
    b = simulation->GetBpn();
    if (transnumber == -1){
      cout << "\n---------------------------------------------------------------"
           << "\nInspecting all transitions"
           << "\n---------------------------------------------------------------\n\n";

      for (int i=0; i<b->NumberOfTransitions(); i++) {
        t = b->GetNode(b->GetTransitionName(i));
        t->Print(std::cout);
      }
      return;
    }
    cout << "\n---------------------------------------------------------------"
         << "\nInspecting transition " << b->GetTransitionName(transnumber)
         << "\n---------------------------------------------------------------\n\n";

    t = b->GetNode(b->GetTransitionName(transnumber));
    t->Print(std::cout);
  }
}

//====================================================================================//
//  See_events                                                                        //
//====================================================================================//
void See_events()
{
  Schedule *s;

  if ((simulation != NULL)){
    cout << "\n---------------------------------------------------------------"
         << "\nInspecting all scheduled events"
         << "\n---------------------------------------------------------------\n\n";
    s = simulation->GetSchedule();
    s->Print(std::cout);
  }
}


//====================================================================================//
//  Ask an Answer                                                                     //
//====================================================================================//
bool Ask_an_Answer(const simuleau_name name)
{
 bool answer = true;
 char y_n;

 cout << name << " (y/n)? ";
 cin >> y_n;
 answer = ((y_n == 'y') || (y_n == 'Y') || (y_n == '0'));
 cout << "\n";
 return(answer);
}
//====================================================================================//
//  Ask an Error                                                                      //
//====================================================================================//
double Ask_an_Error()
{
 double  error;

 cout << "Enter tolerance: ";
 cin >> error;
 if (error < 0) {
   error = def_err;
   cout << "Assuming default value: " << def_err << "\n";
 }
 return(error);
}
//====================================================================================//
//  Ask an Inspect Time                                                               //
//====================================================================================//
double Ask_an_Inspect_Time()
{
 double  time;

 cout << "Enter time to inspect: ";
 cin >> time;
 if (time < 0) {
   time = zero;
   cout << "Assuming default value: " << zero << "\n";
 }
 return(time);
}

//====================================================================================//
//  Ask a Simulation Time                                                             //
//====================================================================================//
double Ask_a_Simulation_Time()
{
 double  time;

 cout << "Enter maximum simulation time: ";
 cin >> time;
 if (time <= 0) {
   time = def_time;
   cout << "Assuming default value: " << def_time << "\n";
 }
 return(time);
}

//====================================================================================//
//  Ask a Length Unity                                                                //
//====================================================================================//
char* Ask_a_Length_Unity()
{
 simuleau_name length;

 cout << "Enter the lenght unity: ";
 cin >> length;
 if (!strcmp(length,"0")) {
   strcpy(length, def_length_unity);
   cout << "Assuming default value: " << def_length_unity << "\n";
 }
 return(length);
}

//====================================================================================//
//  Ask a Time Unity                                                                  //
//====================================================================================//
char* Ask_a_Time_Unity()
{
 simuleau_name time;

 cout << "Enter the time unity: ";
 cin >> time;
 if (!strcmp(time,"0")) {
   strcpy(time, def_time_unity);
   cout << "Assuming default value: " << def_time_unity << "\n";
 }
 return(time);
}


//====================================================================================//
//  Notify_Time_Spend (stdout) - translations version                                 //
//====================================================================================//
void Notify_Time_Spend(const Timer T, const simuleau_name op)
{
 ofstream out_file;

 cout << "\nTranslation performed:  " << op << endl
      <<   " - user time spent:     " << T.usertime() << " seconds\n"
      <<   " - system time spent:   " << T.systime()  << " seconds\n"
      <<   " - real time spent:     " << T.realtime() << " seconds\n";
}


//====================================================================================//
//  Show Credits                                                                      //
//====================================================================================//
void Show_Credits()
{
 cout << "\n\n         Simuleau - Simulator of Batch Petri Nets - version 2016        "
      << "\n\n     Simuleau is a software tool for simulate Batch Petri Nets (BPN)"
      << "\n\n     The current version (2016)  is  (still)  an  experimental  version."
      <<   "\n Mathematical basis for BPN may be obtained in the literature."
//      << "\n\n     Simuleau is a collective software effort and its  authors  may  be"
//      <<   "\n reached at:"
//      <<   "\n     simuleau@lsis.org or http://www.lsis.org/spip.php?id_rubrique=385"
      << "\n\n     The distribution of this software is free.  Any  comercial  use  of"
      <<   "\n Simuleauu or modified versions of Simuleau cannot be performed  without"
      <<   "\n the  formal autorization of the authors.  Any scientific benefit of the"
      <<   "\n use of the Simuleau tool must mention it."
      << "\n\n     This version of Simuleau was released on: " << __RELEASE_DATE__
      <<   "\n                  and it has been compiled on: " << __DATE__
#ifdef _SIMULEAU_DEBUG_
      << " (debug version)\n\n";
#else
      << " (optimized version)\n\n";
#endif
}
//====================================================================================//
//  Say Goodbye                                                                       //
//====================================================================================//
void Say_Goodbye()
{
 cout << "\n\nThanks for using Simuleau!\n\n";
}
//====================================================================================//
//  Say Sorry                                                                         //
//====================================================================================//
void Say_Sorry()
{
 cout << "\nI'm sorry this option does not work right now!\n";
}

//====================================================================================//
//  Welcome_Get_Simuleau_Option                                                           //
//====================================================================================//
simuleau_options Welcome_Get_Simuleau_Option()
{
 char             option;
 simuleau_options answer = nothing;
 simuleau_name    buf;

 do
{cout << "\n\n     +--------------------------------------------------------+"
      <<   "\n     |          This is Simuleau 2016 - the BPN tool          |"
      <<   "\n     |  released on: " << __RELEASE_DATE__  << " -- compiled on: " << __DATE__ << "  |"
#ifdef _SIMULEAU_DEBUG_
      <<   "\n     |                  *** debug version ***                 |"
#endif
      <<   "\n     +--------------------------------------------------------+"
      << "\n\n  1) Compile a BPN model             4) Inspect data structures"
      <<   "\n  2) Simulate a compiled BPN model   5) Dynamics facilities"
      <<   "\n  3) Preferences                     6) About this version"
      << "\n\n    0) Exit Simuleau (Option 0 always exits the current menu)\n";

 cin >> option;
 switch (option) {
 case '1': cout << "\n\n             ******* Compiling a BPN Model *******"
                << "\n\n 1) Standard Compilation\n";
           cin >> option;
           switch (option) {
           case '1': answer = compile; break;
           } break;
 case '2': cout << "\n\n       ******* Simulating a BPN model *******"
                << "\n\n 1) With controlled events       3) On/Off control method\n"
                << " 2) Without conntrolled events   4) MF-On/Off control method\n";
//                << " 4) Computation of steady state  6) Computation of periodic steady state\n";




           cin >> option;
           switch (option) {
           case '1': answer = simulate_xct; break;
           case '2': answer = simulate; break;
           case '3': answer = simulate_onoff; break;
           case '4': answer = simulate_mf_onoff; break;
           case '5': answer = compute_steadystate; break;
           case '6': answer = compute_periodicsteadystate; break;
           } break;
 case '3': cout << "\n\n         ******* Changing Preferences *******"
                << "\n\n 1) Verbose Mode:                                  ";
           cout << PRF::prf.Sverb();
           cout <<   "\n 2) Maximum simulation time:                       "
                << PRF::prf.Max_Time();
           cout <<   "\n 3) Tolerance accepted:                            "
                << PRF::prf.Min_Err();
           cout <<   "\n 4) Length unity:                                  "
                << PRF::prf.Length_Unity();
           cout <<   "\n 5) Time unity:                                    "
                << PRF::prf.Time_Unity();
           cout << "\n\nEnter a number to change the corresponding parameter: ";
           cin >> option;
           switch (option) {
           case  '1': PRF::prf.Verb(Ask_an_Answer("Do you want verbose mode")); break;
           case  '2': PRF::prf.Max_Time(Ask_a_Simulation_Time());               break;
           case  '3': PRF::prf.Min_Err(Ask_an_Error());                         break;
           case  '4': PRF::prf.Length_Unity(Ask_a_Length_Unity());              break;
           case  '5': PRF::prf.Time_Unity(Ask_a_Time_Unity());                  break;
	       } break;
 case '4': cout << "\n\n              ******* Inspect Data Structures *******"
                << "\n\n 1) BPN in Memory "
                <<   "\n 2) Place         "
                <<   "\n 3) Transition    "
                <<   "\n 4) Controlled events\n";
           cin >> option;
           switch (option) {
           case '1': answer = see_bpn; break;
           case '2': answer = see_place; break;
           case '3': answer = see_trans; break;
           case '4': answer = see_events; } break;
/*case '5': cout << "\n\n         ******* Probability Vector Facilities *******"
                << "\n\n 1) local state probabilities   5) global state probabilities"
                <<   "\n 2) integrate results           6) compute residue"
                <<   "\n 3) compare two vectors         7) vector characteristics"
                <<   "\n 4) inspect probability vector  8) inspect reachable state space\n"
                <<   "\n                 9) import a MARCA vector\n";
         cin >> option;
         switch (option) {
         case  '1': answer =   integrate_local; break;
         case  '2': answer = integrate_results; break;
         case  '3': answer =       compare_vct; break;
         case  '4': answer =         slice_vct; break;
         case  '5': answer =   look_global_vct; break;
         case  '6': answer =       residue_vct; break;
         case  '7': answer =        charac_vct; break;
         case  '8': answer = analyse_reachable; break;
         case  '9': answer =  import_marca_vct; } break;*/
 case '6': answer = credits; break;
 /*case 'd': cout << "\n\n         ******* Entering Simuleau 2016 debug mode *******"
                <<   "\n              *** i.e., you shouldn't be here ***"
                << "\n\n 1) edit inner preferences   5) print rss"
                <<   "\n 2) print dsc                6) print cnd"
                <<   "\n 3) print dct                7) print cnd-diag"
                <<   "\n 4) print ftb                8) print inf"
                <<   "\n             9) Show Structures\n";
           cin >> option;
           switch (option) {
           case '1': Preferences();      break;
           case '2': DSC::dsc.print();   break;
           case '3': DCT::dct.print();   break;
           case '4': FTB::ftb.print();   break;
           case '5': RSS::rss.print();   break;
           case '6': CND::cnd.print();   break;
           case '7': CND::cnd.dprint();  break;
           case '8': INF::inf.print();   break;
           case '9': Show_Structures();  } break;*/
 case '0': answer = goodbye;
           break;
 default : answer = nothing;
           break;
 }
}
 while (answer == nothing);
 return(answer);
}








