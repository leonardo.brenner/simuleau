/* A Bison parser, made by GNU Bison 3.5.1.  */

/* Bison implementation for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2015, 2018-2020 Free Software Foundation,
   Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* C LALR(1) parser skeleton written by Richard Stallman, by
   simplifying the original so-called "semantic" parser.  */

/* All symbols defined below should begin with yy or YY, to avoid
   infringing on user name space.  This should be done even for local
   variables, as they might otherwise be expanded by user macros.
   There are some unavoidable exceptions within include files to
   define necessary library symbols; they are noted "INFRINGES ON
   USER NAME SPACE" below.  */

/* Undocumented macros, especially those whose name start with YY_,
   are private implementation details.  Do not rely on them.  */

/* Identify Bison output.  */
#define YYBISON 1

/* Bison version.  */
#define YYBISON_VERSION "3.5.1"

/* Skeleton name.  */
#define YYSKELETON_NAME "yacc.c"

/* Pure parsers.  */
#define YYPURE 0

/* Push parsers.  */
#define YYPUSH 0

/* Pull parsers.  */
#define YYPULL 1




/* First part of user prologue.  */
#line 1 "src/interf/glc.y"

//====================================================================================//
//                                                                                    //
//                           Parser and Semantic Analysis                             //
//                                                                                    //
//====================================================================================//
//  This File:   glc.y                           Tool: YACC                           //
//  Software:    SIMULEAU                                                             //
//====================================================================================//
//  Creation:    15/apr/16                        by: Leonardo.Brenner@lsis.org       //
//  Last Change: 15/apr/16                        by: Leonardo.Brenner@lsis.org       //
//====================================================================================//
//  YACC is a general-purpose parser generator that converts a context-free grammar   //
// a C program.In this module are make the actions to generate the internal SAN's     //
// tables and the others functionalities of this textual interface for PEPS.          //
//====================================================================================//
#include "bpnfile.h"
#include "simuleau.h"

//====================================================================================//
//                                   Global variables                                 //
//====================================================================================//

extern BPN *bpn; // Network structure
extern Schedule *schedule; // schedule structure

int not_error = 1;
int is_integer = 1;

int tour; // Set the times that the parser was called, the first to count the number
          // of places, the number of transitions and others

//====================================================================================//
//                                      Variables                                     //
//====================================================================================//

  int discreteplaces;             // number of discrete places
  int continuousplaces;           // number of continuous places
  int batchplaces;                // number of batch places
  int triangularplaces;           // number of triangular batch places

  int currentdiscreteplace;       // index of the current discrete place used to add place
  int currentcontinuousplace;     // index of the current continuous place
  int currentbatchplace;          // index of the current batch place
  int currenttriangularplace;     // index of the current triangular batch place

  int discretetransitions;        // number of discrete transitions
  int continuoustransitions;      // number of continuous transitions
  int batchtransitions;           // number of batch transitions

  int currentdiscretetransition;  // index of the current discrete place used to add place
  int currentcontinuoustransition;// index of the current continuous place
  int currentbatchtransition;     // index of the current batch place

  place_id      idplacenumber; // number of the place id
  simuleau_name idplace;       // name of the place id
  place_type    ptype;         // place type
  double        pdensity;      // place density
  double        pspeed;        // place speed
  double        plength;       // place length
  double        pflow;         // place flow

  double        bdensity;      // batch density
  double        bspeed;        // batch speed
  double        blength;       // batch length
  double        bposition;     // batch position

  trans_id      idtransitionnumber; // number of the transition id
  simuleau_name idtransition;       // name of the transition id
  trans_type    ttype;              // transition type
  double        ttime;              // time of the discrete transitions
  double        tsteadytime;        // steady time of the discrete transitions
  double        tflow;              // flow of the continuous and batch transitions
  double        tsteadyflow;        // steady flow of the continuous and batch transitions

  double        arcweight;     // arc weight

  simuleau_name idcevent;      // name of the controlled event
  ev_type       cetype;        // type of the controlled event
  simuleau_name idnode;        // name of the place or transition
  double        cedate;        // controlled event date
  double        cevalue;       // new value of the flow/speed

  simuleau_name   idbreakpoint;  // name of the breakpoint
  breakpoint_type bptype;        // type of the breakpoint (at (?) or every (*))
  double          bpdate;        // date or frequency


//====================================================================================//
//                                   Parser variables                                 //
//====================================================================================//

extern int yynerrs;  // Variable of the YACC generator to count the number of errors.
extern int yylineno;        // Variable of the YACC generator to count the current line

extern int yylex(void); // Get the next token
extern void yyrestart(FILE *input_file);
extern char *yytext;
extern FILE *yyin; // Input file


//====================================================================================//
//                                   Function errors                                  //
//====================================================================================//
//  These functions analysis which error occurred and put the error into a message    //
// more specific.                                                                     //
//====================================================================================//


//====================================================================================
// Function that YACC use to put syntatic message errors
//====================================================================================
extern void yyerror(string s)
{
  yynerrs++;
  not_error = 0;
  Syntatic_Error(yynerrs, yylineno, s, yytext);
}

//====================================================================================
// Function to put semantic error messages
//====================================================================================
void type_error(char* text, const char* error_number)
{
  yynerrs++;
  not_error = 0;
  Semantic_Error_YACC(yynerrs, yylineno, text, error_number);
}



#line 202 "src/interf/y.tab.cpp"

# ifndef YY_CAST
#  ifdef __cplusplus
#   define YY_CAST(Type, Val) static_cast<Type> (Val)
#   define YY_REINTERPRET_CAST(Type, Val) reinterpret_cast<Type> (Val)
#  else
#   define YY_CAST(Type, Val) ((Type) (Val))
#   define YY_REINTERPRET_CAST(Type, Val) ((Type) (Val))
#  endif
# endif
# ifndef YY_NULLPTR
#  if defined __cplusplus
#   if 201103L <= __cplusplus
#    define YY_NULLPTR nullptr
#   else
#    define YY_NULLPTR 0
#   endif
#  else
#   define YY_NULLPTR ((void*)0)
#  endif
# endif

/* Enabling verbose error messages.  */
#ifdef YYERROR_VERBOSE
# undef YYERROR_VERBOSE
# define YYERROR_VERBOSE 1
#else
# define YYERROR_VERBOSE 0
#endif

/* Use api.header.include to #include this header
   instead of duplicating it here.  */
#ifndef YY_YY_SRC_INTERF_Y_TAB_HPP_INCLUDED
# define YY_YY_SRC_INTERF_Y_TAB_HPP_INCLUDED
/* Debug traces.  */
#ifndef YYDEBUG
# define YYDEBUG 1
#endif
#if YYDEBUG
extern int yydebug;
#endif

/* Token type.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
  enum yytokentype
  {
    TK_ARC = 258,
    TK_BATCH = 259,
    TK_CONTINUOUS = 260,
    TK_CONTROLLED = 261,
    TK_DESCRIPTION = 262,
    TK_DISCRETE = 263,
    TK_EVENTS = 264,
    TK_FLOW = 265,
    TK_FUNCTION = 266,
    TK_INITIAL = 267,
    TK_INSPECTION = 268,
    TK_LENGTH = 269,
    TK_MARKING = 270,
    TK_MODEL = 271,
    TK_NETWORK = 272,
    TK_OUTPUT = 273,
    TK_PLACE = 274,
    TK_PLACES = 275,
    TK_SPEED = 276,
    TK_STEADY = 277,
    TK_TIME = 278,
    TK_TIMING = 279,
    TK_TRANSITION = 280,
    TK_TRANSITIONS = 281,
    TK_TRIANGULAR = 282,
    TK_UNITY = 283,
    TK_ID = 284,
    TK_INTEGER = 285,
    TK_REAL = 286,
    TK_EXPONENT = 287,
    TK_AT = 288,
    TK_EVERY = 289,
    TK_LEFT_BRACKET = 290,
    TK_RIGHT_BRACKET = 291,
    TK_PL_TR = 292,
    TK_ALL = 293,
    TK_LEFT_PARENTHESIS = 294,
    TK_RIGHT_PARENTHESIS = 295,
    TK_LEFT_SQUARE_BRACKET = 296,
    TK_RIGHT_SQUARE_BRACKET = 297,
    TK_SEMICOLON = 298,
    TK_COMMA = 299,
    TK_ATTRIB = 300,
    TK_COLON = 301
  };
#endif
/* Tokens.  */
#define TK_ARC 258
#define TK_BATCH 259
#define TK_CONTINUOUS 260
#define TK_CONTROLLED 261
#define TK_DESCRIPTION 262
#define TK_DISCRETE 263
#define TK_EVENTS 264
#define TK_FLOW 265
#define TK_FUNCTION 266
#define TK_INITIAL 267
#define TK_INSPECTION 268
#define TK_LENGTH 269
#define TK_MARKING 270
#define TK_MODEL 271
#define TK_NETWORK 272
#define TK_OUTPUT 273
#define TK_PLACE 274
#define TK_PLACES 275
#define TK_SPEED 276
#define TK_STEADY 277
#define TK_TIME 278
#define TK_TIMING 279
#define TK_TRANSITION 280
#define TK_TRANSITIONS 281
#define TK_TRIANGULAR 282
#define TK_UNITY 283
#define TK_ID 284
#define TK_INTEGER 285
#define TK_REAL 286
#define TK_EXPONENT 287
#define TK_AT 288
#define TK_EVERY 289
#define TK_LEFT_BRACKET 290
#define TK_RIGHT_BRACKET 291
#define TK_PL_TR 292
#define TK_ALL 293
#define TK_LEFT_PARENTHESIS 294
#define TK_RIGHT_PARENTHESIS 295
#define TK_LEFT_SQUARE_BRACKET 296
#define TK_RIGHT_SQUARE_BRACKET 297
#define TK_SEMICOLON 298
#define TK_COMMA 299
#define TK_ATTRIB 300
#define TK_COLON 301

/* Value type.  */
#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED
typedef int YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define YYSTYPE_IS_DECLARED 1
#endif


extern YYSTYPE yylval;

int yyparse (void);

#endif /* !YY_YY_SRC_INTERF_Y_TAB_HPP_INCLUDED  */



#ifdef short
# undef short
#endif

/* On compilers that do not define __PTRDIFF_MAX__ etc., make sure
   <limits.h> and (if available) <stdint.h> are included
   so that the code can choose integer types of a good width.  */

#ifndef __PTRDIFF_MAX__
# include <limits.h> /* INFRINGES ON USER NAME SPACE */
# if defined __STDC_VERSION__ && 199901 <= __STDC_VERSION__
#  include <stdint.h> /* INFRINGES ON USER NAME SPACE */
#  define YY_STDINT_H
# endif
#endif

/* Narrow types that promote to a signed type and that can represent a
   signed or unsigned integer of at least N bits.  In tables they can
   save space and decrease cache pressure.  Promoting to a signed type
   helps avoid bugs in integer arithmetic.  */

#ifdef __INT_LEAST8_MAX__
typedef __INT_LEAST8_TYPE__ yytype_int8;
#elif defined YY_STDINT_H
typedef int_least8_t yytype_int8;
#else
typedef signed char yytype_int8;
#endif

#ifdef __INT_LEAST16_MAX__
typedef __INT_LEAST16_TYPE__ yytype_int16;
#elif defined YY_STDINT_H
typedef int_least16_t yytype_int16;
#else
typedef short yytype_int16;
#endif

#if defined __UINT_LEAST8_MAX__ && __UINT_LEAST8_MAX__ <= __INT_MAX__
typedef __UINT_LEAST8_TYPE__ yytype_uint8;
#elif (!defined __UINT_LEAST8_MAX__ && defined YY_STDINT_H \
       && UINT_LEAST8_MAX <= INT_MAX)
typedef uint_least8_t yytype_uint8;
#elif !defined __UINT_LEAST8_MAX__ && UCHAR_MAX <= INT_MAX
typedef unsigned char yytype_uint8;
#else
typedef short yytype_uint8;
#endif

#if defined __UINT_LEAST16_MAX__ && __UINT_LEAST16_MAX__ <= __INT_MAX__
typedef __UINT_LEAST16_TYPE__ yytype_uint16;
#elif (!defined __UINT_LEAST16_MAX__ && defined YY_STDINT_H \
       && UINT_LEAST16_MAX <= INT_MAX)
typedef uint_least16_t yytype_uint16;
#elif !defined __UINT_LEAST16_MAX__ && USHRT_MAX <= INT_MAX
typedef unsigned short yytype_uint16;
#else
typedef int yytype_uint16;
#endif

#ifndef YYPTRDIFF_T
# if defined __PTRDIFF_TYPE__ && defined __PTRDIFF_MAX__
#  define YYPTRDIFF_T __PTRDIFF_TYPE__
#  define YYPTRDIFF_MAXIMUM __PTRDIFF_MAX__
# elif defined PTRDIFF_MAX
#  ifndef ptrdiff_t
#   include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  endif
#  define YYPTRDIFF_T ptrdiff_t
#  define YYPTRDIFF_MAXIMUM PTRDIFF_MAX
# else
#  define YYPTRDIFF_T long
#  define YYPTRDIFF_MAXIMUM LONG_MAX
# endif
#endif

#ifndef YYSIZE_T
# ifdef __SIZE_TYPE__
#  define YYSIZE_T __SIZE_TYPE__
# elif defined size_t
#  define YYSIZE_T size_t
# elif defined __STDC_VERSION__ && 199901 <= __STDC_VERSION__
#  include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  define YYSIZE_T size_t
# else
#  define YYSIZE_T unsigned
# endif
#endif

#define YYSIZE_MAXIMUM                                  \
  YY_CAST (YYPTRDIFF_T,                                 \
           (YYPTRDIFF_MAXIMUM < YY_CAST (YYSIZE_T, -1)  \
            ? YYPTRDIFF_MAXIMUM                         \
            : YY_CAST (YYSIZE_T, -1)))

#define YYSIZEOF(X) YY_CAST (YYPTRDIFF_T, sizeof (X))

/* Stored state numbers (used for stacks). */
typedef yytype_uint8 yy_state_t;

/* State numbers in computations.  */
typedef int yy_state_fast_t;

#ifndef YY_
# if defined YYENABLE_NLS && YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> /* INFRINGES ON USER NAME SPACE */
#   define YY_(Msgid) dgettext ("bison-runtime", Msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(Msgid) Msgid
# endif
#endif

#ifndef YY_ATTRIBUTE_PURE
# if defined __GNUC__ && 2 < __GNUC__ + (96 <= __GNUC_MINOR__)
#  define YY_ATTRIBUTE_PURE __attribute__ ((__pure__))
# else
#  define YY_ATTRIBUTE_PURE
# endif
#endif

#ifndef YY_ATTRIBUTE_UNUSED
# if defined __GNUC__ && 2 < __GNUC__ + (7 <= __GNUC_MINOR__)
#  define YY_ATTRIBUTE_UNUSED __attribute__ ((__unused__))
# else
#  define YY_ATTRIBUTE_UNUSED
# endif
#endif

/* Suppress unused-variable warnings by "using" E.  */
#if ! defined lint || defined __GNUC__
# define YYUSE(E) ((void) (E))
#else
# define YYUSE(E) /* empty */
#endif

#if defined __GNUC__ && ! defined __ICC && 407 <= __GNUC__ * 100 + __GNUC_MINOR__
/* Suppress an incorrect diagnostic about yylval being uninitialized.  */
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN                            \
    _Pragma ("GCC diagnostic push")                                     \
    _Pragma ("GCC diagnostic ignored \"-Wuninitialized\"")              \
    _Pragma ("GCC diagnostic ignored \"-Wmaybe-uninitialized\"")
# define YY_IGNORE_MAYBE_UNINITIALIZED_END      \
    _Pragma ("GCC diagnostic pop")
#else
# define YY_INITIAL_VALUE(Value) Value
#endif
#ifndef YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_END
#endif
#ifndef YY_INITIAL_VALUE
# define YY_INITIAL_VALUE(Value) /* Nothing. */
#endif

#if defined __cplusplus && defined __GNUC__ && ! defined __ICC && 6 <= __GNUC__
# define YY_IGNORE_USELESS_CAST_BEGIN                          \
    _Pragma ("GCC diagnostic push")                            \
    _Pragma ("GCC diagnostic ignored \"-Wuseless-cast\"")
# define YY_IGNORE_USELESS_CAST_END            \
    _Pragma ("GCC diagnostic pop")
#endif
#ifndef YY_IGNORE_USELESS_CAST_BEGIN
# define YY_IGNORE_USELESS_CAST_BEGIN
# define YY_IGNORE_USELESS_CAST_END
#endif


#define YY_ASSERT(E) ((void) (0 && (E)))

#if ! defined yyoverflow || YYERROR_VERBOSE

/* The parser invokes alloca or malloc; define the necessary symbols.  */

# ifdef YYSTACK_USE_ALLOCA
#  if YYSTACK_USE_ALLOCA
#   ifdef __GNUC__
#    define YYSTACK_ALLOC __builtin_alloca
#   elif defined __BUILTIN_VA_ARG_INCR
#    include <alloca.h> /* INFRINGES ON USER NAME SPACE */
#   elif defined _AIX
#    define YYSTACK_ALLOC __alloca
#   elif defined _MSC_VER
#    include <malloc.h> /* INFRINGES ON USER NAME SPACE */
#    define alloca _alloca
#   else
#    define YYSTACK_ALLOC alloca
#    if ! defined _ALLOCA_H && ! defined EXIT_SUCCESS
#     include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
      /* Use EXIT_SUCCESS as a witness for stdlib.h.  */
#     ifndef EXIT_SUCCESS
#      define EXIT_SUCCESS 0
#     endif
#    endif
#   endif
#  endif
# endif

# ifdef YYSTACK_ALLOC
   /* Pacify GCC's 'empty if-body' warning.  */
#  define YYSTACK_FREE(Ptr) do { /* empty */; } while (0)
#  ifndef YYSTACK_ALLOC_MAXIMUM
    /* The OS might guarantee only one guard page at the bottom of the stack,
       and a page size can be as small as 4096 bytes.  So we cannot safely
       invoke alloca (N) if N exceeds 4096.  Use a slightly smaller number
       to allow for a few compiler-allocated temporary stack slots.  */
#   define YYSTACK_ALLOC_MAXIMUM 4032 /* reasonable circa 2006 */
#  endif
# else
#  define YYSTACK_ALLOC YYMALLOC
#  define YYSTACK_FREE YYFREE
#  ifndef YYSTACK_ALLOC_MAXIMUM
#   define YYSTACK_ALLOC_MAXIMUM YYSIZE_MAXIMUM
#  endif
#  if (defined __cplusplus && ! defined EXIT_SUCCESS \
       && ! ((defined YYMALLOC || defined malloc) \
             && (defined YYFREE || defined free)))
#   include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#   ifndef EXIT_SUCCESS
#    define EXIT_SUCCESS 0
#   endif
#  endif
#  ifndef YYMALLOC
#   define YYMALLOC malloc
#   if ! defined malloc && ! defined EXIT_SUCCESS
void *malloc (YYSIZE_T); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
#  ifndef YYFREE
#   define YYFREE free
#   if ! defined free && ! defined EXIT_SUCCESS
void free (void *); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
# endif
#endif /* ! defined yyoverflow || YYERROR_VERBOSE */


#if (! defined yyoverflow \
     && (! defined __cplusplus \
         || (defined YYSTYPE_IS_TRIVIAL && YYSTYPE_IS_TRIVIAL)))

/* A type that is properly aligned for any stack member.  */
union yyalloc
{
  yy_state_t yyss_alloc;
  YYSTYPE yyvs_alloc;
};

/* The size of the maximum gap between one aligned stack and the next.  */
# define YYSTACK_GAP_MAXIMUM (YYSIZEOF (union yyalloc) - 1)

/* The size of an array large to enough to hold all stacks, each with
   N elements.  */
# define YYSTACK_BYTES(N) \
     ((N) * (YYSIZEOF (yy_state_t) + YYSIZEOF (YYSTYPE)) \
      + YYSTACK_GAP_MAXIMUM)

# define YYCOPY_NEEDED 1

/* Relocate STACK from its old location to the new one.  The
   local variables YYSIZE and YYSTACKSIZE give the old and new number of
   elements in the stack, and YYPTR gives the new location of the
   stack.  Advance YYPTR to a properly aligned location for the next
   stack.  */
# define YYSTACK_RELOCATE(Stack_alloc, Stack)                           \
    do                                                                  \
      {                                                                 \
        YYPTRDIFF_T yynewbytes;                                         \
        YYCOPY (&yyptr->Stack_alloc, Stack, yysize);                    \
        Stack = &yyptr->Stack_alloc;                                    \
        yynewbytes = yystacksize * YYSIZEOF (*Stack) + YYSTACK_GAP_MAXIMUM; \
        yyptr += yynewbytes / YYSIZEOF (*yyptr);                        \
      }                                                                 \
    while (0)

#endif

#if defined YYCOPY_NEEDED && YYCOPY_NEEDED
/* Copy COUNT objects from SRC to DST.  The source and destination do
   not overlap.  */
# ifndef YYCOPY
#  if defined __GNUC__ && 1 < __GNUC__
#   define YYCOPY(Dst, Src, Count) \
      __builtin_memcpy (Dst, Src, YY_CAST (YYSIZE_T, (Count)) * sizeof (*(Src)))
#  else
#   define YYCOPY(Dst, Src, Count)              \
      do                                        \
        {                                       \
          YYPTRDIFF_T yyi;                      \
          for (yyi = 0; yyi < (Count); yyi++)   \
            (Dst)[yyi] = (Src)[yyi];            \
        }                                       \
      while (0)
#  endif
# endif
#endif /* !YYCOPY_NEEDED */

/* YYFINAL -- State number of the termination state.  */
#define YYFINAL  6
/* YYLAST -- Last index in YYTABLE.  */
#define YYLAST   147

/* YYNTOKENS -- Number of terminals.  */
#define YYNTOKENS  47
/* YYNNTS -- Number of nonterminals.  */
#define YYNNTS  92
/* YYNRULES -- Number of rules.  */
#define YYNRULES  131
/* YYNSTATES -- Number of states.  */
#define YYNSTATES  232

#define YYUNDEFTOK  2
#define YYMAXUTOK   301


/* YYTRANSLATE(TOKEN-NUM) -- Symbol number corresponding to TOKEN-NUM
   as returned by yylex, with out-of-bounds checking.  */
#define YYTRANSLATE(YYX)                                                \
  (0 <= (YYX) && (YYX) <= YYMAXUTOK ? yytranslate[YYX] : YYUNDEFTOK)

/* YYTRANSLATE[TOKEN-NUM] -- Symbol number corresponding to TOKEN-NUM
   as returned by yylex.  */
static const yytype_int8 yytranslate[] =
{
       0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    31,    32,    33,    34,
      35,    36,    37,    38,    39,    40,    41,    42,    43,    44,
      45,    46
};

#if YYDEBUG
  /* YYRLINE[YYN] -- Source line where rule number YYN was defined.  */
static const yytype_int16 yyrline[] =
{
       0,   155,   155,   163,   167,   167,   168,   172,   176,   176,
     177,   181,   181,   182,   186,   187,   191,   192,   193,   202,
     206,   206,   210,   210,   220,   227,   219,   252,   253,   257,
     258,   259,   260,   268,   271,   272,   273,   268,   274,   278,
     282,   292,   296,   314,   318,   318,   319,   320,   318,   337,
     341,   342,   346,   347,   355,   356,   360,   378,   382,   382,
     383,   384,   382,   401,   405,   406,   410,   411,   419,   420,
     419,   436,   440,   444,   444,   445,   456,   463,   467,   455,
     489,   490,   494,   495,   496,   500,   500,   501,   501,   505,
     506,   510,   510,   511,   511,   521,   522,   521,   533,   537,
     541,   541,   542,   550,   550,   551,   555,   555,   556,   556,
     557,   555,   596,   600,   604,   605,   613,   613,   614,   618,
     618,   618,   619,   618,   633,   637,   641,   642,   646,   647,
     651,   652
};
#endif

#if YYDEBUG || YYERROR_VERBOSE || 0
/* YYTNAME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
   First, the terminals, then, starting at YYNTOKENS, nonterminals.  */
static const char *const yytname[] =
{
  "$end", "error", "$undefined", "TK_ARC", "TK_BATCH", "TK_CONTINUOUS",
  "TK_CONTROLLED", "TK_DESCRIPTION", "TK_DISCRETE", "TK_EVENTS", "TK_FLOW",
  "TK_FUNCTION", "TK_INITIAL", "TK_INSPECTION", "TK_LENGTH", "TK_MARKING",
  "TK_MODEL", "TK_NETWORK", "TK_OUTPUT", "TK_PLACE", "TK_PLACES",
  "TK_SPEED", "TK_STEADY", "TK_TIME", "TK_TIMING", "TK_TRANSITION",
  "TK_TRANSITIONS", "TK_TRIANGULAR", "TK_UNITY", "TK_ID", "TK_INTEGER",
  "TK_REAL", "TK_EXPONENT", "TK_AT", "TK_EVERY", "TK_LEFT_BRACKET",
  "TK_RIGHT_BRACKET", "TK_PL_TR", "TK_ALL", "TK_LEFT_PARENTHESIS",
  "TK_RIGHT_PARENTHESIS", "TK_LEFT_SQUARE_BRACKET",
  "TK_RIGHT_SQUARE_BRACKET", "TK_SEMICOLON", "TK_COMMA", "TK_ATTRIB",
  "TK_COLON", "$accept", "start", "blk_def", "blk_model", "$@1",
  "blk_unity", "blk_lenght", "$@2", "blk_time", "$@3", "semi_colon",
  "number", "blk_net", "blk_places", "$@4", "blk_transitions", "$@5",
  "blk_place", "$@6", "$@7", "blk_place2", "blk_place_type",
  "blk_function", "$@8", "$@9", "$@10", "$@11", "blk_triangular_place",
  "blk_initial_marking", "blk_initial_marking_int", "blk_batch", "$@12",
  "$@13", "$@14", "$@15", "blk_batch2", "blk_triangular_batch",
  "blk_steady_marking", "blk_steady_marking_int", "blk_steady_batch",
  "$@16", "$@17", "$@18", "$@19", "blk_steady_batch2",
  "blk_steady_triangular_batch", "blk_place_arcs", "$@20", "$@21",
  "blk_place_arcs2", "blk_place_weight", "$@22", "blk_transition", "$@23",
  "$@24", "$@25", "blk_transition2", "blk_transition_type",
  "blk_transition_function", "$@26", "$@27",
  "blk_steady_transition_function", "blk_steady_transition_function_int",
  "$@28", "$@29", "blk_transition_arcs", "$@30", "$@31",
  "blk_transition_arcs2", "blk_transition_weight", "$@32",
  "blk_controlled_events", "$@33", "controlled_events", "$@34", "$@35",
  "$@36", "$@37", "$@38", "controlled_events2", "controlled_events_type",
  "blk_time_inspection", "$@39", "time_inspection", "$@40", "$@41", "$@42",
  "$@43", "time_inspection2", "blk_op_time", "blk_op_pt", "blk_pl_tr", YY_NULLPTR
};
#endif

# ifdef YYPRINT
/* YYTOKNUM[NUM] -- (External) token number corresponding to the
   (internal) symbol number NUM (which must be that of a token).  */
static const yytype_int16 yytoknum[] =
{
       0,   256,   257,   258,   259,   260,   261,   262,   263,   264,
     265,   266,   267,   268,   269,   270,   271,   272,   273,   274,
     275,   276,   277,   278,   279,   280,   281,   282,   283,   284,
     285,   286,   287,   288,   289,   290,   291,   292,   293,   294,
     295,   296,   297,   298,   299,   300,   301
};
# endif

#define YYPACT_NINF (-113)

#define yypact_value_is_default(Yyn) \
  ((Yyn) == YYPACT_NINF)

#define YYTABLE_NINF (-1)

#define yytable_value_is_error(Yyn) \
  0

  /* YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
     STATE-NUM.  */
static const yytype_int8 yypact[] =
{
      -8,    10,    20,    11,    26,  -113,  -113,    38,    40,    21,
    -113,    27,     9,    33,    45,    32,    13,    28,  -113,  -113,
    -113,  -113,    34,  -113,    46,  -113,    35,    16,    43,  -113,
    -113,    36,  -113,  -113,    39,    41,  -113,    42,  -113,  -113,
      44,     9,  -113,  -113,    47,  -113,    18,  -113,  -113,  -113,
       9,    30,  -113,    48,    29,  -113,    -2,    50,     0,     1,
    -113,  -113,  -113,  -113,    31,    14,  -113,  -113,    37,  -113,
    -113,    12,    61,  -113,  -113,  -113,    52,    53,  -113,  -113,
    -113,  -113,  -113,  -113,     2,  -113,    -5,    51,    63,    54,
      55,  -113,    56,    57,    -9,  -113,    12,    64,    62,    12,
      12,    73,    12,  -113,  -113,  -113,  -113,     9,  -113,    66,
      68,    67,  -113,  -113,     3,  -113,  -113,  -113,    58,   -16,
      69,    93,    43,    59,    70,    72,    74,  -113,    79,    65,
      44,    12,    12,  -113,    71,  -113,    -1,    77,  -113,  -113,
    -113,  -113,    12,    12,    95,    42,    12,  -113,  -113,  -113,
    -113,  -113,    12,  -113,    76,  -113,  -113,  -113,  -113,    85,
    -113,  -113,  -113,    75,    78,  -113,  -113,    82,    83,    84,
    -113,    86,    12,    12,    87,    12,  -113,  -113,  -113,    88,
       9,  -113,  -113,    12,  -113,    67,    12,  -113,  -113,    89,
      90,  -113,    92,  -113,  -113,  -113,    79,    36,    12,    96,
      12,    91,  -113,    97,  -113,  -113,  -113,  -113,  -113,  -113,
    -113,    12,  -113,    94,  -113,    12,   100,    98,  -113,  -113,
      12,   101,    99,  -113,  -113,   105,  -113,   102,  -113,   106,
    -113,  -113
};

  /* YYDEFACT[STATE-NUM] -- Default reduction number in state STATE-NUM.
     Performed when YYTABLE does not specify something else to do.  Zero
     means the default is an error.  */
static const yytype_uint8 yydefact[] =
{
       6,     0,     0,     0,    10,     4,     1,     0,   105,     0,
       3,    13,    15,     0,     0,   118,     0,     0,     7,    14,
       5,    20,     0,   103,     0,     2,     0,     0,     0,    22,
      19,   112,   116,     8,     0,     0,    21,     0,   106,   104,
     124,    15,    11,    24,     0,    23,     0,   119,   117,     9,
      15,     0,    76,     0,     0,    12,     0,     0,     0,     0,
      31,    30,    29,    32,     0,     0,   114,   115,     0,   126,
     127,     0,    38,    84,    83,    82,     0,     0,    17,    16,
      18,   120,    33,    25,     0,   107,     0,     0,     0,     0,
       0,    77,     0,     0,     0,   121,     0,     0,    55,     0,
       0,    90,     0,   128,   130,   131,   129,    15,    34,     0,
       0,    71,    87,    85,     0,    78,   108,   122,     0,    49,
       0,     0,    28,     0,     0,     0,     0,    89,    98,     0,
     124,     0,     0,    42,     0,    43,    63,     0,    27,    26,
      88,    86,     0,     0,     0,    81,     0,   125,   123,    35,
      44,    41,     0,    56,     0,    57,    68,    93,    91,     0,
      80,    79,   109,     0,     0,    58,    54,    75,     0,     0,
      95,     0,     0,     0,     0,     0,    69,    94,    92,   102,
      15,    36,    45,     0,    73,    71,     0,    96,   110,    40,
       0,    59,     0,    72,    70,   100,    98,   112,     0,     0,
       0,     0,    74,     0,    99,    97,   113,   111,    39,    37,
      46,     0,   101,    53,    60,     0,     0,    67,    52,    47,
       0,     0,    51,    66,    61,    49,    48,    65,    50,    63,
      62,    64
};

  /* YYPGOTO[NTERM-NUM].  */
static const yytype_int8 yypgoto[] =
{
    -113,  -113,  -113,  -113,  -113,  -113,  -113,  -113,  -113,  -113,
     -41,   -95,  -113,  -113,  -113,  -113,  -113,   -14,  -113,  -113,
    -113,  -113,  -113,  -113,  -113,  -113,  -113,  -113,  -113,  -113,
    -110,  -113,  -113,  -113,  -113,  -113,  -113,  -113,  -113,  -112,
    -113,  -113,  -113,  -113,  -113,  -113,   -67,  -113,  -113,  -113,
    -113,  -113,   -17,  -113,  -113,  -113,  -113,  -113,  -113,  -113,
    -113,  -113,  -113,  -113,  -113,   -66,  -113,  -113,  -113,  -113,
    -113,  -113,  -113,   -68,  -113,  -113,  -113,  -113,  -113,  -113,
    -113,  -113,  -113,    17,  -113,  -113,  -113,  -113,  -113,  -113,
    -113,  -113
};

  /* YYDEFGOTO[NTERM-NUM].  */
static const yytype_int16 yydefgoto[] =
{
      -1,     2,     3,     4,    12,    10,    11,    41,    18,    50,
      20,    81,     8,    22,    28,    30,    37,    36,    51,    88,
     139,    64,    83,    87,   118,   163,   189,   199,    98,   134,
     135,   164,   190,   213,   222,   226,   216,   111,   154,   155,
     174,   201,   217,   227,   230,   221,   122,   167,   185,   194,
     176,   192,    45,    57,   101,   128,   161,    76,    91,   124,
     123,   115,   127,   169,   168,   145,   179,   196,   205,   187,
     203,    15,    31,    39,    46,    92,   129,   171,   197,   207,
      68,    25,    40,    48,    54,    86,   107,   130,   148,    71,
      95,   106
};

  /* YYTABLE[YYPACT[STATE-NUM]] -- What to do in state STATE-NUM.  If
     positive, shift that token.  If negative, reduce the rule whose
     number is the opposite.  If YYTABLE_NINF, syntax error.  */
static const yytype_uint8 yytable[] =
{
      49,   108,    60,    61,   112,   113,    62,   116,     1,    55,
      66,   104,    89,   125,    78,    79,    80,   105,    73,    74,
       6,    67,    75,   132,   133,    63,    90,   126,     7,    78,
      79,    80,    93,    94,    69,    70,   149,   150,   152,     5,
       9,   153,    78,    79,    80,    13,    14,   157,   158,    16,
      17,   162,    19,    21,    23,    24,    27,   165,    26,    32,
      29,    34,    35,    53,    33,    38,   117,    44,    42,    56,
      43,    72,    82,    47,    59,    97,    52,   181,   182,   109,
     184,    77,    85,   120,   110,   121,   103,    58,   191,    65,
      96,   195,    84,    99,   100,   114,   137,   144,   159,   140,
     102,   119,   131,   208,   136,   210,   156,   151,   138,   146,
     141,   142,   166,   143,   170,   228,   214,   231,   193,   172,
     218,   175,   173,   177,   178,   223,   180,   186,   160,   206,
     204,   183,   202,   198,   200,   211,   209,   212,   215,   188,
     219,   224,   220,   225,   132,   152,   229,   147
};

static const yytype_uint8 yycheck[] =
{
      41,    96,     4,     5,    99,   100,     8,   102,    16,    50,
      10,    20,    10,    10,    30,    31,    32,    26,     4,     5,
       0,    21,     8,    39,   119,    27,    24,    24,    17,    30,
      31,    32,    37,    38,    33,    34,   131,   132,    39,    29,
      14,   136,    30,    31,    32,     7,     6,   142,   143,    28,
      23,   146,    43,    20,     9,    23,    28,   152,    45,    13,
      26,    45,    19,    45,    29,    29,   107,    25,    29,    39,
      29,    40,    11,    29,    45,    12,    29,   172,   173,    15,
     175,    44,    29,    15,    22,    18,    29,    39,   183,    39,
      39,   186,    40,    39,    39,    22,     3,    18,     3,    40,
      44,    35,    44,   198,    35,   200,    29,    36,   122,    44,
      40,    39,    36,    39,    29,   225,   211,   229,   185,    44,
     215,    39,    44,    40,    40,   220,    40,    39,   145,   197,
     196,    44,    40,    44,    44,    44,    40,    40,    44,   180,
      40,    40,    44,    44,    39,    39,    44,   130
};

  /* YYSTOS[STATE-NUM] -- The (internal number of the) accessing
     symbol of state STATE-NUM.  */
static const yytype_uint8 yystos[] =
{
       0,    16,    48,    49,    50,    29,     0,    17,    59,    14,
      52,    53,    51,     7,     6,   118,    28,    23,    55,    43,
      57,    20,    60,     9,    23,   128,    45,    28,    61,    26,
      62,   119,    13,    29,    45,    19,    64,    63,    29,   120,
     129,    54,    29,    29,    25,    99,   121,    29,   130,    57,
      56,    65,    29,    45,   131,    57,    39,   100,    39,    45,
       4,     5,     8,    27,    68,    39,    10,    21,   127,    33,
      34,   136,    40,     4,     5,     8,   104,    44,    30,    31,
      32,    58,    11,    69,    40,    29,   132,    70,    66,    10,
      24,   105,   122,    37,    38,   137,    39,    12,    75,    39,
      39,   101,    44,    29,    20,    26,   138,   133,    58,    15,
      22,    84,    58,    58,    22,   108,    58,    57,    71,    35,
      15,    18,    93,   107,   106,    10,    24,   109,   102,   123,
     134,    44,    39,    58,    76,    77,    35,     3,    64,    67,
      40,    40,    39,    39,    18,   112,    44,   130,   135,    58,
      58,    36,    39,    58,    85,    86,    29,    58,    58,     3,
      99,   103,    58,    72,    78,    58,    36,    94,   111,   110,
      29,   124,    44,    44,    87,    39,    97,    40,    40,   113,
      40,    58,    58,    44,    58,    95,    39,   116,    57,    73,
      79,    58,    98,    93,    96,    58,   114,   125,    44,    74,
      44,    88,    40,   117,   112,   115,   120,   126,    58,    40,
      58,    44,    40,    80,    58,    44,    83,    89,    58,    40,
      44,    92,    81,    58,    40,    44,    82,    90,    77,    44,
      91,    86
};

  /* YYR1[YYN] -- Symbol number of symbol that rule YYN derives.  */
static const yytype_uint8 yyr1[] =
{
       0,    47,    48,    49,    51,    50,    50,    52,    54,    53,
      53,    56,    55,    55,    57,    57,    58,    58,    58,    59,
      61,    60,    63,    62,    65,    66,    64,    67,    67,    68,
      68,    68,    68,    70,    71,    72,    73,    69,    69,    74,
      74,    75,    76,    76,    78,    79,    80,    81,    77,    77,
      82,    82,    83,    83,    84,    84,    85,    85,    87,    88,
      89,    90,    86,    86,    91,    91,    92,    92,    94,    95,
      93,    93,    96,    98,    97,    97,   100,   101,   102,    99,
     103,   103,   104,   104,   104,   106,   105,   107,   105,   108,
     108,   110,   109,   111,   109,   113,   114,   112,   112,   115,
     117,   116,   116,   119,   118,   118,   121,   122,   123,   124,
     125,   120,   120,   126,   127,   127,   129,   128,   128,   131,
     132,   133,   134,   130,   130,   135,   136,   136,   137,   137,
     138,   138
};

  /* YYR2[YYN] -- Number of symbols on the right hand side of rule YYN.  */
static const yytype_int8 yyr2[] =
{
       0,     2,     4,     2,     0,     4,     0,     2,     0,     6,
       0,     0,     6,     0,     1,     0,     1,     1,     1,     4,
       0,     3,     0,     3,     0,     0,    12,     1,     0,     1,
       1,     1,     1,     0,     0,     0,     0,    13,     0,     2,
       0,     5,     1,     1,     0,     0,     0,     0,    13,     0,
       2,     0,     2,     0,     5,     0,     1,     1,     0,     0,
       0,     0,    13,     0,     2,     0,     2,     0,     0,     0,
       7,     0,     1,     0,     4,     0,     0,     0,     0,    12,
       1,     0,     1,     1,     1,     0,     5,     0,     5,     2,
       0,     0,     5,     0,     5,     0,     0,     7,     0,     1,
       0,     4,     0,     0,     4,     0,     0,     0,     0,     0,
       0,    18,     0,     1,     1,     1,     0,     4,     0,     0,
       0,     0,     0,    11,     0,     1,     1,     1,     2,     2,
       1,     1
};


#define yyerrok         (yyerrstatus = 0)
#define yyclearin       (yychar = YYEMPTY)
#define YYEMPTY         (-2)
#define YYEOF           0

#define YYACCEPT        goto yyacceptlab
#define YYABORT         goto yyabortlab
#define YYERROR         goto yyerrorlab


#define YYRECOVERING()  (!!yyerrstatus)

#define YYBACKUP(Token, Value)                                    \
  do                                                              \
    if (yychar == YYEMPTY)                                        \
      {                                                           \
        yychar = (Token);                                         \
        yylval = (Value);                                         \
        YYPOPSTACK (yylen);                                       \
        yystate = *yyssp;                                         \
        goto yybackup;                                            \
      }                                                           \
    else                                                          \
      {                                                           \
        yyerror (YY_("syntax error: cannot back up")); \
        YYERROR;                                                  \
      }                                                           \
  while (0)

/* Error token number */
#define YYTERROR        1
#define YYERRCODE       256



/* Enable debugging if requested.  */
#if YYDEBUG

# ifndef YYFPRINTF
#  include <stdio.h> /* INFRINGES ON USER NAME SPACE */
#  define YYFPRINTF fprintf
# endif

# define YYDPRINTF(Args)                        \
do {                                            \
  if (yydebug)                                  \
    YYFPRINTF Args;                             \
} while (0)

/* This macro is provided for backward compatibility. */
#ifndef YY_LOCATION_PRINT
# define YY_LOCATION_PRINT(File, Loc) ((void) 0)
#endif


# define YY_SYMBOL_PRINT(Title, Type, Value, Location)                    \
do {                                                                      \
  if (yydebug)                                                            \
    {                                                                     \
      YYFPRINTF (stderr, "%s ", Title);                                   \
      yy_symbol_print (stderr,                                            \
                  Type, Value); \
      YYFPRINTF (stderr, "\n");                                           \
    }                                                                     \
} while (0)


/*-----------------------------------.
| Print this symbol's value on YYO.  |
`-----------------------------------*/

static void
yy_symbol_value_print (FILE *yyo, int yytype, YYSTYPE const * const yyvaluep)
{
  FILE *yyoutput = yyo;
  YYUSE (yyoutput);
  if (!yyvaluep)
    return;
# ifdef YYPRINT
  if (yytype < YYNTOKENS)
    YYPRINT (yyo, yytoknum[yytype], *yyvaluep);
# endif
  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  YYUSE (yytype);
  YY_IGNORE_MAYBE_UNINITIALIZED_END
}


/*---------------------------.
| Print this symbol on YYO.  |
`---------------------------*/

static void
yy_symbol_print (FILE *yyo, int yytype, YYSTYPE const * const yyvaluep)
{
  YYFPRINTF (yyo, "%s %s (",
             yytype < YYNTOKENS ? "token" : "nterm", yytname[yytype]);

  yy_symbol_value_print (yyo, yytype, yyvaluep);
  YYFPRINTF (yyo, ")");
}

/*------------------------------------------------------------------.
| yy_stack_print -- Print the state stack from its BOTTOM up to its |
| TOP (included).                                                   |
`------------------------------------------------------------------*/

static void
yy_stack_print (yy_state_t *yybottom, yy_state_t *yytop)
{
  YYFPRINTF (stderr, "Stack now");
  for (; yybottom <= yytop; yybottom++)
    {
      int yybot = *yybottom;
      YYFPRINTF (stderr, " %d", yybot);
    }
  YYFPRINTF (stderr, "\n");
}

# define YY_STACK_PRINT(Bottom, Top)                            \
do {                                                            \
  if (yydebug)                                                  \
    yy_stack_print ((Bottom), (Top));                           \
} while (0)


/*------------------------------------------------.
| Report that the YYRULE is going to be reduced.  |
`------------------------------------------------*/

static void
yy_reduce_print (yy_state_t *yyssp, YYSTYPE *yyvsp, int yyrule)
{
  int yylno = yyrline[yyrule];
  int yynrhs = yyr2[yyrule];
  int yyi;
  YYFPRINTF (stderr, "Reducing stack by rule %d (line %d):\n",
             yyrule - 1, yylno);
  /* The symbols being reduced.  */
  for (yyi = 0; yyi < yynrhs; yyi++)
    {
      YYFPRINTF (stderr, "   $%d = ", yyi + 1);
      yy_symbol_print (stderr,
                       yystos[+yyssp[yyi + 1 - yynrhs]],
                       &yyvsp[(yyi + 1) - (yynrhs)]
                                              );
      YYFPRINTF (stderr, "\n");
    }
}

# define YY_REDUCE_PRINT(Rule)          \
do {                                    \
  if (yydebug)                          \
    yy_reduce_print (yyssp, yyvsp, Rule); \
} while (0)

/* Nonzero means print parse trace.  It is left uninitialized so that
   multiple parsers can coexist.  */
int yydebug;
#else /* !YYDEBUG */
# define YYDPRINTF(Args)
# define YY_SYMBOL_PRINT(Title, Type, Value, Location)
# define YY_STACK_PRINT(Bottom, Top)
# define YY_REDUCE_PRINT(Rule)
#endif /* !YYDEBUG */


/* YYINITDEPTH -- initial size of the parser's stacks.  */
#ifndef YYINITDEPTH
# define YYINITDEPTH 200
#endif

/* YYMAXDEPTH -- maximum size the stacks can grow to (effective only
   if the built-in stack extension method is used).

   Do not make this value too large; the results are undefined if
   YYSTACK_ALLOC_MAXIMUM < YYSTACK_BYTES (YYMAXDEPTH)
   evaluated with infinite-precision integer arithmetic.  */

#ifndef YYMAXDEPTH
# define YYMAXDEPTH 10000
#endif


#if YYERROR_VERBOSE

# ifndef yystrlen
#  if defined __GLIBC__ && defined _STRING_H
#   define yystrlen(S) (YY_CAST (YYPTRDIFF_T, strlen (S)))
#  else
/* Return the length of YYSTR.  */
static YYPTRDIFF_T
yystrlen (const char *yystr)
{
  YYPTRDIFF_T yylen;
  for (yylen = 0; yystr[yylen]; yylen++)
    continue;
  return yylen;
}
#  endif
# endif

# ifndef yystpcpy
#  if defined __GLIBC__ && defined _STRING_H && defined _GNU_SOURCE
#   define yystpcpy stpcpy
#  else
/* Copy YYSRC to YYDEST, returning the address of the terminating '\0' in
   YYDEST.  */
static char *
yystpcpy (char *yydest, const char *yysrc)
{
  char *yyd = yydest;
  const char *yys = yysrc;

  while ((*yyd++ = *yys++) != '\0')
    continue;

  return yyd - 1;
}
#  endif
# endif

# ifndef yytnamerr
/* Copy to YYRES the contents of YYSTR after stripping away unnecessary
   quotes and backslashes, so that it's suitable for yyerror.  The
   heuristic is that double-quoting is unnecessary unless the string
   contains an apostrophe, a comma, or backslash (other than
   backslash-backslash).  YYSTR is taken from yytname.  If YYRES is
   null, do not copy; instead, return the length of what the result
   would have been.  */
static YYPTRDIFF_T
yytnamerr (char *yyres, const char *yystr)
{
  if (*yystr == '"')
    {
      YYPTRDIFF_T yyn = 0;
      char const *yyp = yystr;

      for (;;)
        switch (*++yyp)
          {
          case '\'':
          case ',':
            goto do_not_strip_quotes;

          case '\\':
            if (*++yyp != '\\')
              goto do_not_strip_quotes;
            else
              goto append;

          append:
          default:
            if (yyres)
              yyres[yyn] = *yyp;
            yyn++;
            break;

          case '"':
            if (yyres)
              yyres[yyn] = '\0';
            return yyn;
          }
    do_not_strip_quotes: ;
    }

  if (yyres)
    return yystpcpy (yyres, yystr) - yyres;
  else
    return yystrlen (yystr);
}
# endif

/* Copy into *YYMSG, which is of size *YYMSG_ALLOC, an error message
   about the unexpected token YYTOKEN for the state stack whose top is
   YYSSP.

   Return 0 if *YYMSG was successfully written.  Return 1 if *YYMSG is
   not large enough to hold the message.  In that case, also set
   *YYMSG_ALLOC to the required number of bytes.  Return 2 if the
   required number of bytes is too large to store.  */
static int
yysyntax_error (YYPTRDIFF_T *yymsg_alloc, char **yymsg,
                yy_state_t *yyssp, int yytoken)
{
  enum { YYERROR_VERBOSE_ARGS_MAXIMUM = 5 };
  /* Internationalized format string. */
  const char *yyformat = YY_NULLPTR;
  /* Arguments of yyformat: reported tokens (one for the "unexpected",
     one per "expected"). */
  char const *yyarg[YYERROR_VERBOSE_ARGS_MAXIMUM];
  /* Actual size of YYARG. */
  int yycount = 0;
  /* Cumulated lengths of YYARG.  */
  YYPTRDIFF_T yysize = 0;

  /* There are many possibilities here to consider:
     - If this state is a consistent state with a default action, then
       the only way this function was invoked is if the default action
       is an error action.  In that case, don't check for expected
       tokens because there are none.
     - The only way there can be no lookahead present (in yychar) is if
       this state is a consistent state with a default action.  Thus,
       detecting the absence of a lookahead is sufficient to determine
       that there is no unexpected or expected token to report.  In that
       case, just report a simple "syntax error".
     - Don't assume there isn't a lookahead just because this state is a
       consistent state with a default action.  There might have been a
       previous inconsistent state, consistent state with a non-default
       action, or user semantic action that manipulated yychar.
     - Of course, the expected token list depends on states to have
       correct lookahead information, and it depends on the parser not
       to perform extra reductions after fetching a lookahead from the
       scanner and before detecting a syntax error.  Thus, state merging
       (from LALR or IELR) and default reductions corrupt the expected
       token list.  However, the list is correct for canonical LR with
       one exception: it will still contain any token that will not be
       accepted due to an error action in a later state.
  */
  if (yytoken != YYEMPTY)
    {
      int yyn = yypact[+*yyssp];
      YYPTRDIFF_T yysize0 = yytnamerr (YY_NULLPTR, yytname[yytoken]);
      yysize = yysize0;
      yyarg[yycount++] = yytname[yytoken];
      if (!yypact_value_is_default (yyn))
        {
          /* Start YYX at -YYN if negative to avoid negative indexes in
             YYCHECK.  In other words, skip the first -YYN actions for
             this state because they are default actions.  */
          int yyxbegin = yyn < 0 ? -yyn : 0;
          /* Stay within bounds of both yycheck and yytname.  */
          int yychecklim = YYLAST - yyn + 1;
          int yyxend = yychecklim < YYNTOKENS ? yychecklim : YYNTOKENS;
          int yyx;

          for (yyx = yyxbegin; yyx < yyxend; ++yyx)
            if (yycheck[yyx + yyn] == yyx && yyx != YYTERROR
                && !yytable_value_is_error (yytable[yyx + yyn]))
              {
                if (yycount == YYERROR_VERBOSE_ARGS_MAXIMUM)
                  {
                    yycount = 1;
                    yysize = yysize0;
                    break;
                  }
                yyarg[yycount++] = yytname[yyx];
                {
                  YYPTRDIFF_T yysize1
                    = yysize + yytnamerr (YY_NULLPTR, yytname[yyx]);
                  if (yysize <= yysize1 && yysize1 <= YYSTACK_ALLOC_MAXIMUM)
                    yysize = yysize1;
                  else
                    return 2;
                }
              }
        }
    }

  switch (yycount)
    {
# define YYCASE_(N, S)                      \
      case N:                               \
        yyformat = S;                       \
      break
    default: /* Avoid compiler warnings. */
      YYCASE_(0, YY_("syntax error"));
      YYCASE_(1, YY_("syntax error, unexpected %s"));
      YYCASE_(2, YY_("syntax error, unexpected %s, expecting %s"));
      YYCASE_(3, YY_("syntax error, unexpected %s, expecting %s or %s"));
      YYCASE_(4, YY_("syntax error, unexpected %s, expecting %s or %s or %s"));
      YYCASE_(5, YY_("syntax error, unexpected %s, expecting %s or %s or %s or %s"));
# undef YYCASE_
    }

  {
    /* Don't count the "%s"s in the final size, but reserve room for
       the terminator.  */
    YYPTRDIFF_T yysize1 = yysize + (yystrlen (yyformat) - 2 * yycount) + 1;
    if (yysize <= yysize1 && yysize1 <= YYSTACK_ALLOC_MAXIMUM)
      yysize = yysize1;
    else
      return 2;
  }

  if (*yymsg_alloc < yysize)
    {
      *yymsg_alloc = 2 * yysize;
      if (! (yysize <= *yymsg_alloc
             && *yymsg_alloc <= YYSTACK_ALLOC_MAXIMUM))
        *yymsg_alloc = YYSTACK_ALLOC_MAXIMUM;
      return 1;
    }

  /* Avoid sprintf, as that infringes on the user's name space.
     Don't have undefined behavior even if the translation
     produced a string with the wrong number of "%s"s.  */
  {
    char *yyp = *yymsg;
    int yyi = 0;
    while ((*yyp = *yyformat) != '\0')
      if (*yyp == '%' && yyformat[1] == 's' && yyi < yycount)
        {
          yyp += yytnamerr (yyp, yyarg[yyi++]);
          yyformat += 2;
        }
      else
        {
          ++yyp;
          ++yyformat;
        }
  }
  return 0;
}
#endif /* YYERROR_VERBOSE */

/*-----------------------------------------------.
| Release the memory associated to this symbol.  |
`-----------------------------------------------*/

static void
yydestruct (const char *yymsg, int yytype, YYSTYPE *yyvaluep)
{
  YYUSE (yyvaluep);
  if (!yymsg)
    yymsg = "Deleting";
  YY_SYMBOL_PRINT (yymsg, yytype, yyvaluep, yylocationp);

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  YYUSE (yytype);
  YY_IGNORE_MAYBE_UNINITIALIZED_END
}




/* The lookahead symbol.  */
int yychar;

/* The semantic value of the lookahead symbol.  */
YYSTYPE yylval;
/* Number of syntax errors so far.  */
int yynerrs;


/*----------.
| yyparse.  |
`----------*/

int
yyparse (void)
{
    yy_state_fast_t yystate;
    /* Number of tokens to shift before error messages enabled.  */
    int yyerrstatus;

    /* The stacks and their tools:
       'yyss': related to states.
       'yyvs': related to semantic values.

       Refer to the stacks through separate pointers, to allow yyoverflow
       to reallocate them elsewhere.  */

    /* The state stack.  */
    yy_state_t yyssa[YYINITDEPTH];
    yy_state_t *yyss;
    yy_state_t *yyssp;

    /* The semantic value stack.  */
    YYSTYPE yyvsa[YYINITDEPTH];
    YYSTYPE *yyvs;
    YYSTYPE *yyvsp;

    YYPTRDIFF_T yystacksize;

  int yyn;
  int yyresult;
  /* Lookahead token as an internal (translated) token number.  */
  int yytoken = 0;
  /* The variables used to return semantic value and location from the
     action routines.  */
  YYSTYPE yyval;

#if YYERROR_VERBOSE
  /* Buffer for error messages, and its allocated size.  */
  char yymsgbuf[128];
  char *yymsg = yymsgbuf;
  YYPTRDIFF_T yymsg_alloc = sizeof yymsgbuf;
#endif

#define YYPOPSTACK(N)   (yyvsp -= (N), yyssp -= (N))

  /* The number of symbols on the RHS of the reduced rule.
     Keep to zero when no symbol should be popped.  */
  int yylen = 0;

  yyssp = yyss = yyssa;
  yyvsp = yyvs = yyvsa;
  yystacksize = YYINITDEPTH;

  YYDPRINTF ((stderr, "Starting parse\n"));

  yystate = 0;
  yyerrstatus = 0;
  yynerrs = 0;
  yychar = YYEMPTY; /* Cause a token to be read.  */
  goto yysetstate;


/*------------------------------------------------------------.
| yynewstate -- push a new state, which is found in yystate.  |
`------------------------------------------------------------*/
yynewstate:
  /* In all cases, when you get here, the value and location stacks
     have just been pushed.  So pushing a state here evens the stacks.  */
  yyssp++;


/*--------------------------------------------------------------------.
| yysetstate -- set current state (the top of the stack) to yystate.  |
`--------------------------------------------------------------------*/
yysetstate:
  YYDPRINTF ((stderr, "Entering state %d\n", yystate));
  YY_ASSERT (0 <= yystate && yystate < YYNSTATES);
  YY_IGNORE_USELESS_CAST_BEGIN
  *yyssp = YY_CAST (yy_state_t, yystate);
  YY_IGNORE_USELESS_CAST_END

  if (yyss + yystacksize - 1 <= yyssp)
#if !defined yyoverflow && !defined YYSTACK_RELOCATE
    goto yyexhaustedlab;
#else
    {
      /* Get the current used size of the three stacks, in elements.  */
      YYPTRDIFF_T yysize = yyssp - yyss + 1;

# if defined yyoverflow
      {
        /* Give user a chance to reallocate the stack.  Use copies of
           these so that the &'s don't force the real ones into
           memory.  */
        yy_state_t *yyss1 = yyss;
        YYSTYPE *yyvs1 = yyvs;

        /* Each stack pointer address is followed by the size of the
           data in use in that stack, in bytes.  This used to be a
           conditional around just the two extra args, but that might
           be undefined if yyoverflow is a macro.  */
        yyoverflow (YY_("memory exhausted"),
                    &yyss1, yysize * YYSIZEOF (*yyssp),
                    &yyvs1, yysize * YYSIZEOF (*yyvsp),
                    &yystacksize);
        yyss = yyss1;
        yyvs = yyvs1;
      }
# else /* defined YYSTACK_RELOCATE */
      /* Extend the stack our own way.  */
      if (YYMAXDEPTH <= yystacksize)
        goto yyexhaustedlab;
      yystacksize *= 2;
      if (YYMAXDEPTH < yystacksize)
        yystacksize = YYMAXDEPTH;

      {
        yy_state_t *yyss1 = yyss;
        union yyalloc *yyptr =
          YY_CAST (union yyalloc *,
                   YYSTACK_ALLOC (YY_CAST (YYSIZE_T, YYSTACK_BYTES (yystacksize))));
        if (! yyptr)
          goto yyexhaustedlab;
        YYSTACK_RELOCATE (yyss_alloc, yyss);
        YYSTACK_RELOCATE (yyvs_alloc, yyvs);
# undef YYSTACK_RELOCATE
        if (yyss1 != yyssa)
          YYSTACK_FREE (yyss1);
      }
# endif

      yyssp = yyss + yysize - 1;
      yyvsp = yyvs + yysize - 1;

      YY_IGNORE_USELESS_CAST_BEGIN
      YYDPRINTF ((stderr, "Stack size increased to %ld\n",
                  YY_CAST (long, yystacksize)));
      YY_IGNORE_USELESS_CAST_END

      if (yyss + yystacksize - 1 <= yyssp)
        YYABORT;
    }
#endif /* !defined yyoverflow && !defined YYSTACK_RELOCATE */

  if (yystate == YYFINAL)
    YYACCEPT;

  goto yybackup;


/*-----------.
| yybackup.  |
`-----------*/
yybackup:
  /* Do appropriate processing given the current state.  Read a
     lookahead token if we need one and don't already have one.  */

  /* First try to decide what to do without reference to lookahead token.  */
  yyn = yypact[yystate];
  if (yypact_value_is_default (yyn))
    goto yydefault;

  /* Not known => get a lookahead token if don't already have one.  */

  /* YYCHAR is either YYEMPTY or YYEOF or a valid lookahead symbol.  */
  if (yychar == YYEMPTY)
    {
      YYDPRINTF ((stderr, "Reading a token: "));
      yychar = yylex ();
    }

  if (yychar <= YYEOF)
    {
      yychar = yytoken = YYEOF;
      YYDPRINTF ((stderr, "Now at end of input.\n"));
    }
  else
    {
      yytoken = YYTRANSLATE (yychar);
      YY_SYMBOL_PRINT ("Next token is", yytoken, &yylval, &yylloc);
    }

  /* If the proper action on seeing token YYTOKEN is to reduce or to
     detect an error, take that action.  */
  yyn += yytoken;
  if (yyn < 0 || YYLAST < yyn || yycheck[yyn] != yytoken)
    goto yydefault;
  yyn = yytable[yyn];
  if (yyn <= 0)
    {
      if (yytable_value_is_error (yyn))
        goto yyerrlab;
      yyn = -yyn;
      goto yyreduce;
    }

  /* Count tokens shifted since error; after three, turn off error
     status.  */
  if (yyerrstatus)
    yyerrstatus--;

  /* Shift the lookahead token.  */
  YY_SYMBOL_PRINT ("Shifting", yytoken, &yylval, &yylloc);
  yystate = yyn;
  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END

  /* Discard the shifted token.  */
  yychar = YYEMPTY;
  goto yynewstate;


/*-----------------------------------------------------------.
| yydefault -- do the default action for the current state.  |
`-----------------------------------------------------------*/
yydefault:
  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;
  goto yyreduce;


/*-----------------------------.
| yyreduce -- do a reduction.  |
`-----------------------------*/
yyreduce:
  /* yyn is the number of a rule to reduce with.  */
  yylen = yyr2[yyn];

  /* If YYLEN is nonzero, implement the default value of the action:
     '$$ = $1'.

     Otherwise, the following line sets YYVAL to garbage.
     This behavior is undocumented and Bison
     users should not rely upon it.  Assigning to YYVAL
     unconditionally makes the parser a bit smaller, and it avoids a
     GCC warning that YYVAL may be used uninitialized.  */
  yyval = yyvsp[1-yylen];


  YY_REDUCE_PRINT (yyn);
  switch (yyn)
    {
  case 4:
#line 167 "src/interf/glc.y"
                   {if (tour == 1) bpn->PutName(yytext);}
#line 1694 "src/interf/y.tab.cpp"
    break;

  case 8:
#line 176 "src/interf/glc.y"
                                       {PRF::prf.Length_Unity(yytext);}
#line 1700 "src/interf/y.tab.cpp"
    break;

  case 11:
#line 181 "src/interf/glc.y"
                                     {PRF::prf.Time_Unity(yytext);}
#line 1706 "src/interf/y.tab.cpp"
    break;

  case 16:
#line 191 "src/interf/glc.y"
            { is_integer = 0; }
#line 1712 "src/interf/y.tab.cpp"
    break;

  case 17:
#line 192 "src/interf/glc.y"
                { is_integer = 1; }
#line 1718 "src/interf/y.tab.cpp"
    break;

  case 18:
#line 193 "src/interf/glc.y"
                 { is_integer = 0; }
#line 1724 "src/interf/y.tab.cpp"
    break;

  case 20:
#line 206 "src/interf/glc.y"
              { cout << "Compiling places\n";}
#line 1730 "src/interf/y.tab.cpp"
    break;

  case 22:
#line 210 "src/interf/glc.y"
                   { cout << "Compiling transitions\n";}
#line 1736 "src/interf/y.tab.cpp"
    break;

  case 24:
#line 220 "src/interf/glc.y"
    { strcpy(idplace,yytext);
      if (tour == 1) {
        if (bpn->VerifyName(idplace))
          type_error(idplace, Parser_00);
      }
    }
#line 1747 "src/interf/y.tab.cpp"
    break;

  case 25:
#line 227 "src/interf/glc.y"
    { if (tour == 1) {
        Place *newp;
        switch (ptype) {
          case Discrete_pl :   newp = new DiscretePlace(idplace);
                               bpn->AddPlace(currentdiscreteplace, newp);
                               currentdiscreteplace++;
               break;
          case Continuous_pl : newp = new ContinuousPlace(idplace);
                               bpn->AddPlace(currentcontinuousplace, newp);
                               currentcontinuousplace++;
               break;
          case Batch_pl :      newp = new BatchPlace(idplace, pspeed, pdensity, plength);
                               bpn->AddPlace(currentbatchplace, newp);
                               currentbatchplace++;
               break;
          case Triangular_pl : newp = new TriangularBatchPlace(idplace, pspeed, pdensity, plength, pflow);
                               bpn->AddPlace(currenttriangularplace, newp);
                               currenttriangularplace++;
               break;
        }
      }
    }
#line 1774 "src/interf/y.tab.cpp"
    break;

  case 29:
#line 257 "src/interf/glc.y"
                { discreteplaces++; ptype = Discrete_pl; }
#line 1780 "src/interf/y.tab.cpp"
    break;

  case 30:
#line 258 "src/interf/glc.y"
                    { continuousplaces++; ptype = Continuous_pl; }
#line 1786 "src/interf/y.tab.cpp"
    break;

  case 31:
#line 259 "src/interf/glc.y"
               { batchplaces++; ptype = Batch_pl; }
#line 1792 "src/interf/y.tab.cpp"
    break;

  case 32:
#line 260 "src/interf/glc.y"
                   { triangularplaces++; ptype = Triangular_pl;}
#line 1798 "src/interf/y.tab.cpp"
    break;

  case 33:
#line 268 "src/interf/glc.y"
                { if ((ptype != Batch_pl) && (ptype != Triangular_pl)) {
                   type_error(yytext, Parser_100);
                  }
                }
#line 1807 "src/interf/y.tab.cpp"
    break;

  case 34:
#line 271 "src/interf/glc.y"
                                             { pspeed = atof(yytext); }
#line 1813 "src/interf/y.tab.cpp"
    break;

  case 35:
#line 272 "src/interf/glc.y"
                                { pdensity = atof(yytext); }
#line 1819 "src/interf/y.tab.cpp"
    break;

  case 36:
#line 273 "src/interf/glc.y"
                                { plength = atof(yytext); }
#line 1825 "src/interf/y.tab.cpp"
    break;

  case 39:
#line 278 "src/interf/glc.y"
                    { if (ptype != Triangular_pl) {
                        type_error(yytext, Parser_101);
                      }
                      pflow = atof(yytext); }
#line 1834 "src/interf/y.tab.cpp"
    break;

  case 40:
#line 282 "src/interf/glc.y"
      { if (ptype == Triangular_pl) {
          yyerror(Parser_102);
        }
      }
#line 1843 "src/interf/y.tab.cpp"
    break;

  case 42:
#line 297 "src/interf/glc.y"
    { if (tour == 1){
        switch (ptype) {
          case Discrete_pl :   if (!is_integer){
                                 type_error(yytext, Parser_104);
                               }
                               else{
                                 bpn->SetInitialMarking((currentdiscreteplace-1), atoi(yytext));
                               }
               break;
          case Continuous_pl : bpn->SetInitialMarking((currentcontinuousplace-1), atof(yytext));
               break;
          case Batch_pl      :
          case Triangular_pl : type_error(yytext, Parser_103);
               break;
        }
      }
    }
#line 1865 "src/interf/y.tab.cpp"
    break;

  case 44:
#line 318 "src/interf/glc.y"
                               { blength = atof(yytext); }
#line 1871 "src/interf/y.tab.cpp"
    break;

  case 45:
#line 318 "src/interf/glc.y"
                                                                           { bdensity = atof(yytext); }
#line 1877 "src/interf/y.tab.cpp"
    break;

  case 46:
#line 319 "src/interf/glc.y"
                    { bposition = atof(yytext); }
#line 1883 "src/interf/y.tab.cpp"
    break;

  case 47:
#line 320 "src/interf/glc.y"
    { if (tour == 1){
        Batch *newb;
        ControllableBatch *newcb;
        switch (ptype) {
          case Discrete_pl   : type_error(yytext, Parser_104);
               break;
          case Continuous_pl : type_error(yytext, Parser_105);
               break;
          case Batch_pl      : newb = new Batch(blength, bdensity, bposition);
                               bpn->AddBatchToInitialMarking((currentbatchplace-1), *newb);
               break;
          case Triangular_pl : newcb = new ControllableBatch(blength, bdensity, bposition, bspeed);
                               bpn->AddTriangularToInitialMarking((currenttriangularplace-1), *newcb);
               break;
        }
      }
    }
#line 1905 "src/interf/y.tab.cpp"
    break;

  case 52:
#line 346 "src/interf/glc.y"
                    { bspeed = atof(yytext); }
#line 1911 "src/interf/y.tab.cpp"
    break;

  case 53:
#line 347 "src/interf/glc.y"
      { bspeed = -1;}
#line 1917 "src/interf/y.tab.cpp"
    break;

  case 56:
#line 361 "src/interf/glc.y"
    { if (tour == 1){
        switch (ptype) {
          case Discrete_pl :   if (!is_integer){
                                 type_error(yytext, Parser_104);
                               }
                               else{
                                 bpn->SetSteadyMarking((currentdiscreteplace-1), atoi(yytext));
                               }
               break;
          case Continuous_pl : bpn->SetSteadyMarking((currentcontinuousplace-1), atof(yytext));
               break;
          case Batch_pl      :
          case Triangular_pl : type_error(yytext, Parser_103);
               break;
        }
      }
    }
#line 1939 "src/interf/y.tab.cpp"
    break;

  case 58:
#line 382 "src/interf/glc.y"
                               { blength = atof(yytext); }
#line 1945 "src/interf/y.tab.cpp"
    break;

  case 59:
#line 382 "src/interf/glc.y"
                                                                           { bdensity = atof(yytext); }
#line 1951 "src/interf/y.tab.cpp"
    break;

  case 60:
#line 383 "src/interf/glc.y"
                    { bposition = atof(yytext); }
#line 1957 "src/interf/y.tab.cpp"
    break;

  case 61:
#line 384 "src/interf/glc.y"
    { if (tour == 1){
        Batch *newb;
        ControllableBatch *newcb;
        switch (ptype) {
          case Discrete_pl   : type_error(yytext, Parser_104);
               break;
          case Continuous_pl : type_error(yytext, Parser_105);
               break;
          case Batch_pl      : newb = new Batch(blength, bdensity, bposition);
                               bpn->AddBatchToSteadyMarking((currentbatchplace-1), *newb);
               break;
          case Triangular_pl : newcb = new ControllableBatch(blength, bdensity, bposition, bspeed);
                               bpn->AddTriangularToSteadyMarking((currenttriangularplace-1), *newcb);
               break;
        }
      }
    }
#line 1979 "src/interf/y.tab.cpp"
    break;

  case 66:
#line 410 "src/interf/glc.y"
                    { bspeed = atof(yytext); }
#line 1985 "src/interf/y.tab.cpp"
    break;

  case 67:
#line 411 "src/interf/glc.y"
      { bspeed = -1;}
#line 1991 "src/interf/y.tab.cpp"
    break;

  case 68:
#line 419 "src/interf/glc.y"
                           { strcpy (idtransition, yytext); }
#line 1997 "src/interf/y.tab.cpp"
    break;

  case 69:
#line 420 "src/interf/glc.y"
    { if (tour == 2){
        idplacenumber = bpn->GetPlacePosition(idplace);
        idtransitionnumber = bpn->GetTransitionPosition(idtransition);
        if (idtransitionnumber < 0){
          type_error(idtransition, Parser_106);
        }
        else{
          if ((bpn->GetTransitionType(idtransition) == Discrete_tr) && !((bpn->GetPlaceType(idplace) == Discrete_pl) || (bpn->GetPlaceType(idplace) == Continuous_pl))){
            type_error(idtransition, Parser_108);
          }
          else{
            bpn->AddPlaceOutputArc(idplacenumber, idtransitionnumber, arcweight);
          }
        }
      }
    }
#line 2018 "src/interf/y.tab.cpp"
    break;

  case 73:
#line 444 "src/interf/glc.y"
                               { arcweight = atof(yytext); }
#line 2024 "src/interf/y.tab.cpp"
    break;

  case 75:
#line 445 "src/interf/glc.y"
      {arcweight = 1.0;}
#line 2030 "src/interf/y.tab.cpp"
    break;

  case 76:
#line 456 "src/interf/glc.y"
    { strcpy(idtransition,yytext);
      if (tour == 1) {
        if (bpn->VerifyName(idtransition))
          type_error(idtransition, Parser_01);
      }
    }
#line 2041 "src/interf/y.tab.cpp"
    break;

  case 77:
#line 463 "src/interf/glc.y"
    { tsteadytime = 0.0;
      tsteadyflow = 0.0;
    }
#line 2049 "src/interf/y.tab.cpp"
    break;

  case 78:
#line 467 "src/interf/glc.y"
    { if (tour == 1) {
        Transition *newt;
        switch (ttype) {
          case Discrete_tr :   newt = new DiscreteTransition(idtransition, ttime, tsteadytime);
                               bpn->AddTransition(currentdiscretetransition, newt);
                               currentdiscretetransition++;
               break;
          case Continuous_tr : newt = new ContinuousTransition(idtransition, tflow, tsteadyflow);
                               bpn->AddTransition(currentcontinuoustransition, newt);
                               currentcontinuoustransition++;
               break;
          case Batch_tr :      newt = new BatchTransition(idtransition, tflow, tsteadyflow);
                               bpn->AddTransition(currentbatchtransition, newt);
                               currentbatchtransition++;
               break;
        }
      }
    }
#line 2072 "src/interf/y.tab.cpp"
    break;

  case 82:
#line 494 "src/interf/glc.y"
                {discretetransitions++; ttype = Discrete_tr; }
#line 2078 "src/interf/y.tab.cpp"
    break;

  case 83:
#line 495 "src/interf/glc.y"
                    {continuoustransitions++; ttype = Continuous_tr; }
#line 2084 "src/interf/y.tab.cpp"
    break;

  case 84:
#line 496 "src/interf/glc.y"
               {batchtransitions++; ttype = Batch_tr; }
#line 2090 "src/interf/y.tab.cpp"
    break;

  case 85:
#line 500 "src/interf/glc.y"
                                         { ttime = atof(yytext); }
#line 2096 "src/interf/y.tab.cpp"
    break;

  case 87:
#line 501 "src/interf/glc.y"
                                         { tflow = atof(yytext); }
#line 2102 "src/interf/y.tab.cpp"
    break;

  case 91:
#line 510 "src/interf/glc.y"
                                         { tsteadytime = atof(yytext); }
#line 2108 "src/interf/y.tab.cpp"
    break;

  case 93:
#line 511 "src/interf/glc.y"
                                         { tsteadyflow = atof(yytext); }
#line 2114 "src/interf/y.tab.cpp"
    break;

  case 95:
#line 521 "src/interf/glc.y"
                           { strcpy (idplace, yytext); }
#line 2120 "src/interf/y.tab.cpp"
    break;

  case 96:
#line 522 "src/interf/glc.y"
    { if (tour == 2){
        idtransitionnumber = bpn->GetTransitionPosition(idtransition);
        idplacenumber = bpn->GetPlacePosition(idplace);
        if (idplacenumber < 0){
          type_error(idplace, Parser_107);
        }
        else{
          bpn->AddTransitionOutputArc(idtransitionnumber, idplacenumber, arcweight);
        }
      }
    }
#line 2136 "src/interf/y.tab.cpp"
    break;

  case 100:
#line 541 "src/interf/glc.y"
                                { arcweight = atof(yytext); }
#line 2142 "src/interf/y.tab.cpp"
    break;

  case 102:
#line 542 "src/interf/glc.y"
      {arcweight = 1.0;}
#line 2148 "src/interf/y.tab.cpp"
    break;

  case 103:
#line 550 "src/interf/glc.y"
                            { cout << "Compiling controlled events\n";}
#line 2154 "src/interf/y.tab.cpp"
    break;

  case 106:
#line 555 "src/interf/glc.y"
          { strcpy(idcevent,yytext); }
#line 2160 "src/interf/y.tab.cpp"
    break;

  case 107:
#line 555 "src/interf/glc.y"
                                                                                                           { strcpy(idnode, yytext); }
#line 2166 "src/interf/y.tab.cpp"
    break;

  case 108:
#line 556 "src/interf/glc.y"
                    { cevalue = atof(yytext); }
#line 2172 "src/interf/y.tab.cpp"
    break;

  case 109:
#line 556 "src/interf/glc.y"
                                                                { cedate = atof(yytext); }
#line 2178 "src/interf/y.tab.cpp"
    break;

  case 110:
#line 557 "src/interf/glc.y"
    {
      if (tour == 1){
        Node *evnode = bpn->GetNode(idnode);
        ControlledEvent *ev;
        if (evnode){
          switch (cetype){
            case Trans_flow_ce  : if (bpn->GetTransitionPosition(idnode) == -1){
                                    type_error(idnode, Parser_110);
                                  }
                                  else{
                                    if (bpn->GetTransitionType(idnode) == Discrete_tr) {
                                      type_error(idnode, Parser_113);
                                    }
                                    else{
                                      ev = new ControlledEvent(idnode, *evnode, cedate, Trans_flow_ce, cevalue, idcevent);
                                      schedule->AddControlledEvent(*ev, Trans_flow_ce);
                                    }
                                  }
                break;
            case Place_speed_ce : if (bpn->GetPlacePosition(idnode) == -1){
                                    type_error(idnode, Parser_111);
                                  }
                                  else{
                                    if (bpn->GetPlaceType(idnode) != Triangular_pl) {
                                      type_error(idnode, Parser_112);
                                    }
                                    else{
                                      ev = new ControlledEvent(idnode, *evnode, cedate, Place_speed_ce, cevalue, idcevent);
                                      schedule->AddControlledEvent(*ev, Place_speed_ce);
                                    }
                                  }
                break;
          }
        }
        else{
          type_error(idnode, Parser_109);
        }
      }
    }
#line 2222 "src/interf/y.tab.cpp"
    break;

  case 114:
#line 604 "src/interf/glc.y"
            { cetype = Trans_flow_ce; }
#line 2228 "src/interf/y.tab.cpp"
    break;

  case 115:
#line 605 "src/interf/glc.y"
               {cetype = Place_speed_ce; }
#line 2234 "src/interf/y.tab.cpp"
    break;

  case 116:
#line 613 "src/interf/glc.y"
                          { cout << "Compiling inspection points\n";}
#line 2240 "src/interf/y.tab.cpp"
    break;

  case 119:
#line 618 "src/interf/glc.y"
          { strcpy(idbreakpoint,yytext); }
#line 2246 "src/interf/y.tab.cpp"
    break;

  case 120:
#line 618 "src/interf/glc.y"
                                                                         { bpdate = atof(yytext); }
#line 2252 "src/interf/y.tab.cpp"
    break;

  case 121:
#line 618 "src/interf/glc.y"
                                                                                                               { strcpy(idnode, yytext); }
#line 2258 "src/interf/y.tab.cpp"
    break;

  case 122:
#line 619 "src/interf/glc.y"
    {
      if (tour == 1){
        Node *bpnode = bpn->GetNode(idnode);
        Breakpoint *bp;
        if ((bpnode) || (!strcmp(idnode, "places")) || (!strcmp(idnode, "transitions"))){
          bp = new Breakpoint(idnode, *bpnode, bpdate, bptype, idbreakpoint);
          schedule->AddBreakpoint(*bp);
        }
        else{
          type_error(idnode, Parser_109);
        }
      }
    }
#line 2276 "src/interf/y.tab.cpp"
    break;

  case 126:
#line 641 "src/interf/glc.y"
          { bptype = at_bp; }
#line 2282 "src/interf/y.tab.cpp"
    break;

  case 127:
#line 642 "src/interf/glc.y"
               { bptype = every_bp; }
#line 2288 "src/interf/y.tab.cpp"
    break;


#line 2292 "src/interf/y.tab.cpp"

      default: break;
    }
  /* User semantic actions sometimes alter yychar, and that requires
     that yytoken be updated with the new translation.  We take the
     approach of translating immediately before every use of yytoken.
     One alternative is translating here after every semantic action,
     but that translation would be missed if the semantic action invokes
     YYABORT, YYACCEPT, or YYERROR immediately after altering yychar or
     if it invokes YYBACKUP.  In the case of YYABORT or YYACCEPT, an
     incorrect destructor might then be invoked immediately.  In the
     case of YYERROR or YYBACKUP, subsequent parser actions might lead
     to an incorrect destructor call or verbose syntax error message
     before the lookahead is translated.  */
  YY_SYMBOL_PRINT ("-> $$ =", yyr1[yyn], &yyval, &yyloc);

  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);

  *++yyvsp = yyval;

  /* Now 'shift' the result of the reduction.  Determine what state
     that goes to, based on the state we popped back to and the rule
     number reduced by.  */
  {
    const int yylhs = yyr1[yyn] - YYNTOKENS;
    const int yyi = yypgoto[yylhs] + *yyssp;
    yystate = (0 <= yyi && yyi <= YYLAST && yycheck[yyi] == *yyssp
               ? yytable[yyi]
               : yydefgoto[yylhs]);
  }

  goto yynewstate;


/*--------------------------------------.
| yyerrlab -- here on detecting error.  |
`--------------------------------------*/
yyerrlab:
  /* Make sure we have latest lookahead translation.  See comments at
     user semantic actions for why this is necessary.  */
  yytoken = yychar == YYEMPTY ? YYEMPTY : YYTRANSLATE (yychar);

  /* If not already recovering from an error, report this error.  */
  if (!yyerrstatus)
    {
      ++yynerrs;
#if ! YYERROR_VERBOSE
      yyerror (YY_("syntax error"));
#else
# define YYSYNTAX_ERROR yysyntax_error (&yymsg_alloc, &yymsg, \
                                        yyssp, yytoken)
      {
        char const *yymsgp = YY_("syntax error");
        int yysyntax_error_status;
        yysyntax_error_status = YYSYNTAX_ERROR;
        if (yysyntax_error_status == 0)
          yymsgp = yymsg;
        else if (yysyntax_error_status == 1)
          {
            if (yymsg != yymsgbuf)
              YYSTACK_FREE (yymsg);
            yymsg = YY_CAST (char *, YYSTACK_ALLOC (YY_CAST (YYSIZE_T, yymsg_alloc)));
            if (!yymsg)
              {
                yymsg = yymsgbuf;
                yymsg_alloc = sizeof yymsgbuf;
                yysyntax_error_status = 2;
              }
            else
              {
                yysyntax_error_status = YYSYNTAX_ERROR;
                yymsgp = yymsg;
              }
          }
        yyerror (yymsgp);
        if (yysyntax_error_status == 2)
          goto yyexhaustedlab;
      }
# undef YYSYNTAX_ERROR
#endif
    }



  if (yyerrstatus == 3)
    {
      /* If just tried and failed to reuse lookahead token after an
         error, discard it.  */

      if (yychar <= YYEOF)
        {
          /* Return failure if at end of input.  */
          if (yychar == YYEOF)
            YYABORT;
        }
      else
        {
          yydestruct ("Error: discarding",
                      yytoken, &yylval);
          yychar = YYEMPTY;
        }
    }

  /* Else will try to reuse lookahead token after shifting the error
     token.  */
  goto yyerrlab1;


/*---------------------------------------------------.
| yyerrorlab -- error raised explicitly by YYERROR.  |
`---------------------------------------------------*/
yyerrorlab:
  /* Pacify compilers when the user code never invokes YYERROR and the
     label yyerrorlab therefore never appears in user code.  */
  if (0)
    YYERROR;

  /* Do not reclaim the symbols of the rule whose action triggered
     this YYERROR.  */
  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);
  yystate = *yyssp;
  goto yyerrlab1;


/*-------------------------------------------------------------.
| yyerrlab1 -- common code for both syntax error and YYERROR.  |
`-------------------------------------------------------------*/
yyerrlab1:
  yyerrstatus = 3;      /* Each real token shifted decrements this.  */

  for (;;)
    {
      yyn = yypact[yystate];
      if (!yypact_value_is_default (yyn))
        {
          yyn += YYTERROR;
          if (0 <= yyn && yyn <= YYLAST && yycheck[yyn] == YYTERROR)
            {
              yyn = yytable[yyn];
              if (0 < yyn)
                break;
            }
        }

      /* Pop the current state because it cannot handle the error token.  */
      if (yyssp == yyss)
        YYABORT;


      yydestruct ("Error: popping",
                  yystos[yystate], yyvsp);
      YYPOPSTACK (1);
      yystate = *yyssp;
      YY_STACK_PRINT (yyss, yyssp);
    }

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END


  /* Shift the error token.  */
  YY_SYMBOL_PRINT ("Shifting", yystos[yyn], yyvsp, yylsp);

  yystate = yyn;
  goto yynewstate;


/*-------------------------------------.
| yyacceptlab -- YYACCEPT comes here.  |
`-------------------------------------*/
yyacceptlab:
  yyresult = 0;
  goto yyreturn;


/*-----------------------------------.
| yyabortlab -- YYABORT comes here.  |
`-----------------------------------*/
yyabortlab:
  yyresult = 1;
  goto yyreturn;


#if !defined yyoverflow || YYERROR_VERBOSE
/*-------------------------------------------------.
| yyexhaustedlab -- memory exhaustion comes here.  |
`-------------------------------------------------*/
yyexhaustedlab:
  yyerror (YY_("memory exhausted"));
  yyresult = 2;
  /* Fall through.  */
#endif


/*-----------------------------------------------------.
| yyreturn -- parsing is finished, return the result.  |
`-----------------------------------------------------*/
yyreturn:
  if (yychar != YYEMPTY)
    {
      /* Make sure we have latest lookahead translation.  See comments at
         user semantic actions for why this is necessary.  */
      yytoken = YYTRANSLATE (yychar);
      yydestruct ("Cleanup: discarding lookahead",
                  yytoken, &yylval);
    }
  /* Do not reclaim the symbols of the rule whose action triggered
     this YYABORT or YYACCEPT.  */
  YYPOPSTACK (yylen);
  YY_STACK_PRINT (yyss, yyssp);
  while (yyssp != yyss)
    {
      yydestruct ("Cleanup: popping",
                  yystos[+*yyssp], yyvsp);
      YYPOPSTACK (1);
    }
#ifndef yyoverflow
  if (yyss != yyssa)
    YYSTACK_FREE (yyss);
#endif
#if YYERROR_VERBOSE
  if (yymsg != yymsgbuf)
    YYSTACK_FREE (yymsg);
#endif
  return yyresult;
}
#line 655 "src/interf/glc.y"



void Init_Variables()
{
  yylineno = 0;

  //current place/transition
  currentdiscreteplace = 0;
  currentcontinuousplace = discreteplaces;
  currentbatchplace = continuousplaces + discreteplaces;
  currenttriangularplace = batchplaces + continuousplaces + discreteplaces;

  currentdiscretetransition = 0;
  currentcontinuoustransition = discretetransitions;
  currentbatchtransition = continuoustransitions + discretetransitions;
}


//int main(int argv, char *argc[])
void Compile_Network(const simuleau_name bpn_level_name, const simuleau_name str_level_name)
{
  int totalplaces;
  int totaltransitions;
  ofstream fstr;
  simuleau_name bpn_file;


  strcpy(bpn_file, bpn_level_name); strcat(bpn_file,".bpn");


  //first tour: semantical analysis and number of places and transitions
  tour = 0;
  //counters
  discreteplaces = 0;
  continuousplaces = 0;
  batchplaces = 0;
  triangularplaces = 0;

  discretetransitions = 0;
  continuoustransitions = 0;
  batchtransitions = 0;

  Init_Variables();
  yyin = fopen(bpn_file, "r");
  cout << "Start model compilation\n\n";

  cout << "First turn\n"
       << "==========\n";
  cout << "Creating places and transitions structures\n";
  cout << "Creating controlled events schedule\n";
  yyparse();
  fclose(yyin);

  totalplaces = discreteplaces + continuousplaces + batchplaces + triangularplaces;
  totaltransitions = discretetransitions + continuoustransitions + batchtransitions;

  if (bpn!=NULL)
    delete bpn;
  bpn = new BPN(totalplaces, totaltransitions);

  if (schedule!=NULL)
    delete schedule;
  schedule = new Schedule;

  // places initialisation
  bpn->NumberOfDiscretePlaces(discreteplaces);
  bpn->NumberOfContinuousPlaces(continuousplaces);
  bpn->NumberOfBatchPlaces(batchplaces);
  bpn->NumberOfTriangularBatchPlaces(triangularplaces);

  // transitions initialisation
  bpn->NumberOfDiscreteTransitions(discretetransitions);
  bpn->NumberOfContinuousTransitions(continuoustransitions);
  bpn->NumberOfBatchTransitions(batchtransitions);


  // second tour
  if(!not_error){
    exit (EXIT_FAILURE);
  }

  cout << "\nSecond turn\n"
      << "===========\n";
  cout << "Initialising places and transitions parameters\n";
  tour++;
  Init_Variables();
  yyin = fopen(bpn_file, "r");
  yyrestart(yyin);
  yyparse();
  fclose(yyin);


  // thrid tour
  if(!not_error){
    exit (EXIT_FAILURE);
  }

  cout << "\nThrid turn\n"
      << "==========\n";
  cout << "Initialising places and transitions arcs\n";
  tour++;
  bpn->AllocArcs();
  Init_Variables();
  yyin = fopen(bpn_file, "r");
  yyrestart(yyin);
  yyparse();
  fclose(yyin);
  bpn->AddNodeLinks();
  if(!not_error){
    exit (EXIT_FAILURE);
  }

  cout << endl << bpn->GetName() << " model created\n";

  Open_File(str_level_name, str_file, fstr);
  bpn->Print(fstr);
  Close_File(str_level_name, str_file, fstr);
}
