//====================================================================================//
//                                                                                    //
//                             Preferences Structure                                  //
//                                                                                    //
//====================================================================================//
//  This File:   prf.cpp                    Language: C++  (xlC and CC)               //
//  Software:    SIMULEAU                                                             //
//  Error Codes: 6100                                                                 //
//====================================================================================//
//  Creation:    12/apr/16                        by: Leonardo.Brenner@lsis.org       //
//  Last Change: 12/apr/16                        by: Leonardo.Brenner@lsis.org       //
//====================================================================================//
#include "simuleau.h"

     //--------------------------------------------------------------------------//
     //                              Constructor                                 //
     //--------------------------------------------------------------------------//
PRF::PRF()
{
 strcpy(file_name, "defaults");
 Defaults();
}
     //--------------------------------------------------------------------------//
     //                              Destructor                                  //
     //--------------------------------------------------------------------------//
PRF::~PRF()
{
}
     //--------------------------------------------------------------------------//
     //                       It puts the defaults values                        //
     //--------------------------------------------------------------------------//
void PRF::Defaults()
{
 verbose     = def_verbose;
 max_time    = def_time;
 min_error   = def_err;
 thresh      = def_thresh;
 strcpy(length_unity, def_length_unity);
 strcpy(time_unity, def_time_unity);
}
     //--------------------------------------------------------------------------//
     //                         It returns the file name                         //
     //--------------------------------------------------------------------------//
char* PRF::Name()
{
 return(file_name);
}

     //--------------------------------------------------------------------------//
     //                          It assigns a file name                          //
     //--------------------------------------------------------------------------//
void PRF::Baptise(const simuleau_name file)
{
 strcpy(file_name, file);
}

     //--------------------------------------------------------------------------//
     //                    It returns the current verbose mode                   //
     //--------------------------------------------------------------------------//
int PRF::Verb() const
{
 return(verbose);
}
     //--------------------------------------------------------------------------//
     //                      It sets the verbose mode to 'm'                     //
     //--------------------------------------------------------------------------//
void PRF::Verb(const int m)
{
 verbose = m;
}
     //--------------------------------------------------------------------------//
     //                   It returns the current verbose mode                    //
     //--------------------------------------------------------------------------//
char* PRF::Sverb()
{
 switch (verbose) {
   case 0  : return("level 0");
   case 1  : return("level 1");
   case 2  : return("level 1");
   default : Programming_Error("preference options must be updated", 6101)
 }
}
     //--------------------------------------------------------------------------//
     //             It returns the current maximum simulation time               //
     //--------------------------------------------------------------------------//
double PRF::Max_Time() const
{
 return(max_time);
}
     //--------------------------------------------------------------------------//
     //               It sets the maximum simulation time to 't'                 //
     //--------------------------------------------------------------------------//
void PRF::Max_Time(const double t)
{
 max_time = t;
}
     //--------------------------------------------------------------------------//
     //                  It returns the current minimum error                    //
     //--------------------------------------------------------------------------//
double PRF::Min_Err() const
{
 return(min_error);
}

     //--------------------------------------------------------------------------//
     //         It returns the current minimum error for date computation        //
     //--------------------------------------------------------------------------//
double PRF::Min_Err_Date() const
{
 return(min_error/1000);
}

     //--------------------------------------------------------------------------//
     //                    It sets the minimum error to 'e'                      //
     //--------------------------------------------------------------------------//
void PRF::Min_Err(const double e)
{
 min_error = e;
}
     //--------------------------------------------------------------------------//
     //                     It returns the current threshold                     //
     //--------------------------------------------------------------------------//
double PRF::Thrs() const
{
 return(thresh);
}
     //--------------------------------------------------------------------------//
     //                        It sets the threshold to 't'                      //
     //--------------------------------------------------------------------------//
void PRF::Thrs(const double t)
{
 thresh = t;
}

     //--------------------------------------------------------------------------//
     //                    It returns the current length unity                   //
     //--------------------------------------------------------------------------//
char* PRF::Length_Unity()
{
 return(length_unity);
}
     //--------------------------------------------------------------------------//
     //                      It sets the length unity to 'lu'                    //
     //--------------------------------------------------------------------------//
void PRF::Length_Unity(const simuleau_name lu)
{
 strcpy(length_unity, lu);
}

     //--------------------------------------------------------------------------//
     //                     It returns the current time unity                    //
     //--------------------------------------------------------------------------//
char* PRF::Time_Unity()
{
 return(time_unity);
}
     //--------------------------------------------------------------------------//
     //                       It sets the time unity to 'tu'                     //
     //--------------------------------------------------------------------------//
void PRF::Time_Unity(const simuleau_name tu)
{
 strcpy(time_unity, tu);
}

     //--------------------------------------------------------------------------//
     //                      Output on 'fout' file stream                        //
     //--------------------------------------------------------------------------//
void PRF::Write() const
{
 ofstream fout;

 Open_File(file_name, prf_file, fout);
 fout << verbose         << "\n";
 fout << max_time        << "\n";
 fout << min_error       << "\n";
 fout << thresh          << "\n";
 fout << length_unity    << "\n";
 fout << time_unity      << "\n";
 Close_File(file_name, prf_file, fout);
}
     //--------------------------------------------------------------------------//
     //                      Input from 'fin' file stream                        //
     //--------------------------------------------------------------------------//
void PRF::Read(const simuleau_name file)
{
 ifstream fin;

 Open_File(file, prf_file, fin);
 Baptise(file);
 fin >> verbose;
 fin >> max_time;
 fin >> min_error;
 fin >> thresh;
 fin >> length_unity;
 fin >> time_unity;
 Close_File(file_name, prf_file, fin);
}
     //--------------------------------------------------------------------------//
     //              Memory space used by 'this' (in memory units)               //
     //--------------------------------------------------------------------------//
int PRF::Mem_use() const
{
 return(max_simuleau_name+sizeof(bool)+3*sizeof(double));
}

     //--------------------------------------------------------------------------//
     //                   static variable: The User Preferences                  //
     //--------------------------------------------------------------------------//
PRF PRF::prf = PRF();
