%{
//====================================================================================//
//                                                                                    //
//                           Parser and Semantic Analysis                             //
//                                                                                    //
//====================================================================================//
//  This File:   glc.y                           Tool: YACC                           //
//  Software:    SIMULEAU                                                             //
//====================================================================================//
//  Creation:    15/apr/16                        by: Leonardo.Brenner@lsis.org       //
//  Last Change: 15/apr/16                        by: Leonardo.Brenner@lsis.org       //
//====================================================================================//
//  YACC is a general-purpose parser generator that converts a context-free grammar   //
// a C program.In this module are make the actions to generate the internal SAN's     //
// tables and the others functionalities of this textual interface for PEPS.          //
//====================================================================================//
#include "bpnfile.h"
#include "simuleau.h"

//====================================================================================//
//                                   Global variables                                 //
//====================================================================================//

extern BPN *bpn; // Network structure
extern Schedule *schedule; // schedule structure

int not_error = 1;
int is_integer = 1;

int tour; // Set the times that the parser was called, the first to count the number
          // of places, the number of transitions and others

//====================================================================================//
//                                      Variables                                     //
//====================================================================================//

  int discreteplaces;             // number of discrete places
  int continuousplaces;           // number of continuous places
  int batchplaces;                // number of batch places
  int triangularplaces;           // number of triangular batch places

  int currentdiscreteplace;       // index of the current discrete place used to add place
  int currentcontinuousplace;     // index of the current continuous place
  int currentbatchplace;          // index of the current batch place
  int currenttriangularplace;     // index of the current triangular batch place

  int discretetransitions;        // number of discrete transitions
  int continuoustransitions;      // number of continuous transitions
  int batchtransitions;           // number of batch transitions

  int currentdiscretetransition;  // index of the current discrete place used to add place
  int currentcontinuoustransition;// index of the current continuous place
  int currentbatchtransition;     // index of the current batch place

  place_id      idplacenumber; // number of the place id
  simuleau_name idplace;       // name of the place id
  place_type    ptype;         // place type
  double        pdensity;      // place density
  double        pspeed;        // place speed
  double        plength;       // place length
  double        pflow;         // place flow

  double        bdensity;      // batch density
  double        bspeed;        // batch speed
  double        blength;       // batch length
  double        bposition;     // batch position

  trans_id      idtransitionnumber; // number of the transition id
  simuleau_name idtransition;       // name of the transition id
  trans_type    ttype;              // transition type
  double        ttime;              // time of the discrete transitions
  double        tsteadytime;        // steady time of the discrete transitions
  double        tflow;              // flow of the continuous and batch transitions
  double        tsteadyflow;        // steady flow of the continuous and batch transitions

  double        arcweight;     // arc weight

  simuleau_name idcevent;      // name of the controlled event
  ev_type       cetype;        // type of the controlled event
  simuleau_name idnode;        // name of the place or transition
  double        cedate;        // controlled event date
  double        cevalue;       // new value of the flow/speed

  simuleau_name   idbreakpoint;  // name of the breakpoint
  breakpoint_type bptype;        // type of the breakpoint (at (?) or every (*))
  double          bpdate;        // date or frequency


//====================================================================================//
//                                   Parser variables                                 //
//====================================================================================//

extern int yynerrs;  // Variable of the YACC generator to count the number of errors.
extern int yylineno;        // Variable of the YACC generator to count the current line

extern int yylex(void); // Get the next token
extern void yyrestart(FILE *input_file);
extern char *yytext;
extern FILE *yyin; // Input file


//====================================================================================//
//                                   Function errors                                  //
//====================================================================================//
//  These functions analysis which error occurred and put the error into a message    //
// more specific.                                                                     //
//====================================================================================//


//====================================================================================
// Function that YACC use to put syntatic message errors
//====================================================================================
extern void yyerror(string s)
{
  yynerrs++;
  not_error = 0;
  Syntatic_Error(yynerrs, yylineno, s, yytext);
}

//====================================================================================
// Function to put semantic error messages
//====================================================================================
void type_error(char* text, const char* error_number)
{
  yynerrs++;
  not_error = 0;
  Semantic_Error_YACC(yynerrs, yylineno, text, error_number);
}


%}

             //-----------------------------------------------------------//
             //  Definition of tokens (terminals) of the grammar.         //
             // YACC used these definitions to generate the file          //
             // y.tab.h that is included in a LEX file (tokens.l)         //
             //-----------------------------------------------------------//

%token TK_ARC TK_BATCH TK_CONTINUOUS TK_CONTROLLED TK_DESCRIPTION
%token TK_DISCRETE TK_EVENTS TK_FLOW TK_FUNCTION TK_INITIAL TK_INSPECTION
%token TK_LENGTH TK_MARKING TK_MODEL TK_NETWORK TK_OUTPUT TK_PLACE
%token TK_PLACES TK_SPEED TK_STEADY TK_TIME TK_TIMING TK_TRANSITION TK_TRANSITIONS
%token TK_TRIANGULAR TK_UNITY TK_ID TK_INTEGER TK_REAL TK_EXPONENT TK_AT
%token TK_EVERY TK_LEFT_BRACKET TK_RIGHT_BRACKET TK_PL_TR TK_ALL
%token TK_LEFT_PARENTHESIS TK_RIGHT_PARENTHESIS
%token TK_LEFT_SQUARE_BRACKET TK_RIGHT_SQUARE_BRACKET
%token TK_SEMICOLON TK_COMMA TK_ATTRIB TK_COLON

%%

             //-----------------------------------------------------------//
             //                      Begin of grammar                     //
             //-----------------------------------------------------------//

start:blk_def blk_net blk_controlled_events blk_time_inspection
    ;

             //-----------------------------------------------------------//
             //  Block of definitions (model name, length and time unity) //
             //-----------------------------------------------------------//

blk_def:
    blk_model blk_unity
    ;

blk_model:
    TK_MODEL TK_ID {if (tour == 1) bpn->PutName(yytext);} semi_colon
    |
    ;

blk_unity:
    blk_lenght blk_time
	;

blk_lenght:
    TK_LENGTH TK_UNITY TK_ATTRIB TK_ID {PRF::prf.Length_Unity(yytext);} semi_colon
    |
    ;

blk_time:
    TK_TIME TK_UNITY TK_ATTRIB TK_ID {PRF::prf.Time_Unity(yytext);} semi_colon
    |
    ;

semi_colon:
  TK_SEMICOLON
  |
  ;

number:
    TK_REAL { is_integer = 0; }
    |TK_INTEGER { is_integer = 1; }
    |TK_EXPONENT { is_integer = 0; }
    ;


             //-----------------------------------------------------------//
             //                 Block to network description              //
             //-----------------------------------------------------------//

blk_net:
    TK_NETWORK TK_DESCRIPTION blk_places blk_transitions
    ;

blk_places:
    TK_PLACES { cout << "Compiling places\n";} blk_place
    ;

blk_transitions:
    TK_TRANSITIONS { cout << "Compiling transitions\n";} blk_transition
    ;


             //-----------------------------------------------------------//
             //                  Block to place description              //
             //-----------------------------------------------------------//

blk_place:
    TK_PLACE TK_ID
    { strcpy(idplace,yytext);
      if (tour == 1) {
        if (bpn->VerifyName(idplace))
          type_error(idplace, Parser_00);
      }
    } TK_LEFT_PARENTHESIS blk_place_type TK_RIGHT_PARENTHESIS
    blk_function
    { if (tour == 1) {
        Place *newp;
        switch (ptype) {
          case Discrete_pl :   newp = new DiscretePlace(idplace);
                               bpn->AddPlace(currentdiscreteplace, newp);
                               currentdiscreteplace++;
               break;
          case Continuous_pl : newp = new ContinuousPlace(idplace);
                               bpn->AddPlace(currentcontinuousplace, newp);
                               currentcontinuousplace++;
               break;
          case Batch_pl :      newp = new BatchPlace(idplace, pspeed, pdensity, plength);
                               bpn->AddPlace(currentbatchplace, newp);
                               currentbatchplace++;
               break;
          case Triangular_pl : newp = new TriangularBatchPlace(idplace, pspeed, pdensity, plength, pflow);
                               bpn->AddPlace(currenttriangularplace, newp);
                               currenttriangularplace++;
               break;
        }
      }
    } blk_initial_marking blk_steady_marking blk_place_arcs blk_place2
    ;

blk_place2:
	blk_place
	|
	;

blk_place_type:
    TK_DISCRETE { discreteplaces++; ptype = Discrete_pl; }
    | TK_CONTINUOUS { continuousplaces++; ptype = Continuous_pl; }
    | TK_BATCH { batchplaces++; ptype = Batch_pl; }
    | TK_TRIANGULAR{ triangularplaces++; ptype = Triangular_pl;}
    ;

             //-----------------------------------------------------------//
             //       Block to function description for batch places      //
             //-----------------------------------------------------------//

blk_function:
    TK_FUNCTION { if ((ptype != Batch_pl) && (ptype != Triangular_pl)) {
                   type_error(yytext, Parser_100);
                  }
                } TK_LEFT_PARENTHESIS number { pspeed = atof(yytext); }
                TK_COMMA number { pdensity = atof(yytext); }
                TK_COMMA number { plength = atof(yytext); } blk_triangular_place TK_RIGHT_PARENTHESIS
    |
    ;

blk_triangular_place:
    TK_COMMA number { if (ptype != Triangular_pl) {
                        type_error(yytext, Parser_101);
                      }
                      pflow = atof(yytext); }
    | { if (ptype == Triangular_pl) {
          yyerror(Parser_102);
        }
      }
    ;
             //-----------------------------------------------------------//
             //           Block to initial marking description            //
             //-----------------------------------------------------------//

blk_initial_marking:
    TK_INITIAL TK_MARKING TK_LEFT_BRACKET blk_initial_marking_int TK_RIGHT_BRACKET
    ;

blk_initial_marking_int:
    number
    { if (tour == 1){
        switch (ptype) {
          case Discrete_pl :   if (!is_integer){
                                 type_error(yytext, Parser_104);
                               }
                               else{
                                 bpn->SetInitialMarking((currentdiscreteplace-1), atoi(yytext));
                               }
               break;
          case Continuous_pl : bpn->SetInitialMarking((currentcontinuousplace-1), atof(yytext));
               break;
          case Batch_pl      :
          case Triangular_pl : type_error(yytext, Parser_103);
               break;
        }
      }
    }
    | blk_batch
    ;

blk_batch:
    TK_LEFT_PARENTHESIS number { blength = atof(yytext); } TK_COMMA number { bdensity = atof(yytext); }
    TK_COMMA number { bposition = atof(yytext); } blk_triangular_batch TK_RIGHT_PARENTHESIS
    { if (tour == 1){
        Batch *newb;
        ControllableBatch *newcb;
        switch (ptype) {
          case Discrete_pl   : type_error(yytext, Parser_104);
               break;
          case Continuous_pl : type_error(yytext, Parser_105);
               break;
          case Batch_pl      : newb = new Batch(blength, bdensity, bposition);
                               bpn->AddBatchToInitialMarking((currentbatchplace-1), *newb);
               break;
          case Triangular_pl : newcb = new ControllableBatch(blength, bdensity, bposition, bspeed);
                               bpn->AddTriangularToInitialMarking((currenttriangularplace-1), *newcb);
               break;
        }
      }
    } blk_batch2
    |
    ;

blk_batch2:
    TK_COMMA blk_batch
    |
    ;

blk_triangular_batch:
    TK_COMMA number { bspeed = atof(yytext); }
    | { bspeed = -1;}
    ;

             //-----------------------------------------------------------//
             //           Block to steady marking description            //
             //-----------------------------------------------------------//

blk_steady_marking:
    TK_STEADY TK_MARKING TK_LEFT_BRACKET blk_steady_marking_int TK_RIGHT_BRACKET
    |
    ;

blk_steady_marking_int:
    number
    { if (tour == 1){
        switch (ptype) {
          case Discrete_pl :   if (!is_integer){
                                 type_error(yytext, Parser_104);
                               }
                               else{
                                 bpn->SetSteadyMarking((currentdiscreteplace-1), atoi(yytext));
                               }
               break;
          case Continuous_pl : bpn->SetSteadyMarking((currentcontinuousplace-1), atof(yytext));
               break;
          case Batch_pl      :
          case Triangular_pl : type_error(yytext, Parser_103);
               break;
        }
      }
    }
    | blk_steady_batch
    ;

blk_steady_batch:
    TK_LEFT_PARENTHESIS number { blength = atof(yytext); } TK_COMMA number { bdensity = atof(yytext); }
    TK_COMMA number { bposition = atof(yytext); } blk_steady_triangular_batch TK_RIGHT_PARENTHESIS
    { if (tour == 1){
        Batch *newb;
        ControllableBatch *newcb;
        switch (ptype) {
          case Discrete_pl   : type_error(yytext, Parser_104);
               break;
          case Continuous_pl : type_error(yytext, Parser_105);
               break;
          case Batch_pl      : newb = new Batch(blength, bdensity, bposition);
                               bpn->AddBatchToSteadyMarking((currentbatchplace-1), *newb);
               break;
          case Triangular_pl : newcb = new ControllableBatch(blength, bdensity, bposition, bspeed);
                               bpn->AddTriangularToSteadyMarking((currenttriangularplace-1), *newcb);
               break;
        }
      }
    } blk_steady_batch2
    |
    ;

blk_steady_batch2:
    TK_COMMA blk_steady_batch
    |
    ;

blk_steady_triangular_batch:
    TK_COMMA number { bspeed = atof(yytext); }
    | { bspeed = -1;}
    ;

             //-----------------------------------------------------------//
             //              Block to place arcs description              //
             //-----------------------------------------------------------//

blk_place_arcs:
    TK_OUTPUT TK_ARC TK_ID { strcpy (idtransition, yytext); } blk_place_weight
    { if (tour == 2){
        idplacenumber = bpn->GetPlacePosition(idplace);
        idtransitionnumber = bpn->GetTransitionPosition(idtransition);
        if (idtransitionnumber < 0){
          type_error(idtransition, Parser_106);
        }
        else{
          if ((bpn->GetTransitionType(idtransition) == Discrete_tr) && !((bpn->GetPlaceType(idplace) == Discrete_pl) || (bpn->GetPlaceType(idplace) == Continuous_pl))){
            type_error(idtransition, Parser_108);
          }
          else{
            bpn->AddPlaceOutputArc(idplacenumber, idtransitionnumber, arcweight);
          }
        }
      }
    } blk_place_arcs2
    |
    ;

blk_place_arcs2:
    blk_place_arcs
    ;

blk_place_weight:
    TK_LEFT_PARENTHESIS number { arcweight = atof(yytext); } TK_RIGHT_PARENTHESIS
    | {arcweight = 1.0;}
    ;



             //-----------------------------------------------------------//
             //               Block to transition description             //
             //-----------------------------------------------------------//

blk_transition:
    TK_TRANSITION TK_ID
    { strcpy(idtransition,yytext);
      if (tour == 1) {
        if (bpn->VerifyName(idtransition))
          type_error(idtransition, Parser_01);
      }
    } TK_LEFT_PARENTHESIS blk_transition_type TK_RIGHT_PARENTHESIS
    blk_transition_function
    { tsteadytime = 0.0;
      tsteadyflow = 0.0;
    }
    blk_steady_transition_function
    { if (tour == 1) {
        Transition *newt;
        switch (ttype) {
          case Discrete_tr :   newt = new DiscreteTransition(idtransition, ttime, tsteadytime);
                               bpn->AddTransition(currentdiscretetransition, newt);
                               currentdiscretetransition++;
               break;
          case Continuous_tr : newt = new ContinuousTransition(idtransition, tflow, tsteadyflow);
                               bpn->AddTransition(currentcontinuoustransition, newt);
                               currentcontinuoustransition++;
               break;
          case Batch_tr :      newt = new BatchTransition(idtransition, tflow, tsteadyflow);
                               bpn->AddTransition(currentbatchtransition, newt);
                               currentbatchtransition++;
               break;
        }
      }
    }
     blk_transition_arcs blk_transition2
    ;

blk_transition2:
	blk_transition
	|
	;

blk_transition_type:
    TK_DISCRETE {discretetransitions++; ttype = Discrete_tr; }
    | TK_CONTINUOUS {continuoustransitions++; ttype = Continuous_tr; }
    | TK_BATCH {batchtransitions++; ttype = Batch_tr; }
    ;

blk_transition_function:
    TK_TIMING TK_LEFT_PARENTHESIS number { ttime = atof(yytext); } TK_RIGHT_PARENTHESIS
    | TK_FLOW TK_LEFT_PARENTHESIS number { tflow = atof(yytext); } TK_RIGHT_PARENTHESIS
    ;

blk_steady_transition_function:
    TK_STEADY blk_steady_transition_function_int
    |
    ;

blk_steady_transition_function_int:
    TK_TIMING TK_LEFT_PARENTHESIS number { tsteadytime = atof(yytext); } TK_RIGHT_PARENTHESIS
    | TK_FLOW TK_LEFT_PARENTHESIS number { tsteadyflow = atof(yytext); } TK_RIGHT_PARENTHESIS
    ;


             //-----------------------------------------------------------//
             //           Block to transition arcs description            //
             //-----------------------------------------------------------//


blk_transition_arcs:
    TK_OUTPUT TK_ARC TK_ID { strcpy (idplace, yytext); } blk_transition_weight
    { if (tour == 2){
        idtransitionnumber = bpn->GetTransitionPosition(idtransition);
        idplacenumber = bpn->GetPlacePosition(idplace);
        if (idplacenumber < 0){
          type_error(idplace, Parser_107);
        }
        else{
          bpn->AddTransitionOutputArc(idtransitionnumber, idplacenumber, arcweight);
        }
      }
    } blk_transition_arcs2
    |
    ;

blk_transition_arcs2:
    blk_transition_arcs
    ;

blk_transition_weight:
    TK_LEFT_PARENTHESIS number  { arcweight = atof(yytext); } TK_RIGHT_PARENTHESIS
    | {arcweight = 1.0;}
    ;

             //-----------------------------------------------------------//
             //          Block to evaluation of controlled events         //
             //-----------------------------------------------------------//

blk_controlled_events:
    TK_CONTROLLED TK_EVENTS { cout << "Compiling controlled events\n";} controlled_events
    |
    ;

controlled_events:
    TK_ID { strcpy(idcevent,yytext); } TK_ATTRIB TK_LEFT_PARENTHESIS controlled_events_type TK_COMMA TK_ID { strcpy(idnode, yytext); }
    TK_COMMA number { cevalue = atof(yytext); } TK_COMMA number { cedate = atof(yytext); } TK_RIGHT_PARENTHESIS semi_colon
    {
      if (tour == 1){
        Node *evnode = bpn->GetNode(idnode);
        ControlledEvent *ev;
        if (evnode){
          switch (cetype){
            case Trans_flow_ce  : if (bpn->GetTransitionPosition(idnode) == -1){
                                    type_error(idnode, Parser_110);
                                  }
                                  else{
                                    if (bpn->GetTransitionType(idnode) == Discrete_tr) {
                                      type_error(idnode, Parser_113);
                                    }
                                    else{
                                      ev = new ControlledEvent(idnode, *evnode, cedate, Trans_flow_ce, cevalue, idcevent);
                                      schedule->AddControlledEvent(*ev, Trans_flow_ce);
                                    }
                                  }
                break;
            case Place_speed_ce : if (bpn->GetPlacePosition(idnode) == -1){
                                    type_error(idnode, Parser_111);
                                  }
                                  else{
                                    if (bpn->GetPlaceType(idnode) != Triangular_pl) {
                                      type_error(idnode, Parser_112);
                                    }
                                    else{
                                      ev = new ControlledEvent(idnode, *evnode, cedate, Place_speed_ce, cevalue, idcevent);
                                      schedule->AddControlledEvent(*ev, Place_speed_ce);
                                    }
                                  }
                break;
          }
        }
        else{
          type_error(idnode, Parser_109);
        }
      }
    } controlled_events2
    |
    ;

controlled_events2:
    controlled_events
    ;

controlled_events_type:
    TK_FLOW { cetype = Trans_flow_ce; }
    | TK_SPEED {cetype = Place_speed_ce; }
    ;

             //-----------------------------------------------------------//
             //           Block to evaluation of time inspection          //
             //-----------------------------------------------------------//

blk_time_inspection:
    TK_TIME TK_INSPECTION { cout << "Compiling inspection points\n";} time_inspection
    |
    ;

time_inspection:
    TK_ID { strcpy(idbreakpoint,yytext); }  TK_ATTRIB blk_op_time number { bpdate = atof(yytext); }  blk_op_pt { strcpy(idnode, yytext); } semi_colon
    {
      if (tour == 1){
        Node *bpnode = bpn->GetNode(idnode);
        Breakpoint *bp;
        if ((bpnode) || (!strcmp(idnode, "places")) || (!strcmp(idnode, "transitions"))){
          bp = new Breakpoint(idnode, *bpnode, bpdate, bptype, idbreakpoint);
          schedule->AddBreakpoint(*bp);
        }
        else{
          type_error(idnode, Parser_109);
        }
      }
    }
    time_inspection2
    |
    ;

time_inspection2:
    time_inspection
    ;

blk_op_time:
    TK_AT { bptype = at_bp; }
    | TK_EVERY { bptype = every_bp; }
    ;

blk_op_pt:
    TK_PL_TR TK_ID
    | TK_ALL blk_pl_tr
    ;

blk_pl_tr:
    TK_PLACES
    | TK_TRANSITIONS
    ;

%%


void Init_Variables()
{
  yylineno = 0;

  //current place/transition
  currentdiscreteplace = 0;
  currentcontinuousplace = discreteplaces;
  currentbatchplace = continuousplaces + discreteplaces;
  currenttriangularplace = batchplaces + continuousplaces + discreteplaces;

  currentdiscretetransition = 0;
  currentcontinuoustransition = discretetransitions;
  currentbatchtransition = continuoustransitions + discretetransitions;
}


//int main(int argv, char *argc[])
void Compile_Network(const simuleau_name bpn_level_name, const simuleau_name str_level_name)
{
  int totalplaces;
  int totaltransitions;
  ofstream fstr;
  simuleau_name bpn_file;


  strcpy(bpn_file, bpn_level_name); strcat(bpn_file,".bpn");


  //first tour: semantical analysis and number of places and transitions
  tour = 0;
  //counters
  discreteplaces = 0;
  continuousplaces = 0;
  batchplaces = 0;
  triangularplaces = 0;

  discretetransitions = 0;
  continuoustransitions = 0;
  batchtransitions = 0;

  Init_Variables();
  yyin = fopen(bpn_file, "r");
  cout << "Start model compilation\n\n";

  cout << "First turn\n"
       << "==========\n";
  cout << "Creating places and transitions structures\n";
  cout << "Creating controlled events schedule\n";
  yyparse();
  fclose(yyin);

  totalplaces = discreteplaces + continuousplaces + batchplaces + triangularplaces;
  totaltransitions = discretetransitions + continuoustransitions + batchtransitions;

  if (bpn!=NULL)
    delete bpn;
  bpn = new BPN(totalplaces, totaltransitions);

  if (schedule!=NULL)
    delete schedule;
  schedule = new Schedule;

  // places initialisation
  bpn->NumberOfDiscretePlaces(discreteplaces);
  bpn->NumberOfContinuousPlaces(continuousplaces);
  bpn->NumberOfBatchPlaces(batchplaces);
  bpn->NumberOfTriangularBatchPlaces(triangularplaces);

  // transitions initialisation
  bpn->NumberOfDiscreteTransitions(discretetransitions);
  bpn->NumberOfContinuousTransitions(continuoustransitions);
  bpn->NumberOfBatchTransitions(batchtransitions);


  // second tour
  if(!not_error){
    exit (EXIT_FAILURE);
  }

  cout << "\nSecond turn\n"
      << "===========\n";
  cout << "Initialising places and transitions parameters\n";
  tour++;
  Init_Variables();
  yyin = fopen(bpn_file, "r");
  yyrestart(yyin);
  yyparse();
  fclose(yyin);


  // thrid tour
  if(!not_error){
    exit (EXIT_FAILURE);
  }

  cout << "\nThrid turn\n"
      << "==========\n";
  cout << "Initialising places and transitions arcs\n";
  tour++;
  bpn->AllocArcs();
  Init_Variables();
  yyin = fopen(bpn_file, "r");
  yyrestart(yyin);
  yyparse();
  fclose(yyin);
  bpn->AddNodeLinks();
  if(!not_error){
    exit (EXIT_FAILURE);
  }

  cout << endl << bpn->GetName() << " model created\n";

  Open_File(str_level_name, str_file, fstr);
  bpn->Print(fstr);
  Close_File(str_level_name, str_file, fstr);
}
