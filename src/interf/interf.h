//====================================================================================//
//                                                                                    //
//                               SIMULEAU User Interface                              //
//                                                                                    //
//====================================================================================//
//  This File:   interf.h                         Language: C++  (xlC and CC)         //
//  Software:    SIMULEAU                                                             //
//====================================================================================//
//  Creation:    12/apr/16                        by: Leonardo.Brenner@lsis.org       //
//  Last Change: 12/apr/16                        by: Leonardo.Brenner@lsis.org       //
//====================================================================================//
#ifndef INTERF_H
#define INTERF_H


//------------------------------------------------------------------------------------//
//                          Operations Performed by Simuleau                          //
//------------------------------------------------------------------------------------//

enum simuleau_options {  // all operations that simuleau can do
//====================================================================================//
//------------------------------- Compiling Operations -------------------------------//
//====================================================================================//
        compile ,  // compile                          (bpn -> str)

//====================================================================================//
//--------------------------------- Simule Operations --------------------------------//
//====================================================================================//
        simulate  ,  // simulate
    simulate_xct  ,  // simulate with external controlled events
    simulate_onoff,  // simulate with on/off control method
    compute_steadystate, //simulate with computation of steady state
    simulate_mf_onoff,  // simulate with maximal flow based on/off control method
    compute_periodicsteadystate, //simulate with computation of periodic steady state

//====================================================================================//
//--------------------------------- Inspect Operations -------------------------------//
//====================================================================================//
        see_bpn    ,  // inspect BPN internal representation
        see_place  ,  // inspect a specific place
        see_trans  ,  // inspect a specific transition
        see_events ,  // inspect current events
        see_dyn    ,  // inspect dynamics
        see_tim    ,  // inspect dynamics in a specific instant

//====================================================================================//
//---------------------------------- Other operations --------------------------------//
//====================================================================================//
        credits ,  // show Simuleau credits
        nothing ,  // do nothing
        goodbye }; // exit Simuleau tool


//------------------------------------------------------------------------------------//
//                        Interface Functions used by Simuleau                        //
//------------------------------------------------------------------------------------//
void Open_File(const simuleau_name file, const file_types ft, ifstream & f_tmp);

void Open_File(const simuleau_name file, const file_types ft, ofstream & f_tmp);

void Append_File(const simuleau_name file, const file_types ft, ofstream & f_tmp);

void Close_File(const simuleau_name file, const file_types ft, ifstream & f_tmp);

void Close_File(const simuleau_name file, const file_types ft, ofstream & f_tmp);

bool Exists_File(const simuleau_name file, const file_types ft);

void Create_Temporary_Directories();

void Remove_Temporary_Directories();

void Remove_Dynamics_Files();

void Remove_Tim_Files();

void Ask_a_File_Name(simuleau_name & name, const file_types ft);

void Ask_an_Existing_File_Name(simuleau_name & name, const file_types ft);

bool Permission_to_Proceed(const simuleau_name name, const file_types ft);

void Get_Only_the_File_Name(const simuleau_name full_name, simuleau_name & just_the_file_name);

place_id Ask_a_Place();

trans_id Ask_a_Transition();

bool Provide_BPN();

bool Provide_XCT();

void See_BPN();

void See_place();

void See_trans();

void See_events();

void See_DYN();

void See_TIM();

bool Ask_an_Answer(const simuleau_name name);

double Ask_an_Error();

double Ask_an_Inspect_Time();

double Ask_a_Simulation_Time();

void Show_User_Preferences();

void Change_User_Preferences();

void Notify_Time_Spend(const Timer T, const simuleau_name op);

void Show_Credits();

void Say_Goodbye();

void Say_Sorry();

simuleau_options Welcome_Get_Simuleau_Option();

void Show_Structures();

void Preferences();


//------------------------------------------------------------------------------------//
//                       Macro definition of error messages                           //
//------------------------------------------------------------------------------------//

#define Programming_Error(text, number) \
        { cerr <<  "Programming Error - " << text << " (**** " << number << " ****)\n" \
               << "(file '" << __FILE__ << "' - line " << __LINE__ << ")\n" \
               << "****************************************************************\n"; \
        exit(1); }

#define Programming_Warning(text, number) \
        { cerr <<  "Programming Warning - " << text << " (++++ " << number << " ++++)\n" \
               << "(file '" << __FILE__ << "' - line " << __LINE__ << ")\n"; }

#define Compilation_Error(structure, text) \
        { cerr <<  structure << " Compilation Error - " << text << "\n" \
               << "****************************************************************\n"; \
        exit(1); }


#endif // INTERF_H
