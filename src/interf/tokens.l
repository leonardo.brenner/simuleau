%{
//====================================================================================//
//                                                                                    //
//                                Lexical  Analyzer                                   //
//                                                                                    //
//====================================================================================//
//  This File:   tokens.l                    Tool: lex                                //
//  Software:    SIMULEAU                                                             //
//  Doc:                                                                              //
//====================================================================================//
//  Creation:    15/Apr/16                   by: Leonardo.Brenner@lsis.org            //
//  Last Change: 15/Apr/16                   by: Leonardo.Brenner@lsis.org            //
//====================================================================================//
//  This file has the definitions of the tokens that must to be recognized by the     //
// grammar contained in the "glc.y" file. These definitions are used by LEX tool,     //
// which is a generator of lexical parsers.                                           //
//  The lexical analyzer (scanner) is a function that reads an input stream and       //
// returns the correspondents tokens.                                                 //
//====================================================================================//
#include "bpnfile.h"

extern int yynerrs;  // Variable of the YACC generator to count the number of errors.
extern int yylineno;

//====================================================================================//
//  Below are defined the R.E.(Regular Expressions) that defines the format of the    //
// digits (symbols between 0 and 9, inclusively), integers, reals, exponents and      //
// identifies. The symbol "+" indicate one or more occurrences, the symbol "*"        //
// indicate zero or more occurrences and the symbol "?" indicate that the occurrence  //
// is optional.                                                                       //
//====================================================================================//

%}

digit    [0-9]
integer  {digit}+
real     {integer}"."{integer}
exponent ({integer}|{real})[Ee][+-]?{integer}
id       [A-Za-z_][A-Za-z0-9_]*

%%

"//"[0-9A-Za-z\'\-\!\"\#\$\%\&\(\)\*\+\,\-\.\/:\;\<\>\=\?\@\\\]\[\}\{\^\_\~\| ]*
"arc"           {return(TK_ARC);}
"batch"         {return(TK_BATCH);}
"continuous"    {return(TK_CONTINUOUS);}
"controlled"    {return(TK_CONTROLLED);}
"description"   {return(TK_DESCRIPTION);}
"discrete"      {return(TK_DISCRETE);}
"events"        {return(TK_EVENTS);}
"flow"          {return(TK_FLOW);}
"function"      {return(TK_FUNCTION);}
"initial"       {return(TK_INITIAL);}
"inspection"    {return(TK_INSPECTION);}
"length"        {return(TK_LENGTH);}
"marking"       {return(TK_MARKING);}
"model"         {return(TK_MODEL);}
"network"       {return(TK_NETWORK);}
"output"        {return(TK_OUTPUT);}
"place"         {return(TK_PLACE);}
"places"        {return(TK_PLACES);}
"speed"         {return(TK_SPEED);}
"steady"        {return(TK_STEADY);}
"time"          {return(TK_TIME);}
"timing"        {return(TK_TIMING);}
"transition"    {return(TK_TRANSITION);}
"transitions"   {return(TK_TRANSITIONS);}
"triangular"    {return(TK_TRIANGULAR);}
"unity"         {return(TK_UNITY);}
{id}            {return(TK_ID);}
{integer}       {return(TK_INTEGER);}
{real}          {return(TK_REAL);}
{exponent}      {return(TK_EXPONENT);}
"@"             {return(TK_AT);}
"#"             {return(TK_EVERY);}
"?"             {return(TK_PL_TR);}
"*"             {return(TK_ALL);}
"{"             {return(TK_LEFT_BRACKET);}
"}"             {return(TK_RIGHT_BRACKET);}
"("             {return(TK_LEFT_PARENTHESIS);}
")"             {return(TK_RIGHT_PARENTHESIS);}
"["             {return(TK_LEFT_SQUARE_BRACKET);}
"]"             {return(TK_RIGHT_SQUARE_BRACKET);}
";"             {return(TK_SEMICOLON);}
","             {return(TK_COMMA);}
"="             {return(TK_ATTRIB);}
":"             {return(TK_COLON);}
[ \t]*          {/* Ignore the blanks, tabulates and new line */}
[\n]            {yylineno++;}
.               { /* If neither token return, means that the input
                     stream has a lexical error */
                  yynerrs++;
                  Lexical_Error(yynerrs, yylineno, yytext);}
%%

int yywrap ()
{
  return(1);
}

void go2init()
{
  yy_init = 1;
}
