//====================================================================================//
//                                                                                    //
//                             Preferences Structure                                  //
//                                                                                    //
//====================================================================================//
//  This File:   prf.h                      Language: C++  (xlC and CC)               //
//  Software:    SIMULEAU                                                             //
//  Error Codes: 6100                                                                 //
//====================================================================================//
//  Creation:    12/apr/16                        by: Leonardo.Brenner@lsis.org       //
//  Last Change: 12/apr/16                        by: Leonardo.Brenner@lsis.org       //
//====================================================================================//
//    This class is used to store the static variable 'PRF::prf' which contains most  //
// of the user choices in Simuleau.                                                   //
//====================================================================================//
#ifndef PRF_H
#define PRF_H

class PRF
{
public:
  PRF();                                                                  // Constructor
  ~PRF();                                                                  // Destructor
  void Defaults();                                        // It puts the defaults values

  char* Name();                                              // It returns the file name
  void  Baptise(const simuleau_name file);                     // It assigns a file name

  int   Verb() const;                            // It returns the current verbose mode
  void   Verb(const int m);                          // It sets the verbose mode to 'm'
  char*  Sverb();                                                      // string answer

  double Max_Time() const;             // It returns the current maximum simulation time
  void   Max_Time(const double t);         // It sets the maximum simulation time to 't'

  double Min_Err() const;                       // It returns the current minimum error
  double Min_Err_Date() const;                  // It returns the current minimum error
  void   Min_Err(const double e);                   // It sets the minimum error to 'e'

  double Thrs() const;                              // It returns the current threshold
  void   Thrs(const double t);                          // It sets the threshold to 't'

  char*  Length_Unity();                         // It returns the current lenght unity
  void   Length_Unity(const simuleau_name lu);       //It sets the lenght unity to 'lu'

  char*  Time_Unity();                             // It returns the current time unity
  void   Time_Unity(const simuleau_name tu);           //It sets the time unity to 'tu'

  void Write() const;                                   // Output on 'fout' file stream
  void Read(const simuleau_name file);                  // Input from 'fin' file stream
  int  Mem_use() const;                       // Memory space used by 'this' (in bytes)

        //--------------------------------------------------------------------------//
        //                             Static variable                              //
        //--------------------------------------------------------------------------//
        static PRF prf;

        //--------------------------------------------------------------------------//
        //                          Data Structures                                 //
        //--------------------------------------------------------------------------//
private: simuleau_name  file_name;    // the file name of this structure
         int           verbose;       // Verbose mode
         double         max_time;     // Maximum simulation time
         double         min_error;    // Minimum error
         double         thresh;       // Threshold
         simuleau_name  length_unity; // Length unity
         simuleau_name  time_unity;   // Time unity
};
#endif // PRF_H
