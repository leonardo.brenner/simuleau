//====================================================================================//
//                                                                                    //
//                                   Error Messages                                   //
//                                                                                    //
//====================================================================================//
//  This File:   error_msgs.h                     Language: C++  (xlC and CC)         //
//  Software:    SIMULEAU                                                             //
//====================================================================================//
//  Creation:    12/apr/16                        by: Leonardo.Brenner@lsis.org       //
//  Last Change: 12/apr/16                        by: Leonardo.Brenner@lsis.org       //
//====================================================================================//
// This file has macro definitions to put error messages of SIMULEAU parser           //
//====================================================================================//
#ifndef _ERROR_MSGS_H_
#define _ERROR_MSGS_H_

#define Lexical_Error(yacc_number, yacc_line, id) \
        cout << " " << yacc_number  \
             << ":Line(" << yacc_line \
             << ") - Lexical Error - \"" << id \
             << "\" is an unrecognized symbol." \
             << endl;

#define Syntatic_Error(yacc_number, yacc_line, str, id) \
        cout << " " << yacc_number  \
             << ":Line(" << yacc_line \
             << ") - " << str << " - \"" << id \
             << "\" (not expected)." \
             << endl;

#define Semantic_Error_YACC(yacc_number, yacc_line, id, number) \
        cout << " " << yacc_number  \
             << ":Line(" << yacc_line \
             << ") - Semantic Error - " << id \
             << " - (" << number << ")." \
             << endl;

#define Semantic_Error(text, number) \
        cout << "Semantic Error - " << number << " (** " \
             << text << " **).\n";

#define Semantic_Warning(text_1, text_2, number) \
        cout << "Semantic Warning - " << number << " (** " \
             << text_1 << " - " << text_2 << " **)." \
             << endl;

#define Programming_Error1(text) \
        cout << "Programming Error - at \"" << text \
             << "\" (file '" << __FILE__ << "' - line " << __LINE__ << ")" \
             << endl;

#define Programming_Error2(text, number) \
        cout << "Programming Error - " << number << " (at " \
             << text << ")\n" \
             << "(file '" << __FILE__ << "' - line " << __LINE__ << ")" \
             << endl;

// Semantic Errors - YACC (00 .. 99)
#define Parser_00 "Place name already defined in this model"
#define Parser_01 "Transition name already defined in this model"
#define Parser_02 "can not solve identifier."
#define Parser_03 "automaton identifier already defined."
#define Parser_04 "event not declared."
#define Parser_05 "state identifier already defined for this automaton."
#define Parser_06 "state identifier already defined as function identifier."

// Semantic Errors - ordinary files (100 - 199)
#define Parser_100 "Functions are only defined for batch or triangular batch places"
#define Parser_101 "The flow parameter are only defined for triangular batch places"
#define Parser_102 "The flow parameter must be defined for triangular batch places"
#define Parser_103 "The marking of batches or triangular batches is a list of batches"
#define Parser_104 "The marking of discrete places is an integer"
#define Parser_105 "The marking of discrete places is a float"
#define Parser_106 "Transition arc destination doesn't exist"
#define Parser_107 "Place arc destination doesn't exist"
#define Parser_108 "Bad arc destination (no arc allowed from batch/triangular batch place and discrete transition)"
#define Parser_109 "Place or transition doesn't exist"
#define Parser_110 "Transition doesn't exist"
#define Parser_111 "Place doesn't exist"
#define Parser_112 "Speed modification are only allowed for triangular batch places"
#define Parser_113 "Flow modification are only allowed for continuous or batch transitions"
#define Parser_114 "Wrong interval (final limit appears before initial)."
#define Parser_115 "Bad destination state (this operation cannot be used with the \'from\' primitive)"
#define Parser_116 "Bad destination state (this operation can be used only in a replicated state)"

// Programming Errors (200 - 299)
#define Parser_200 "Out of range (see FUNCTION define)."
#define Parser_201 "Index out of range."
#define Parser_202 "Can not alloc memory."


#endif
