/* A Bison parser, made by GNU Bison 3.5.1.  */

/* Bison interface for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2015, 2018-2020 Free Software Foundation,
   Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* Undocumented macros, especially those whose name start with YY_,
   are private implementation details.  Do not rely on them.  */

#ifndef YY_YY_SRC_INTERF_Y_TAB_HPP_INCLUDED
# define YY_YY_SRC_INTERF_Y_TAB_HPP_INCLUDED
/* Debug traces.  */
#ifndef YYDEBUG
# define YYDEBUG 1
#endif
#if YYDEBUG
extern int yydebug;
#endif

/* Token type.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
  enum yytokentype
  {
    TK_ARC = 258,
    TK_BATCH = 259,
    TK_CONTINUOUS = 260,
    TK_CONTROLLED = 261,
    TK_DESCRIPTION = 262,
    TK_DISCRETE = 263,
    TK_EVENTS = 264,
    TK_FLOW = 265,
    TK_FUNCTION = 266,
    TK_INITIAL = 267,
    TK_INSPECTION = 268,
    TK_LENGTH = 269,
    TK_MARKING = 270,
    TK_MODEL = 271,
    TK_NETWORK = 272,
    TK_OUTPUT = 273,
    TK_PLACE = 274,
    TK_PLACES = 275,
    TK_SPEED = 276,
    TK_STEADY = 277,
    TK_TIME = 278,
    TK_TIMING = 279,
    TK_TRANSITION = 280,
    TK_TRANSITIONS = 281,
    TK_TRIANGULAR = 282,
    TK_UNITY = 283,
    TK_ID = 284,
    TK_INTEGER = 285,
    TK_REAL = 286,
    TK_EXPONENT = 287,
    TK_AT = 288,
    TK_EVERY = 289,
    TK_LEFT_BRACKET = 290,
    TK_RIGHT_BRACKET = 291,
    TK_PL_TR = 292,
    TK_ALL = 293,
    TK_LEFT_PARENTHESIS = 294,
    TK_RIGHT_PARENTHESIS = 295,
    TK_LEFT_SQUARE_BRACKET = 296,
    TK_RIGHT_SQUARE_BRACKET = 297,
    TK_SEMICOLON = 298,
    TK_COMMA = 299,
    TK_ATTRIB = 300,
    TK_COLON = 301
  };
#endif
/* Tokens.  */
#define TK_ARC 258
#define TK_BATCH 259
#define TK_CONTINUOUS 260
#define TK_CONTROLLED 261
#define TK_DESCRIPTION 262
#define TK_DISCRETE 263
#define TK_EVENTS 264
#define TK_FLOW 265
#define TK_FUNCTION 266
#define TK_INITIAL 267
#define TK_INSPECTION 268
#define TK_LENGTH 269
#define TK_MARKING 270
#define TK_MODEL 271
#define TK_NETWORK 272
#define TK_OUTPUT 273
#define TK_PLACE 274
#define TK_PLACES 275
#define TK_SPEED 276
#define TK_STEADY 277
#define TK_TIME 278
#define TK_TIMING 279
#define TK_TRANSITION 280
#define TK_TRANSITIONS 281
#define TK_TRIANGULAR 282
#define TK_UNITY 283
#define TK_ID 284
#define TK_INTEGER 285
#define TK_REAL 286
#define TK_EXPONENT 287
#define TK_AT 288
#define TK_EVERY 289
#define TK_LEFT_BRACKET 290
#define TK_RIGHT_BRACKET 291
#define TK_PL_TR 292
#define TK_ALL 293
#define TK_LEFT_PARENTHESIS 294
#define TK_RIGHT_PARENTHESIS 295
#define TK_LEFT_SQUARE_BRACKET 296
#define TK_RIGHT_SQUARE_BRACKET 297
#define TK_SEMICOLON 298
#define TK_COMMA 299
#define TK_ATTRIB 300
#define TK_COLON 301

/* Value type.  */
#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED
typedef int YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define YYSTYPE_IS_DECLARED 1
#endif


extern YYSTYPE yylval;

int yyparse (void);

#endif /* !YY_YY_SRC_INTERF_Y_TAB_HPP_INCLUDED  */
