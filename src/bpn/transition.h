//====================================================================================//
//                                                                                    //
//                       Transition classes inherit of node class                     //
//                                                                                    //
//====================================================================================//
//  This File:   transition.h                     Language: C++  (xlC and CC)         //
//  Software:    SIMULEAU                                                             //
//====================================================================================//
//  Creation:    22/apr/16                        by: Leonardo.Brenner@lsis.org       //
//  Last Change: 22/apr/16                        by: Leonardo.Brenner@lsis.org       //
//====================================================================================//
#ifndef TRANSITION_H
#define TRANSITION_H
#include <simuleau.h>
#include <flow.h>
#include <firingquantity.h>


//====================================================================================//
//                              Generic transition class                              //
//====================================================================================//
//    Transition class implements a generic transition that will be used to implement //
// all other transition classes (discrete, continuous, batch) that can compose a      //
// Batch Petri Net.                                                                   //
//====================================================================================//
class Transition : public Node
{
  public:
    Transition();
    Transition(simuleau_name _name);
    virtual ~Transition();


    virtual void SetSteadyFiringQuantity(double _firingquantity);
    FiringQuantity* GetFiringQuantity();


    void Copy(const Transition *_transition);
    virtual trans_type IsA(){};

    virtual Flow* GetFlow(){};
    virtual Flow* GetSteadyFlow(){};
    virtual int IsSteadyFlowReached(){};

    void SetPreviousState(double _state);
    virtual void ComputeState();
    int IsOnState();
    int IsFiringQuantityOnState();      // The current firing quantity satisfy belongs to T_z.
//    double SteadyFiringQuantity();      // The steady firing quantity computed by the initial marking to a
                                               //                         given steady state. (minimal firing quantity)
    double GetSteadyFiringQuantity();
    double CurrentFiringQuantity(double steptime);     //   current firing quantity of transition.
    double GetCurrentFiringQuantity(); //Get the current firing quantity

    double GetRBFEvent(); //



    virtual void WalkThrough(double duration){};
 //   virtual void CurrentFiringQuantity(Simtime *_stime);

    // print functions
    virtual void Print(ostream &fout);

  protected:
    double previousstate;
    FiringQuantity *firingquantity;
    double steadyfiringquantity;
    double currentfiringquantity; //Firing quantity from initial marking
  private:
};


//====================================================================================//
//                              Discrete transition class                             //
//====================================================================================//
//    Discrete transition class associates a fire timing to discrete transitions.     //
//====================================================================================//
class DiscreteTransition : public Transition
{
  public:
    DiscreteTransition();
    DiscreteTransition(simuleau_name _name, double _time, double _steadytime);
    virtual ~DiscreteTransition();

    void Copy(const DiscreteTransition *_transition);
    trans_type IsA(){return Discrete_tr;};

    // simulation functions
    void   InitEnabledDate();
    void   SetEnabledDate(double _date);
    void   ReserveMarksInputPlaces();

    double GetTime();
    double GetSteadyTime();
    double GetEnabledDate();
    void   ComputeState();
    double GetRBFEvent();


    void WalkThrough(double duration);
    void FireDiscreteTransition();


    // print functions
    void Print(ostream &fout);


  protected:
    double time;
    double steadytime;
    double enableddate;
  private:
};

//====================================================================================//
//                             Continuous transition class                            //
//====================================================================================//
//    Continuous transition class associates a flow to continuous transitions.        //
//====================================================================================//
class ContinuousTransition : public Transition
{
  public:
    ContinuousTransition();
    ContinuousTransition(simuleau_name _name, double _flow, double _steadyflow);
    virtual ~ContinuousTransition();

    void Copy(const ContinuousTransition *_transition);
    virtual trans_type IsA(){return Continuous_tr;};

    Flow* GetFlow();
    void SetInitialFlow(double _flow);
    void SetTheoreticalFlow(double _flow);
    int IsSteadyFlowReached();

            double ComputeMinimumFlow(double f1, double f2);
    virtual void   ComputeTheoreticalFlow();

    virtual void WalkThrough(double duration);
            void FireContinuousTransition(double duration);

    double GetRBFEvent();

    // print functions
    void Print(ostream &fout);
    virtual void Write(ostream &fout);

  protected:
    Flow *flow;

    double ComputeTheoreticalFlowOfPlace();

  private:
};


//====================================================================================//
//                                Batch transition class                              //
//====================================================================================//
//    Batch transition class associates a flow to batch transitions.                  //
//====================================================================================//
class BatchTransition : public ContinuousTransition
{
  public:
    BatchTransition();
    BatchTransition(simuleau_name _name, double _flow, double _steadyflow);
    virtual ~BatchTransition();

    void Copy(const BatchTransition *_transition);
    trans_type IsA(){return Batch_tr;};

    double GetInputTheoreticalFlow();
    double GetOutputTheoreticalFlow();

    void ComputeInputTheoreticalFlow();
    void ComputeOutputTheoreticalFlow();
    void ComputeTheoreticalFlow();
    double GetRBFEvent();
    // print functions
    void Print(ostream &fout);
    void Write(ostream &fout);

  protected:
    double inputtheoreticalflow;
    double outputtheoreticalflow;
  private:
};

#endif // TRANSITION_H
