//====================================================================================//
//                                                                                    //
//                                     Batch class                                    //
//                                                                                    //
//====================================================================================//
//  This File:   batch.h                          Language: C++  (xlC and CC)         //
//  Software:    SIMULEAU                                                             //
//====================================================================================//
//  Creation:    28/apr/16                        by: Leonardo.Brenner@lsis.org       //
//  Last Change: 28/apr/16                        by: Leonardo.Brenner@lsis.org       //
//====================================================================================//
#ifndef BATCH_H
#define BATCH_H


class Batch // batch.cpp
{
  public:
    Batch();
    Batch(char *_name);
    Batch(double _length, double _density, double _position);
    Batch(double _inputflow, double _speed);
    virtual ~Batch();

    virtual batch_type IsA(){return Batch_bt;};
    char* GetName();
    void Copy(const Batch &_batch);
    int IsEqual(const Batch &_batch);

    // input functions
    void SetDensity(double _density);
    void SetLength(double _length);
    void SetPosition(double _position);
    void MovePosition(double _length);

    // output functions
    double GetDensity();
    double GetLength();
    double GetPosition();
    double GetOutputFlow(double _speed);

    // batch evolution functions
    void EvolveInFreeBehaviour(Simtime *_stime, BatchPlace *bp); //RL
    void EvolveInAccumulatedOuputBehaviour(Simtime *_stime, BatchPlace *bp, double _flow); //SA
    void EvolveInFreeToAccumulatedBehaviourAccBatch(Simtime *_stime, BatchPlace *bp, double _flow); //RLAa
    void EvolveInFreeToAccumulatedBehaviourFreeBatch(Simtime *_stime, BatchPlace *bp, double ouputbatchlength, double _flow); //RLAl
    void EvolveInPartiallyAccumulatedBehaviourAccBatch(Simtime *_stime, BatchPlace *bp, double ouputbatchlength, double _flow); //RAPa1
    void EvolveInPartiallyAccumulatedBehaviourFreeBatch(Simtime *_stime, BatchPlace *bp, double outputbatchlength, double batch2length, double _flow); //RAPl

    //print function
    virtual void Print(ostream &fout);
    virtual void Write(ostream &fout);


  protected:
    simuleau_name name;
    double length;
    double density;
    double position;
    double outputflow;

  private:
};


class ControllableBatch : public Batch // controllablebatch.cpp
{
  public:
    typedef void (ControllableBatch::*EvolveFunction)(Simtime *_stime, TriangularBatchPlace *tbp, ControllableBatch *pcb, ControllableBatch *ncb);

  public:
    ControllableBatch();
    ControllableBatch(char *_name);
    ControllableBatch(double _length, double _density, double _position, double _speed);
    ControllableBatch(double _inputflow, double _speed);
    virtual ~ControllableBatch();

    virtual batch_type IsA(){return Controllable_bt;};
    void Copy(const ControllableBatch &_controllablebatch);
    int IsEqual(const ControllableBatch &_controllablebatch);


    // input functions
    void SetSpeed(double _speed);
    void SetState(ctrl_batch_state _state);
    void SetBehaviour(ctrl_batch_behaviour _behaviour);

    // output functions
    double GetSpeed();
    double GetFlow();
    ctrl_batch_state GetState();
    ctrl_batch_state GetState(TriangularBatchPlace *tbp);
    char* GetStateName();
    ctrl_batch_behaviour GetBehaviour();
    char* GetBehaviourName();

    int IsOutputControllableBatch(BatchPlace *bp);
    int IsInContactWithByFront(ControllableBatch &cb);
    int IsInContactWithByRear(ControllableBatch &cb);


    // controllable batch evolution functions
    void SetEvolveFunction(EvolveFunction _func);
    void Evolve(Simtime *_stime, TriangularBatchPlace *tbp, ControllableBatch *pcb, ControllableBatch *ncb);

    void EvolveInFreeBehaviour(Simtime *_stime, TriangularBatchPlace *tbp, ControllableBatch *pcb, ControllableBatch *ncb); //RL
    void EvolveInCongestingOutputBehaviour(Simtime *_stime, TriangularBatchPlace *tbp, ControllableBatch *pcb, ControllableBatch *ncb); //RL
    void EvolveInFreeToCongestingBehaviourCongBatch(Simtime *_stime, TriangularBatchPlace *tbp, ControllableBatch *pcb, ControllableBatch *ncb); //RL
    void EvolveInFreeToCongestingBehaviourFreeBatch(Simtime *_stime, TriangularBatchPlace *tbp, ControllableBatch *pcb, ControllableBatch *ncb); //RL
    void EvolveInFreeToCongestingBehaviourMiddlePlaceWithContactCongBatch(Simtime *_stime, TriangularBatchPlace *tbp, ControllableBatch *pcb, ControllableBatch *ncb); //RL
    void EvolveInFreeToCongestingBehaviourMiddlePlaceWithContactFreeBatch(Simtime *_stime, TriangularBatchPlace *tbp, ControllableBatch *pcb, ControllableBatch *ncb); //RL
    void EvolveInUncongestingOutputBehaviour(Simtime *_stime, TriangularBatchPlace *tbp, ControllableBatch *pcb, ControllableBatch *ncb); //RL
    void EvolveInUncongestingToFreeBehaviourCongBatch(Simtime *_stime, TriangularBatchPlace *tbp, ControllableBatch *pcb, ControllableBatch *ncb); //RL
    void EvolveInUncongestingToFreeBehaviourFreeBatch(Simtime *_stime, TriangularBatchPlace *tbp, ControllableBatch *pcb, ControllableBatch *ncb); //RL


    //print function
    void Print(ostream &fout);
    void Write(ostream &fout);


  protected:
    double speed;
    ctrl_batch_state state;
    ctrl_batch_behaviour behaviour;
    EvolveFunction evolvefunction;

  private:
};

#endif // BATCH_H
