//====================================================================================//
//                                                                                    //
//                        Batch place classes inherit of place class                  //
//                                                                                    //
//====================================================================================//
//  This File:   batchplace.cpp                   Language: C++  (xlC and CC)         //
//  Software:    SIMULEAU                                                             //
//====================================================================================//
//  Creation:    22/apr/16                        by: Leonardo.Brenner@lsis.org       //
//  Last Change: 22/apr/16                        by: Leonardo.Brenner@lsis.org       //
//====================================================================================//
#include "simuleau.h"

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
// Methods of the Batch Place class
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Constructor
//------------------------------------------------------------------------------
BatchPlace::BatchPlace()
{
  behaviour          = Free_behaviour;
  formalisation      = no;
  speed              = 0.0;
  maxspeed           = 0.0;
  instantaneousspeed = 0.0;
  density            = 0.0;
  length             = 0.0;

  marks     = new list<Batch>;
  marks->clear();
  initialmarks = new list<Batch>;
  initialmarks->clear();
  steadymarks  = new list<Batch>;
  steadymarks->clear();

}

//------------------------------------------------------------------------------
// Initialized Constructor
//------------------------------------------------------------------------------
BatchPlace::BatchPlace(simuleau_name _name, double _speed, double _density, double _length):Place(_name)
{
  behaviour          = Free_behaviour;
  formalisation      = no;
  speed              = _speed;
  maxspeed           = _speed;
  instantaneousspeed = _speed;
  density            = _density;
  length             = _length;

  marks = new list<Batch>;
  marks->clear();
  initialmarks = new list<Batch>;
  initialmarks->clear();
  steadymarks  = new list<Batch>;
  steadymarks->clear();

}

//------------------------------------------------------------------------------
// Destructor
//------------------------------------------------------------------------------
BatchPlace::~BatchPlace()
{
  if (marks)
    delete[] marks;
  if (initialmarks)
    delete[] initialmarks;
  if (steadymarks)
    delete[] steadymarks;
}

//------------------------------------------------------------------------------
// Copy
//------------------------------------------------------------------------------
void BatchPlace::Copy(const BatchPlace * _place)
{
  TransferNode(_place);

  conflict           = _place->conflict;
  structuralconflict = _place->structuralconflict;

  // batch place
  behaviour     = _place->behaviour;
  formalisation = _place->formalisation;


  density  = _place->density;
  speed    = _place->speed;
  maxspeed = _place->maxspeed;
  instantaneousspeed = _place->instantaneousspeed;

  length   = _place->length;

  marks = new list<Batch>;
  marks->clear();

  for (list<Batch>::iterator it=_place->marks->begin(); it!=_place->marks->end(); ++it) {
    Batch *newbatch = new Batch;
    newbatch->Copy(*it);
    marks->push_back(*newbatch);
  }

  initialmarks = new list<Batch>;
  initialmarks->clear();

  for (list<Batch>::iterator it=_place->initialmarks->begin(); it!=_place->initialmarks->end(); ++it) {
    Batch *newbatch = new Batch;
    newbatch->Copy(*it);
    initialmarks->push_back(*newbatch);
  }

  steadymarks = new list<Batch>;
  steadymarks->clear();

  for (list<Batch>::iterator it=_place->steadymarks->begin(); it!=_place->steadymarks->end(); ++it) {
    Batch *newbatch = new Batch;
    newbatch->Copy(*it);
    steadymarks->push_back(*newbatch);
  }

}

//------------------------------------------------------------------------------
// It adds the initial marking of the place
//------------------------------------------------------------------------------
void BatchPlace::AddBatchToInitialMarking(Batch &b)
{
  initialmarks->push_back(b);
  marks->push_back(b);
}

//------------------------------------------------------------------------------
// It adds a batch to steady marking of the place
//------------------------------------------------------------------------------
void BatchPlace::AddBatchToSteadyMarking(Batch &b)
{
  steadymarks->push_back(b);
}
//------------------------------------------------------------------------------
// It adds a batch to marking of the place
//------------------------------------------------------------------------------
void BatchPlace::AddBatchToMarking(Batch &b)
{
  marks->push_back(b);
}

//------------------------------------------------------------------------------
// It replace the current marking of a place
// TODO test this method
//------------------------------------------------------------------------------
void BatchPlace::ChangeMarks(list<Batch> *lb)
{
  marks->clear();
  delete marks;
  marks = lb;
}

//------------------------------------------------------------------------------
// It sets the new speed
//------------------------------------------------------------------------------
void BatchPlace::SetSpeed(double _speed)
{
  speed = _speed;
  instantaneousspeed = _speed;
}

//------------------------------------------------------------------------------
// It sets the new maximum speed
//------------------------------------------------------------------------------
void BatchPlace::SetMaxSpeed(double _maxspeed)
{
  maxspeed = _maxspeed;
}

//------------------------------------------------------------------------------
// It sets the new density
//------------------------------------------------------------------------------
void BatchPlace::SetDensity(double _density)
{
  density = _density;
}

//------------------------------------------------------------------------------
// It sets the new length
//------------------------------------------------------------------------------
void BatchPlace::SetLength(double _length)
{
  length = _length;
}

//------------------------------------------------------------------------------
// It returns the current speed
//------------------------------------------------------------------------------
double BatchPlace::GetSpeed()
{
  return (speed);
}

//------------------------------------------------------------------------------
// It returns the maximum/initial speed
//------------------------------------------------------------------------------
double BatchPlace::GetMaxSpeed()
{
  return (maxspeed);
}


//------------------------------------------------------------------------------
// It returns the instantaneous speed
//------------------------------------------------------------------------------
double BatchPlace::GetInstantaneousSpeed()
{
  return (instantaneousspeed);
}

//------------------------------------------------------------------------------
// It returns the current density
//------------------------------------------------------------------------------
double BatchPlace::GetDensity()
{
  return (density);
}

//-------------------Near-optimal---------------------------------------------------------
// It returns the free steady marking quantity
//-----------------------------------------------------------------------------
double BatchPlace::GetSteadyDensity()
{
   double steadydensity;

   if (steadymarks->size() != 0){
    for (list<Batch>::iterator b=steadymarks->begin(); b!=steadymarks->end();b++) {
        steadydensity = b->GetDensity();
      }
    }
    return(steadydensity);
}




//------------------------------------------------------------------------------
// It returns the length
//------------------------------------------------------------------------------
double BatchPlace::GetLength()
{
  return (length);
}

//------------------------------------------------------------------------------
// It returns the place flow
//------------------------------------------------------------------------------
double BatchPlace::GetOutputFlow()
{
  double outputflow = 0.0;
  list<Batch>::iterator b;

  if(!HasOutputBatch())
    return outputflow;

  b = marks->begin();
  outputflow = b->GetOutputFlow(instantaneousspeed);

  return outputflow;
}

//-----------------------------MCFF-------------------------------------------------
// It returns the place steadyinputflow or output flow in a place
//------------------------------------------------------------------------------
double BatchPlace::GetSteadyPlaceFlow()
{
  double steadyplaceflow = 0.0;
  Transition *t;
  Flow *f;

    for (int i=0; i<numberofarcs; i++){
      if (inputarcs[i] != 0){
        t = (Transition*)inputnodes[i];
        f = t->GetFlow();
        steadyplaceflow += f->GetSteadyFlow();
        }
        }
    return (steadyplaceflow);
}


//------------------------------------------------------------------------------
// It returns the place marks
//------------------------------------------------------------------------------
list<Batch>* BatchPlace::GetMarking()
{
  return (marks);
}

//------------------------------------------------------------------------------
// It returns the place initial marks
//------------------------------------------------------------------------------
list<Batch>* BatchPlace::GetInitialMarking()
{
  return (initialmarks);
}

//------------------------------------------------------------------------------
// It returns the place steady marks
//------------------------------------------------------------------------------
list<Batch>* BatchPlace::GetSteadyMarking()
{
  return (steadymarks);
}

//--------------------------------------
//It returns the quantity
//--------------------------------------
double BatchPlace::GetInitialQuantity()
{
   if (marks->size() != 0){
    for (list<Batch>::iterator b=marks->begin(); b!=marks->end(); b++) {
      quantity += b->GetDensity() * b->GetLength();
      }
    }
    return(quantity);
}

//----------------------------------------------------------------------------
// It returns the free steady marking quantity
//-----------------------------------------------------------------------------
double BatchPlace::GetFreeSteadyQuantity()
{

   double freesteadyquantity = 0.0;
   double steadyplaceflow;

   Flow *f;
   Transition *t;

    for (int i=0; i<numberofarcs; i++){
      if (inputarcs[i] != 0){
        t = (Transition*)inputnodes[i];
        f = t->GetFlow();
        steadyplaceflow += f->GetSteadyFlow();
        }
        }

   if (steadymarks->size() == 1){
    for (list<Batch>::iterator b=steadymarks->begin(); b!=steadymarks->end();b++) {
      if (b->GetDensity() == steadyplaceflow / maxspeed ){
        freesteadyquantity = b->GetDensity() * b->GetLength();
      }
      else{
      freesteadyquantity = 0;
      }
    }
    }
    if (steadymarks->size() == 2){
      for (list<Batch>::iterator b=steadymarks->begin(); b!=steadymarks->end(); b++){
        freesteadyquantity = b->GetDensity() * b->GetLength();
      }
    }
    return(freesteadyquantity);
}

//----------------------------------------------------------------------------
// It returns the steady marking quantity
//-----------------------------------------------------------------------------

double BatchPlace::GetSteadyQuantity()
{

 if (steadymarks->size() != 0){
    for (list<Batch>::iterator b=steadymarks->begin(); b!=steadymarks->end(); b++) {
      steadyquantity += b->GetDensity() * b->GetLength();
    }
 return(steadyquantity);

}
}



//------------------------------------------------------------------------------
// It returns the place behaviour
//------------------------------------------------------------------------------
int BatchPlace::GetBehaviour()
{
  return (behaviour);
}

//------------------------------------------------------------------------------
// It returns the place behaviour
// We suppose only one output transition
// TODO verify if two or more tranistion
//------------------------------------------------------------------------------
double BatchPlace::GetFlowOfOutputTransition()
{
  double outputtransitionflow = -1.0;

  Transition *t;
  Flow *f;

  for (int i=0; i<numberofarcs; i++){
    if (outputarcs[i] != 0.0){
      t = outputnodes[i];
      switch (t->IsA()){
        case Batch_tr: f = t->GetFlow();
                       outputtransitionflow = f->GetCurrentFlow();
        break;
      }
    }
  }
  return (outputtransitionflow);
}

//------------------------------------------------------------------------------
// It returns the output flow of all input transitions.
//------------------------------------------------------------------------------
double BatchPlace::GetInputTransitonsFlow()
{
  double inflow = 0.0;
  Transition *t;
  Flow *f;

  for (int i=0; i<numberofarcs; i++){
    if (inputarcs[i] != 0.0){
      t = inputnodes[i];
      f = t->GetFlow();
      inflow += f->GetCurrentFlow();
    }
  }

  return (inflow);
}


//------------------------------------------------------------------------------
// It returns the output flow of all output transitions
//------------------------------------------------------------------------------
double BatchPlace::GetOutputTransitonsFlow()
{
  double outflow = 0.0;
  Transition *t;
  Flow *f;

  for (int i=0; i<numberofarcs; i++){
    if (outputarcs[i] != 0.0){
      t = outputnodes[i];
      f = t->GetFlow();
      outflow += f->GetCurrentFlow();
    }
  }

  return (outflow);
}

//------------------------------------------------------------------------------
// It returns true if the place has an output batch
//------------------------------------------------------------------------------
int BatchPlace::HasOutputBatch()
{
  int result;

  Batch b;

  if (marks->size() ==  0){
    result = 0;
  }
  else{
    b = marks->front();
    if ((length - b.GetPosition()) <= PRF::prf.Min_Err()){
      result = 1;
    }
    else{
      result = 0;
    }
  }
  return (result);
}


//------------------------------------------------------------------------------
// It returns the output batch
//------------------------------------------------------------------------------
Batch* BatchPlace::GetOutputBatch()
{
  return (&marks->front());
}

//------------------------------------------------------------------------------
// It returns the density of the output batch
//------------------------------------------------------------------------------
double BatchPlace::GetDensityOutputBatch()
{
  double densityoutputbatch = 0.0;
  Batch *b;

  if (HasOutputBatch()){
    b = GetOutputBatch();
    densityoutputbatch = b->GetDensity();
  }
  return (densityoutputbatch);
}

//------------------------------------------------------------------------------
// It returns true if the batch place is full
//------------------------------------------------------------------------------
int BatchPlace::IsFull()
{
  double full = length * density;
  quantity = 0.0;

  if (marks->size() != 0){
    for (list<Batch>::iterator b=marks->begin(); b!=marks->end(); b++) {
      quantity += b->GetDensity() * b->GetLength();
    }
    if (abs(quantity - full) < PRF::prf.Min_Err()){
      return 1;
    }
    else{
      return 0;
    }
  }
  else {
    return 0;
  }
}

//===========================ON/Off==============================================
//------------------------------------------------------------------------------
// It returns true if the batch place reaches steady marking quantity
//------------------------------------------------------------------------------
int BatchPlace::IsEqualSteadyQuantity()
{
  ComputeQuantities();

	if (abs(quantity -  steadyquantity) < PRF::prf.Min_Err()){
    return 1;
  }
  else{
    return 0;
	}
}


//------------------------------------------------------------------------------
// It returns true if the marking quantity is greater than steady marking quantity
//------------------------------------------------------------------------------
int BatchPlace::IsGreaterThanSteadyQuantity()
{
  ComputeQuantities();
	if ((quantity - steadyquantity) > PRF::prf.Min_Err()){
    return 1;
  }
  else{
    return 0;
	}
}


//------------------------------------------------------------------------------
// It returns true if the marking quantity is less than steady marking quantity
//------------------------------------------------------------------------------
int BatchPlace::IsLessThanSteadyQuantity()
{

  ComputeQuantities();

  if (((abs(quantity - steadyquantity) > PRF::prf.Min_Err()) && ((quantity - steadyquantity) < 0.0))){
    return 1;
  }
  else{
    return 0;
	}
}

//------------------------------------------------------------------------------
// It returns true if the marking is equal than steady marking
//------------------------------------------------------------------------------
int BatchPlace::IsSteadyMarkingReached()
{
  if (steadymarks->size() != marks->size())
  return 0;

  list<Batch>::iterator b=marks->begin();
  if (steadymarks->size() != 0){
    for (list<Batch>::iterator sb=steadymarks->begin() ; sb!=steadymarks->end(); ++b,++sb) {
      if(!sb->IsEqual(*b))
        return 0;
    }
	}

  return 1;
}
//==============================================================================


//===================Vecos==================================================
// It returns ture if the marking of continuous place is equal to steady marking
// and all of its input and output transition belongs to T_Z.
//==========================================================================
int BatchPlace::IsSteadyMarkingQuantityReachedandInputOutputTransitionBelongTz()
{
    Transition *t;
    double weight;
    int placestate =0;
    int counter = 0;
    int countertz=0;

    ComputeQuantities();

    if (abs(steadyquantity -  quantity) < PRF::prf.Min_Err()){
        if (GetNumberOfOutputArcs() == 0){
            return 1; // the conservative net and consistent net
        }
        else{
            for (int i=0;  i<numberofarcs; i++){
                if (outputarcs[i] != 0.0){
//        t = simulation->sbpn->GetTransition(i);
                    t=outputnodes[i];
                        counter++;
                }
                if (outputarcs[i] != 0.0 && t->IsFiringQuantityOnState()==0)
                        countertz++;

            }
            }

        if (GetNumberOfInputArcs() == 0){
            return 1; // the conservative net and consistent net
        }
        else{
            for (int i=0;  i<numberofarcs; i++){
                if (inputarcs[i] != 0.0){
//        t = simulation->sbpn->GetTransition(i);
                        t=inputnodes[i];
                        counter++;
                }
                if (inputarcs[i] != 0.0 && t->IsFiringQuantityOnState()==0)
                        countertz++;

            }
            }
        if (counter == countertz)
                return 1;
    return placestate;
    }
    return placestate;
}










//------------------------------------------------------------------------------
// It computes the state of the batch place
//------------------------------------------------------------------------------
void BatchPlace::ComputeState()
{
  if (marks->empty()){
    state =  0.0;
  }
  else{
    if (HasOutputBatch() == 1){
      state = 1.0;
    }
    else{
      state = 0.0;
    }
  }
}

//------------------------------------------------------------------------------
// It creates a batch if the place has a least an input transition (continuous or batch)
// and the input flow is non-null
// It returns a positive value if a batch was created and 0 otherwise
// TODO verify multiple input transitions
//------------------------------------------------------------------------------
int BatchPlace::CreateBatches()
{
  double inputtransflow = 0.0;
  int    result = 0;

  Transition *t;
  Flow *f;
  Batch *newb;

  formalisation = no;

  if (GetNumberOfInputArcs() != 0){
    for (int i=0; i<numberofarcs; i++){
      if (inputarcs[i] != 0.0){
        t = inputnodes[i];
        switch (t->IsA()){
          case Continuous_tr:
          case Batch_tr:  f = t->GetFlow();
                          inputtransflow = f->GetCurrentFlow();
                          if (inputtransflow > 0.0){
                            if (!marks->empty()){
                              Batch &b = marks->back();
                              if ((b.GetLength() != 0.0 ) && (b.GetPosition() != 0.0)){
                                newb = new Batch(inputtransflow, instantaneousspeed);
                                AddBatchToMarking(*newb);
                                i =  numberofarcs;
                                result += 1;
                              }
                            }
                            else{
                              newb = new Batch(inputtransflow, instantaneousspeed);
                              AddBatchToMarking(*newb);
                              i =  numberofarcs;
                              result += 1;
                            }
                          }
               break;
        }
      }
    }
  }
}

//------------------------------------------------------------------------------
// It merges the batch that have the same speed and density and are in contact
//------------------------------------------------------------------------------
void BatchPlace::MergeBatches()
{
  int merged = 0;
  double endposition;
  double newlength;
  int numberofbatches = marks->size();
  Batch *newbatch;
  list<Batch> *newmarks = new list<Batch>;
  list<Batch>::iterator b1, b2, bi;

  if (numberofbatches > 1){
    b1 = marks->begin();
    bi = marks->begin();
    bi++;
    for (int i=1; i<numberofbatches; i++){
      endposition = b1->GetPosition() - b1->GetLength();
      b2 = bi;

      if ((abs(b1->GetDensity() - b2->GetDensity()) < PRF::prf.Min_Err()) &&
          (abs(b2->GetPosition() - endposition) < PRF::prf.Min_Err())){ // merge batches
        newlength = b1->GetLength() + b2->GetLength();
        b1->SetLength(newlength);
        bi++;
        merged = 1;
      }
      else{
        newbatch = new Batch;
        newbatch->Copy(*b1);
        newmarks->push_back(*newbatch);
        b1 = b2;
        bi++;
        merged = 0;
      }
    }

    newbatch = new Batch;
    if (merged){
      newbatch->Copy(*b1);
    }
    else{
      newbatch->Copy(*b2);
    }

    newmarks->push_back(*newbatch);
    marks->clear();
    marks = newmarks;
  }
}

//------------------------------------------------------------------------------
// It destructs the batches that have a length or density equal to 0 and are not
// the input batch (position equal to length)
//------------------------------------------------------------------------------
void BatchPlace::DestructBatches()
{
  int numberofbatches = marks->size();
  Batch *newbatch;
  list<Batch> *newmarks = new list<Batch>;
  list<Batch>::iterator b1;

  double batchquantity = 1.0;

  if (numberofbatches != 0){
    for (b1=marks->begin(); b1!=marks->end(); ++b1) {
      batchquantity = b1->GetDensity() * b1->GetLength() ;
// last next date
      if ((b1->GetPosition() - PRF::prf.Min_Err() ) > length){
        b1->SetLength(0.0);
        b1->SetPosition(length);
      }
// end

// maybe compute batchquantity as a pourcentage of the total place capacity

      if (((b1->GetLength() > PRF::prf.Min_Err()) || (batchquantity > (PRF::prf.Min_Err()/10))) || ((b1->GetPosition() == 0.0)  && (b1->GetLength() == 0.0))){
        newbatch = new Batch;
        newbatch->Copy(*b1);
        newmarks->push_back(*newbatch);
      }
    }
    marks->clear();
    marks = newmarks;
  }
}

//------------------------------------------------------------------------------
// It destructs the batches that have a length or density equal to 0 and are not
// the input batch (position equal to length)
//------------------------------------------------------------------------------
void BatchPlace::ComputeQuantities()
{
  steadyquantity = 0.0;
  quantity = 0.0;

  if (steadymarks->size() != 0){
    for (list<Batch>::iterator b=steadymarks->begin(); b!=steadymarks->end(); b++) {
      steadyquantity += b->GetDensity() * b->GetLength();
    }
	}

  if (marks->size() != 0){
    for (list<Batch>::iterator b=marks->begin(); b!=marks->end(); b++) {
      quantity += b->GetDensity() * b->GetLength();
    }
  }
}



//---------------------------------------------------------------------------------------------
// It computes that two types of quantity in the free part and accumulated part of steady state.
//----------------------------------------------------------------------------------------------
//void BatchPlace::ComputeTwoTypesSteadyQuantities()
//{
//  steadyquantity = 0.0;
//  double  freesteadyquantity = 0.0;
//  double  accumulatedsteadyquantity = 0.0;
//
//
//  if (steadymarks->size() != 0){
//    for (list<Batch>::iterator b=steadymarks->begin(); b!=steadymarks->end(); b++) {
//      steadyquantity += b->GetDensity() * b->GetLength();
//    }
//	}
//
//    if (steadymarks->size() ==2){
//    for (list<Batch>::iterator b=steadymarks->begin(); b!=steadymarks->begin(); b++) {
//      freesteadyquantity += b->GetDensity() * b->GetLength();
//      accumulatedsteadyquantity = steadyquantity-freesteadyquantity;
//    }
//	}
//	if (steadymarks->size() ==1){
//        for (list<Batch>::iterator b=steadymarks->begin(); b!=steadymarks->begin(); b++){
//        if ( GetSteadyPlaceFlow() == b->GetDensity() * GetSpeed() ){
//             freesteadyquantity = b->GetDensity() * b->GetLength();
//             accumulatedsteadyquantity = 0.0;
//             }
//        else{
//             freesteadyquantity = 0.0;
//             accumulatedsteadyquantity = b->GetDensity() * b->GetLength();
//            }
//        }
//    }
//    }


//------------------------------------------------------------------------------
// It computes the behaviour of a batch place
// TODO all batch places have no structural conflit
//------------------------------------------------------------------------------
void BatchPlace::ComputeBehaviour()
{
  double outputtransitionflow;
  double outputtransitionflowth;
  Transition *t;
  Flow *f;

  if (state == 0.0){
    behaviour = Free_behaviour;
    return;
  }

  outputtransitionflow = GetOutputTransitonsFlow(); // all output transitions flow
  outputtransitionflowth = GetOutputFlow();

  if (outputtransitionflow < outputtransitionflowth){
    behaviour = Accumulating_behaviour;
  }
  else{
    behaviour = Free_behaviour;
  }

/*  for (int i=0; i<numberofarcs; i++){
    if (outputarcs[i] != 0.0){
      t = outputnodes[i];
      switch (t->IsA()){
        case Batch_tr:  f = t->GetFlow();
                        //outputtransitionflow = f->GetCurrentFlow();
                        ((BatchTransition*)t)->ComputeInputTheoreticalFlow();
                        outputtransitionflowth = ((BatchTransition*)t)->GetInputTheoreticalFlow();
                        if (outputtransitionflow < outputtransitionflowth){
                          behaviour = Accumulating_behaviour;
                        }
                        else{
                          behaviour = Free_behaviour;
                        }
             break;
      }
    }
  }*/
}


//------------------------------------------------------------------------------
// It evolves the marks (batches) of the batch place
//------------------------------------------------------------------------------
void BatchPlace::EvolveMarks(double _date)
{
  Batch *ob;
  list<Batch>::iterator b;

  if (formalisation == yes)
    return;

  if (behaviour == Free_behaviour){
    EvolveInFreeBehaviour(simulation->stime);
  }
  else{
    if (HasOutputBatch() ==  1){ // place is in accumulating behaviour and has an output batch
      ob = GetOutputBatch();
      if ((density - ob->GetDensity()) <= PRF::prf.Min_Err()){ // the output batch is full accumulated
        if (marks->size() > 1){ // new marking depends of a second batch
          b = marks->begin();
          b++;
          if (b->GetPosition() > (length - ob->GetLength())){
            b->SetPosition(length - ob->GetLength());
          }

          if (abs(b->GetPosition() - (length - ob->GetLength())) <= PRF::prf.Min_Err()) { // two batches are in contact
            EvolveInPartiallyAccumulatedBehaviour(simulation->stime);
          }
          else{ // batch are not in contact
            EvolveInAccumulatedOutputBehaviour(simulation->stime);
          }
        }
        else{ // only one batch
          EvolveInAccumulatedOutputBehaviour(simulation->stime);
        }
      }
      else{ // the output batch has a different density of the place density
        EvolveInFreeToAccumulatedBehaviour(simulation->stime);
      }
    }
  }
  formalisation == yes;
  steadyquantity = 0.0;
  if (steadymarks->size() != 0){
    for (list<Batch>::iterator b=steadymarks->begin(); b!=steadymarks->end(); b++) {
      steadyquantity += b->GetDensity() * b->GetLength();
    }
	}
  quantity = 0.0;
  if (marks->size() != 0){
    for (list<Batch>::iterator b=marks->begin(); b!=marks->end(); b++) {
      quantity += b->GetDensity() * b->GetLength();
    }
  }

}

//------------------------------------------------------------------------------
// It computes the date of the next event to a Batch becomes an Output Batch type
// returns -1.0 if no BOB event is possible
//------------------------------------------------------------------------------
double BatchPlace::GetBOBEvent()
{
  double pos;
  double date;
  Batch b;

  if (marks->empty()){
    return (-1.0); // no event of this type
  }

  if (state != 1.0){
    b = marks->front();
    pos = b.GetPosition();
    date = (length - pos) / instantaneousspeed;
    date += simulation->stime->GetCurrentDate();
    return (date);
  }
  return (-1.0);
}


//------------------------------------------------------------------------------
// It computes the date of the next event to a Destruction of an Output Batch type
//------------------------------------------------------------------------------
double BatchPlace::GetDOBEvent()
{
  double date = -1.0;
  double outputtransitionflow = GetOutputTransitonsFlow();
  Transition *t;
  Flow *f;
  Batch *b;

  if (HasOutputBatch() == 1){
    b = GetOutputBatch();
    if ((density - b->GetDensity()) > PRF::prf.Min_Err()){
      if ((behaviour == Free_behaviour) && (instantaneousspeed != 0.0)){
        date = simulation->stime->GetCurrentDate() + (b->GetLength()/instantaneousspeed);
      }
    }
    else{
      if (outputtransitionflow != 0.0){
        date = b->GetLength() * density / outputtransitionflow;
        date += simulation->stime->GetCurrentDate();
      }
/*      for (int i=0; i<numberofarcs; i++){
        if (outputarcs[i] != 0.0){
          t = outputnodes[i];
          switch (t->IsA()){
            case Batch_tr:  f = t->GetFlow();
                            outputtransitionflow = f->GetCurrentFlow();
                            if (outputtransitionflow != 0.0){
                              date = b->GetLength() * density / outputtransitionflow;
                              date += simulation->stime->GetCurrentDate();
                            }
                 break;
          }
        }
      }
*/
    }
  }
  return (date);
}


//------------------------------------------------------------------------------
// It computes the date of the next event to a Full accumulation/congestion of a batch
// (Batch Becomes Dense) type
//------------------------------------------------------------------------------
double BatchPlace::GetBBDEvent()
{
  double date = -1.0;
  double outputtransitionflow = GetOutputTransitonsFlow();
  Transition *t;
  Flow *f;
  Batch *b;

  if (HasOutputBatch() == 1){
    b = GetOutputBatch();
    if ((density - b->GetDensity()) > PRF::prf.Min_Err()){
      if ((behaviour == Accumulating_behaviour)){
        if (((instantaneousspeed * density) - outputtransitionflow) != 0.0){
          date = b->GetLength() * (density - b->GetDensity()) / (instantaneousspeed * density - outputtransitionflow);
          date += simulation->stime->GetCurrentDate();
        }

/*
        for (int i=0; i<numberofarcs; i++){
          if (outputarcs[i] != 0.0){
            t = outputnodes[i];
            switch (t->IsA()){
              case Batch_tr:  f = t->GetFlow();
                              outputtransitionflow = f->GetCurrentFlow();
                              if (((instantaneousspeed * density) - outputtransitionflow) != 0.0){
                                date = b->GetLength() * (density - b->GetDensity()) / (instantaneousspeed * density - outputtransitionflow);
                                date += simulation->stime->GetCurrentDate();
                              }
                  break;
            }
          }
        }
*/
      }
    }
  }
  return (date);
}


//------------------------------------------------------------------------------
// It computes the date of the next event to Two Batches Meet type
// TODO verify multiple output transitions
//------------------------------------------------------------------------------
double BatchPlace::GetTBMEvent()
{
  double date = -1.0;
  double outputtransitionflow = GetOutputTransitonsFlow();
  Transition *t;
  Flow *f;
  list<Batch>::iterator b;
  Batch *outputbatch;

  if ((marks->size() < 2) || (HasOutputBatch() == 0)){
    return (date);
  }

  outputbatch = GetOutputBatch();
  b = marks->begin();
  b++;
  if (((density - outputbatch->GetDensity()) < PRF::prf.Min_Err()) && (behaviour == Accumulating_behaviour)){
    if (((length - outputbatch->GetLength()) - b->GetPosition()) > PRF::prf.Min_Err()){
      if ((instantaneousspeed * density - outputtransitionflow) != 0.0){
        date = (density * (length - outputbatch->GetLength() - b->GetPosition())/(instantaneousspeed * density - outputtransitionflow));
        date += simulation->stime->GetCurrentDate();
      }


/*      for (int i=0; i<numberofarcs; i++){
        if (outputarcs[i] != 0.0){
          t = outputnodes[i];
          switch (t->IsA()){
            case Batch_tr : f = t->GetFlow();
                            outputtransitionflow = f->GetCurrentFlow();
                            if ((instantaneousspeed * density - outputtransitionflow) != 0.0){
                              date = (density * (length - outputbatch->GetLength() - b->GetPosition())/(instantaneousspeed * density - outputtransitionflow));
                              date += simulation->stime->GetCurrentDate();
                            }
                 break;
          }
        }
      }
*/
    }
    else{
      if (((density - b->GetDensity()) > PRF::prf.Min_Err()) && (instantaneousspeed != 0.0) && (density != 0.0) && (b->GetLength() != 0.0)){
//        outputtransitionflow = GetOutputTransitonsFlow();
        date = (density - b->GetDensity()) * b->GetLength()/(instantaneousspeed * density - outputtransitionflow);
        date += simulation->stime->GetCurrentDate();
      }
    }
  }
  return (date);
}

//----------------- On off considered events-------------------------------------------------
// It computes the date of the next event to marking quantity of a batch place
// reaches its steady marking quantity (Place reaches Steady marking Quantity).
//------------------------------------------------------------------------------
double BatchPlace::GetPSQEvent()
{
  double steadyquantity; // we need input this value before the simulation. !!!
  double quantity = 0.0;
  double date;
  double transitionflow=0.0;
  double weightflow=0.0;
  Batch *b;
  Transition *t;
  Flow *f;

  if (steadymarks->size() != 0){
    for (list<Batch>::iterator b=steadymarks->begin(); b!=steadymarks->end(); b++) {
      steadyquantity += b->GetDensity() * b->GetLength();
    }
	}

  if (marks->size() != 0){
    for (list<Batch>::iterator b=marks->begin(); b!=marks->end(); b++) {
      quantity += b->GetDensity() * b->GetLength();
    }
    }

 if (abs(quantity - steadyquantity) < PRF::prf.Min_Err()){
        date= simulation->stime->GetCurrentDate();
      }

  else{
  if (GetNumberOfOutputArcs()){
    for (int i=0; i<numberofarcs; i++){
      if (outputarcs[i] != 0.0){
//        t = simulation->sbpn->GetTransition(i);
        t = outputnodes[i];
        switch (t->IsA()){
          case Continuous_tr:
          case Batch_tr:  f = t->GetFlow();
                          transitionflow = f->GetCurrentFlow();
                            weightflow -= (outputarcs[i] * transitionflow);
               break;
        }
      }
    }
  }

    if (GetNumberOfInputArcs()){
      for (int i=0; i<numberofarcs; i++){
        if (inputarcs[i] != 0){
//          t = simulation->sbpn->GetTransition(i);
          t = inputnodes[i];
          switch (t->IsA()){
            case Continuous_tr:
            case Batch_tr:  f = t->GetFlow();
                            transitionflow = f->GetCurrentFlow();

                              weightflow += (inputarcs[i] * transitionflow);

                 break;
          }
        }
      }
    }

  if ((weightflow != 0.0)){
    date = simulation->stime->GetCurrentDate() - ((quantity-steadyquantity)/weightflow);
  }
  return (date);
}
}






  /* if (HasOutputBatch() == 1){
    b = GetOutputBatch();
    if ((density - b->GetDensity()) > PRF::prf.Min_Err()){
        for (int i=0; i<numberofarcs; i++){
          if (outputarcs[i] != 0.0){
            t = outputnodes[i];
            switch (t->IsA()){
              case Batch_tr:  u = t->GetFlow();
                              outputtransitionflow = u->GetCurrentFlow();
                              if (((instantaneousspeed * density) - outputtransitionflow) != 0.0){
                                date = b->GetLength() * (density - b->GetDensity()) / (instantaneousspeed * density - outputtransitionflow);
                                date += simulation->stime->GetCurrentDate();
                              }
                  break;
            }
          }
        }
    }
  }
  return (date);
}
}
*/

//-----------------------end----------------------------------------------------

//------------------------------------------------------------------------------
// Print
//------------------------------------------------------------------------------
void BatchPlace::Print(ostream& fout)
{
  fout << "Name:              " << name << endl;
  fout << "   Type:           " << "Batch" << endl;
  fout << "   Behaviour:      " << behaviour << endl;
  fout << "   Density:        " << density << endl;
  fout << "   Maximum speed:  " << maxspeed << endl;
  fout << "   Speed:          " << speed << endl;
  fout << "   Length:         " << length << endl;
  fout << "   Initial Batches:" << initialmarks->size() << endl;

  for (list<Batch>::iterator it=initialmarks->begin(); it!=initialmarks->end(); ++it) {
    it->Print(fout);
  }

  if (!steadymarks->empty()){
    fout << "   Steady Batches:" << steadymarks->size() << endl;

    for (list<Batch>::iterator it=steadymarks->begin(); it!=steadymarks->end(); ++it) {
      it->Print(fout);
    }
  }

  fout << "   Batches:        " << marks->size() << endl;

  for (list<Batch>::iterator it=marks->begin(); it!=marks->end(); ++it) {
    it->Print(fout);
  }

  fout << "    Arcs:          " << endl;
  PrintArcs(fout);
}

//------------------------------------------------------------------------------
// Write
//------------------------------------------------------------------------------
void BatchPlace::Write(ostream &fout)
{
  ComputeQuantities();
  fout << "Name:             " << name << endl;
  fout << "   Type:          " << "Batch" << endl;
  fout << "   Density:       " << density << endl;
  fout << "   Speed:         " << speed << endl;
  fout << "   Length:        " << length << endl;
  fout << "   Batches:       " << marks->size() << ": " << quantity  << endl;

  for (list<Batch>::iterator it=marks->begin(); it!=marks->end(); ++it) {
    it->Write(fout);
  }

  if (!steadymarks->empty()){
    fout << "   Steady Batches:" << steadymarks->size() ;
    if (IsSteadyMarkingReached())
      fout << "(steady marking reached) ";
    fout << ": " << steadyquantity << endl;

    for (list<Batch>::iterator it=steadymarks->begin(); it!=steadymarks->end(); ++it) {
      it->Write(fout);
    }
  }
}

//------------------------------------------------------------------------------
// It evolves the marking in a free behaviour
// old fonctionRL
//------------------------------------------------------------------------------
void BatchPlace::EvolveInFreeBehaviour(Simtime *st)
{
  if (!marks->empty()){
    for (list<Batch>::iterator b = marks->begin(); b!=marks->end(); b++){
      b->EvolveInFreeBehaviour(st, this);
    }
  }
}

//------------------------------------------------------------------------------
// It evolves the marking in a accumulated output behaviour
// Non other batch in contact with the output batch
// old fonctionSA
// TODO verify structural conflit
//------------------------------------------------------------------------------
void BatchPlace::EvolveInAccumulatedOutputBehaviour(Simtime *st)
{
  int i;
  double flowofoutputtransition = GetOutputTransitonsFlow();
  Transition *t;
  Flow *f;
  list<Batch>::iterator b;

  if (!marks->empty()){
    for (i=0, b = marks->begin(); b!=marks->end(); b++, i++){
      if ((HasOutputBatch()) && (i == 0)){ // the accumulated output batch
        b->EvolveInAccumulatedOuputBehaviour(st, this, flowofoutputtransition);
/*        for (int j=0; j<numberofarcs; j++){
          if (outputarcs[j] != 0.0){
            t = outputnodes[j];
            switch (t->IsA()){
              case Batch_tr : f = t->GetFlow();
                              flowofoutputtransition = f->GetCurrentFlow();
                              b->EvolveInAccumulatedOuputBehaviour(st, this, flowofoutputtransition);
                  break;
            }
          }
        }
*/

      }
      else{
        b->EvolveInFreeBehaviour(st, this); // other free batches
      }
    }
  }

}

//------------------------------------------------------------------------------
// It evolves the marking when it has an output batch and a second batch in contact not completely accumulated
// old fonctionRAP
//------------------------------------------------------------------------------
void BatchPlace::EvolveInPartiallyAccumulatedBehaviour(Simtime *st)
{
  int i;
  double flowofoutputtransition = GetOutputTransitonsFlow();
  double lengthofoutputbatch;
  Batch *newbatch, *newbatch2;
  Batch *outputbatch;
  list<Batch> *newmarks = new list<Batch>;
  list<Batch>::iterator b;

  newmarks->clear();

  if (!marks->empty()){
    for (i=0, b = marks->begin(); b!=marks->end(); b++, i++){
      if ((HasOutputBatch()) && (i == 0)){ // output batch
        b->EvolveInAccumulatedOuputBehaviour(st, this, flowofoutputtransition);
        newbatch = new Batch;
        newbatch->Copy(*b);
        newmarks->push_back(*newbatch);
      }
      else{
        if (i == 1){ // first batch after output batch
          outputbatch = GetOutputBatch();
          lengthofoutputbatch = outputbatch->GetLength();
          newbatch = new Batch;
          newbatch->Copy(*b);
          newbatch->EvolveInPartiallyAccumulatedBehaviourAccBatch(st, this, lengthofoutputbatch, flowofoutputtransition);

          newbatch2 = new Batch;
          newbatch2->Copy(*b);
          newbatch2->EvolveInPartiallyAccumulatedBehaviourFreeBatch(st, this, lengthofoutputbatch, newbatch->GetLength(), flowofoutputtransition);

          newmarks->push_back(*newbatch);
          newmarks->push_back(*newbatch2);
        }
        else{
          newbatch = new Batch;
          newbatch->Copy(*b);
          newbatch->EvolveInFreeBehaviour(st, this);
          newmarks->push_back(*newbatch);
        }
      }
    }
    marks->clear();
    marks = newmarks;
  }
}


//------------------------------------------------------------------------------
// It evolves the marking when the batch passe from free to accumulated
// old fonctionRLA
//------------------------------------------------------------------------------
void BatchPlace::EvolveInFreeToAccumulatedBehaviour(Simtime *st)
{
  double outputtransitionflow = GetOutputTransitonsFlow();;
  int i;
  double lengthofoutputbatch;
  Batch *newbatch, *newbatch2;
  Batch outputbatch;
  list<Batch> *newmarks = new list<Batch>;
  list<Batch>::iterator b;
  Transition *t;
  Flow *f;

  if (!marks->empty()){
    for (i=0, b = marks->begin(); b!=marks->end(); b++, i++){
      if ((HasOutputBatch()) && (i == 0)){ // output batch
        newbatch = new Batch;
        newbatch->Copy(*b);
        newbatch->EvolveInFreeToAccumulatedBehaviourAccBatch(st, this, outputtransitionflow);

        newbatch2 = new Batch;
        newbatch2->Copy(*b);
        newbatch2->EvolveInFreeToAccumulatedBehaviourFreeBatch(st, this, newbatch->GetLength(), outputtransitionflow);

        newmarks->push_back(*newbatch);
        newmarks->push_back(*newbatch2);
/*        for (int j=0; j<numberofarcs; j++){
          if (outputarcs[j] != 0.0){
            t = outputnodes[j];
            switch (t->IsA()){
              case Batch_tr : f = t->GetFlow();
                              _flow = f->GetCurrentFlow();
                              newbatch = new Batch;
                              newbatch->Copy(*b);
                              newbatch->EvolveInFreeToAccumulatedBehaviourAccBatch(st, this, _flow);

                              newbatch2 = new Batch;
                              newbatch2->Copy(*b);
                              newbatch2->EvolveInFreeToAccumulatedBehaviourFreeBatch(st, this, newbatch->GetLength(), _flow);

                              newmarks->push_back(*newbatch);
                              newmarks->push_back(*newbatch2);
                  break;
            }
          }
        }
*/
      }
      else{
        newbatch = new Batch;
        newbatch->Copy(*b);
        newbatch->EvolveInFreeBehaviour(st, this);
        newmarks->push_back(*newbatch);
      }
    }
    marks->clear();
    marks = newmarks;
  }
}

//-----------------------------------Near-optimal case---------------------------------------------
// It returns ture if the batch place satisies the conditions.
//    condition: 1) It has an output batch whose density greater than d_i^s
//               2) The last several batches having marking quantity greater than free steady marking quantity -mininus the quantity of
//                  first steady batch
//               3) Before output batch is destroyed, the next batch becomes output batch, we assume that it keeps a steady output flow value.
//--------------------------------------------------------------------------------------------------
int BatchPlace::IsHoldSteadyPlaceFlowUntilSteadyState()
{
  double frontquantity;
  double remainingfreesteadyquantity;
  double currentfreesteadyquantity;
  int placestate = 0;
  Transition *t;
  list<Batch>::iterator bl;
  Batch *outputbatch;
  Batch bf;

  if ((marks->size() == 0) || (HasOutputBatch() == 0) || (IsSteadyMarkingQuantityReachedandInputOutputTransitionBelongTz() == 1)){
    return 0;
  }
  bl = marks->begin();

  if (bl->GetDensity() != GetSteadyDensity()){
    currentfreesteadyquantity = 0;
  }
  else{
    currentfreesteadyquantity = bl->GetDensity() * bl->GetLength();
  }


  outputbatch = GetOutputBatch();
  bf = marks->front();

  if (GetSteadyDensity() >= outputbatch->GetDensity() ){
      frontquantity = outputbatch->GetDensity() * outputbatch->GetLength();
      if (frontquantity >= (GetFreeSteadyQuantity()-currentfreesteadyquantity)){
  //    bf++;
      }
  }
}

//    if (((length - outputbatch->GetLength()) - b->GetPosition()) > PRF::prf.Min_Err()){
//      if ((instantaneousspeed * density - outputtransitionflow) != 0.0){
//        date = (density * (length - outputbatch->GetLength() - b->GetPosition())/(instantaneousspeed * density - outputtransitionflow));
//        date += simulation->stime->GetCurrentDate();
//      }
//
////
////
////
////}
//
//
////int BatchPlace::IsSteadyMarkingQuantityReachedandInputOutputTransitionBelongTz()
////{
////    Transition *t;
////    double weight;
////    int placestate =0;
////    int counter = 0;
////    int countertz=0;
////
////    ComputeQuantities();
////
////    if (abs(steadyquantity -  quantity) < PRF::prf.Min_Err()){
////        if (GetNumberOfOutputArcs() == 0){
////            return 1; // the conservative net and consistent net
////        }
////        else{
////            for (int i=0;  i<numberofarcs; i++){
////                if (outputarcs[i] != 0.0){
//////        t = simulation->sbpn->GetTransition(i);
////                    t=outputnodes[i];
////                        counter++;
////                }
////                if (outputarcs[i] != 0.0 && t->IsFiringQuantityOnState()==0)
////                        countertz++;
////
////            }
////            }
////
////        if (GetNumberOfInputArcs() == 0){
////            return 1; // the conservative net and consistent net
////        }
////        else{
////            for (int i=0;  i<numberofarcs; i++){
////                if (inputarcs[i] != 0.0){
//////        t = simulation->sbpn->GetTransition(i);
////                        t=inputnodes[i];
////                        counter++;
////                }
////                if (inputarcs[i] != 0.0 && t->IsFiringQuantityOnState()==0)
////                        countertz++;
////
////            }
////            }
////        if (counter == countertz)
////                return 1;
////    return placestate;
////    }
////    return placestate;
////}
//   steadyquantity = 0.0;
//  double  freesteadyquantity = 0.0;
//  double  accumulatedsteadyquantity = 0.0;
//
//
//  if (steadymarks->size() != 0){
//    for (list<Batch>::iterator b=steadymarks->begin(); b!=steadymarks->end(); b++) {
//      steadyquantity += b->GetDensity() * b->GetLength();
//    }
//	}
//
//    if (steadymarks->size() ==2){
//    for (list<Batch>::iterator b=steadymarks->begin(); b!=steadymarks->begin(); b++) {
//      freesteadyquantity += b->GetDensity() * b->GetLength();
//      steadydensity=b->GetInstan
//      accumulatedsteadyquantity = steadyquantity-freesteadyquantity;
//    }
//	}
//	if (steadymarks->size() ==1){
//        for (list<Batch>::iterator b=steadymarks->begin(); b!=steadymarks->begin(); b++){
//        if ( p->GetSteadyPlaceFlow() == b->GetDensity() * p->GetSpeed() ){
//             freesteadyquantity = b->GetDensity() * b->GetLength();
//             accumulatedsteadyquantity = 0.0;
//             }
//        else{
//             freesteadyquantity = 0.0;
//             accumulatedsteadyquantity = b->GetDensity() * b->GetLength();
//            }
//        }
//    }



