//====================================================================================//
//                                                                                    //
//             Continuous transition classes inherit of transition class              //
//                                                                                    //
//====================================================================================//
//  This File:   continuoustransition.cpp         Language: C++  (xlC and CC)         //
//  Software:    SIMULEAU                                                             //
//====================================================================================//
//  Creation:    22/apr/16                        by: Leonardo.Brenner@lsis.org       //
//  Last Change: 22/apr/16                        by: Leonardo.Brenner@lsis.org       //
//====================================================================================//
#include "simuleau.h"



//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
// Methods of the Continuous Transition class
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Constructor
//------------------------------------------------------------------------------
ContinuousTransition::ContinuousTransition()
{

}

//------------------------------------------------------------------------------
// Named Constructor
//------------------------------------------------------------------------------
ContinuousTransition::ContinuousTransition(simuleau_name _name, double _flow, double _steadyflow):Transition(_name)
{
  flow = new Flow;
  flow->SetAllFlows(_flow);
  flow->SetSteadyFlow(_steadyflow);

}

//------------------------------------------------------------------------------
// Destructor
//------------------------------------------------------------------------------
ContinuousTransition::~ContinuousTransition()
{
 if (flow)
  delete flow;
}

//------------------------------------------------------------------------------
// Copy
//------------------------------------------------------------------------------
void ContinuousTransition::Copy(const ContinuousTransition *_transition)
{
  TransferNode(_transition);
  flow = new Flow;
  flow->Copy(_transition->flow);                 // transition flows
  firingquantity = new FiringQuantity;
  firingquantity->Copy(_transition->firingquantity);
}


//------------------------------------------------------------------------------
// Returns flows
//------------------------------------------------------------------------------
Flow* ContinuousTransition::GetFlow()
{
  return (flow);
}

//------------------------------------------------------------------------------
// set the initial flow
//------------------------------------------------------------------------------
void ContinuousTransition::SetInitialFlow(double _flow)
{
  flow->SetInitialFlow(_flow);
}

//------------------------------------------------------------------------------
// set the initial flow
//------------------------------------------------------------------------------
void ContinuousTransition::SetTheoreticalFlow(double _flow)
{
  flow->SetTheoreticalFlow(_flow);
}

//------------------------------------------------------------------------------
// It returns true if the current flow is equal to the steady flow
//------------------------------------------------------------------------------
int ContinuousTransition::IsSteadyFlowReached()
{
  if (abs(flow->GetCurrentFlow() - flow->GetSteadyFlow())  < PRF::prf.Min_Err())
    return 1;
  return 0;
}

//------------------------------------------------------------------------------
// Compute minimum positive flow
//------------------------------------------------------------------------------
double ContinuousTransition::ComputeMinimumFlow(double f1, double f2)
{
	double minimum;

	if ((f1 >= 0.0) && (f2 >= 0.0)){
		  minimum = min(f1,f2);
  }
	else {
		if ((f1 < 0.0) && (f2 < 0.0)){
      minimum = f2;
    }
		if (f1 < 0.0){
      minimum = f2;
    }
		if (f2 < 0.0) {
      minimum = f1;
    }
	}
  return (minimum);
}

//------------------------------------------------------------------------------
// It computes the theoretical flow
//------------------------------------------------------------------------------
void ContinuousTransition::ComputeTheoreticalFlow()
{
  double flowvalue;


  if (state == 0.0){ // not enable transition
    flowvalue = 0.0; // null flow
  }
  else{
    if (state == 1.0){ // transition is strongly validated
      flowvalue = flow->GetMaximumFlow();
    }
    else{
      flowvalue = ComputeMinimumFlow(flow->GetMaximumFlow(), ComputeTheoreticalFlowOfPlace());
    }
    flow->SetTheoreticalFlow(flowvalue);
  }
}

//------------------------------------------------------------------------------
// Fire transition
//------------------------------------------------------------------------------
void ContinuousTransition::WalkThrough(double duration)
{
  if (duration > 0.0)
    FireContinuousTransition(duration);
}

//------------------------------------------------------------------------------
// Fire continuous transition
//------------------------------------------------------------------------------
void ContinuousTransition::FireContinuousTransition(double duration)
{
  double quantity;
  double curflow;
  double weight;
  Place *p;

  curflow = flow->GetCurrentFlow();

  // not fireable transition
  if ((curflow == 0.0) || (duration  == 0.0))
    return;

  quantity = curflow * duration;

  // remove marks of the input places
  for (int i=0; i<numberofarcs; i++){
    if (inputarcs[i] != 0){
      weight = inputarcs[i];
//      p = simulation->sbpn->GetPlace(i);
      p = inputnodes[i];

      switch (p->IsA()){
        case Continuous_pl : ((ContinuousPlace*)p)->ReduceMarks(weight * quantity);
//                              cout << "reducing from :"  << i << " val: " << weight * quantity << endl;
             break;
      }
    }
  }

  // add marks of the output places
  for (int i=0; i<numberofarcs; i++){
    if (outputarcs[i] != 0){
      weight = outputarcs[i];
//      p = simulation->sbpn->GetPlace(i);
      p = outputnodes[i];

      switch (p->IsA()){
        case Continuous_pl : ((ContinuousPlace*)p)->AddMarks(weight * quantity);
//                              cout << "adding to :"  << i << " val: " << weight * quantity << endl;
             break;
      }
    }
  }
}

//------------------------------------------------------------------------------
// Print
//------------------------------------------------------------------------------
void ContinuousTransition::Print(ostream &fout)
{
  fout << "Name:              " << name << endl;
  fout << "    Type:          " << "Continuous" << endl;
  fout << "    Flow:          " << endl;
  flow->Print(fout);
  fout << "    Arcs:          " << endl;
  PrintArcs(fout);
}

//------------------------------------------------------------------------------
// It writes the transition flows
//------------------------------------------------------------------------------
void ContinuousTransition::Write(ostream &fout)
{
  fout << "Name:              " << name << endl;
  fout << "    Type:          " << "Continuous" << endl;
  fout << "    State:         " << state << endl;
  fout << "    Current Flow:  " << flow->GetCurrentFlow() << endl;
  if (flow->GetSteadyFlow() != -1.0)
  fout << "    Steady Flow:   " << flow->GetSteadyFlow() << endl;

}


//------------------------------------------------------------------------------
// It computes the theoretical flow taking into account the input places
// TODO verify flow when non place is fed
//------------------------------------------------------------------------------
double ContinuousTransition::ComputeTheoreticalFlowOfPlace()
{
  double placeflow, f;
  double placestate;

  Place *p;

  if (GetNumberOfInputArcs() == 0){
    return (flow->GetMaximumFlow());
  }

  placeflow = flow->GetMaximumFlow();

  for (int i=0; i<numberofarcs; i++){
    if (inputarcs[i] != 0){
//      p = simulation->sbpn->GetPlace(i);
      p = inputnodes[i];

      switch (p->IsA()){
        case Continuous_pl :  placestate = p->GetState();
                              if (placestate == 0.5){ // place is fed
                                f = ((ContinuousPlace*)p)->ComputeFlowFedPlace(this);
                                placeflow = ComputeMinimumFlow(placeflow, f);
                              }
             break;
      }
    }
  }
  return (placeflow);
}

//========================RBF========Event=================================
// It returns the rbf event for each continuous transition:
//===========================================================================
double ContinuousTransition::GetRBFEvent()
{
  double date;
  double min_date = -1.0;
  double weight;
  double transitionflow;
  Place *p;
//  Transition *t;
//  FiringQuantity *q;
  double Zs;
  double z;

 // q=t->GetFiringQuantity();
  Zs=firingquantity->GetSteadyFiringQuantity();
  z =firingquantity->GetCurrentFiringQuantity();

  if (GetNumberOfOutputArcs() == 0){
     return (-1.0);
  }
  else if(z >= Zs){
     return (-1.0);
  }
  else{
    for (int i=0; i<numberofarcs; i++){
      if (outputarcs[i] != 0){
//        p = simulation->sbpn->GetPlace(i);
        p = (Place*)outputnodes[i];

          weight =  outputarcs[i];
 //         f=t->GetFlow();
          transitionflow=flow->GetCurrentFlow();
        switch (p->IsA()){
          case Continuous_pl :
                              date = (Zs-z) / transitionflow; // FreeSteadyMarkingQuantity= 0 in P^C
                              date += simulation->stime->GetCurrentDate();
               break;
          case Batch_pl :
               break;
        }
      }
      if (date < min_date || min_date==-1.0){
                                 min_date= date;
          }
    }
    return (min_date);
    }
  return (-1.0);
}
//-----------------------------------------------------------------------------------
