//====================================================================================//
//                                                                                    //
//                            FiringQuantity class                                    //
//                                                                                    //
//====================================================================================//
//  This File:   flow.cpp                         Language: C++  (xlC and CC)         //
//  Software:    SIMULEAU                                                             //
//====================================================================================//
//  Creation:    29/apr/16                        by: Leonardo.Brenner@lsis.org       //
//  Last Change: 29/apr/16                        by: Leonardo.Brenner@lsis.org       //
//====================================================================================//
#include "simuleau.h"

//------------------------------------------------------------------------------
// Constructor
//------------------------------------------------------------------------------
FiringQuantity::FiringQuantity()
{
  firingquantity=steadyfiringquantity=currentfiringquantity = 0.0;
}

//------------------------------------------------------------------------------
// Destructor
//------------------------------------------------------------------------------
FiringQuantity::~FiringQuantity()
{
  //dtor
}

//------------------------------------------------------------------------------
// Copy
//------------------------------------------------------------------------------
void FiringQuantity::Copy(const FiringQuantity *_firingquantity)
{
  steadyfiringquantity     = _firingquantity->steadyfiringquantity;
  currentfiringquantity     = _firingquantity->currentfiringquantity;
}

//------------------------------------------------------------------------------
// Set up all firingquanties
//------------------------------------------------------------------------------
void FiringQuantity::SetAllFiringQuantities(double _firingquantity)
{
  steadyfiringquantity=currentfiringquantity = _firingquantity;
}

//------------------------------------------------------------------------------
// Set up the intial flow
//------------------------------------------------------------------------------
//void Flow::SetInitialFlow(double _flow)
//{
//  initialflow = _flow;
//}

//------------------------------------------------------------------------------
// Set up the current firingquantity
//------------------------------------------------------------------------------
void FiringQuantity::SetCurrentFiringQuantity(double _firingquantity)
{
  currentfiringquantity= _firingquantity;
}


//------------------------------------------------------------------------------
// Set up the steady firingquantity
//------------------------------------------------------------------------------
void FiringQuantity::SetSteadyFiringQuantity(double _firingquantity)
{
  steadyfiringquantity= _firingquantity;
}

//------------------------------------------------------------------------------
// Return the current firing quantity
//------------------------------------------------------------------------------
double FiringQuantity::GetCurrentFiringQuantity()
{
  return (currentfiringquantity);
}

//------------------------------------------------------------------------------
// Return the steady firing quantity
//------------------------------------------------------------------------------
double FiringQuantity::GetSteadyFiringQuantity()
{
  return (steadyfiringquantity);
}

//------------------------------------------------------------------------------
// Print
//------------------------------------------------------------------------------
void FiringQuantity::Print(ostream &fout)
{
  fout << "      Current     : " << currentfiringquantity << endl;
 // fout << "      Initial     : " << initialflow << endl;
  if (steadyfiringquantity!=-1.0)
    fout << "      Steady      : " << steadyfiringquantity << endl << endl;

}
