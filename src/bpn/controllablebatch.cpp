//====================================================================================//
//                                                                                    //
//                                Controllable Batch class                            //
//                                                                                    //
//====================================================================================//
//  This File:   controllablebatch.cpp            Language: C++  (xlC and CC)         //
//  Software:    SIMULEAU                                                             //
//====================================================================================//
//  Creation:    28/apr/16                        by: Leonardo.Brenner@lsis.org       //
//  Last Change: 28/apr/16                        by: Leonardo.Brenner@lsis.org       //
//====================================================================================//
#include "simuleau.h"

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
// Methods of the Triangular Batch class
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Constructor
//------------------------------------------------------------------------------
ControllableBatch::ControllableBatch():Batch()
{
  speed = 0.0;
  state = Free_st;
  behaviour = Free_behaviour;
  evolvefunction = NULL;
}

//------------------------------------------------------------------------------
// Constructor
//------------------------------------------------------------------------------
ControllableBatch::ControllableBatch(char *_name):Batch(_name)
{
  speed = 0.0;
  state = Free_st;
  behaviour = Free_behaviour;
  evolvefunction = NULL;
}

//------------------------------------------------------------------------------
// Initialized constructor
//------------------------------------------------------------------------------
ControllableBatch::ControllableBatch(double _length, double _density, double _position, double _speed):Batch(_length, _density, _position)
{
  speed = _speed;
  state = Free_st;
  behaviour = Free_behaviour;
  evolvefunction = NULL;
}

//------------------------------------------------------------------------------
// Initialized constructor
//------------------------------------------------------------------------------
ControllableBatch::ControllableBatch(double _inputflow, double _speed):Batch(_inputflow, _speed)
{
  speed = _speed;
  state = Free_st;
  behaviour = Free_behaviour;
  evolvefunction = NULL;
}

//------------------------------------------------------------------------------
// Destructor
//------------------------------------------------------------------------------
ControllableBatch::~ControllableBatch()
{

}

//------------------------------------------------------------------------------
// Copy
//------------------------------------------------------------------------------
void ControllableBatch::Copy(const ControllableBatch &_controllablebatch)
{
  strcpy(name, _controllablebatch.name);
  length     = _controllablebatch.length;
  density    = _controllablebatch.density;
  position   = _controllablebatch.position;
  outputflow = _controllablebatch.outputflow;

  speed     = _controllablebatch.speed;
  state     = _controllablebatch.state;
  behaviour = _controllablebatch.behaviour;
  evolvefunction = _controllablebatch.evolvefunction;
}

//------------------------------------------------------------------------------
// IsEqual
//------------------------------------------------------------------------------
int ControllableBatch::IsEqual(const ControllableBatch &_controllablebatch)
{
  if (length != _controllablebatch.length)
    return 0;
  if (density != _controllablebatch.density)
    return 0;
  if (position != _controllablebatch.position)
    return 0;
  if (speed != _controllablebatch.speed)
    return 0;
  return 1;
}

//------------------------------------------------------------------------------
// It sets a new speed
//------------------------------------------------------------------------------
void ControllableBatch::SetSpeed(double _speed)
{
  speed = _speed;
}

//------------------------------------------------------------------------------
// It sets a new state
//------------------------------------------------------------------------------
void ControllableBatch::SetState(ctrl_batch_state _state)
{
  state = _state;
}

//------------------------------------------------------------------------------
// It sets a new behaviour
//------------------------------------------------------------------------------
void ControllableBatch::SetBehaviour(ctrl_batch_behaviour _behaviour)
{
  behaviour = _behaviour;
}

//------------------------------------------------------------------------------
// It returns the speed
//------------------------------------------------------------------------------
double ControllableBatch::GetSpeed()
{
  return (speed);
}

//------------------------------------------------------------------------------
// It returns the batch flow
//------------------------------------------------------------------------------
double ControllableBatch::GetFlow()
{
  return (speed * density);
}

//------------------------------------------------------------------------------
// It returns the state
//------------------------------------------------------------------------------
ctrl_batch_state ControllableBatch::GetState()
{
  return (state);
}

//------------------------------------------------------------------------------
// It returns the state
//------------------------------------------------------------------------------
ctrl_batch_state ControllableBatch::GetState(TriangularBatchPlace *tbp)
{
  double ratio = tbp->GetMaxFlow() / tbp->GetInstantaneousSpeed();

  if ((density - ratio) < PRF::prf.Min_Err())
    state = Free_st;
  else
    state = Congested_st;

  return (state);
}

//------------------------------------------------------------------------------
// It returns the state name
//------------------------------------------------------------------------------
char* ControllableBatch::GetStateName()
{
  switch (state) {
    case No_st : return ("None");
    case Free_st : return ("Free");
    case Congested_st : return ("Congested");
    default : return ("None");
  }

}

//------------------------------------------------------------------------------
// It returns the behaviour
//------------------------------------------------------------------------------
ctrl_batch_behaviour ControllableBatch::GetBehaviour()
{
  return (behaviour);
}

//------------------------------------------------------------------------------
// It returns the behaviour name
//------------------------------------------------------------------------------
char* ControllableBatch::GetBehaviourName()
{
  switch (behaviour) {
    case No_behaviour : return ("None");
    case Free_behaviour : return ("Free");
    case Congesting_behaviour : return ("Congesting");
    case Uncongesting_behaviour : return ("Uncongesting");
    default : return ("None");
  }
}

//------------------------------------------------------------------------------
// It returns the behaviour
//------------------------------------------------------------------------------
int ControllableBatch::IsOutputControllableBatch(BatchPlace *bp)
{
  if ((bp->GetLength() - position) < PRF::prf.Min_Err())
    return (1);
  else
    return (0);
}

//------------------------------------------------------------------------------
// It returns true if this batch is in contact with another batch by the front
//------------------------------------------------------------------------------
int ControllableBatch::IsInContactWithByFront(ControllableBatch &cb)
{
  if (((cb.GetPosition() - cb.GetLength()) - GetPosition()) < PRF::prf.Min_Err())
    return (1);
  else
    return (0);
}

//------------------------------------------------------------------------------
// It returns true if this batch is in contact with another batch by the rear
//------------------------------------------------------------------------------
int ControllableBatch::IsInContactWithByRear(ControllableBatch &cb)
{
  if (((GetPosition() - GetLength()) - cb.GetPosition()) < PRF::prf.Min_Err())
    return (1);
  else
    return (0);
}

//------------------------------------------------------------------------------
// It sets the function to be applied in the evolution
//------------------------------------------------------------------------------
void ControllableBatch::SetEvolveFunction(EvolveFunction _func)
{
  evolvefunction = _func;
}

//------------------------------------------------------------------------------
// It calls the evolution function of the controllable batch
//------------------------------------------------------------------------------
void ControllableBatch::Evolve(Simtime *_stime, TriangularBatchPlace *tbp, ControllableBatch *pcb, ControllableBatch *ncb)
{
  if(evolvefunction == NULL)
    return;

  (this->*evolvefunction)(_stime, tbp, pcb, ncb);
  evolvefunction = NULL;
}

//------------------------------------------------------------------------------
// It applies the free behaviour at this controllable batch
//------------------------------------------------------------------------------
void ControllableBatch::EvolveInFreeBehaviour(Simtime *_stime, TriangularBatchPlace *tbp, ControllableBatch *pcb, ControllableBatch *ncb)
{
  double move = (speed * _stime->StepTime());

  if ((position == 0.0) && (length == 0.0)){ //new batch, input in free behaviour
    length = move;
    position = move;
  }
  else{
    if ((tbp->GetLength() - position) < PRF::prf.Min_Err()){
      length = length - move;
      position = tbp->GetLength();
    }
    else{
      position = position + move;
    }
  }
}

//------------------------------------------------------------------------------
// It applies the output in congestion behaviour at this controllable batch
//------------------------------------------------------------------------------
void ControllableBatch::EvolveInCongestingOutputBehaviour(Simtime *_stime, TriangularBatchPlace *tbp, ControllableBatch *pcb, ControllableBatch *ncb)
{
  double downflow = tbp->GetOutputTransitonsFlow();
  double congestiondensity = tbp->GetCongestionDensity();

  length = length - ((downflow/density) * _stime->StepTime());
}


//------------------------------------------------------------------------------
// It applies the free to congestion behaviour (congestion batch case) at this controllable batch
//------------------------------------------------------------------------------
void ControllableBatch::EvolveInFreeToCongestingBehaviourCongBatch(Simtime *_stime, TriangularBatchPlace *tbp, ControllableBatch *pcb, ControllableBatch *ncb)
{
  double vr = ncb->GetSpeed();
  double dr = ncb->GetDensity();
  double downflow = tbp->GetOutputTransitonsFlow();
  double drp = density;

  length = (((vr * dr) - downflow) / (drp - dr)) *_stime->StepTime();
}

//------------------------------------------------------------------------------
// It applies the free to congestion behaviour (free batch case) at this controllable batch
//------------------------------------------------------------------------------
void ControllableBatch::EvolveInFreeToCongestingBehaviourFreeBatch(Simtime *_stime, TriangularBatchPlace *tbp, ControllableBatch *pcb, ControllableBatch *ncb)
{
  double vr = speed;
  double dr = density;
  double downflow = tbp->GetOutputTransitonsFlow();
  double drp = pcb->GetDensity();

  position = position + ((downflow - vr * dr) / (drp - dr)) *_stime->StepTime();

  length = length +  ((downflow - vr * drp) / (drp - dr)) *_stime->StepTime();
}

//------------------------------------------------------------------------------
// It applies the free to congestion behaviour to this batch in the middle of the place with
// contact with another batch (congestion batch case)
//------------------------------------------------------------------------------
void ControllableBatch::EvolveInFreeToCongestingBehaviourMiddlePlaceWithContactCongBatch(Simtime *_stime, TriangularBatchPlace *tbp, ControllableBatch *pcb, ControllableBatch *ncb)
{
  double vrp = speed;
  double vr = ncb->GetSpeed();
  double drp = density;
  double dr = ncb->GetDensity();

  position = position + vrp *_stime->StepTime();

  length =  (dr / (dr - drp)) * (vrp - vr) *_stime->StepTime();
}

//------------------------------------------------------------------------------
// It applies the free to congestion behaviour to this batch in the middle of the place with
// contact with another batch (free batch case)
//------------------------------------------------------------------------------
void ControllableBatch::EvolveInFreeToCongestingBehaviourMiddlePlaceWithContactFreeBatch(Simtime *_stime, TriangularBatchPlace *tbp, ControllableBatch *pcb, ControllableBatch *ncb)
{
  double vrp = pcb->GetSpeed();
  double vr = speed;
  double drp = pcb->GetDensity();
  double dr = density;

  position = position + ((vrp*drp - vr*dr) / (drp - dr)) * _stime->StepTime();

//  length =  length  - (drp / (dr - drp)) * (vrp - vr) * _stime->StepTime();
  length = length +  (((vrp*drp) - vr * drp) / (drp - dr)) *_stime->StepTime();
}

//------------------------------------------------------------------------------
// It applies the uncongestion behaviour to an output batch
//------------------------------------------------------------------------------
void ControllableBatch::EvolveInUncongestingOutputBehaviour(Simtime *_stime, TriangularBatchPlace *tbp, ControllableBatch *pcb, ControllableBatch *ncb)
{
  double Vi = tbp->GetInstantaneousSpeed();

  length = length - Vi * _stime->StepTime();
}

//------------------------------------------------------------------------------
// It applies the uncongestion to free behaviour to this batch in the middle of the place (congestion batch case)
//------------------------------------------------------------------------------
void ControllableBatch::EvolveInUncongestingToFreeBehaviourCongBatch(Simtime *_stime, TriangularBatchPlace *tbp, ControllableBatch *pcb, ControllableBatch *ncb)
{
  double Vi = tbp->GetInstantaneousSpeed();
  double d_cri = tbp->GetCriticalDensity();
  double vr = speed;
  double dr = density;

  position = position + ((Vi*d_cri) - (vr*dr)) / (d_cri-dr) * _stime->StepTime();
  length = length - ((Vi - vr) * d_cri / (dr - d_cri)) * _stime->StepTime();
}

//------------------------------------------------------------------------------
// It applies the uncongestion to free behaviour to this batch in the middle of the place (Free batch case)
//------------------------------------------------------------------------------
void ControllableBatch::EvolveInUncongestingToFreeBehaviourFreeBatch(Simtime *_stime, TriangularBatchPlace *tbp, ControllableBatch *pcb, ControllableBatch *ncb)
{
  double Vi = tbp->GetInstantaneousSpeed();
  double d_cri = tbp->GetCriticalDensity();
  double vr = ncb->GetSpeed();
  double dr = ncb->GetDensity();

  position = position + Vi * _stime->StepTime();
  length = ((Vi - vr) * dr / (dr - d_cri)) * _stime->StepTime();
}

//------------------------------------------------------------------------------
// Print
//------------------------------------------------------------------------------
void ControllableBatch::Print(ostream &fout)
{
  fout << "                   (" << length << ", " << density << ", " << position << ", " << speed << ") - State: " << GetStateName() << " - Behaviour: " << GetBehaviourName() << endl;
}

//------------------------------------------------------------------------------
// Write
//------------------------------------------------------------------------------
void ControllableBatch::Write(ostream &fout)
{
  fout << "                   (" << length << ", " << density << ", " << position << ", " << speed << ") - State: " <<  GetStateName() << " - Behaviour: " << GetBehaviourName() << endl;
}
