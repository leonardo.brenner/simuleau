//====================================================================================//
//                                                                                    //
//                         Place classes inherit of node class                        //
//                                                                                    //
//====================================================================================//
//  This File:   place.h                          Language: C++  (xlC and CC)         //
//  Software:    SIMULEAU                                                             //
//====================================================================================//
//  Creation:    22/apr/16                        by: Leonardo.Brenner@lsis.org       //
//  Last Change: 22/apr/16                        by: Leonardo.Brenner@lsis.org       //
//====================================================================================//
#ifndef PLACE_H
#define PLACE_H
#include <simuleau.h>

//====================================================================================//
//                                 Generic place class                                //
//====================================================================================//
//    Place class implements a generic place that will be used to implement all other //
// place classes (discrete, continuous, batch and triangular batch) that can compose  //
// a Batch Petri Net.                                                                 //
//====================================================================================//
class Place : public Node
{
  public:
    Place();
    Place(simuleau_name _name);
    virtual ~Place();

    void Copy (const Place * _place);
    virtual place_type IsA() {};

    // input functions
    virtual void SetInitialMarking(int m);
    virtual void SetSteadyMarking(int m);
    virtual void SetInitialMarking(double m){};
    virtual void SetSteadyMarking(double m){};
    virtual void AddBatchToInitialMarking(Batch &b){};
    virtual void AddBatchToSteadyMarking(Batch &b){};
    virtual void AddBatchToInitialMarking(ControllableBatch &b){};
    virtual void AddBatchToSteadyMarking(ControllableBatch &b){};

    //simulation functions
    virtual void ComputeState(){};
            int  GetMarks();
    virtual void EvolveMarks(double _date){};
    virtual void ResearchStructuralConflict();
    virtual int  GetStructuralConflict();
    virtual int  VerifyConflict();

    // print functions
    virtual void Print(ostream &fout);
    virtual void Write(ostream &fout);

  protected:
    int marks;
  	int initialmarks;
    int steadymarks;
    int structuralconflict;
    int conflict;

  private:
};


//====================================================================================//
//                                 Discrete place class                               //
//====================================================================================//
//    Discrete place class is generally used to model control in BPN.                 //
//    These place have a integer number of tokens.                                    //
//====================================================================================//
class DiscretePlace : public Place
{
  public:
    DiscretePlace();
    DiscretePlace(simuleau_name _name);
    virtual ~DiscretePlace();

    void Copy (const DiscretePlace * _place);
    place_type IsA() {return Discrete_pl;};


    // input functions
    void SetInitialMarking(int m);
    void SetSteadyMarking(int m);

    //simulation functions
    void ChangeMarks(int m);
    void ReserveMarks(int m);
    void RemoveReservedMarks(int m);
    void AddMarks(int m);

    void ComputeState();

    int  VerifyConflict();


    // print functions
    void Print(ostream &fout);
    void Write(ostream &fout);

  protected:
    int reservedmarks;
  	int nonreservedmarks;


  private:
};


//====================================================================================//
//                                Continuous place class                              //
//====================================================================================//
//    These place have a real number of tokens.                                       //
//====================================================================================//
class ContinuousPlace : public Place
{
  public:
    ContinuousPlace();
    ContinuousPlace(simuleau_name _name);
    virtual ~ContinuousPlace();

    void Copy (const ContinuousPlace * _place);
    virtual place_type IsA() {return Continuous_pl;};

    // input functions
    void SetInitialMarking(double m);
    void SetSteadyMarking(double m);

    //output functions
    //double GetOutputTransitonsFlow();

    //simulation function
    double GetMarks();
    double GetInitialMarks();
    double GetSteadyMarks();
    void   ChangeMarks(double m);
    void   ReduceMarks(double m);
    void   ReserveMarks(double m);
    void   RemoveReservedMarks(double m);
    void   AddMarks(double m);
    void   ComputeState();
    void   EvolveMarks(double _date);
    void   VerifyMarks();

    int  VerifyConflict();

    double PlaceIsFed();
    virtual int IsEqualSteadyQuantity();
    virtual int IsGreaterThanSteadyQuantity();
    virtual int IsLessThanSteadyQuantity();
    virtual int IsSteadyMarkingReached();
    virtual int IsSteadyMarkingReachedandInputOutputTransitionBelongTz();
    double ComputeFlowFedPlace(ContinuousTransition *ct);
    double GetCPEEvent();
    double GetDTEEvent();
    double GetPSQEvent();

    // print functions
    void Print(ostream &fout);
    void Write(ostream &fout);


  protected:
    double marks;
  	double initialmarks;
  	double steadymarks;
  	double reservedmarks;
  	double nonreservedmarks;


    double ComputeDateDTEEvent(DiscreteTransition *t, double weight);
    double ComputeInputFlow();

  private:
};

//====================================================================================//
//                                   Batch place class                                //
//====================================================================================//
//    Batch places model a transfer of batch entities over a space/temporal place     //
//    These place have batches.                                                       //
//====================================================================================//
class BatchPlace : public Place
{
  public:
    BatchPlace();
    BatchPlace(simuleau_name _name, double _speed, double _density, double _length);
    virtual ~BatchPlace();

    void Copy (const BatchPlace * _place);
    virtual place_type IsA() {return Batch_pl;};

    // input functions
    void AddBatchToInitialMarking(Batch &b);
    void AddBatchToSteadyMarking(Batch &b);
    void AddBatchToMarking(Batch &b);
    void ChangeMarks(list<Batch> *lb);
    void SetSpeed(double _speed);
    void SetMaxSpeed(double _maxspeed);
    void SetDensity(double _density);
    void SetLength(double _length);

    // output functions
    double GetSpeed();
    double GetMaxSpeed();
    double GetInstantaneousSpeed();
    double GetDensity();
    double GetLength();
    double GetOutputFlow();
    double GetInitialQuantity();
    double GetFreeSteadyQuantity(); // q_i^{f,s}
    double GetSteadyQuantity();
    list<Batch>* GetInitialMarking();
    list<Batch>* GetSteadyMarking();
    list<Batch>* GetMarking();
    int    GetBehaviour();
    double GetFlowOfOutputTransition();
    double GetInputTransitonsFlow();
    double GetOutputTransitonsFlow();
    double GetSteadyPlaceFlow();   // \phi_i^s  The steady flow in a batch place.
    double GetSteadyDensity(); // d_i^s the steady density of a batch place.


    virtual int HasOutputBatch();
    Batch * GetOutputBatch();
    virtual double GetDensityOutputBatch();

    virtual int IsFull();
    virtual int IsEqualSteadyQuantity();
    virtual int IsGreaterThanSteadyQuantity();
    virtual int IsLessThanSteadyQuantity();
    virtual int IsSteadyMarkingReached();
    virtual int IsSteadyMarkingQuantityReachedandInputOutputTransitionBelongTz();
    virtual int IsHoldSteadyPlaceFlowUntilSteadyState();


    //simulation functions
    virtual void ComputeState();

    virtual int CreateBatches();
    virtual void MergeBatches();
    virtual void DestructBatches();
    virtual void ComputeQuantities();
//    virtual void ComputeTwoTypesSteadyQuantities(); // the steady marking quantity in a free part and accumulated part.

    virtual void ComputeBehaviour();
    virtual void EvolveMarks(double _date);

    virtual double GetBOBEvent();
    virtual double GetDOBEvent();
    virtual double GetBBDEvent();
    virtual double GetTBMEvent();
    virtual double GetPSQEvent();


    // print functions
    virtual void Print(ostream &fout);
    virtual void Write(ostream &fout);

  protected:
    int behaviour;
    int formalisation;
    list<Batch> *marks;
    list<Batch> *initialmarks;
    list<Batch> *steadymarks;
    double density;
    double maxspeed;
    double speed;
    double instantaneousspeed;
    double length;
    double quantity;
    double steadyquantity;

    void EvolveInFreeBehaviour(Simtime *st); // RL
    void EvolveInAccumulatedOutputBehaviour(Simtime *st); // SA
    void EvolveInPartiallyAccumulatedBehaviour(Simtime *st); //RAP
    void EvolveInFreeToAccumulatedBehaviour(Simtime *st);// RLA

  private:
};

//====================================================================================//
//                              Triangular Batch place class                          //
//====================================================================================//
//    Triangular batch places are a special kind of batch places dedicated to model   //
// trafic route systems. The batches in batch place must follow a triangular fundamen-//
// tal diagram issue of the trafic route model.                                       //
//    These place have triangular batches.                                            //
//====================================================================================//
class TriangularBatchPlace : public BatchPlace
{
  public:
    TriangularBatchPlace();
    TriangularBatchPlace(simuleau_name _name, double _speed, double _density, double _length, double _flow);
    virtual ~TriangularBatchPlace();

    void Copy (const TriangularBatchPlace * _place);
    place_type IsA() {return Triangular_pl;};


    // input functions
    void AddBatchToInitialMarking(ControllableBatch &b);
    void AddBatchToSteadyMarking(ControllableBatch &b);
    void AddBatchToMarking(ControllableBatch &b);
    void SetFlow(double _flow);
    void SetMaxFlow(double _flow);

    // output functions
    double GetFlow();
    double GetMaxFlow();

    int IsFull();
    int HasOutputBatch();
    ControllableBatch* GetOutputControllableBatch();

    double GetCriticalDensity();
    double GetDensityOutputBatch();
    double GetSpeedOutputBatch();
    double GetFlowOutputBatch();
    double GetCongestionDensity();
    double GetPropagationSpeed();


    //simulation functions
    void ComputeState();

    int CreateBatches();
    void MergeBatches();
    void DestructBatches();

    void ComputeBehaviour();
    void SetAndRecomputeSpeed(double newspeed);
    void SetBehaviourFunctions(); //precomputebehaviour
    void SetFunctionFreeBatch(list<ControllableBatch>::iterator cbi, int i);
    void SetFunctionCongestedBatch(list<ControllableBatch>::iterator cbi, int i);
    void SetFunctionUncongestingBatch(list<ControllableBatch>::iterator cbi, int i);
    void EvolveMarks(double _date);
    void EvolveControllableBatch(list<ControllableBatch>::iterator cbi, double _date);

    double GetBOBEvent();
    double GetDOBEvent();
    double GetBBDEvent();
    double GetTBMEvent();
    double GetBMOBEvent();
    double GetOBDEvent();
    double GetBDEvent();
    double GetBBFEvent();



    // print functions
    void Print(ostream &fout);
    void Write(ostream &fout);

  protected:
    list<ControllableBatch> *marks;
    list<ControllableBatch> *initialmarks;
    list<ControllableBatch> *steadymarks;
    double maxflow;
    double flow;

  private:
};


#endif // PLACE_H
