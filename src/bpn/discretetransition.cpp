//====================================================================================//
//                                                                                    //
//               Discrete transition classes inherit of transition class              //
//                                                                                    //
//====================================================================================//
//  This File:   discretetransition.cpp           Language: C++  (xlC and CC)         //
//  Software:    SIMULEAU                                                             //
//====================================================================================//
//  Creation:    22/apr/16                        by: Leonardo.Brenner@lsis.org       //
//  Last Change: 22/apr/16                        by: Leonardo.Brenner@lsis.org       //
//====================================================================================//
#include "simuleau.h"

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
// Methods of the Discrete Transition class
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Constructor
//------------------------------------------------------------------------------
DiscreteTransition::DiscreteTransition()
{

}

//------------------------------------------------------------------------------
// Initialized Constructor
//------------------------------------------------------------------------------
DiscreteTransition::DiscreteTransition(simuleau_name _name, double _time, double _steadytime):Transition(_name)
{
  time = _time;
  steadytime = _steadytime;
  enableddate = -_time;

}

//------------------------------------------------------------------------------
// Destructor
//------------------------------------------------------------------------------
DiscreteTransition::~DiscreteTransition()
{

}

//------------------------------------------------------------------------------
// Copy
//------------------------------------------------------------------------------
void DiscreteTransition::Copy(const DiscreteTransition *_transition)
{
  TransferNode(_transition);
  time = _transition->time;                 // transition time
  steadytime = _transition->steadytime;     // transition steady time
  enableddate = _transition->enableddate;   // enabled date
  firingquantity = new FiringQuantity;
  firingquantity->Copy(_transition->firingquantity);
}

//------------------------------------------------------------------------------
// Initializes  enabled date
//------------------------------------------------------------------------------
void DiscreteTransition::InitEnabledDate()
{
  enableddate = -1.0;
}


//------------------------------------------------------------------------------
// Set enabled date
//------------------------------------------------------------------------------
void DiscreteTransition::SetEnabledDate(double _date)
{
  enableddate = _date;
}

//------------------------------------------------------------------------------
// Reserve marks in input places of the transition
//------------------------------------------------------------------------------
void DiscreteTransition::ReserveMarksInputPlaces()
{
  double weight;
  Place *p;

  for (int i=0; i<numberofarcs; i++){
    if (inputarcs[i] != 0){
        weight = inputarcs[i];
//        p = simulation->sbpn->GetPlace(i);
        p = inputnodes[i];

        switch (p->IsA()){
          case Discrete_pl : ((DiscretePlace*)p)->ReserveMarks((int)weight);
//                              cout << "\n\n\n discrete \n\n\n";
               break;
          case Continuous_pl : ((ContinuousPlace*)p)->ReserveMarks(weight);
//                              cout << "\n\n\n continuous \n\n\n";

               break;
        }
      }
  }
}


//------------------------------------------------------------------------------
// Get time
//------------------------------------------------------------------------------
double DiscreteTransition::GetTime()
{
  return (time);
}

//------------------------------------------------------------------------------
// Get enabled date
//------------------------------------------------------------------------------
double DiscreteTransition::GetEnabledDate()
{
  return (enableddate);
}


//------------------------------------------------------------------------------
// Compute states
//------------------------------------------------------------------------------
void DiscreteTransition::ComputeState()
{
  double transstate = 1.0; // enabled
  double placestate;
  Place *p;

  if (GetNumberOfInputArcs() == 0){
    if ((enableddate + time) > simulation->stime->GetCurrentDate()){
      state = 0.5; // enable transition but not fired
    }
    else {
      state = 1.0;  // transition not enable
    }
  }
  else{
    for (int i=0; i<numberofarcs; i++){
      if (inputarcs[i] != 0){
//        p = simulation->sbpn->GetPlace(i);
        p = inputnodes[i];
        placestate = p->GetState();
//          cout << "\n==========================\nplace: " << placestate << "\n==========================\n";
        transstate = min(transstate, placestate);
      }
    }
    state = transstate;
  }
//  cout << "\n==========================\nState: " << state << "\n==========================\n";
}

//------------------------------------------------------------------------------
// Fire transition
//------------------------------------------------------------------------------
void DiscreteTransition::WalkThrough(double duration)
{
  if (duration < 0.0)
    FireDiscreteTransition();
}

//------------------------------------------------------------------------------
// Fire discrete transition
//------------------------------------------------------------------------------
void DiscreteTransition::FireDiscreteTransition()
{
  double weight;
  Place *p;

  // remove marks of the input places
  for (int i=0; i<numberofarcs; i++){
    if (inputarcs[i] != 0){
      weight = inputarcs[i];
//      p = simulation->sbpn->GetPlace(i);
      p = inputnodes[i];

      switch (p->IsA()){
        case Discrete_pl : ((DiscretePlace*)p)->RemoveReservedMarks((int)weight);
//                              cout << "\n\n\n discrete \n\n\n";
             break;
        case Continuous_pl : ((ContinuousPlace*)p)->RemoveReservedMarks(weight);
//                              cout << "\n\n\n continuous \n\n\n";

             break;
      }
    }
  }

  // add marks of the output places
  for (int i=0; i<numberofarcs; i++){
    if (outputarcs[i] != 0){
      weight = outputarcs[i];
//      p = simulation->sbpn->GetPlace(i);
      p = outputnodes[i];

      switch (p->IsA()){
        case Discrete_pl : ((DiscretePlace*)p)->AddMarks((int)weight);
//                              cout << "\n\n\n discrete \n\n\n";
             break;
        case Continuous_pl : ((ContinuousPlace*)p)->AddMarks(weight);
//                              cout << "\n\n\n continuous \n\n\n";

             break;
      }
    }
  }
  InitEnabledDate();
}

//------------------------------------------------------------------------------
// Print
//------------------------------------------------------------------------------
void DiscreteTransition::Print(ostream &fout)
{
  fout << "Name:              " << name << endl;
  fout << "    Type:          " << "Discrete" << endl;
  fout << "    Time:          " << time << endl;
  if (steadytime!=0.0)
    fout << "    Steady Time:   " << time << endl;

  fout << "    Arcs:          " << endl;
  PrintArcs(fout);
}

