//====================================================================================//
//                                                                                    //
//                                       Arc class                                    //
//                                                                                    //
//====================================================================================//
//  This File:   arc.h                            Language: C++  (xlC and CC)         //
//  Software:    SIMULEAU                                                             //
//====================================================================================//
//  Creation:    22/apr/16                        by: Leonardo.Brenner@lsis.org       //
//  Last Change: 22/apr/16                        by: Leonardo.Brenner@lsis.org       //
//====================================================================================//
#ifndef ARC_H
#define ARC_H

class Arc
{
  public:
    Arc();
    virtual ~Arc();

  protected:
    double weight;      // weight associated to this arc
    Node *input;        // pointer to the input node
    Node *output;       // pointer to the input node

  private:
};

#endif // ARC_H
