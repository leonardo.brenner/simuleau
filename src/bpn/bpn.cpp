
//====================================================================================//
//                                                                                    //
//                             Batch Petri Nets main class                            //
//                                                                                    //
//====================================================================================//
//  This File:   bpn.cpp                          Language: C++  (xlC and CC)         //
//  Software:    SIMULEAU                                                             //
//====================================================================================//
//  Creation:    21/apr/16                        by: Leonardo.Brenner@lsis.org       //
//  Last Change: 21/apr/16                        by: Leonardo.Brenner@lsis.org       //
//====================================================================================//
#include "simuleau.h"

//------------------------------------------------------------------------------
// Empty contructor
//------------------------------------------------------------------------------
BPN::BPN()
{
  strcpy(bpnname,"");

  numberofdiscreteplaces = 0;
  numberofcontinuousplaces = 0;
  numberofbatchplaces = 0;
  numberoftriangularplaces = 0;

  numberofdiscretetransitions = 0;
  numberofcontinuoustransitions = 0;
  numberofbatchtransitions = 0;

  places = NULL;
  transitions = NULL;
}

//------------------------------------------------------------------------------
// Sized contructor
//------------------------------------------------------------------------------
BPN::BPN(const int _numberofplaces, const int _numberoftransitions)
{
  strcpy(bpnname,"");

  numberofdiscreteplaces = 0;
  numberofcontinuousplaces = 0;
  numberofbatchplaces = 0;
  numberoftriangularplaces = 0;

  numberofdiscretetransitions = 0;
  numberofcontinuoustransitions = 0;
  numberofbatchtransitions = 0;

  places = new  Place* [_numberofplaces];
  for (int i=0; i<_numberofplaces; i++)
    places[i] = NULL;

  transitions = new  Transition* [_numberoftransitions];
  for (int i=0; i<_numberoftransitions; i++)
    transitions[i] = NULL;
}


//------------------------------------------------------------------------------
// Destructor
//------------------------------------------------------------------------------
BPN::~BPN()
{
  if (places)
    delete [] places;
  if (transitions)
    delete [] transitions;
}


//------------------------------------------------------------------------------
// copy
//------------------------------------------------------------------------------
void BPN::Copy(const BPN * _bpn)
{
  strcpy(bpnname, _bpn->bpnname);

  numberofdiscreteplaces = _bpn->numberofdiscreteplaces;
  numberofcontinuousplaces = _bpn->numberofcontinuousplaces;
  numberofbatchplaces = _bpn->numberofbatchplaces;
  numberoftriangularplaces = _bpn->numberoftriangularplaces;

  numberofdiscretetransitions = _bpn->numberofdiscretetransitions;
  numberofcontinuoustransitions = _bpn->numberofcontinuoustransitions;;
  numberofbatchtransitions = _bpn->numberofbatchtransitions;;

  places = new  Place* [NumberOfPlaces()];

  int j = 0;
  int k = numberofdiscreteplaces;
  for (int i=j; i<k; i++){
    DiscretePlace *place = new DiscretePlace;
    place->Copy(_bpn->places[i]);
    places[i] = place;
  }

  j = k;
  k += numberofcontinuousplaces;
  for (int i=j; i<k; i++){
    ContinuousPlace *place = new ContinuousPlace;
    place->Copy(_bpn->places[i]);
    places[i] = place;
  }

  j = k;
  k += numberofbatchplaces;
  for (int i=j; i<k; i++){
    BatchPlace *place = new BatchPlace;
    place->Copy(_bpn->places[i]);
    places[i] = place;
  }

  j = k;
  k += numberoftriangularplaces;
  for (int i=j; i<k; i++){
    TriangularBatchPlace *place = new TriangularBatchPlace;
    place->Copy(_bpn->places[i]);
    places[i] = place;
  }

  transitions = new  Transition* [NumberOfTransitions()];

  j = 0;
  k = numberofdiscretetransitions;
  for (int i=j; i<k; i++){
    DiscreteTransition *transition = new DiscreteTransition;
    transition->Copy(_bpn->transitions[i]);
    transitions[i] = transition;
  }

  j = k;
  k += numberofcontinuoustransitions;
  for (int i=j; i<k; i++){
    ContinuousTransition *transition = new ContinuousTransition;
    transition->Copy(_bpn->transitions[i]);
    transitions[i] = transition;
  }

  j = k;
  k += numberofbatchtransitions;
  for (int i=j; i<k; i++){
    BatchTransition *transition = new BatchTransition;
    transition->Copy(_bpn->transitions[i]);
    transitions[i] = transition;
  }

  //update node pointes
  for (int i=0; i<NumberOfPlaces(); i++){
    places[i]->AddInputNodes(transitions);
    places[i]->AddOutputNodes(transitions);
  }

  for (int i=0; i<NumberOfTransitions(); i++){
    transitions[i]->AddInputNodes(places);
    transitions[i]->AddOutputNodes(places);
  }

}

//------------------------------------------------------------------------------
// It allocs input/output array for nodes
//------------------------------------------------------------------------------
void BPN::AllocArcs()
{
  for (int i=0; i<NumberOfPlaces(); i++)
    places[i]->AllocArcs(NumberOfTransitions());

    for (int i=0; i<NumberOfTransitions(); i++)
    transitions[i]->AllocArcs(NumberOfPlaces());
}

//------------------------------------------------------------------------------
// It links input/output arc to nodes
//------------------------------------------------------------------------------
void BPN::AddNodeLinks()
{
  //update node pointes
  for (int i=0; i<NumberOfPlaces(); i++){
    places[i]->AddInputNodes(transitions);
    places[i]->AddOutputNodes(transitions);
  }

  for (int i=0; i<NumberOfTransitions(); i++){
    transitions[i]->AddInputNodes(places);
    transitions[i]->AddOutputNodes(places);
  }
}

//------------------------------------------------------------------------------
// It assigns a name to the model
//------------------------------------------------------------------------------
void BPN::PutName(simuleau_name _name)
{
  strcpy(bpnname, _name);
}

//------------------------------------------------------------------------------
// It set the number of discrete places in the model
//------------------------------------------------------------------------------
void BPN::NumberOfDiscretePlaces(const int n)
{
  numberofdiscreteplaces = n;
}

//------------------------------------------------------------------------------
// It set the number of continuous places in the model
//------------------------------------------------------------------------------
void BPN::NumberOfContinuousPlaces(const int n)
{
  numberofcontinuousplaces = n;
}

//------------------------------------------------------------------------------
// It set the number of batch places in the model
//------------------------------------------------------------------------------
void BPN::NumberOfBatchPlaces(const int n)
{
  numberofbatchplaces = n;
}

//------------------------------------------------------------------------------
// It set the number of triangular batch places in the model
//------------------------------------------------------------------------------
void BPN::NumberOfTriangularBatchPlaces(const int n)
{
  numberoftriangularplaces = n;
}

//------------------------------------------------------------------------------
// It adds the new place to the model in the position pos
//------------------------------------------------------------------------------
int BPN::AddPlace(int pos, Place *p)
{
  if (places[pos] != NULL)
    return (1);
  places[pos] = p;
  return (0);
}

//------------------------------------------------------------------------------
// It set the initial marking of a discrete place in the position pos to m
//------------------------------------------------------------------------------
void BPN::SetInitialMarking(int pos, int m)
{
  places[pos]->SetInitialMarking(m);
}

//------------------------------------------------------------------------------
// It set the steady marking of a discrete place in the position pos to m
//------------------------------------------------------------------------------
void BPN::SetSteadyMarking(int pos, int m)
{
  places[pos]->SetSteadyMarking(m);
}

//------------------------------------------------------------------------------
// It set the initial marking of a continuous place in the position pos to m
//------------------------------------------------------------------------------
void BPN::SetInitialMarking(int pos, double m)
{
  places[pos]->SetInitialMarking(m);
}

//------------------------------------------------------------------------------
// It set the steady marking of a continuous place in the position pos to m
//------------------------------------------------------------------------------
void BPN::SetSteadyMarking(int pos, double m)
{
  places[pos]->SetSteadyMarking(m);
}

//------------------------------------------------------------------------------
// It set the initial marking of a batch place in the position pos to m
//------------------------------------------------------------------------------
void BPN::AddBatchToInitialMarking(int pos, Batch &b)
{
  places[pos]->AddBatchToInitialMarking(b);
}

//------------------------------------------------------------------------------
// It set the steady marking of a batch place in the position pos to m
//------------------------------------------------------------------------------
void BPN::AddBatchToSteadyMarking(int pos, Batch &b)
{
  places[pos]->AddBatchToSteadyMarking(b);
}

//------------------------------------------------------------------------------
// It set the initial marking of a batch place in the position pos to m
//------------------------------------------------------------------------------
void BPN::AddTriangularToInitialMarking(int pos, ControllableBatch &b)
{
  places[pos]->AddBatchToInitialMarking(b);
}

//------------------------------------------------------------------------------
// It set the steady marking of a batch place in the position pos to m
//------------------------------------------------------------------------------
void BPN::AddTriangularToSteadyMarking(int pos, ControllableBatch &b)
{
  places[pos]->AddBatchToSteadyMarking(b);
}

//------------------------------------------------------------------------------
// It adds an output arc from the place in position _place to the transition in
// in position _position with weight
//------------------------------------------------------------------------------
int BPN::AddPlaceOutputArc(int _place, int _transition, double _weight)
{
  places[_place]->AddOutputArc(_transition, _weight);
  transitions[_transition]->AddInputArc(_place, _weight);
}

//------------------------------------------------------------------------------
// It set the number of discrete transitions in the model
//------------------------------------------------------------------------------
void BPN::NumberOfDiscreteTransitions(const int n)
{
  numberofdiscretetransitions = n;
}

//------------------------------------------------------------------------------
// It set the number of continuous transitions in the model
//------------------------------------------------------------------------------
void BPN::NumberOfContinuousTransitions(const int n)
{
  numberofcontinuoustransitions = n;
}

//------------------------------------------------------------------------------
// It set the number of batch transitions in the model
//------------------------------------------------------------------------------
void BPN::NumberOfBatchTransitions(const int n)
{
  numberofbatchtransitions = n;
}

//------------------------------------------------------------------------------
// It adds the new transition to the model in the position pos
//------------------------------------------------------------------------------
int BPN::AddTransition(int pos, Transition *t)
{
  if (transitions[pos] != NULL)
    return (1);
  transitions[pos] = t;
  return (0);
}

//------------------------------------------------------------------------------
// It adds an output arc from the place in position _place to the transition in
// in position _position with weight
//------------------------------------------------------------------------------
int BPN::AddTransitionOutputArc(int _transition, int _place,  double _weight)
{ transitions[_transition]->AddOutputArc(_place, _weight);
  places[_place]->AddInputArc(_transition, _weight);
}

//------------------------------------------------------------------------------
// It verifies if the name is already used for another place/transition
//------------------------------------------------------------------------------
int BPN::VerifyName(simuleau_name _name)
{
  for (int i=0; i<NumberOfPlaces(); i++){
    if (places[i] != NULL){
      if (!strcmp(places[i]->GetName(),_name))
        return (1);
    }
  }
  for (int i=0; i<NumberOfTransitions(); i++){
    if (transitions[i] != NULL){
      if (!strcmp(transitions[i]->GetName(),_name))
        return (1);
    }
  }
  return (0);
}

//------------------------------------------------------------------------------
// It returns the bpn model name
//------------------------------------------------------------------------------
char* BPN::GetName()
{
  return (bpnname);
}

//------------------------------------------------------------------------------
// It returns the addresse of the node name
//------------------------------------------------------------------------------
Node* BPN::GetNode(simuleau_name _name)
{
  for (int i=0; i<NumberOfPlaces(); i++){
    if (places[i] != NULL){
      if (!strcmp(places[i]->GetName(),_name))
        return (places[i]);
    }
  }
  for (int i=0; i<NumberOfTransitions(); i++){
    if (transitions[i] != NULL){
      if (!strcmp(transitions[i]->GetName(),_name))
        return (transitions[i]);
    }
  }
  return (NULL);
}
//------------------------------------------------------------------------------
// It returns the number of discrete places in the model
//------------------------------------------------------------------------------
int BPN::NumberOfDiscretePlaces()
{
  return (numberofdiscreteplaces);
}

//------------------------------------------------------------------------------
// It returns the number of continuous places in the model
//------------------------------------------------------------------------------
int BPN::NumberOfContinuousPlaces()
{
  return (numberofcontinuousplaces);
}

//------------------------------------------------------------------------------
// It returns the number of batch places in the model
//------------------------------------------------------------------------------
int BPN::NumberOfBatchPlaces()
{
  return (numberofbatchplaces);
}

//------------------------------------------------------------------------------
// It returns the number of triangular batch places in the model
//------------------------------------------------------------------------------
int BPN::NumberOfTriangularBatchPlaces()
{
  return (numberoftriangularplaces);
}

//------------------------------------------------------------------------------
// It returns the number of all places in the model
//------------------------------------------------------------------------------
int BPN::NumberOfPlaces()
{
  return (numberofdiscreteplaces + numberofcontinuousplaces + numberofbatchplaces + numberoftriangularplaces);
}

//------------------------------------------------------------------------------
// It returns the position of the place _name
//------------------------------------------------------------------------------
int BPN::GetPlacePosition(simuleau_name _name)
{
  for (int i=0; i<NumberOfPlaces(); i++){
    if (places[i] != NULL){
      if (!strcmp(places[i]->GetName(),_name))
        return (i);
    }
  }
  return (-1);
}


//------------------------------------------------------------------------------
// It returns the name of the place in the position _place
//------------------------------------------------------------------------------
char* BPN::GetPlaceName(place_id _place)
{
  return(places[_place]->GetName());
}

//------------------------------------------------------------------------------
//  It returns the pointer of the place in the position _place
//------------------------------------------------------------------------------
Place* BPN::GetPlace(place_id _place)
{
  return(places[_place]);
}

//------------------------------------------------------------------------------
// It returns the place type
//------------------------------------------------------------------------------
place_type BPN::GetPlaceType(simuleau_name _name)
{
  int j = 0;
  int k = numberofdiscreteplaces;
  for (int i=j; i<k; i++){
    if (places[i] != NULL){
      if (!strcmp(places[i]->GetName(),_name))
        return (Discrete_pl);
    }
  }

  j = k;
  k += numberofcontinuousplaces;
  for (int i=j; i<k; i++){
    if (places[i] != NULL){
      if (!strcmp(places[i]->GetName(),_name))
        return (Continuous_pl);
    }
  }

  j = k;
  k += numberofbatchplaces;
  for (int i=j; i<k; i++){
    if (places[i] != NULL){
      if (!strcmp(places[i]->GetName(),_name))
        return (Batch_pl);
    }
  }

  j = k;
  k += numberoftriangularplaces;
  for (int i=j; i<k; i++){
    if (places[i] != NULL){
      if (!strcmp(places[i]->GetName(),_name))
        return (Triangular_pl);
    }
  }
}

//------------------------------------------------------------------------------
// It returns the number of discrete transitions in the model
//------------------------------------------------------------------------------
int BPN::NumberOfDiscreteTransitions()
{
  return (numberofdiscretetransitions);
}

//------------------------------------------------------------------------------
// It returns the number of continuous transitions in the model
//------------------------------------------------------------------------------
int BPN::NumberOfContinuousTransitions()
{
  return (numberofcontinuoustransitions);
}

//------------------------------------------------------------------------------
// It returns the number of batch transitions in the model
//------------------------------------------------------------------------------
int BPN::NumberOfBatchTransitions()
{
  return (numberofbatchtransitions);
}


//------------------------------------------------------------------------------
// It returns the number of all transitions in the model
//------------------------------------------------------------------------------
int BPN::NumberOfTransitions()
{
  return (numberofdiscretetransitions + numberofcontinuoustransitions + numberofbatchtransitions);
}

//------------------------------------------------------------------------------
// It returns the position of the transition _name
//------------------------------------------------------------------------------
int BPN::GetTransitionPosition(simuleau_name _name)
{
  for (int i=0; i<NumberOfTransitions(); i++){
    if (transitions[i] != NULL){
      if (!strcmp(transitions[i]->GetName(),_name))
        return (i);
    }
  }
  return (-1);
}

//------------------------------------------------------------------------------
// It returns the name of the transition in the position _trans
//------------------------------------------------------------------------------
char* BPN::GetTransitionName(trans_id _trans)
{
  return(transitions[_trans]->GetName());
}

//------------------------------------------------------------------------------
// It returns the pointer  of the transition in the position _trans
//------------------------------------------------------------------------------
Transition* BPN::GetTransition(trans_id _trans)
{
  return(transitions[_trans]);
}


//------------------------------------------------------------------------------
// It returns the place type
//------------------------------------------------------------------------------
trans_type BPN::GetTransitionType(simuleau_name _name)
{
  int j = 0;
  int k = numberofdiscretetransitions;
  for (int i=j; i<k; i++){
    if (transitions[i] != NULL){
      if (!strcmp(transitions[i]->GetName(),_name))
        return (Discrete_tr);
    }
  }

  j = k;
  k += numberofcontinuoustransitions;
  for (int i=j; i<k; i++){
    if (transitions[i] != NULL){
      if (!strcmp(transitions[i]->GetName(),_name))
        return (Continuous_tr);
    }
  }

  j = k;
  k += numberofbatchtransitions;
  for (int i=j; i<k; i++){
    if (transitions[i] != NULL){
      if (!strcmp(transitions[i]->GetName(),_name))
        return (Batch_tr);
    }
  }
}

//------------------------------------------------------------------------------
// It prints the bpn model
//------------------------------------------------------------------------------
void BPN::Print(ostream &fout)
{
  fout << "BPN model name: " << bpnname << endl << endl;

  fout << "======" << endl;
  fout << "Places" << endl;
  fout << "======" << endl << endl;

  fout << "Number of places:             " << (numberofdiscreteplaces + numberofcontinuousplaces +
                                               numberofbatchplaces + numberoftriangularplaces) << endl;
  fout << "   Discrete places:           " << numberofdiscreteplaces << endl;
  fout << "   Continuous places:         " << numberofcontinuousplaces << endl;
  fout << "   Batch places:              " << numberofbatchplaces << endl;
  fout << "   Triangular batch places:   " << numberoftriangularplaces << endl << endl;

  fout << "Description of places " << endl;
  fout << "=====================" << endl << endl;

  for (int i=0; i<NumberOfPlaces(); i++){
    places[i]->Print(fout);
  }

  fout << "===========" << endl;
  fout << "Transitions" << endl;
  fout << "===========" << endl << endl;

  fout << "Number of transitions:        " << (numberofdiscretetransitions + numberofcontinuoustransitions +
                                               numberofbatchtransitions) << endl;
  fout << "   Discrete transitions:      " << numberofdiscretetransitions << endl;
  fout << "   Continuous transitions:    " << numberofcontinuoustransitions << endl;
  fout << "   Batch transitions:         " << numberofbatchtransitions << endl << endl;

  fout << "Description of Transitions" << endl;
  fout << "==========================" << endl << endl;

  for (int i=0; i<NumberOfTransitions(); i++){
    transitions[i]->Print(fout);
  }
}

//------------------------------------------------------------------------------
// It writes place marks in the file
//------------------------------------------------------------------------------
void BPN::WritePlaces(ostream &fout)
{
  for (int i=0; i<NumberOfPlaces(); i++){
    places[i]->Write(fout);
  }
}

//------------------------------------------------------------------------------
// It writes continuous and batch transitions after to compute IFF
//------------------------------------------------------------------------------
void BPN::WriteFlows(ostream &fout)
{
  for (int i=numberofdiscretetransitions; i<NumberOfTransitions(); i++){
    ((ContinuousTransition*)transitions[i])->Write(fout);
  }
}

//------------------------------------------------------------------------------
// Compute new marks
//------------------------------------------------------------------------------
void BPN::ComputeNewMarks(double duration)
{
  for (int i=0; i<NumberOfTransitions(); i++){
    transitions[i]->WalkThrough(duration);
  }

  for (int i=0; i<NumberOfPlaces(); i++){
    places[i]->EvolveMarks(simulation->stime->GetPreviousDate());
  }
}

//------------------------------------------------------------------------------
// Reserve marks in discrete places
//------------------------------------------------------------------------------
void BPN::ReserveMarks()
{
  for (int i=0; i<numberofdiscretetransitions; i++){
    if (transitions[i]->GetState() == 1.0){
      ((DiscreteTransition*)transitions[i])->SetEnabledDate(simulation->stime->GetCurrentDate());
      ((DiscreteTransition*)transitions[i])->ReserveMarksInputPlaces();
    }
  }
}

//------------------------------------------------------------------------------
// Create batches in batch places with input flow non-zero
// TODO verify correct function call
//------------------------------------------------------------------------------
void BPN::CreateBatches()
{
  int k = numberofdiscreteplaces + numberofcontinuousplaces;

  for (int i=k; i<NumberOfPlaces(); i++){
    switch (places[i]->IsA()){
      case Batch_pl : ((BatchPlace*)places[i])->CreateBatches();
           break;
      case Triangular_pl: ((TriangularBatchPlace*)places[i])->CreateBatches();
           break;
    }
  }
}

//------------------------------------------------------------------------------
// Destruct batch into the batch places
//------------------------------------------------------------------------------
void BPN::DestructBatches()
{
  int k = numberofdiscreteplaces + numberofcontinuousplaces;

  for (int i=k; i<NumberOfPlaces(); i++){
    ((BatchPlace*)places[i])->DestructBatches();
  }
}

//------------------------------------------------------------------------------
// Merge batches into the batch places
//------------------------------------------------------------------------------
void BPN::MergeBatches()
{
  int k = numberofdiscreteplaces + numberofcontinuousplaces;

  for (int i=k; i<NumberOfPlaces(); i++){
    ((BatchPlace*)places[i])->MergeBatches();
  }
}

//------------------------------------------------------------------------------
// Compute the state of each transition
// TODO verify transitions
//------------------------------------------------------------------------------
int BPN::ComputeTransitionsStates()
{

  if( (NumberOfPlaces() == 0) && (NumberOfTransitions() == 0)){
		return 0;        // No place or transition
	}

  double p[NumberOfPlaces()];
  double t[NumberOfTransitions()];
  int steps = 0;

  for (int i=0; i<NumberOfPlaces(); i++){
    places[i]->SetState(0.0);
  }

  for (int i=0; i<NumberOfTransitions(); i++){
    t[i] = transitions[i]->GetState();
    transitions[i]->SetPreviousState(t[i]);
    transitions[i]->SetState(0.0);
  }

  do{
    steps++;

    for (int i=0; i<NumberOfPlaces(); i++){
      p[i] = places[i]->GetState();
      places[i]->ComputeState();
    }

    for (int i=0; i<NumberOfTransitions(); i++){
      t[i] = transitions[i]->GetState();
      transitions[i]->ComputeState(); // to verify
    }

  } while (StateStability(p,t) != 0);

  return (steps);
}

//------------------------------------------------------------------------------
// Verify the stabilty of the state of each place and transition
//------------------------------------------------------------------------------
int BPN::StateStability(double *p, double *t)
{
  int result = 0;

  for (int i=0; i<NumberOfPlaces(); i++){
    if (p[i] != places[i]->GetState())
      result++;
  }

  for (int i=0; i<NumberOfTransitions(); i++){
    if (t[i] != transitions[i]->GetState())
      result++;
  }
  return (result);
}

//------------------------------------------------------------------------------
// Compute batches behaviors
//------------------------------------------------------------------------------
void BPN::ComputeBatchesBehaviours()
{
// cout << "Compute batches behaviours\n";

  int k = numberofdiscreteplaces + numberofcontinuousplaces; // only batch places

  for (int i=k; i<NumberOfPlaces(); i++){
    switch (places[i]->IsA()){
      case Batch_pl : ((BatchPlace*)places[i])->ComputeBehaviour();
           break;
      case Triangular_pl: ((TriangularBatchPlace*)places[i])->ComputeBehaviour();
           break;
    }
  }
}

//------------------------------------------------------------------------------
// Merge batches into the batch places
//------------------------------------------------------------------------------
void BPN::SetBehaviourFunctions()
{
  int k = numberofdiscreteplaces + numberofcontinuousplaces + numberofbatchplaces;

  for (int i=k; i<NumberOfPlaces(); i++){
    ((TriangularBatchPlace*)places[i])->SetBehaviourFunctions();
  }
}


//------------------------------------------------------------------------------
// Research structural conflit in the model
//------------------------------------------------------------------------------
void BPN::ResearchStructuralConflict()
{
  for(int i=0; i<NumberOfPlaces(); i++){
    places[i]->ResearchStructuralConflict();
  }
}

//------------------------------------------------------------------------------
// Verify conflit in the model
// 0 - No conflit
// 1 - Conflit in discrete place
// 2 - Conflit in continuous place
//------------------------------------------------------------------------------
int  BPN::VerifyConflict()
{
//  cout << "Verifying conflicts\n";
  int conflict = 0;

  for(int i=0; i<NumberOfPlaces(); i++){
    if (places[i]->GetStructuralConflict()){
      conflict = places[i]->VerifyConflict();
      if (conflict)
        return (conflict);
    }
  }
  return (0);
}

//------------------------------------------------------------------------------
// It verifies if the net reaches the steady state
//------------------------------------------------------------------------------
int  BPN::IsSteadyStateReached()
{

  for(int i=numberofdiscreteplaces; i<NumberOfPlaces(); i++){
    switch(places[i]->IsA()){
      case Continuous_pl :  if (!((ContinuousPlace*)places[i])->IsSteadyMarkingReached())
                              return 0;
           break;

      case Batch_pl :  if (!((BatchPlace*)places[i])->IsSteadyMarkingReached())
                              return 0;
           break;
    }
  }

  for(int i=numberofdiscretetransitions; i<NumberOfTransitions(); i++){
    if (!((ContinuousTransition*)transitions[i])->IsSteadyFlowReached()){
      return 0;
    }
  }

  return (1);
}




//------------------------------------------------------------------------------
// Compute next event to Discrete Transition Fire type and put it in dtf list
//------------------------------------------------------------------------------
void BPN::ComputeNextEventDTF(list<Event> * _dtf)
{
  double eventdate = 0.0;

  cout << "ComputeNextEventDTF\n";

  for(int i=0; i<numberofdiscretetransitions; i++){
    if (transitions[i]->GetState() != 0.0){
      eventdate = ((DiscreteTransition*)transitions[i])->GetTime() + ((DiscreteTransition*)transitions[i])->GetEnabledDate();
      if ((eventdate - simulation->stime->GetCurrentDate()) >= PRF::prf.Min_Err_Date()){
        cout << "adding event DTF\n";
        Event *e = new Event(*transitions[i], eventdate, Discr_trans_fires);
        _dtf->push_back(*e);
      }
    }
  }
}

//------------------------------------------------------------------------------
// Compute next event to Continuous Place becomes Empty type and put it in cpe list
//------------------------------------------------------------------------------
void BPN::ComputeNextEventCPE(list<Event> * _cpe)
{
  double eventdate = 0.0;

  cout << "ComputeNextEventCPE\n";

  int beginofcp = numberofdiscreteplaces; //  start from the end of discrete places
  int endofcp   = numberofdiscreteplaces+numberofcontinuousplaces; // stop to the end of continuous places

  for (int i=beginofcp; i<endofcp; i++){
    if (places[i]->GetState() == 1){
      eventdate = ((ContinuousPlace*)places[i])->GetCPEEvent();
      if ((eventdate - simulation->stime->GetCurrentDate()) >= PRF::prf.Min_Err_Date()){
        cout << "adding event CPE\n";
        Event *e = new Event(*places[i], eventdate, Cont_place_empty);
        _cpe->push_back(*e);
      }
    }
  }

}

//------------------------------------------------------------------------------
// Compute next event to Discrete transistion becomes enabled type and put it in dte list
//------------------------------------------------------------------------------
void BPN::ComputeNextEventDTE(list<Event> * _dte)
{
  double eventdate = 0.0;

  cout << "ComputeNextEventDTE\n";

  int beginofcp = numberofdiscreteplaces; //  start from the end of discrete places
  int endofcp   = numberofdiscreteplaces+numberofcontinuousplaces; // stop to the end of continuous places

  for (int i=beginofcp; i<endofcp; i++){
    eventdate = ((ContinuousPlace*)places[i])->GetDTEEvent();
    if ((eventdate - simulation->stime->GetCurrentDate()) >= PRF::prf.Min_Err_Date()){
      cout << "adding event DTE\n";
      Event *e = new Event(*places[i], eventdate, Discr_trans_enabled);
      _dte->push_back(*e);
    }
  }
}

//------------------------------------------------------------------------------
// Compute next event to BOB type and put it in bob list
//------------------------------------------------------------------------------
void BPN::ComputeNextEventBOB(list<Event> * _bob)
{
  double eventdate = 0.0;

  cout << "ComputeNextEventBOB\n";

  int beginofbp = numberofdiscreteplaces+numberofcontinuousplaces; //  start from the end of continuous places
  int endofbp   = NumberOfPlaces(); // stop to the end of all places

  for (int i=beginofbp; i<endofbp; i++){
    switch(places[i]->IsA()){
      case Batch_pl :  eventdate = ((BatchPlace*)places[i])->GetBOBEvent();
           break;
      case Triangular_pl :  eventdate = ((TriangularBatchPlace*)places[i])->GetBOBEvent();
           break;
    }

    if ((eventdate - simulation->stime->GetCurrentDate()) >= PRF::prf.Min_Err_Date()){
      cout << "adding event BOB\n";
      Event *e = new Event(*places[i], eventdate, Becomes_output_batch);
      _bob->push_back(*e);
    }
  }
}

//------------------------------------------------------------------------------
// Compute next event to DOB type and put it in dob list
//------------------------------------------------------------------------------
void BPN::ComputeNextEventDOB(list<Event> * _dob)
{
  double eventdate = 0.0;

  cout << "ComputeNextEventDOB\n";

  int beginofbp = numberofdiscreteplaces+numberofcontinuousplaces; //  start from the end of continuous places
  int endofbp   = NumberOfPlaces(); // stop to the end of all places

  for (int i=beginofbp; i<endofbp; i++){
    switch(places[i]->IsA()){
      case Batch_pl :  eventdate = ((BatchPlace*)places[i])->GetDOBEvent();
           break;
      case Triangular_pl :  eventdate = ((TriangularBatchPlace*)places[i])->GetDOBEvent();
           break;
    }
    if ((eventdate - simulation->stime->GetCurrentDate()) >= PRF::prf.Min_Err_Date()){
      cout << "adding event DOB\n";
      Event *e = new Event(*places[i], eventdate, Destr_output_batch);
      _dob->push_back(*e);
    }
  }
}

//------------------------------------------------------------------------------
// Compute next event to BBD type and put it in dtf list
//------------------------------------------------------------------------------
void BPN::ComputeNextEventBBD(list<Event> * _bbd)
{
 double eventdate = 0.0;

  cout << "ComputeNextEventBBD\n";

  int beginofbp = numberofdiscreteplaces+numberofcontinuousplaces; //  start from the end of continuous places
  int endofbp   = NumberOfPlaces(); // stop to the end of all places

  for (int i=beginofbp; i<endofbp; i++){
    switch(places[i]->IsA()){
      case Batch_pl :  eventdate = ((BatchPlace*)places[i])->GetBBDEvent();
           break;
      case Triangular_pl :  eventdate = ((TriangularBatchPlace*)places[i])->GetBBDEvent();
           break;
    }
    if ((eventdate - simulation->stime->GetCurrentDate()) >= PRF::prf.Min_Err_Date()){
      cout << "adding event BBD\n";
      Event *e = new Event(*places[i], eventdate, Batch_becomes_dense);
      _bbd->push_back(*e);
    }
  }
}

//------------------------------------------------------------------------------
// Compute next event to TBM type and put it in tbm list
//------------------------------------------------------------------------------
void BPN::ComputeNextEventTBM(list<Event> * _tbm)
{
 double eventdate = 0.0;

  cout << "ComputeNextEventTBM\n";

  int beginofbp = numberofdiscreteplaces+numberofcontinuousplaces; //  start from the end of continuous places
  int endofbp   = NumberOfPlaces(); // stop to the end of all places

  for (int i=beginofbp; i<endofbp; i++){
    switch(places[i]->IsA()){
      case Batch_pl :  eventdate = ((BatchPlace*)places[i])->GetTBMEvent();
           break;
      case Triangular_pl :  eventdate = ((TriangularBatchPlace*)places[i])->GetTBMEvent();
           break;
    }
    if ((eventdate - simulation->stime->GetCurrentDate()) >= PRF::prf.Min_Err_Date()){
      cout << "adding event TBM\n";
      Event *e = new Event(*places[i], eventdate, Two_batches_meet);
      _tbm->push_back(*e);
    }
  }
}

//------------------------------------------------------------------------------
// Compute next event to BMOB type and put it in bmob list
//------------------------------------------------------------------------------
void BPN::ComputeNextEventBMOB(list<Event> * _bmob)
{
  double eventdate = 0.0;

  cout << "ComputeNextEventBMOB\n";

  int beginoftbp = numberofdiscreteplaces + numberofcontinuousplaces + numberofbatchplaces; //  start from the end of batch places
  int endoftbp   = NumberOfPlaces(); // stop to the end of all places

  for (int i=beginoftbp; i<endoftbp; i++){
    eventdate = ((TriangularBatchPlace*)places[i])->GetBMOBEvent();
    if ((eventdate - simulation->stime->GetCurrentDate()) >= PRF::prf.Min_Err_Date()){
      cout << "adding event BMOB\n";
      Event *e = new Event(*places[i], eventdate, Batch_meets_output_batch);
      _bmob->push_back(*e);
    }
  }
}

//------------------------------------------------------------------------------
// Compute next event to OBD (outpu batch decongestion) type and put it in obd list
//------------------------------------------------------------------------------
void BPN::ComputeNextEventOBD(list<Event> * _obd)
{
  double eventdate = 0.0;

  cout << "ComputeNextEventOBD\n";

  int beginoftbp = numberofdiscreteplaces + numberofcontinuousplaces + numberofbatchplaces; //  start from the end of batch places
  int endoftbp   = NumberOfPlaces(); // stop to the end of all places

  for (int i=beginoftbp; i<endoftbp; i++){
    eventdate = ((TriangularBatchPlace*)places[i])->GetOBDEvent();
    if ((eventdate - simulation->stime->GetCurrentDate()) >= PRF::prf.Min_Err_Date()){
      cout << "adding event OBD\n";
      Event *e = new Event(*places[i], eventdate, Output_batch_decongestion);
      _obd->push_back(*e);
    }
  }
}

//------------------------------------------------------------------------------
// Compute next event to BD type and put it in bd list
//------------------------------------------------------------------------------
void BPN::ComputeNextEventBD(list<Event> * _bd)
{
  double eventdate = 0.0;

  cout << "ComputeNextEventBD\n";

  int beginoftbp = numberofdiscreteplaces + numberofcontinuousplaces + numberofbatchplaces; //  start from the end of batch places
  int endoftbp   = NumberOfPlaces(); // stop to the end of all places

  for (int i=beginoftbp; i<endoftbp; i++){
    eventdate = ((TriangularBatchPlace*)places[i])->GetBDEvent();
    if ((eventdate - simulation->stime->GetCurrentDate()) >= PRF::prf.Min_Err_Date()){
      cout << "adding event BD\n";
      Event *e = new Event(*places[i], eventdate, Batch_decongestion);
      _bd->push_back(*e);
    }
  }
}

//------------------------------------------------------------------------------
// Compute next event to BBF type and put it in bbf list
//------------------------------------------------------------------------------
void BPN::ComputeNextEventBBF(list<Event> * _bbf)
{
  double eventdate = 0.0;

  cout << "ComputeNextEventBBF\n";

  int beginoftbp = numberofdiscreteplaces + numberofcontinuousplaces + numberofbatchplaces; //  start from the end of batch places
  int endoftbp   = NumberOfPlaces(); // stop to the end of all places

  for (int i=beginoftbp; i<endoftbp; i++){
    eventdate = ((TriangularBatchPlace*)places[i])->GetBBFEvent();
    if ((eventdate - simulation->stime->GetCurrentDate()) >= PRF::prf.Min_Err_Date()){
      cout << "adding event BBF\n";
      Event *e = new Event(*places[i], eventdate, Batch_becomes_free);
      _bbf->push_back(*e);
    }
  }
}

//=============================Onoff======CFF======================================
//------------------------------------------------------------------------------
// Compute next event to Place reaches steadying marking quantity and put it in psq list
//------------------------------------------------------------------------------
void BPN::ComputeNextEventPSQ(list<Event> * _psq)
{
  double eventdate = 0.0;

  cout << "ComputeNextEventPSQ\n";

  int beginofcbp = numberofdiscreteplaces; //  start from the end of discrete places
  int endofcbp   = numberofdiscreteplaces + numberofcontinuousplaces + numberofbatchplaces; // stop at the end of batch places

  for (int i=beginofcbp; i<endofcbp; i++){
    switch (places[i] ->IsA()){
      case Continuous_pl: eventdate = ((ContinuousPlace*)places[i])->GetPSQEvent(); //
           break;
      case Batch_pl:eventdate = ((BatchPlace*)places[i])->GetPSQEvent(); //
           break;
    }
    if ((eventdate - simulation->stime->GetCurrentDate()) >= PRF::prf.Min_Err_Date()){
      cout << "adding event PSQ\n";
      Event *e = new Event(*places[i], eventdate, Place_steady_quan);   //
      _psq->push_back(*e);
    }
  }
}

//=============================MCFF======event=======RBF===============================
//------------------------------------------------------------------------------
// Compute next event that the firing quantity satisfies the remaining marking quantity
//      becomes free steady marking quantity of its output place and put it in rbf list
//

//   q_{i,j}^{r,s} = Post(p_i,t_j)(z_j^s - z_j(m)) <= q_i^{f,s}
//------------------------------------------------------------------------------
void BPN::ComputeNextEventRBF(list<Event> * _rbf)
{
  double eventdate = 0.0;

  cout << "ComputeNextEventRBF\n";


  int beginofcbt = numberofdiscretetransitions; //  start from the end of discrete transitions
  int endofcbt   = numberofdiscretetransitions + numberofcontinuoustransitions + numberofbatchtransitions;  // stop at the end of batch transitions

  for (int i=beginofcbt; i<endofcbt; i++){
    switch(transitions[i]->IsA()){
        case Discrete_tr:
        case Continuous_tr: eventdate = ((ContinuousTransition*)transitions[i])->GetRBFEvent();
            break;
        case Batch_tr: eventdate = ((BatchTransition*)transitions[i])->GetRBFEvent();
            break;

    }
    if ((eventdate - simulation->stime->GetCurrentDate()) >= PRF::prf.Min_Err_Date()){
      cout << "adding event RBF\n";
      Event *e = new Event(*transitions[i], eventdate, Remaining_becomes_freesteadyquan);   //
      _rbf->push_back(*e);
    }
  }
}

//------------------------------------------------------------------------------
// Compute Instantaneous Firing Flow (IFF)
//------------------------------------------------------------------------------
void BPN::ComputeIFF()
{
  cout << "\nCompute IFF\n\n";

  Flow *f;
  double maxflow;

  // contraints a and b
  int beginofcbt = numberofdiscretetransitions;
  int endofcbt   = NumberOfTransitions();

  int numberofcontandbatchtransitions = numberofcontinuoustransitions + numberofbatchtransitions;
  int numberofnotenabledcontandbatchtransitions = 0;
  for (int i=beginofcbt; i<endofcbt; i++){
    if (transitions[i]->GetState() == 0.0){
      numberofnotenabledcontandbatchtransitions++;
    }
  }

  if (numberofcontandbatchtransitions == 0) // no need to compute IFF
    return;

  // contraint c         empty continuous place set
  int beginofcp = numberofdiscreteplaces;
  int endofcp = numberofdiscreteplaces + numberofcontinuousplaces;
  int numberofemptycontinuousplaces = 0;
  for (int i=beginofcp; i<endofcp; i++){
    if (((ContinuousPlace*)places[i])->GetMarks() == 0.0){
//      cout << "Empty place[" << i << "] : " << ((ContinuousPlace*)places[i])->GetMarks() << endl;
      numberofemptycontinuousplaces++;
    }
  }

  // contraint d               full batch place set
  int beginofbp = numberofdiscreteplaces + numberofcontinuousplaces;
  int endofbp = NumberOfPlaces();
  int numberoffullbatchplaces = 0;
  for (int i=beginofbp; i<endofbp; i++){
    if (((BatchPlace*)places[i])->IsFull() == 1){
      numberoffullbatchplaces++;
    }
  }

  // contraints e, f
  int numberofallbatchplaces = numberofbatchplaces + numberoftriangularplaces;

  // contraint g
  int beginoftbp = numberofdiscreteplaces + numberofcontinuousplaces + numberofbatchplaces;
  int endoftbp = NumberOfPlaces();


  int nc = 0; // total of contraints
  int r  = 0; // current contraint
  char rname[256]; // row name

  double V[NumberOfTransitions()]; // used to compute the incidence matrix, only continuous and batch positions are used


  // variables to the linear program
  glp_prob *lp;
  double  IFF[numberofcontandbatchtransitions]; // to store the result of the linear program

  nc = numberofcontandbatchtransitions; // contraint a
  nc+= numberofnotenabledcontandbatchtransitions; // contraint b
  nc+= numberofemptycontinuousplaces; // contraint c
  nc+= numberoffullbatchplaces; // contraint d
  nc+= (numberofallbatchplaces * 2); // contraints e and f
  nc+= numberoftriangularplaces; // contraints g

  // contraints matrix
  int ia[nc * numberofcontandbatchtransitions];
  int ja[nc * numberofcontandbatchtransitions];
  double ar[nc * numberofcontandbatchtransitions];
  int arindex;


  // initialising linear program
  lp = glp_create_prob();
  glp_set_prob_name(lp, "IFF");
  glp_set_obj_dir(lp, GLP_MAX);
  glp_add_cols(lp, numberofcontandbatchtransitions);
  glp_add_rows(lp, nc);

  // initialising transitions bound
//  cout << "\nTransitions bound\n\n";
  for (int i=1, k=beginofcbt; k<endofcbt; k++, i++) {
    glp_set_obj_coef(lp, i, 1.0); // weigth of each transition
    glp_set_col_name(lp, i, transitions[k]->GetName()); // name of the transition
    f = transitions[k]->GetFlow();
    if (f->GetMaximumFlow() != 0.0)
      glp_set_col_bnds(lp, i, GLP_DB, 0.0, f->GetMaximumFlow());
    else
      glp_set_col_bnds(lp, i, GLP_FX, 0.0, 0.0);

//    cout << transitions[k]->GetName() << " : 0.0 - " << f->GetMaximumFlow() << endl;
  }

  r = 1;
  arindex = 1;

//  cout << "\nStart contraints\n\n";
  // constraint a
//  cout << "Contraint a\n";
  for (int i=1, k=beginofcbt; k<endofcbt; k++, i++) {
    sprintf(rname, "a.%d", i);
    f = transitions[k]->GetFlow();
    glp_set_row_name(lp, r, rname);
    if (f->GetMaximumFlow() != 0.0)
      glp_set_row_bnds(lp, r, GLP_DB, 0.0, f->GetMaximumFlow());
    else
      glp_set_row_bnds(lp, r, GLP_FX, 0.0, 0.0);

    ia[arindex] = r;
    ja[arindex] = i;
    ar[arindex] = 1.0; // weigth
    arindex++;
    r++;

//    cout << rname << " : " << "0.0 <= x <= " <<  f->GetMaximumFlow() << endl;
  }

  // constraint b
//  cout << "Contraint b\n";
  for (int i=1, k=beginofcbt; k<endofcbt; k++, i++) {
    sprintf(rname, "b.%d", i);
    if (transitions[k]->GetState() == 0.0){
      glp_set_row_name(lp, r, rname);
      glp_set_row_bnds(lp, r, GLP_FX, 0.0, 0.0);

      ia[arindex] = r;
      ja[arindex] = i;
      ar[arindex] = 1.0; // weigth
      arindex++;
      r++;
//      cout << rname << " : " << ja[arindex-1] << ": 0.0 = x = 0.0 " << endl;
    }
  }

  // constraint c
//  cout << "Contraint c\n";
  for (int i=1, k=beginofcp; k<endofcp; k++, i++) {
    sprintf(rname, "c.%d", i);
    if (((ContinuousPlace*)places[k])->GetMarks() == 0.0){
      glp_set_row_name(lp, r, rname);
      glp_set_row_bnds(lp, r, GLP_LO, 0.0, 0.0);
      for (int j=0; j<NumberOfTransitions(); j++) {
        V[j] = 0.0;
      }

      for (int j=0; j<NumberOfTransitions(); j++){
        if (places[k]->GetWeightOfOutputArc(j) != 0.0 ){
          V[j] = - places[k]->GetWeightOfOutputArc(j);
//          cout << " - V [" << j << "] " ;
        }
        if (places[k]->GetWeightOfInputArc(j) != 0.0 ){
//          cout << " + V [" << j << "] " ;
          V[j] +=  places[k]->GetWeightOfInputArc(j);
        }
      }
//      cout << endl;
//      cout << rname << " : ";
      for (int j=1, l=beginofcbt; l<endofcbt; j++, l++) {
        if (V[l] != 0.0){
          ia[arindex] = r;
          ja[arindex] = j;
          ar[arindex] = V[l];
          arindex++;
//          cout << j << "(" << V[l] << ") - ";
        }
      }
//      cout << " >= 0.0" << endl;
      r++;
    }
  }


  // constraint d
//  cout << "Contraint d\n";
  for (int i=1, k=beginofbp; k<endofbp; k++, i++) {
    sprintf(rname, "d.%d", i);
    if (((BatchPlace *)places[k])->IsFull() == 1.0){
      glp_set_row_name(lp, r, rname);
      glp_set_row_bnds(lp, r, GLP_UP, 0.0, 0.0);
      for (int j=0; j<NumberOfTransitions(); j++) {
        V[j] = 0.0;
      }
      for (int j=0; j<NumberOfTransitions(); j++){
        if (places[k]->GetWeightOfOutputArc(j) != 0.0 ){
          V[j] = - places[k]->GetWeightOfOutputArc(j);
        }
        if (places[k]->GetWeightOfInputArc(j) != 0.0 ){
          V[j] +=  places[k]->GetWeightOfInputArc(j);
        }
      }
//      cout << rname << " : ";
      for (int j=1, l=beginofcbt; l<endofcbt; j++, l++) {
        if (V[l] != 0.0){
          ia[arindex] = r;
          ja[arindex] = j;
          ar[arindex] = V[l];
          arindex++;
//          cout << j << "(" << V[l] << ") - ";
        }
      }
//      cout << " <= 0.0" << endl;
      r++;
    }
  }

  // constraint e
//  cout << "Contraint e\n";
  for (int i=1, k=beginofbp; k<endofbp; k++, i++) {
    switch (places[k]->IsA()){
      case Batch_pl: maxflow = ((BatchPlace*)places[k])->GetInstantaneousSpeed() * ((BatchPlace*)places[k])->GetDensity();
           break;
      case Triangular_pl: maxflow = ((TriangularBatchPlace*)places[k])->GetInstantaneousSpeed() * ((TriangularBatchPlace*)places[k])->GetCriticalDensity();
           break;
    }
    sprintf(rname, "e.%d", i);
    glp_set_row_name(lp, r, rname);
    glp_set_row_bnds(lp, r, GLP_UP, 0.0, maxflow);

    for (int j=0; j<NumberOfTransitions(); j++) {
      V[j] = 0.0;
    }
    for (int j=0; j<NumberOfTransitions(); j++){
      if (places[k]->GetWeightOfInputArc(j) != 0.0 ){
          V[j] =  places[k]->GetWeightOfInputArc(j);
      }
    }
//      cout << rname << " : ";
      for (int j=1, l=beginofcbt; l<endofcbt; j++, l++) {
       if (V[l] != 0.0){
          ia[arindex] = r;
          ja[arindex] = j;
          ar[arindex] = V[l];
          arindex++;
//          cout << j << "(" << V[l] << ") - ";
        }
      }
//      cout << "0.0 - " << maxflow << endl;
      r++;
  }


  // constraint f
//  cout << "Contraint f\n";
  for (int i=1, k=beginofbp; k<endofbp; k++, i++) {
    maxflow = ((BatchPlace*)places[k])->GetInstantaneousSpeed() * ((BatchPlace*)places[k])->GetDensityOutputBatch();
    sprintf(rname, "f.%d", i);
    glp_set_row_name(lp, r, rname);
    glp_set_row_bnds(lp, r, GLP_UP, 0.0, maxflow);

    for (int j=0; j<NumberOfTransitions(); j++) {
      V[j] = 0.0;
    }
    for (int j=0; j<NumberOfTransitions(); j++){
      if (places[k]->GetWeightOfOutputArc(j) != 0.0 ){
          V[j] =  places[k]->GetWeightOfOutputArc(j);
      }
    }
//      cout << rname << " : ";
      for (int j=1, l=beginofcbt; l<endofcbt; j++, l++) {
        if (V[l] != 0.0){
          ia[arindex] = r;
          ja[arindex] = j;
          ar[arindex] = V[l];
          arindex++;
//          cout << j << "(" << V[l] << ") - ";
        }
      }
//      cout << "0.0 - " << maxflow << endl;
      r++;
  }

  // constraint g
//  cout << "Contraint g\n";
  for (int i=1, k=beginoftbp; k<endoftbp; k++, i++) {
    maxflow = ((TriangularBatchPlace*)places[k])->GetInstantaneousSpeed() * ((TriangularBatchPlace*)places[k])->GetCriticalDensity();
    sprintf(rname, "g.%d", i);
    glp_set_row_name(lp, r, rname);
    glp_set_row_bnds(lp, r, GLP_UP, 0.0, maxflow);

    for (int j=0; j<NumberOfTransitions(); j++) {
      V[j] = 0.0;
    }
    for (int j=0; j<NumberOfTransitions(); j++){
      if (places[k]->GetWeightOfOutputArc(j) != 0.0 ){
          V[j] =  places[k]->GetWeightOfOutputArc(j);
      }
    }
//      cout << rname << " : ";
      for (int j=1, l=beginofcbt; l<endofcbt; j++, l++) {
        if (V[l] != 0.0){
          ia[arindex] = r;
          ja[arindex] = j;
          ar[arindex] = V[l];
          arindex++;
//          cout << j << "(" << V[l] << ") - ";
        }
      }
//      cout << "0.0 - " << maxflow << endl;
      r++;
  }

  glp_term_out(GLP_OFF);
  glp_load_matrix(lp, arindex-1, ia, ja, ar);
  glp_simplex(lp, NULL);

  for (int i=0, k=beginofcbt; k<endofcbt; k++, i++) {
    IFF[i] = glp_get_col_prim(lp, (i+1));
    f =  ((ContinuousTransition*)transitions[k])->GetFlow();
    f->SetTheoreticalFlow(IFF[i]);
    f->SetCurrentFlow(IFF[i]);

//    cout << transitions[k]->GetName() << " : " << IFF[i] << endl;
  }

  int col[numberofcontandbatchtransitions];
  double val[numberofcontandbatchtransitions];

/*  for (int i=1; i<=glp_get_num_rows(lp); i++){
        cout << glp_get_row_name(lp, i) << " : ";
        for (int j=1; j<=glp_get_mat_row(lp, i, col, val); j++){
            cout << col[j] << "(" << val[j] << ") - ";
        }
        cout << glp_get_row_lb(lp, i) << " - " << glp_get_row_ub(lp, i) << endl;
  }
*/
  glp_delete_prob(lp);
}

//------------------------------------------------------------------------------
// Compute Controlled Firing Flow (CFF) based on steady flow
//------------------------------------------------------------------------------
void BPN::ComputeCFF()
{
  cout << "\nCompute CFF\n\n";

  Flow *u;
  double steadyflow;
  double maxflow;


  // Verify if the marking is reached in this procedure



  // contraints a and b    ==> not enabled transition set
  int beginofcbt = numberofdiscretetransitions;
  int endofcbt   = numberofdiscretetransitions + numberofcontinuoustransitions + numberofbatchtransitions;

  int numberofcontandbatchtransitions = numberofcontinuoustransitions + numberofbatchtransitions;
  int numberofnotenabledcontandbatchtransitions = 0;
  int numberofnotenabledorlesssteadcontandbatchtransitions = 0;
  for (int i=beginofcbt; i<endofcbt; i++){
    if (transitions[i]->GetState() == 0.0 ){
      numberofnotenabledcontandbatchtransitions++;
    }

    if ((transitions[i]->GetState() ==  0.0) || (transitions[i]->IsOnState() == 0)){
      numberofnotenabledorlesssteadcontandbatchtransitions++;
    }
  }

  if (numberofcontandbatchtransitions == 0) // no need to compute CFF
    return;

  // contraint c ===>continuous and batch place whose marking quantity is greater than steady marking quantity
  int beginofcbp = numberofdiscreteplaces;
  int endofcbp = numberofdiscreteplaces + numberofcontinuousplaces + numberofbatchplaces;
  int numberofgreatercontandbatchplaces = 0;
  for (int i=beginofcbp; i<endofcbp; i++){
    switch (places[i] ->IsA()){
      case Continuous_pl: if (((ContinuousPlace*)places[i])->IsGreaterThanSteadyQuantity() == 1){
      numberofgreatercontandbatchplaces++;
    }
           break;
      case Batch_pl: if (((BatchPlace*)places[i])->IsGreaterThanSteadyQuantity() == 1){
      numberofgreatercontandbatchplaces++;
    }
           break;

    }
    }

  // contraint d ===>continuous and batch place whose marking quantity is equal to steady marking quantity (S_E)
 // int beginofcbp = numberofdiscreteplaces;
 // int endofcbp = numberofdiscreteplaces + numberofcontinuousplaces + numberofbatchplaces;
  int numberofequalcontandbatchplaces = 0;
  for (int i=beginofcbp; i<endofcbp; i++){
    switch (places[i] ->IsA()){
      case Continuous_pl: if (((ContinuousPlace*)places[i])->IsEqualSteadyQuantity() == 1){
      numberofequalcontandbatchplaces++;
    }
           break;
      case Batch_pl: if (((BatchPlace*)places[i])->IsEqualSteadyQuantity() == 1){
      numberofequalcontandbatchplaces++;
    }
           break;
    }
    }


  // contraint e ===>continuous and batch place whose marking quantity is less than steady marking quantity(S_L)
  int beginofbp = numberofdiscreteplaces + numberofcontinuousplaces;
  int endofbp = numberofdiscreteplaces + numberofcontinuousplaces + numberofbatchplaces;
  int numberoflesscontandbatchplaces = 0;
  for (int i=beginofcbp; i<endofcbp; i++){
    switch (places[i] ->IsA()){
      case Continuous_pl: if (((ContinuousPlace*)places[i])->IsLessThanSteadyQuantity() == 1){
      numberoflesscontandbatchplaces++;
    }
           break;
      case Batch_pl: if (((BatchPlace*)places[i])->IsLessThanSteadyQuantity() == 1){
      numberoflesscontandbatchplaces++;
    }
           break;
    }
    }

// contraint f   ===>transition with at least one of its input places belonging to S_L.
//  int beginofcbt = numberofdiscretetransitions;
 // int endofcbt = numberofdiscretetransitions + numberofcontinuoustransitions + numberofbatchtransitions;
//  int numberofcontandbatchtransitions = numberofcontinuoustransitions + numberofbatchtransitions;
  int numberofinputplacelesscontandbatchtransitions = 0;
  for (int i=beginofcbt; i<endofcbt; i++){
    switch (transitions[i] ->IsA()){
      case Continuous_tr: if (((ContinuousTransition*)transitions[i])->GetState()==0.0){   //GetCFFState()= 0.0
      numberofinputplacelesscontandbatchtransitions++;
           break;
           }
      case Batch_tr: if (((BatchTransition*)transitions[i])->GetState()== 0.0){
      numberofinputplacelesscontandbatchtransitions++;
    }
           break;
    }
}
//constraint g
 // int numberofbatchplaces;




  int nc = 0; // total of contraints
  int r  = 0; // current contraint
  char rname[256]; // row name

  double V[NumberOfTransitions()]; // used to compute the incidence matrix, only continuous and batch positions are used


  // variables to the linear program
  glp_prob *lp;
  double  CFF[numberofcontandbatchtransitions]; // to store the result of the linear program

  nc = numberofcontandbatchtransitions; // contraint a
  nc+= numberofnotenabledorlesssteadcontandbatchtransitions; // contraint b
  nc+= numberofgreatercontandbatchplaces; // contraint c
  nc+= numberofequalcontandbatchplaces; // contraint d
  //nc+= numberoflesscontandbatchplaces; // contraint e
  //nc+= numberofinputplacelesscontandbatchtransitions; // contraint f
  nc+= numberofbatchplaces; // contraints g


  // contraints matrix
  int ia[nc * numberofcontandbatchtransitions];
  int ja[nc * numberofcontandbatchtransitions];
  double ar[nc * numberofcontandbatchtransitions];
  int arindex;


  // initialising linear program
  lp = glp_create_prob();
  glp_set_prob_name(lp, "CFF");
  glp_set_obj_dir(lp, GLP_MAX);
  glp_add_cols(lp, numberofcontandbatchtransitions);
  glp_add_rows(lp, nc);

  // initialising transitions bound
  cout << "\nTransitions bound\n\n";
  for (int i=1, k=beginofcbt; k<endofcbt; k++, i++) {
    glp_set_obj_coef(lp, i, 1.0); // set (change) objective coefficient or constant term
    glp_set_col_name(lp, i, transitions[k]->GetName()); // name of the transition
    u = transitions[k]->GetFlow();
    if (u->GetSteadyFlow() != 0.0)
      glp_set_col_bnds(lp, i, GLP_DB, 0.0, u->GetSteadyFlow());//set (change) column bounds
    else
      glp_set_col_bnds(lp, i, GLP_FX, 0.0, 0.0);

    cout << transitions[k]->GetName() << " : 0.0 - " << u->GetSteadyFlow() << endl;
  }

  r = 1;
  arindex = 1;

  cout << "\nStart contraints\n\n";
  // constraint a
  cout << "Contraint a\n";
  for (int i=1, k=beginofcbt; k<endofcbt; k++, i++) {
    sprintf(rname, "a.%d", i);
    u = transitions[k]->GetFlow();
    glp_set_row_name(lp, r, rname);
    if (u->GetSteadyFlow() != 0.0)
      glp_set_row_bnds(lp, r, GLP_DB, 0.0, u->GetSteadyFlow());  //compare with steadyflow
    else
      glp_set_row_bnds(lp, r, GLP_FX, 0.0, 0.0);

    ia[arindex] = r;
    ja[arindex] = i;
    ar[arindex] = 1.0; // weigth
    arindex++;
    r++;

    cout << rname << " : " << "0.0 <= x <= " <<  u->GetSteadyFlow() << endl;
  }

  // constraint b
  cout << "Contraint b\n";
  for (int i=1, k=beginofcbt; k<endofcbt; k++, i++) {
    sprintf(rname, "b.%d", i);
    if (transitions[k]->GetState() == 0.0 || (transitions[k]->IsOnState() == 0)){
      glp_set_row_name(lp, r, rname);
      glp_set_row_bnds(lp, r, GLP_FX, 0.0, 0.0);

      ia[arindex] = r;
      ja[arindex] = i;
      ar[arindex] = 1.0; // weigth
      arindex++;
      r++;
      cout << rname << " : " << ja[arindex-1] << ": 0.0 = x = 0.0 " << endl;
    }
  }

  // constraint c
  cout << "Contraint c\n";
  for (int i=1, k=beginofcbp; k<endofcbp; k++, i++) {
     switch (places[k] ->IsA()){
      case Continuous_pl: if (((ContinuousPlace*)places[k])->IsGreaterThanSteadyQuantity() == 0)
                            continue;
           break;
      case Batch_pl: if (((BatchPlace*)places[k])->IsGreaterThanSteadyQuantity() == 0)
                        continue;
           break;
     }
      sprintf(rname, "c.%d", i);
      glp_set_row_name(lp, r, rname);
      glp_set_row_bnds(lp, r, GLP_UP, 0.0, 0.0); //look after
      for (int j=0; j<NumberOfTransitions(); j++) {
        V[j] = 0.0;
      }

      for (int j=0; j<NumberOfTransitions(); j++){
        if (places[k]->GetWeightOfOutputArc(j) != 0.0 ){
          V[j] = - places[k]->GetWeightOfOutputArc(j);
          cout << " - V [" << j << "] " ;
        }
        if (places[k]->GetWeightOfInputArc(j) != 0.0 ){
          cout << " + V [" << j << "] " ;
          V[j] +=  places[k]->GetWeightOfInputArc(j);
        }
      }
      cout << endl;
      cout << rname << " : ";
      for (int j=1, l=beginofcbt; l<endofcbt; j++, l++) {
        if (V[l] != 0.0){
          ia[arindex] = r;
          ja[arindex] = j;
          ar[arindex] = V[l];
          arindex++;
          cout << j << "(" << V[l] << ") - ";
        }
      }
      cout << " <= 0.0" << endl;
      r++;
    }

  //constraint d
  cout << "Contraint d\n";
    for (int i=1, k=beginofcbp; k<endofcbp; k++, i++) {
     switch (places[k] ->IsA()){
      case Continuous_pl: if (((ContinuousPlace*)places[k])->IsEqualSteadyQuantity() == 0)
                            continue;
           break;
      case Batch_pl: if (((BatchPlace*)places[k])->IsEqualSteadyQuantity() == 0)
                       continue;
           break;
    }
      sprintf(rname, "d.%d", i);
      glp_set_row_name(lp, r, rname);
      glp_set_row_bnds(lp, r, GLP_FX, 0.0, 0.0);
      for (int j=0; j<NumberOfTransitions(); j++) {
        V[j] = 0.0;
      }

      for (int j=0; j<NumberOfTransitions(); j++){
        if (places[k]->GetWeightOfOutputArc(j) != 0.0 ){
          V[j] = - places[k]->GetWeightOfOutputArc(j);
          cout << " - V [" << j << "] " ;
        }
        if (places[k]->GetWeightOfInputArc(j) != 0.0 ){
          cout << " + V [" << j << "] " ;
          V[j] +=  places[k]->GetWeightOfInputArc(j);
        }
      }
      cout << endl;
      cout << rname << " : ";
      for (int j=1, l=beginofcbt; l<endofcbt; j++, l++) {
        if (V[l] != 0.0){
          ia[arindex] = r;
          ja[arindex] = j;
          ar[arindex] = V[l];
          arindex++;
          cout << j << "(" << V[l] << ") - ";
        }
      }
      cout << " = 0.0" << endl;
      r++;
    }

  // constraint d
//  cout << "Contraint d\n";
 /* for (int i=1, k=beginofcbp; k<endofcbp; k++, i++) {
    switch (places[k] ->IsA()){
      case Continuous_pl: if (((ContinuousPlace*)places[k])->IsEqualSteadyQuantity() == 1.0){
    }
           break;
      case Batch_pl: if (((BatchPlace*)places[k])->IsEqualSteadyQuantity() == 1.0){
    }
           break;
    }
      sprintf(rname, "d.%d", i);
      glp_set_row_name(lp, r, rname);
      glp_set_row_bnds(lp, r, GLP_UP, 0.0, 0.0);
      for (int j=0; j<NumberOfTransitions(); j++) {
        V[j] = 0.0;
      }
      for (int j=0; j<NumberOfTransitions(); j++){
        if (places[k]->GetWeightOfOutputArc(j) != 0.0 ){
          V[j] = - places[k]->GetWeightOfOutputArc(j);
        }
        if (places[k]->GetWeightOfInputArc(j) != 0.0 ){
          V[j] +=  places[k]->GetWeightOfInputArc(j);
        }
      }
//      cout << rname << " : ";
      for (int j=1, l=beginofcbt; l<endofcbt; j++, l++) {
        if (V[l] != 0.0){
          ia[arindex] = r;
          ja[arindex] = j;
          ar[arindex] = V[l];
          arindex++;
//          cout << j << "(" << V[l] << ") - ";
        }
      }
//      cout << " <= 0.0" << endl;
      r++;
    }
*/

  // constraint e         //input place's marking quantity is less than steady marking quantity.
  cout << "Contraint e\n";

  for (int i=1, k=beginofbp; k<endofbp; k++, i++) {
    maxflow = ((BatchPlace*)places[k])->GetInstantaneousSpeed() * ((BatchPlace*)places[k])->GetDensityOutputBatch();
    sprintf(rname, "e.%d", i);
    glp_set_row_name(lp, r, rname);
    glp_set_row_bnds(lp, r, GLP_UP, 0.0, maxflow);

    for (int j=0; j<NumberOfTransitions(); j++) {
      V[j] = 0.0;
    }
    for (int j=0; j<NumberOfTransitions(); j++){
      if (places[k]->GetWeightOfOutputArc(j) != 0.0 ){
          V[j] =  places[k]->GetWeightOfOutputArc(j);
      }
    }
      cout << rname << " : ";
      for (int j=1, l=beginofcbt; l<endofcbt; j++, l++) {
        if (V[l] != 0.0){
          ia[arindex] = r;
          ja[arindex] = j;
          ar[arindex] = V[l];
          arindex++;
          cout << j << "(" << V[l] << ") - ";
        }
      }
      cout << "0.0 <= x <= " << maxflow << endl;
      r++;
}

  glp_term_out(GLP_OFF);
  glp_load_matrix(lp, arindex-1, ia, ja, ar);
  glp_simplex(lp, NULL);

  for (int i=0, k=beginofcbt; k<endofcbt; k++, i++) {
    CFF[i] = glp_get_col_prim(lp, (i+1));
    u =  ((ContinuousTransition*)transitions[k])->GetFlow();
    u->SetTheoreticalFlow(CFF[i]);
    u->SetCurrentFlow(CFF[i]);

//    cout << transitions[k]->GetName() << " : " << CFF[i] << endl;
  }



  int col[numberofcontandbatchtransitions];
  double val[numberofcontandbatchtransitions];

/*  for (int i=1; i<=glp_get_num_rows(lp); i++){
        cout << glp_get_row_name(lp, i) << " : ";
        for (int j=1; j<=glp_get_mat_row(lp, i, col, val); j++){
            cout << col[j] << "(" << val[j] << ") - ";
        }
        cout << glp_get_row_lb(lp, i) << " - " << glp_get_row_ub(lp, i) << endl;
  }
*/
  glp_delete_prob(lp);
}//=======================================================================



//---------------------------------------------------------------------------
// Compute current firing quantity at each time step
//-------------------------------------------------------------------------------
void  BPN::ComputeCurrentFiringQuantity(double steptime)
{
   FiringQuantity *z;
   Flow *f;
   double  currentfiringquantity1;

    for (int i=0; i<NumberOfTransitions(); i++){
        f=transitions[i]->GetFlow();
        z=transitions[i]->GetFiringQuantity();
      //  currentfiringquantity=transitions[i]->CurrentFiringQuantity();
        currentfiringquantity1 = (f->GetCurrentFlow() * steptime) + z->GetCurrentFiringQuantity();
   //     cout<<"current first: "<<currentfiringquantity<<endl;
        z->SetCurrentFiringQuantity(currentfiringquantity1);
   //     cout<<"current second: "<<currentfiringquantity<<endl;
    //    currentfiringquantity-= f->GetCurrentFlow() * steptime;
    //    cout<<"current third: "<<currentfiringquantity<<endl;
        }
}

//--------------------------------------------------------------------------
//  Compute steady firing quantity vector (minimal firing count vector)
//--------------------------------------------------------------------------
// Solve the LLP:
//                 min Z^s
//                 q^s=q^0 + C * Z^s
//                 Z^s >= 0
//--------------------------------------------------------------------------
void BPN::ComputeSteadyFiringQuantity()
{
  Transition *t;
  Place *p;
  Flow *f;
  FiringQuantity *z;
  double steadyfiringquantity;
  double remainingquantity;

 // contraints a
  int beginofcbp = numberofdiscreteplaces;
  int endofcbp = numberofdiscreteplaces + numberofcontinuousplaces + numberofbatchplaces;
  int numberofcontandbatchplaces = numberofcontinuousplaces + numberofbatchplaces;

  //constraints b
  int beginofcbt = numberofdiscretetransitions;
  int endofcbt   = numberofdiscretetransitions + numberofcontinuoustransitions + numberofbatchtransitions;
  int numberofcontandbatchtransitions = numberofcontinuoustransitions + numberofbatchtransitions;


  int nc = 0; // total of contraints
  int r  = 0; // current contraint
  char rname[256]; // row name

  double V[NumberOfTransitions()]; // used to compute the incidence matrix, only continuous and batch positions are used


  // variables to the linear program
  glp_prob *lp;
  double SS[numberofcontandbatchtransitions+1]; // to store the result of the linear program
  //double *SS;

  nc = numberofcontandbatchplaces; // contraint a
  nc+= numberofcontandbatchtransitions;  // contraint b


  // contraints matrix
  int ia[nc * (numberofcontandbatchtransitions)];
  int ja[nc * (numberofcontandbatchtransitions)];
  double ar[nc * (numberofcontandbatchtransitions)];
  int arindex;

  // initialising linear program
  lp = glp_create_prob();
  glp_set_prob_name(lp, "ZS");
  glp_set_obj_dir(lp, GLP_MAX);
  glp_add_cols(lp, (numberofcontandbatchtransitions));
  glp_add_rows(lp, nc);



  cout << "\nSteady firing quantities bound\n\n";
  for (int k=beginofcbt; k<endofcbt; k++) {
    glp_set_obj_coef(lp, k+1, -1.0); // set (change) objective coefficient or constant term
    glp_set_col_name(lp, k+1, transitions[k]->GetName()); // name of the transition
    f = transitions[k]->GetFlow();
    if (f->GetMaximumFlow() != 0.0)
      glp_set_col_bnds(lp, k+1, GLP_LO, 0.0, 0.0);//set (change) column bounds
    else
      glp_set_col_bnds(lp, k+1, GLP_FX, 0.0, 0.0);

    cout << transitions[k]->GetName() << " z: >= " << 0.0 << endl;
  }



  r = 1;
  arindex = 1;


  cout << "\nStart contraints\n\n";
  // constraint a
  cout << "Contraint a\n";
 for (int i=1, k=beginofcbp; k<endofcbp; k++, i++) {
     switch (places[k] ->IsA()){
      case Continuous_pl:
                         remainingquantity = ((ContinuousPlace*)places[k])->GetSteadyMarks()- ((ContinuousPlace*)places[k])->GetInitialMarks();
           break;
      case Batch_pl:
                        remainingquantity = ((BatchPlace*)places[k])->GetSteadyQuantity()-((BatchPlace*)places[k])->GetInitialQuantity();

           break;
    }

      sprintf(rname, "a.%d", i);
      glp_set_row_name(lp, r, rname);
      glp_set_row_bnds(lp, r, GLP_FX, remainingquantity, remainingquantity);


      for (int j=0; j<NumberOfTransitions(); j++) {
        V[j] = 0.0;
      }

      for (int j=0; j<NumberOfTransitions(); j++){
        if (places[k]->GetWeightOfOutputArc(j) != 0.0 ){
          V[j] = - places[k]->GetWeightOfOutputArc(j);
          cout << " - V [" << j << "] " ;
        }
        if (places[k]->GetWeightOfInputArc(j) != 0.0 ){
          cout << " + V [" << j << "] " ;
          V[j] +=  places[k]->GetWeightOfInputArc(j);
        }
      }

      cout << endl;
      cout << rname << " : ";
      for (int j=0, l=beginofcbt; l<endofcbt; j++, l++) {
        if (V[j] != 0.0){
          ia[arindex] = r;
          ja[arindex] = l+1;
          ar[arindex] = V[j];
          arindex++;
          cout << "Zs"<< j << "(" << V[j] << ") + ";
        }
      }
      cout << " = "<< remainingquantity << endl;
      r++;
    }


    // constraint b
  cout << "Contraint b\n";

    for (int i=1, k=beginofcbt; k<endofcbt; k++, i++) {
    sprintf(rname, "b.%d", i);
    glp_set_row_name(lp, r, rname);
    glp_set_row_bnds(lp, r, GLP_LO, 0.0, 0.0);

    ia[arindex] = r;
    ja[arindex] = k+1;
    ar[arindex] = 1.0; // weight
    arindex++;
    r++;
    cout << rname << " : " << "Zs " << i << " >= 0.0  "  << endl;
  }


  glp_term_out(GLP_OFF);
  glp_load_matrix(lp, arindex-1, ia, ja, ar);
  glp_simplex(lp, NULL);


  for (int i=1;i<(numberofcontandbatchtransitions+1); i++){
   SS[i]=glp_get_col_prim(lp, i);
   cout<< "SS[" << i <<"] = " << SS[i]<<endl;
   z=transitions[i-1]->GetFiringQuantity();
   z->SetSteadyFiringQuantity(SS[i]);

//   transitions[i]->SetSteadyFiringQuantity(SS[i]);
   //steadyfiringquantity->SetSteadyFiringQuantity(SS[i]);
  //SS = calloc(1+numberofcontandbatchplaces+2*numberofcontandbatchtransitions, sizeof(double));
  }

//  for (int i=1;i<(numberofcontandbatchtransitions+1); i++){
//  printf("z=%lf,SS[i]=%lf\n",z,i, SS[i]);
//  }

 //   f =  ((ContinuousTransition*)transitions[k-numberofcontandbatchplaces])->GetFlow();
 //   f->SetTheoreticalFlow(SS[k]);
 //   f->SetCurrentFlow(SS[k]);

 //   cout << transitions[k-numberofcontandbatchplaces]->GetName() << " : " << SS[i] << endl;

  int col[numberofcontandbatchtransitions];
  double val[numberofcontandbatchtransitions];

  /*
  for (int i=1; i<=glp_get_num_rows(lp); i++){
        cout << glp_get_row_name(lp, i) << " : ";
        for (int j=1; j<=glp_get_mat_row(lp, i, col, val); j++){
            cout << col[j] << "(" << val[j] << ") - ";
        }
        cout << glp_get_row_lb(lp, i) << " - " << glp_get_row_ub(lp, i) << endl;
  }
  */
  glp_delete_prob(lp);
  return 0;
}


//==============================================================================
// Compute the steady state with assigned transfer speed
//==============================================================================
// To do list:
//    1. Set the result of marking and flow into steady marking and steady flow.
//    2. Give the computation of the steady state with variable transfer speed.
//    3. Consider the discrete nodes.
void BPN::ComputeSteadyState()
{

  cout << "\nCompute Steadystate\n\n";


  double capacity;
  double quantity_initial;
  double maxflow;
  Flow *f;
  double z;


  // contraints a
  int beginofcbt = numberofbatchplaces+numberofcontinuousplaces+numberofdiscreteplaces+numberofdiscretetransitions;
  int endofcbt   = numberofbatchplaces+numberofcontinuousplaces+numberofdiscreteplaces+numberofdiscretetransitions + numberofcontinuoustransitions + numberofbatchtransitions;
  int numberofcontandbatchtransitions = numberofcontinuoustransitions + numberofbatchtransitions;

  //constraints b and b'
  int beginofbp = numberofdiscreteplaces + numberofcontinuousplaces;
  int endofbp   = numberofdiscreteplaces + numberofcontinuousplaces + numberofbatchplaces;
//  int numberofbatchplaces;

  //constraints c

  //constraints d
  int beginofcbp = numberofdiscreteplaces;
  int endofcbp   = numberofdiscreteplaces + numberofcontinuousplaces + numberofbatchplaces;
  int numberofcontandbatchplaces = numberofcontinuousplaces + numberofbatchplaces;

  //constraints e
  int beginofft  = numberofcontandbatchplaces + numberofcontandbatchtransitions;
  int endofft    = numberofcontandbatchplaces + numberofcontandbatchtransitions + numberofcontandbatchtransitions;

  // set value to all places and transitions
 // int numberofcontinuousplaces;
 // int numberofcontinuoustransitions;

 // if (numberofcontandbatchtransitions == 0 || numberofcontandbatchplaces ==0) // no need to compute CFF
 //   return;


  int nc = 0; // total of contraints
  int r  = 0; // current contraint
  char rname[256]; // row name

  double V[NumberOfTransitions()]; // used to compute the incidence matrix, only continuous and batch positions are used


  // variables to the linear program
  glp_prob *lp;
  double SS[numberofcontandbatchplaces+2*numberofcontandbatchtransitions+1]; // to store the result of the linear program
  //double *SS;

  nc = numberofcontandbatchtransitions; // contraint a
  nc+= numberofbatchplaces;  // contraint b
  nc+= numberofbatchplaces;  // contraint b'
  nc+= numberofbatchplaces;  // contraint c
  nc+= numberofcontandbatchplaces; // contraint d
  nc+= numberofcontandbatchplaces; // contraint e


  // contraints matrix
  int ia[nc * (numberofcontandbatchplaces+2*numberofcontandbatchtransitions)];
  int ja[nc * (numberofcontandbatchplaces+2*numberofcontandbatchtransitions)];
  double ar[nc * (numberofcontandbatchplaces+2*numberofcontandbatchtransitions)];
  int arindex;

  // initialising linear program
  lp = glp_create_prob();
  glp_set_prob_name(lp, "SS");
  glp_set_obj_dir(lp, GLP_MAX);
  glp_add_cols(lp, (numberofcontandbatchplaces+2*numberofcontandbatchtransitions));
  glp_add_rows(lp, nc);


  // initialising transitions bound
  cout << "\nTransitions bound\n\n";

 //wodes conflict example coefficient.
 // glp_set_obj_coef(lp, 3, 1.0);
 // glp_set_obj_coef(lp, 6, 10.0);
 // glp_set_obj_coef(lp, 8, 20.0);
 // glp_set_obj_coef(lp, 9, 1.0);

//example1 one batch place
 // glp_set_obj_coef(lp, 1, 1);
 // glp_set_obj_coef(lp, 2, 10);

 // glp_set_obj_coef(lp, 3, 10.0);
 // glp_set_obj_coef(lp, 4, 1.0);

 // glp_set_obj_coef(lp, 5, 1.0);
 // glp_set_obj_coef(lp, 6, 10.0);

 // glp_set_obj_coef(lp, 11, 1.0);

 //example1 two batch place
//  glp_set_obj_coef(lp, 1, 1.0);
//  glp_set_obj_coef(lp, 3, 10.0);
// glp_set_obj_coef(lp, 2, 1.0);
// glp_set_obj_coef(lp, 4, 10.0);

// glp_set_obj_coef(lp, 5, 10.0);
//  glp_set_obj_coef(lp, 6, 1.0);
//glp_set_obj_coef(lp, 8, 1.0);
//glp_set_obj_coef(lp, 9, 10.0);
 // glp_set_obj_coef(lp, 7, 1.0);
 // glp_set_obj_coef(lp, 9, 1.0);
//   glp_set_obj_coef(lp, 1, 1.0);
//  glp_set_obj_coef(lp, 3, 10.0);
//  glp_set_obj_coef(lp, 8, 1.0);
//  glp_set_obj_coef(lp, 10, 3.0);

//wodes+tz1+tz2
//glp_set_obj_coef(lp, 10, 10.0);
//glp_set_obj_coef(lp, 11, 1.0);
//glp_set_obj_coef(lp, 4, 5.0);



  for (int  k=beginofcbt; k<endofcbt; k++) {
     glp_set_obj_coef(lp, k+1, 1.0); // set (change) objective coefficient or constant term
     glp_set_col_name(lp, k+1, transitions[k-numberofcontandbatchplaces]->GetName()); // name of the transition
    f = transitions[k-numberofcontandbatchplaces]->GetFlow();
    if (f->GetMaximumFlow() != 0.0)
      glp_set_col_bnds(lp, k+1, GLP_DB, 0.0, f->GetMaximumFlow());//set (change) column bounds
    else
      glp_set_col_bnds(lp, k+1, GLP_FX, 0.0, 0.0);

    cout << transitions[k-numberofcontandbatchplaces]->GetName() << " : 0.0 - " << f->GetMaximumFlow() << endl;
  }

   // initialising quantities bound
  cout << "\nQuantities bound\n\n";
  for (int k=beginofcbp; k<endofcbp; k++) {
    capacity = ((BatchPlace*)places[k])->GetDensity()*((BatchPlace*)places[k])->GetLength();
 //   glp_set_obj_coef(lp, k+1, 0.0); // set (change) objective coefficient or constant term
    glp_set_col_name(lp, k+1, places[k]->GetName()); // name of the transition
    switch (places[k] ->IsA()){
      case Batch_pl: if (capacity != 0.0){
                               glp_set_col_bnds(lp, k+1, GLP_DB, 0.0, capacity);
                               }
                          else{
                               glp_set_col_bnds(lp, k+1, GLP_FX, 0.0, 0.0);
                               }
                               cout << places[k]->GetName() << " : 0.0 - " << capacity<<"\n";
                               continue;

           break;

      case Continuous_pl: capacity = ((BatchPlace*)places[k+numberofcontinuousplaces])->GetDensity()*((BatchPlace*)places[k+numberofcontinuousplaces])->GetLength();
                              glp_set_col_bnds(lp, k+1, GLP_DB, 0.0, capacity);  //maybe revised!
                        cout << places[k]->GetName() << " : 0.0 - "<<capacity<<"\n";
                        continue;
           break;
     }
  }

   cout << "\nMinimal firing quantities bound\n\n";
  for (int k=beginofft; k<endofft; k++) {
  //  glp_set_obj_coef(lp, k+1, 1.0e-6); // set (change) objective coefficient or constant term
    glp_set_col_name(lp, k+1, transitions[k-numberofcontandbatchplaces-numberofcontandbatchtransitions]->GetName()); // name of the transition
    f = transitions[k-numberofcontandbatchplaces-numberofcontandbatchtransitions]->GetFlow();
    if (f->GetMaximumFlow() != 0.0)
      glp_set_col_bnds(lp, k+1, GLP_LO, 0.0, 0.0);//set (change) column bounds
    else
      glp_set_col_bnds(lp, k+1, GLP_FX, 0.0, 0.0);

    cout << transitions[k-numberofcontandbatchplaces-numberofcontandbatchtransitions]->GetName() << " z: >= " << 0.0 << endl;
  }

  r = 1;
  arindex = 1;


  cout << "\nStart contraints\n\n";
  // constraint a
  cout << "Contraint a\n";
  for (int i=1, k=beginofcbt; k<endofcbt; k++, i++) {
    sprintf(rname, "a.%d", i);
    f = transitions[k-numberofcontandbatchplaces]->GetFlow();
    glp_set_row_name(lp, r, rname);
    if (f->GetMaximumFlow() != 0.0)
      glp_set_row_bnds(lp, r, GLP_DB, 0.0, f->GetMaximumFlow());  //compare with steadyflow
    else
      glp_set_row_bnds(lp, r, GLP_FX, 0.0, 0.0);

    ia[arindex] = r;
    ja[arindex] = k+1;
    ar[arindex] = 1.0; // weigth
    arindex++;
    r++;
    cout << rname << " : " << "0.0 <= y <= " <<  f->GetMaximumFlow() << endl;
  }

    // constraint b
  cout << "Contraint b\n";
  for (int i=1, k=beginofbp; k<endofbp; k++, i++) {
    sprintf(rname, "b.%d", i);
    capacity= ((BatchPlace*)places[k])->GetDensity() * ((BatchPlace*)places[k])->GetLength();
      glp_set_row_name(lp, r, rname);
    if (capacity != 0.0 ){
      glp_set_row_bnds(lp, r, GLP_DB, 0.0, capacity);
        }
    else{
      glp_set_row_bnds(lp, r, GLP_FX, 0.0, 0.0);
        }
      ia[arindex] = r;
      ja[arindex] = k+1;
      ar[arindex] = 1.0; // weigth
      arindex++;
      r++;
      cout << rname << " : "  << " 0.0 <= "<< " q" <<ja[arindex-1] << " <= "<<capacity << endl;
    }

  // constraint b'
  cout << "Contraint b\n";
  for (int i=1, k=beginofbp; k<endofbp; k++, i++) {
    sprintf(rname, "b.%d", i);
    glp_set_row_name(lp, r, rname);
    glp_set_row_bnds(lp, r, GLP_LO, 0.0, 0.0);

          ia[arindex] = r;
          ja[arindex] = k+1;
          ar[arindex] = 1.0 ;
          arindex++;

    for (int j=0; j<NumberOfTransitions(); j++) {
          V[j] = 0.0;
    }
    for (int j=0; j<NumberOfTransitions(); j++){
      if (places[k]->GetWeightOfOutputArc(j) != 0.0 ){
          V[j] =  places[k]->GetWeightOfOutputArc(j);
      }
    }
       cout << rname << " : " << "q" <<k+1;


      for (int j=0, l=beginofcbt; l<endofcbt; j++, l++) {
        if (V[j] != 0.0){
          ia[arindex] = r;
          ja[arindex] = l+1;
          ar[arindex] = -V[j] * ((BatchPlace*)places[k])->GetLength() / ((BatchPlace*)places[k])->GetInstantaneousSpeed() ;
          arindex++;
          cout << ar[arindex-1]<<"y * " <<j+1;
        }
      }
      cout  << ">=0.0" <<endl;
      r++;
}

  // Contraint c
  cout << "Contraint c\n";
  for (int i=1, k=beginofbp; k<endofbp; k++, i++) {
   maxflow = ((BatchPlace*)places[k])->GetInstantaneousSpeed() * ((BatchPlace*)places[k])->GetDensity();

    sprintf(rname, "c.%d", i);
    glp_set_row_name(lp, r, rname);
    glp_set_row_bnds(lp, r, GLP_UP, 0.0, maxflow);

    for (int j=0; j<NumberOfTransitions(); j++) {
      V[j] = 0.0;
    }
    for (int j=0; j<NumberOfTransitions(); j++){
      if (places[k]->GetWeightOfInputArc(j) != 0.0 ){
          V[j] =  places[k]->GetWeightOfInputArc(j);
      }
    }
        cout << rname << " : ";
      for (int j=0, l=beginofcbt; l<endofcbt; j++, l++) {
       if (V[j] != 0.0){
          ia[arindex] = r;
          ja[arindex] = l+1;
          ar[arindex] = V[j];
          arindex++;
          cout << "y"<<j+1 << "(" << V[j] << ") + ";
        }
      }
       cout  <<  "<=" << maxflow << endl;
      r++;
  }

  // constraint d

  cout << "Contraint d\n";

  for (int i=1, k=beginofcbp; k<endofcbp; k++, i++) {

      sprintf(rname, "d.%d", i);
      glp_set_row_name(lp, r, rname);
      glp_set_row_bnds(lp, r, GLP_FX, 0.0, 0.0); //look after
      for (int j=0; j<NumberOfTransitions(); j++) {
        V[j] = 0.0;
      }
    cout << rname<< ":";
      for (int j=0; j<NumberOfTransitions(); j++){
        if (places[k]->GetWeightOfOutputArc(j) != 0.0 ){
          V[j] = - places[k]->GetWeightOfOutputArc(j);
   //       cout << " - V [" << j << "] " ;
        }
        if (places[k]->GetWeightOfInputArc(j) != 0.0 ){
   //       cout << " + V [" << j << "] " ;
          V[j] +=  places[k]->GetWeightOfInputArc(j);
        }
        cout << "y"<<j+1 << "(" << V[j] << ") + ";
      }

      for (int j=0, l=beginofcbt; l<endofcbt; j++, l++) {
        if (V[j] != 0.0){
          ia[arindex] = r;
          ja[arindex] = l+1;
          ar[arindex] = V[j];
          arindex++;

        }
      }
      cout << " = 0.0" << endl;
      r++;
    }


  //constraint e
  cout << "Contraint e\n";
   for (int i=1, k=beginofcbp; k<endofcbp; k++, i++) {
       switch (places[k] ->IsA()){
              case Continuous_pl:  quantity_initial =((ContinuousPlace*)places[k])->GetMarks();
                          break;
              case Batch_pl:quantity_initial= ((BatchPlace*)places[k])->GetInitialQuantity();
                          break;
               }
      sprintf(rname, "e.%d", i);
      glp_set_row_name(lp, r, rname);
      glp_set_row_bnds(lp, r, GLP_FX, quantity_initial, quantity_initial);

      ia[arindex] = r;
      ja[arindex] = k+1;
      ar[arindex] = 1.0; // weight
      arindex++;
      cout << rname<< " : "<<"q"<<k+1;
      for (int j=0; j<NumberOfTransitions(); j++) {
        V[j] = 0.0;
      }

      for (int j=0; j<NumberOfTransitions(); j++){
        if (places[k]->GetWeightOfOutputArc(j) != 0.0 ){
          V[j] = - places[k]->GetWeightOfOutputArc(j);
   //       cout << " - V [" << j << "] " ;
        }
        if (places[k]->GetWeightOfInputArc(j) != 0.0 ){
    //      cout << " + V [" << j << "] " ;
          V[j] +=  places[k]->GetWeightOfInputArc(j);
        }
        cout << "+z"<<j+1 << "(" << V[j] << ") ";
      }

      for (int j=0, l=beginofft; l<endofft; j++, l++) {
        if (V[j] != 0.0){
          ia[arindex] = r;
          ja[arindex] = l+1;
          ar[arindex] = -V[j];
          arindex++;
   //       cout << j << "(" << V[j] << ") - ";
        }
      }
      cout << "=" <<quantity_initial<< endl;
      r++;
    }


  glp_term_out(GLP_OFF);
  glp_load_matrix(lp, arindex-1, ia, ja, ar);
  glp_simplex(lp, NULL);

  cout<<"\nThe steady state is : "<<endl;
  z = glp_get_obj_val(lp);



  for (int i=1;i<(numberofcontandbatchplaces+2*numberofcontandbatchtransitions+1); i++){
   SS[i]=glp_get_col_prim(lp, i);
  //SS = calloc(1+numberofcontandbatchplaces+2*numberofcontandbatchtransitions, sizeof(double));
  }

  for (int i=1;i<(numberofcontandbatchplaces+2*numberofcontandbatchtransitions+1); i++){
     printf("z=%lf,SS[i]=%lf\n",z,i, SS[i]);
 //    if (i< (numberofcontandbatchplaces+1)){

//     if (i< (numberofcontandbatchplaces+numberofcontinuoustransitions+1 && i > numberofcontandbatchplaces)){
//        f =  ((ContinuousTransition*)transitions[i+numberofcontandbatchplaces-1])->GetFlow();
//        f->SetTheoreticalFlow(SS[i+numberofcontandbatchplaces]);
//        f->SetCurrentFlow(SS[i+numberofcontandbatchplaces]);
//      }
  }

   //  f =  ((ContinuousTransition*)transitions[k-numberofcontandbatchplaces])->GetFlow();
   //  f->SetTheoreticalFlow(SS[k]);
   //  f->SetCurrentFlow(SS[k]);

  //  cout << transitions[k-numberofcontandbatchplaces]->GetName() << " : " << SS[i] << endl;

  int col[numberofcontandbatchplaces+2*numberofcontandbatchtransitions];
  double val[numberofcontandbatchplaces+2*numberofcontandbatchtransitions];

  /*
  for (int i=1; i<=glp_get_num_rows(lp); i++){
        cout << glp_get_row_name(lp, i) << " : ";
        for (int j=1; j<=glp_get_mat_row(lp, i, col, val); j++){
            cout << col[j] << "(" << val[j] << ") - ";
        }
        cout << glp_get_row_lb(lp, i) << " - " << glp_get_row_ub(lp, i) << endl;
  }
  */
  glp_delete_prob(lp);
  return 0;

}//========================



//==============================================================================
//Compute the periodic steady state with assigned transfer speed
//==============================================================================
//1) q_i \le pre(p_i, \cdot) y.
//  input and output flow of a place is equal to steady flow.
//To do list
// 2) input and output flow is not equal? what happen

void BPN::ComputePeriodicSteadyState()
{

  cout << "\nCompute PeriodicSteadystate\n\n";


  double capacity;
  double quantity_initial;
  double maxflow;
  Flow *f;
  double z;


  // contraints a
  int beginofcbt = numberofbatchplaces+numberofcontinuousplaces+numberofdiscreteplaces+numberofdiscretetransitions;
  int endofcbt   = numberofbatchplaces+numberofcontinuousplaces+numberofdiscreteplaces+numberofdiscretetransitions + numberofcontinuoustransitions + numberofbatchtransitions;
  int numberofcontandbatchtransitions = numberofcontinuoustransitions + numberofbatchtransitions;

  //constraints b and b'
  int beginofbp = numberofdiscreteplaces + numberofcontinuousplaces;
  int endofbp   = numberofdiscreteplaces + numberofcontinuousplaces + numberofbatchplaces;
//  int numberofbatchplaces;

  //constraints c

  //constraints d
  int beginofcbp = numberofdiscreteplaces;
  int endofcbp   = numberofdiscreteplaces + numberofcontinuousplaces + numberofbatchplaces;
  int numberofcontandbatchplaces = numberofcontinuousplaces + numberofbatchplaces;

  //constraints e
  int beginofft  = numberofcontandbatchplaces + numberofcontandbatchtransitions;
  int endofft    = numberofcontandbatchplaces + numberofcontandbatchtransitions + numberofcontandbatchtransitions;

  // set value to all places and transitions
 // int numberofcontinuousplaces;
 // int numberofcontinuoustransitions;

 // if (numberofcontandbatchtransitions == 0 || numberofcontandbatchplaces ==0) // no need to compute CFF
 //   return;


  int nc = 0; // total of contraints
  int r  = 0; // current contraint
  char rname[256]; // row name

  double V[NumberOfTransitions()]; // used to compute the incidence matrix, only continuous and batch positions are used


  // variables to the linear program
  glp_prob *lp;
  double SS[numberofcontandbatchplaces+2*numberofcontandbatchtransitions+1]; // to store the result of the linear program
  //double *SS;

  nc = numberofcontandbatchtransitions; // contraint a
  nc+= numberofbatchplaces;  // contraint b
  nc+= numberofbatchplaces;  // contraint b'
  nc+= numberofbatchplaces;  // contraint c
  nc+= numberofcontandbatchplaces; // contraint d
  nc+= numberofcontandbatchplaces; // contraint e


  // contraints matrix
  int ia[nc * (numberofcontandbatchplaces+2*numberofcontandbatchtransitions)];
  int ja[nc * (numberofcontandbatchplaces+2*numberofcontandbatchtransitions)];
  double ar[nc * (numberofcontandbatchplaces+2*numberofcontandbatchtransitions)];
  int arindex;

  // initialising linear program
  lp = glp_create_prob();
  glp_set_prob_name(lp, "SS");
  glp_set_obj_dir(lp, GLP_MAX);
  glp_add_cols(lp, (numberofcontandbatchplaces+2*numberofcontandbatchtransitions));
  glp_add_rows(lp, nc);


  // initialising transitions bound
  cout << "\nTransitions bound\n\n";

 //wodes conflict example coefficient.
 // glp_set_obj_coef(lp, 3, 1.0);
 // glp_set_obj_coef(lp, 6, 10.0);
 // glp_set_obj_coef(lp, 8, 20.0);
 // glp_set_obj_coef(lp, 9, 1.0);

//example1 one batch place
  glp_set_obj_coef(lp, 1, 1);
  glp_set_obj_coef(lp, 2, 1);

  glp_set_obj_coef(lp, 3, 10.0);
  glp_set_obj_coef(lp, 4, 10.0);

 // glp_set_obj_coef(lp, 5, 1.0);
 // glp_set_obj_coef(lp, 6, 10.0);

 // glp_set_obj_coef(lp, 11, 1.0);

 //example1 two batch place
//  glp_set_obj_coef(lp, 1, 1.0);
//  glp_set_obj_coef(lp, 3, 10.0);
// glp_set_obj_coef(lp, 2, 1.0);
// glp_set_obj_coef(lp, 4, 10.0);

// glp_set_obj_coef(lp, 5, 10.0);
//  glp_set_obj_coef(lp, 6, 1.0);

 // glp_set_obj_coef(lp, 7, 1.0);
 // glp_set_obj_coef(lp, 9, 1.0);
//   glp_set_obj_coef(lp, 1, 1.0);
//  glp_set_obj_coef(lp, 3, 10.0);
//  glp_set_obj_coef(lp, 8, 1.0);
//  glp_set_obj_coef(lp, 10, 3.0);

//wodes+tz1+tz2
//glp_set_obj_coef(lp, 10, 10.0);
//glp_set_obj_coef(lp, 11, 1.0);
//glp_set_obj_coef(lp, 4, 5.0);



  for (int  k=beginofcbt; k<endofcbt; k++) {
     glp_set_obj_coef(lp, k+1, 1.0); // set (change) objective coefficient or constant term
     glp_set_col_name(lp, k+1, transitions[k-numberofcontandbatchplaces]->GetName()); // name of the transition
    f = transitions[k-numberofcontandbatchplaces]->GetFlow();
    if (f->GetMaximumFlow() != 0.0)
      glp_set_col_bnds(lp, k+1, GLP_DB, 0.0, f->GetMaximumFlow());//set (change) column bounds
    else
      glp_set_col_bnds(lp, k+1, GLP_FX, 0.0, 0.0);

    cout << transitions[k-numberofcontandbatchplaces]->GetName() << " : 0.0 - " << f->GetMaximumFlow() << endl;
  }

   // initialising quantities bound
  cout << "\nQuantities bound\n\n";
  for (int k=beginofcbp; k<endofcbp; k++) {
    capacity = ((BatchPlace*)places[k])->GetDensity()*((BatchPlace*)places[k])->GetLength();
 //   glp_set_obj_coef(lp, k+1, 0.0); // set (change) objective coefficient or constant term
    glp_set_col_name(lp, k+1, places[k]->GetName()); // name of the transition
    switch (places[k] ->IsA()){
      case Batch_pl: if (capacity != 0.0){
                               glp_set_col_bnds(lp, k+1, GLP_DB, 0.0, capacity);
                               }
                          else{
                               glp_set_col_bnds(lp, k+1, GLP_FX, 0.0, 0.0);
                               }
                               cout << places[k]->GetName() << " : 0.0 - " << capacity<<"\n";
                               continue;

           break;

      case Continuous_pl: capacity = ((BatchPlace*)places[k+numberofcontinuousplaces])->GetDensity()*((BatchPlace*)places[k+numberofcontinuousplaces])->GetLength();
                              glp_set_col_bnds(lp, k+1, GLP_DB, 0.0, capacity);  //maybe revised!
                        cout << places[k]->GetName() << " : 0.0 - "<<capacity<<"\n";
                        continue;
           break;
     }
  }

   cout << "\nMinimal firing quantities bound\n\n";
  for (int k=beginofft; k<endofft; k++) {
    glp_set_obj_coef(lp, k+1, 1.0e-6); // set (change) objective coefficient or constant term
    glp_set_col_name(lp, k+1, transitions[k-numberofcontandbatchplaces-numberofcontandbatchtransitions]->GetName()); // name of the transition
    f = transitions[k-numberofcontandbatchplaces-numberofcontandbatchtransitions]->GetFlow();
    if (f->GetMaximumFlow() != 0.0)
      glp_set_col_bnds(lp, k+1, GLP_LO, 0.0, 0.0);//set (change) column bounds
    else
      glp_set_col_bnds(lp, k+1, GLP_FX, 0.0, 0.0);

    cout << transitions[k-numberofcontandbatchplaces-numberofcontandbatchtransitions]->GetName() << " z: >= " << 0.0 << endl;
  }

  r = 1;
  arindex = 1;


  cout << "\nStart contraints\n\n";
  // constraint a
  cout << "Contraint a\n";
  for (int i=1, k=beginofcbt; k<endofcbt; k++, i++) {
    sprintf(rname, "a.%d", i);
    f = transitions[k-numberofcontandbatchplaces]->GetFlow();
    glp_set_row_name(lp, r, rname);
    if (f->GetMaximumFlow() != 0.0)
      glp_set_row_bnds(lp, r, GLP_DB, 0.0, f->GetMaximumFlow());  //compare with steadyflow
    else
      glp_set_row_bnds(lp, r, GLP_FX, 0.0, 0.0);

    ia[arindex] = r;
    ja[arindex] = k+1;
    ar[arindex] = 1.0; // weigth
    arindex++;
    r++;
    cout << rname << " : " << "0.0 <= y <= " <<  f->GetMaximumFlow() << endl;
  }

    // constraint b
//  cout << "Contraint b\n";
//  for (int i=1, k=beginofbp; k<endofbp; k++, i++) {
//    sprintf(rname, "b.%d", i);
//    capacity= ((BatchPlace*)places[k])->GetDensity() * ((BatchPlace*)places[k])->GetLength();
//      glp_set_row_name(lp, r, rname);
//    if (capacity != 0.0 ){
//      glp_set_row_bnds(lp, r, GLP_DB, 0.0, capacity);
//        }
//    else{
//      glp_set_row_bnds(lp, r, GLP_FX, 0.0, 0.0);
//        }
//      ia[arindex] = r;
//      ja[arindex] = k+1;
//      ar[arindex] = 1.0; // weigth
//      arindex++;
//      r++;
//      cout << rname << " : "  << " 0.0 <= "<< " q" <<ja[arindex-1] << " <= "<<capacity << endl;
//    }

  // constraint b' // q_i
  cout << "Contraint b\n";
  for (int i=1, k=beginofbp; k<endofbp; k++, i++) {
    sprintf(rname, "b.%d", i);
    glp_set_row_name(lp, r, rname);
    glp_set_row_bnds(lp, r, GLP_UP, 0.0, 0.0);

          ia[arindex] = r;
          ja[arindex] = k+1;
          ar[arindex] = 1.0 ;
          arindex++;

    for (int j=0; j<NumberOfTransitions(); j++) {
          V[j] = 0.0;
    }
    for (int j=0; j<NumberOfTransitions(); j++){
      if (places[k]->GetWeightOfOutputArc(j) != 0.0 ){
          V[j] =  places[k]->GetWeightOfOutputArc(j);
      }
    }
       cout << rname << " : " << "q" <<k+1;


      for (int j=0, l=beginofcbt; l<endofcbt; j++, l++) {
        if (V[j] != 0.0){
          ia[arindex] = r;
          ja[arindex] = l+1;
          ar[arindex] = -V[j] * ((BatchPlace*)places[k])->GetLength() / ((BatchPlace*)places[k])->GetInstantaneousSpeed() ;
          arindex++;
          cout << ar[arindex-1]<<"y * " <<j+1;
        }
      }
      cout  << "<=0.0" <<endl;
      r++;
}

  // Contraint c
  cout << "Contraint c\n";
  for (int i=1, k=beginofbp; k<endofbp; k++, i++) {
   maxflow = ((BatchPlace*)places[k])->GetInstantaneousSpeed() * ((BatchPlace*)places[k])->GetDensity();

    sprintf(rname, "c.%d", i);
    glp_set_row_name(lp, r, rname);
    glp_set_row_bnds(lp, r, GLP_UP, 0.0, maxflow);

    for (int j=0; j<NumberOfTransitions(); j++) {
      V[j] = 0.0;
    }
    for (int j=0; j<NumberOfTransitions(); j++){
      if (places[k]->GetWeightOfInputArc(j) != 0.0 ){
          V[j] =  places[k]->GetWeightOfInputArc(j);
      }
    }
        cout << rname << " : ";
      for (int j=0, l=beginofcbt; l<endofcbt; j++, l++) {
       if (V[j] != 0.0){
          ia[arindex] = r;
          ja[arindex] = l+1;
          ar[arindex] = V[j];
          arindex++;
          cout << "y"<<j+1 << "(" << V[j] << ") + ";
        }
      }
       cout  <<  "<=" << maxflow << endl;
      r++;
  }

  // constraint d

  cout << "Contraint d\n";

  for (int i=1, k=beginofcbp; k<endofcbp; k++, i++) {

      sprintf(rname, "d.%d", i);
      glp_set_row_name(lp, r, rname);
      glp_set_row_bnds(lp, r, GLP_FX, 0.0, 0.0); //look after
      for (int j=0; j<NumberOfTransitions(); j++) {
        V[j] = 0.0;
      }
    cout << rname<< ":";
      for (int j=0; j<NumberOfTransitions(); j++){
        if (places[k]->GetWeightOfOutputArc(j) != 0.0 ){
          V[j] = - places[k]->GetWeightOfOutputArc(j);
   //       cout << " - V [" << j << "] " ;
        }
        if (places[k]->GetWeightOfInputArc(j) != 0.0 ){
   //       cout << " + V [" << j << "] " ;
          V[j] +=  places[k]->GetWeightOfInputArc(j);
        }
        cout << "y"<<j+1 << "(" << V[j] << ") + ";
      }

      for (int j=0, l=beginofcbt; l<endofcbt; j++, l++) {
        if (V[j] != 0.0){
          ia[arindex] = r;
          ja[arindex] = l+1;
          ar[arindex] = V[j];
          arindex++;

        }
      }
      cout << " = 0.0" << endl;
      r++;
    }


  //constraint e
  cout << "Contraint e\n";
   for (int i=1, k=beginofcbp; k<endofcbp; k++, i++) {
       switch (places[k] ->IsA()){
              case Continuous_pl:  quantity_initial =((ContinuousPlace*)places[k])->GetMarks();
                          break;
              case Batch_pl:quantity_initial= ((BatchPlace*)places[k])->GetInitialQuantity();
                          break;
               }
      sprintf(rname, "e.%d", i);
      glp_set_row_name(lp, r, rname);
      glp_set_row_bnds(lp, r, GLP_FX, quantity_initial, quantity_initial);

      ia[arindex] = r;
      ja[arindex] = k+1;
      ar[arindex] = 1.0; // weight
      arindex++;
      cout << rname<< " : "<<"q"<<k+1;
      for (int j=0; j<NumberOfTransitions(); j++) {
        V[j] = 0.0;
      }

      for (int j=0; j<NumberOfTransitions(); j++){
        if (places[k]->GetWeightOfOutputArc(j) != 0.0 ){
          V[j] = - places[k]->GetWeightOfOutputArc(j);
   //       cout << " - V [" << j << "] " ;
        }
        if (places[k]->GetWeightOfInputArc(j) != 0.0 ){
    //      cout << " + V [" << j << "] " ;
          V[j] +=  places[k]->GetWeightOfInputArc(j);
        }
        cout << "+z"<<j+1 << "(" << V[j] << ") ";
      }

      for (int j=0, l=beginofft; l<endofft; j++, l++) {
        if (V[j] != 0.0){
          ia[arindex] = r;
          ja[arindex] = l+1;
          ar[arindex] = -V[j];
          arindex++;
   //       cout << j << "(" << V[j] << ") - ";
        }
      }
      cout << "=" <<quantity_initial<< endl;
      r++;
    }


  glp_term_out(GLP_OFF);
  glp_load_matrix(lp, arindex-1, ia, ja, ar);
  glp_simplex(lp, NULL);

  cout<<"\nThe steady state is : "<<endl;
  z = glp_get_obj_val(lp);



  for (int i=1;i<(numberofcontandbatchplaces+2*numberofcontandbatchtransitions+1); i++){
   SS[i]=glp_get_col_prim(lp, i);
  //SS = calloc(1+numberofcontandbatchplaces+2*numberofcontandbatchtransitions, sizeof(double));
  }

  for (int i=1;i<(numberofcontandbatchplaces+2*numberofcontandbatchtransitions+1); i++){
     printf("z=%lf,SS[i]=%lf\n",z,i, SS[i]);
 //    if (i< (numberofcontandbatchplaces+1)){

//     if (i< (numberofcontandbatchplaces+numberofcontinuoustransitions+1 && i > numberofcontandbatchplaces)){
//        f =  ((ContinuousTransition*)transitions[i+numberofcontandbatchplaces-1])->GetFlow();
//        f->SetTheoreticalFlow(SS[i+numberofcontandbatchplaces]);
//        f->SetCurrentFlow(SS[i+numberofcontandbatchplaces]);
//      }
  }

   //  f =  ((ContinuousTransition*)transitions[k-numberofcontandbatchplaces])->GetFlow();
   //  f->SetTheoreticalFlow(SS[k]);
   //  f->SetCurrentFlow(SS[k]);

  //  cout << transitions[k-numberofcontandbatchplaces]->GetName() << " : " << SS[i] << endl;

  int col[numberofcontandbatchplaces+2*numberofcontandbatchtransitions];
  double val[numberofcontandbatchplaces+2*numberofcontandbatchtransitions];

  /*
  for (int i=1; i<=glp_get_num_rows(lp); i++){
        cout << glp_get_row_name(lp, i) << " : ";
        for (int j=1; j<=glp_get_mat_row(lp, i, col, val); j++){
            cout << col[j] << "(" << val[j] << ") - ";
        }
        cout << glp_get_row_lb(lp, i) << " - " << glp_get_row_ub(lp, i) << endl;
  }
  */
  glp_delete_prob(lp);
  return 0;

}//========================





//==========================Maximal flow based On/OFF control==================
// Compute the controlled firing flow based on maximal flow (MCFF)
//==============================================================================
void BPN::ComputeMCFF()
{
  cout << "\nCompute MCFF\n\n";

  Flow *u;
  double steadyflow;
  double maxflow;       // maximal outflow of a batch place which depends on the density of output batch and the tranfer speed.
  double maxinputflow; //maximal input flow of a batch place which is the product of density and maximal transfer speed.


  // contraints a
  int beginofcbt = numberofdiscretetransitions;
  int endofcbt   = numberofdiscretetransitions + numberofcontinuoustransitions + numberofbatchtransitions;
  int numberofcontandbatchtransitions = numberofcontinuoustransitions + numberofbatchtransitions;

  //constraints b =====>T_z
  int numberofcontandbatchtransitionsthatbelongstotz = 0;
  for (int i = beginofcbt; i<endofcbt; i++){
    if (transitions[i]->IsFiringQuantityOnState() == 0){
    numberofcontandbatchtransitionsthatbelongstotz++; //T_z
    }
  }

  //Verifiy if all transitions belong to tz.
//  if (numberofcontandbatchtransitionsthatbelongstotz == numberofcontandbatchtransitions ){
//    cout<< "T_z is full\n";
//  }

  if (numberofcontandbatchtransitions == 0) // no need to compute MCFF
    return;

  //constraints c    ==>  constraints c are not enabled transition set and T_ZL.
  //  int numberofnotenabledcontandbatchtransitions = 0;
  int numberofnotenabledorlesssteadyandbelongstofiringquantityofcontandbatchtransitions = 0;
  for (int i=beginofcbt; i<endofcbt; i++){
  //  if (transitions[i]->GetState() == 0.0 ){
  //    numberofnotenabledcontandbatchtransitions++;
  //  }
    if ((transitions[i]->GetState() ==  0.0) || ((transitions[i]->IsFiringQuantityOnState() == 0) && (transitions[i]->IsOnState() == 0))){
      numberofnotenabledorlesssteadyandbelongstofiringquantityofcontandbatchtransitions++;   //the cardinality of T_ZL + T_N
    }
  }


   //constraints d  ==> full batch place
   int beginofbp = numberofdiscreteplaces + numberofcontinuousplaces;
   int endofbp = numberofdiscreteplaces + numberofcontinuousplaces + numberofbatchplaces;
   int numberofcontandbatchplaces = numberofcontinuousplaces + numberofbatchplaces;
   int numberoffullbatchplaces = 0;
   for (int i=beginofbp; i<endofbp; i++){
        if (((BatchPlace*)places[i])->IsFull() == 1){
        numberoffullbatchplaces++;
        }
    }

   //constraints e  ==> empty continuous places
   int beginofcp = numberofdiscreteplaces;
   int endofcp = numberofdiscreteplaces + numberofcontinuousplaces;
   int numberofemptycontinuousplaces = 0;
   for (int i=beginofcp; i<endofcp; i++){
        if (((ContinuousPlace*)places[i])->GetMarks() == 0.0 ){
        numberofemptycontinuousplaces++;
        }
    }




//constraints f ==> continuous and batch place whose marking quantity is equal to steady marking quantity (S_E)
   //                                             and its input and output transition belongs to T_Z.

//  int beginofcbp = numberofdiscreteplaces;
//  int endofcbp = numberofdiscreteplaces + numberofcontinuousplaces + numberofbatchplaces;
//  int numberofequalcontandbatchplacesanditsinputoutputtransitionbelongstotz = 0;
//
//  for (int i=beginofcbp; i<endofcbp; i++){
//    int numberofinputandoutputtransitions = 0;
//    int numberofinputandoutputransitionsthatbelongstotz = 0;
//         switch (places[i] ->IsA()){
//           case Continuous_pl:
//                if (((ContinuousPlace*)places[i])->IsEqualSteadyQuantity() == 1){
//                    for (int j=0; j<NumberOfTransitions(); j++) {
//                        V[j] = 0.0;
//                    }
//                    for (int j=0; j<NumberOfTransitions(); j++){
//                        if (places[i]->GetWeightOfOutputArc(j) != 0.0 ){
//                            V[j] = - places[i]->GetWeightOfOutputArc(j);
//                        }
//                        if (places[i]->GetWeightOfInputArc(j) != 0.0 ){
//                            V[j] +=  places[i]->GetWeightOfInputArc(j);
//                        }
//                        if (V[j] !=0 ){
//                            numberofinputandoutputtransitions++;                      // no self loop net
//                        }
//                        if (V[j] !=0 && transitions[j]->IsFiringQuantityOnState() == 0){
//                            numberofinputandoutputransitionsthatbelongstotz++;
//                        }
//                    }
//                    if (numberofinputandoutputtransitions == numberofinputandoutputransitionsthatbelongstotz){
//                        numberofequalcontandbatchplacesanditsinputoutputtransitionbelongstotz++;
//                    }
//                }
//                 break;
//           case Batch_pl:
//                if (((BatchPlace*)places[i])->IsEqualSteadyQuantity() == 1){
//                    for (int j=0; j<NumberOfTransitions(); j++) {
//                        V[j] = 0.0;
//                    }
//                    for (int j=0; j<NumberOfTransitions(); j++){
//                        if (places[i]->GetWeightOfOutputArc(j) != 0.0 ){
//                            V[j] = - places[i]->GetWeightOfOutputArc(j);
//                        }
//                        if (places[i]->GetWeightOfInputArc(j) != 0.0 ){
//                            V[j] +=  places[i]->GetWeightOfInputArc(j);
//                        }
//                        if (V[j] !=0 ){
//                            numberofinputandoutputtransitions++;                      // no self loop net
//                        }
//                        if (V[j] !=0 && transitions[j]->IsFiringQuantityOnState() == 0){
//                            numberofinputandoutputransitionsthatbelongstotz++;
//                        }
//                    }
//                    if (numberofinputandoutputtransitions == numberofinputandoutputransitionsthatbelongstotz){
//                        numberofequalcontandbatchplacesanditsinputoutputtransitionbelongstotz++;
//                    }
//                }
//                 break;
//        }
//    }


//constraint f
  int beginofcbp = numberofdiscreteplaces;
  int endofcbp = numberofdiscreteplaces + numberofcontinuousplaces + numberofbatchplaces;
  int numberofequalcontandbatchplacesanditsinputoutputtransitionbelongstotz = 0;

  for (int i=beginofcbp; i<endofcbp; i++){
         switch (places[i] ->IsA()){
           case Continuous_pl:
                if (((ContinuousPlace*)places[i])->IsSteadyMarkingReachedandInputOutputTransitionBelongTz() == 1){
                        numberofequalcontandbatchplacesanditsinputoutputtransitionbelongstotz++;
                    }
                 break;
           case Batch_pl:
                if (((BatchPlace*)places[i])->IsSteadyMarkingQuantityReachedandInputOutputTransitionBelongTz() == 1){
                        numberofequalcontandbatchplacesanditsinputoutputtransitionbelongstotz++;
                    }
                 break;
        }
    }

 //constraints g and h.



  int nc = 0; // total of contraints
  int r  = 0; // current contraint
  char rname[256]; // row name

  double V[NumberOfTransitions()]; // used to compute the incidence matrix, only continuous and batch positions are used


  // variables to the linear program
  glp_prob *lp;
  double  MCFF[numberofcontandbatchtransitions]; // to store the result of the linear program

  nc = numberofcontandbatchtransitions; // contraint a
  nc+= numberofcontandbatchtransitionsthatbelongstotz; //constraint b
  nc+= numberofnotenabledorlesssteadyandbelongstofiringquantityofcontandbatchtransitions; //constraint c
  nc+= numberoffullbatchplaces;//constraint d
  nc+= numberofemptycontinuousplaces; // constraint e
  nc+= numberofequalcontandbatchplacesanditsinputoutputtransitionbelongstotz; //constaint f
  nc+= numberofbatchplaces; //constaint g
  nc+= numberofbatchplaces; //constaint h


  // contraints matrix
  int ia[nc * numberofcontandbatchtransitions];
  int ja[nc * numberofcontandbatchtransitions];
  double ar[nc * numberofcontandbatchtransitions];
  int arindex;


  // initialising linear program
  lp = glp_create_prob();
  glp_set_prob_name(lp, "MCFF");
  glp_set_obj_dir(lp, GLP_MAX);
  glp_add_cols(lp, numberofcontandbatchtransitions);
  glp_add_rows(lp, nc);

  // initialising transitions bound
  cout << "\nTransitions bound\n\n";
  for (int i=1, k=beginofcbt; k<endofcbt; k++, i++) {
    glp_set_obj_coef(lp, i, 1.0); // set (change) objective coefficient or constant term
    glp_set_col_name(lp, i, transitions[k]->GetName()); // name of the transition
    u = transitions[k]->GetFlow();
    if (u->GetMaximumFlow() != 0.0)
      glp_set_col_bnds(lp, i, GLP_DB, 0.0, u->GetMaximumFlow());//set (change) column bounds
    else
      glp_set_col_bnds(lp, i, GLP_FX, 0.0, 0.0);

    cout << transitions[k]->GetName() << " : 0.0 - " << u->GetMaximumFlow() << endl;
  }


  r = 1;
  arindex = 1;

  cout << "\nStart contraints\n\n";

 // constraint a
   cout << "Contraint a\n";
  for (int i=1, k=beginofcbt; k<endofcbt; k++, i++) {
    sprintf(rname, "a.%d", i);
    u = transitions[k]->GetFlow();
    glp_set_row_name(lp, r, rname);
    if (u->GetMaximumFlow() != 0.0)
      glp_set_row_bnds(lp, r, GLP_DB, 0.0, u->GetMaximumFlow());  //compare with steadyflow
    else
      glp_set_row_bnds(lp, r, GLP_FX, 0.0, 0.0);

    ia[arindex] = r;
    ja[arindex] = i;
    ar[arindex] = 1.0; // weight
    arindex++;
    r++;
    cout << rname << " : " << "0.0 <= x <= " <<  u->GetMaximumFlow() << endl;
  }


  // constraint b     T_Z
  cout << "Contraint b\n";
  for (int i=1, k=beginofcbt; k<endofcbt; k++, i++) {
    if (transitions[k]->IsFiringQuantityOnState() == 0){
        sprintf(rname, "b.%d", i);
        u = transitions[k]->GetFlow();
        glp_set_row_name(lp, r, rname);

        if (u->GetSteadyFlow() != 0.0)
            glp_set_row_bnds(lp, r, GLP_DB, 0.0, u->GetSteadyFlow());  //compare with steadyflow
        else
            glp_set_row_bnds(lp, r, GLP_FX, 0.0, 0.0);


    ia[arindex] = r;
    ja[arindex] = i;
    ar[arindex] = 1.0; // weigth
    arindex++;
    r++;

    cout << rname << " : " << "0.0 <= x <= " <<  u->GetSteadyFlow() << endl;
  }
  }

   //constraint c     not enabled and T_ZL set into zero.
  cout << "Contraint c\n";
  for (int i=1, k=beginofcbt; k<endofcbt; k++, i++){
    if (transitions[k]->GetState() == 0.0 ||
       ((transitions[k]->IsFiringQuantityOnState() == 0 ) &&
        (transitions[k]->IsOnState() ==0))){
      sprintf(rname, "c.%d", i);
      glp_set_row_name(lp, r, rname);
      glp_set_row_bnds(lp, r, GLP_FX, 0.0, 0.0);

      ia[arindex] = r;
      ja[arindex] = i;
      ar[arindex] = 1.0; // weight
      arindex++;
      r++;
      cout << rname << " : " << ja[arindex-1] << ": 0.0 = x = 0.0 " << endl;
    }
  }

  // constraint d    Full batch places
  cout << "Contraint d\n";
  for (int i=1, k=beginofbp; k<endofbp; k++, i++) {
      if (((BatchPlace*)places[k])->IsFull() == 1){
      sprintf(rname, "d.%d", i);
      glp_set_row_name(lp, r, rname);
      glp_set_row_bnds(lp, r, GLP_UP, 0.0, 0.0); //check after
      for (int j=0; j<NumberOfTransitions(); j++) {
        V[j] = 0.0;
      }

      for (int j=0; j<NumberOfTransitions(); j++){
        if (places[k]->GetWeightOfOutputArc(j) != 0.0 ){
          V[j] = - places[k]->GetWeightOfOutputArc(j);
          cout << " - V [" << j << "] " ;
        }
        if (places[k]->GetWeightOfInputArc(j) != 0.0 ){
          cout << " + V [" << j << "] " ;
          V[j] +=  places[k]->GetWeightOfInputArc(j);
        }
      }
      cout << endl;
      cout << rname << " : ";
      for (int j=1, l=beginofcbt; l<endofcbt; j++, l++) {
        if (V[l] != 0.0){
          ia[arindex] = r;
          ja[arindex] = j;
          ar[arindex] = V[l];
          arindex++;
          cout << j << "(" << V[l] << ") - ";
        }
      }
      cout << " <= 0.0" << endl;
      r++;
    }
    }

  // constraint e      Empty continuous place
  cout << "Contraint e\n";
  for (int i=1, k=beginofcp; k<endofcp; k++, i++) {
      if (((ContinuousPlace*)places[k])->GetMarks() == 0.0){
      sprintf(rname, "d.%d", i);
      glp_set_row_name(lp, r, rname);
      glp_set_row_bnds(lp, r, GLP_LO, 0.0, 0.0); //check after
      for (int j=0; j<NumberOfTransitions(); j++) {
        V[j] = 0.0;
      }

      for (int j=0; j<NumberOfTransitions(); j++){
        if (places[k]->GetWeightOfOutputArc(j) != 0.0 ){
          V[j] = - places[k]->GetWeightOfOutputArc(j);
          cout << " - V [" << j << "] " ;
        }
        if (places[k]->GetWeightOfInputArc(j) != 0.0 ){
          cout << " + V [" << j << "] " ;
          V[j] +=  places[k]->GetWeightOfInputArc(j);
        }
      }
      cout << endl;
      cout << rname << " : ";
      for (int j=1, l=beginofcbt; l<endofcbt; j++, l++) {
        if (V[l] != 0.0){
          ia[arindex] = r;
          ja[arindex] = j;
          ar[arindex] = V[l];
          arindex++;
          cout << j << "(" << V[l] << ") - ";
        }
      }
      cout << " >= 0.0" << endl;
      r++;
    }
    }


  //constraint f
  cout << "Contraint f\n";

    for (int i=1, k=beginofcbp; k<endofcbp; k++, i++){
         switch (places[k] ->IsA()){
            case Continuous_pl:
                if (((ContinuousPlace*)places[k])->IsSteadyMarkingReachedandInputOutputTransitionBelongTz() == 0)
                        continue;
                 break;
           case Batch_pl:
                if (((BatchPlace*)places[k])->IsSteadyMarkingQuantityReachedandInputOutputTransitionBelongTz() == 0)
                        continue;
                 break;
        }

//---------------------------------------------------------------------------
//    for (int i=1, k=beginofcbp; k<endofcbp; k++, i++){
//         int numberofinputandoutputtransitions=0;
//         int numberofinputandoutputransitionsthatbelongstotz = 0;
//         switch (places[k] ->IsA()){
//           case Continuous_pl:
//                if (((ContinuousPlace*)places[k])->IsEqualSteadyQuantity() == 1){
//                    for (int j=0; j<NumberOfTransitions(); j++) {
//                        V[j] = 0.0;
//                    }
//                    for (int j=0; j<NumberOfTransitions(); j++){
//                        if (places[k]->GetWeightOfOutputArc(j) != 0.0 ){
//                            V[j] = - places[k]->GetWeightOfOutputArc(j);
//                        }
//                        if (places[k]->GetWeightOfInputArc(j) != 0.0 ){
//                            V[j] +=  places[k]->GetWeightOfInputArc(j);
//                        }
//                        if (V[j] !=0 ){
//                            numberofinputandoutputtransitions++;                      // no self loop net
//                        }
//                        if (V[j] !=0 && transitions[j]->IsFiringQuantityOnState() == 0){
//                            numberofinputandoutputransitionsthatbelongstotz++;
//                        }
//                    }
//                    if (numberofinputandoutputtransitions == numberofinputandoutputransitionsthatbelongstotz){
//                        continue;
//                    }
//                }
//                 break;
//
//           case Batch_pl:
//                if (((BatchPlace*)places[k])->IsEqualSteadyQuantity() == 1){
//                    for (int j=0; j<NumberOfTransitions(); j++) {
//                        V[j] = 0.0;
//                    }
//                    for (int j=0; j<NumberOfTransitions(); j++){
//                        if (places[i]->GetWeightOfOutputArc(j) != 0.0 ){
//                            V[j] = - places[k]->GetWeightOfOutputArc(j);
//                        }
//                        if (places[i]->GetWeightOfInputArc(j) != 0.0 ){
//                            V[j] +=  places[k]->GetWeightOfInputArc(j);
//                        }
//                        if (V[j] !=0 ){
//                            numberofinputandoutputtransitions++;                      // no self loop net
//                        }
//                        if (V[j] !=0 && transitions[j]->IsFiringQuantityOnState() == 0){
//                            numberofinputandoutputransitionsthatbelongstotz++;
//                        }
//                    }
//                    if (numberofinputandoutputtransitions == numberofinputandoutputransitionsthatbelongstotz)
//                        continue;
//                }
//                 break;
//        }
//-------------------------------------------------------------------------------
      sprintf(rname, "f.%d", i);
      glp_set_row_name(lp, r, rname);
      glp_set_row_bnds(lp, r, GLP_FX, 0.0, 0.0);
      for (int j=0; j<NumberOfTransitions(); j++) {
        V[j] = 0.0;
      }

      for (int j=0; j<NumberOfTransitions(); j++){
        if (places[k]->GetWeightOfOutputArc(j) != 0.0 ){
          V[j] = - places[k]->GetWeightOfOutputArc(j);
          cout << " - V [" << j << "] " ;
        }
        if (places[k]->GetWeightOfInputArc(j) != 0.0 ){
          cout << " + V [" << j << "] " ;
          V[j] +=  places[k]->GetWeightOfInputArc(j);
        }
      }
      cout << endl;
      cout << rname << " : ";
      for (int j=1, l=beginofcbt; l<endofcbt; j++, l++) {
        if (V[l] != 0.0){
          ia[arindex] = r;
          ja[arindex] = j;
          ar[arindex] = V[l];
          arindex++;
          cout << j << "(" << V[l] << ") - ";
        }
      }
      cout << " = 0.0" << endl;
      r++;
    }

  //constraint g
  cout << "Contraint g\n";
  for (int i=1, k=beginofbp; k<endofbp; k++, i++){
    maxinputflow = ((BatchPlace*)places[k])->GetMaxSpeed() * ((BatchPlace*)places[k])->GetDensity();
    sprintf(rname, "g.%d", i);
    glp_set_row_name(lp, r, rname);
    glp_set_row_bnds(lp, r, GLP_UP, 0.0, maxinputflow);

    for (int j=0; j<NumberOfTransitions(); j++) {
      V[j] = 0.0;
    }
    for (int j=0; j<NumberOfTransitions(); j++){
      if (places[k]->GetWeightOfInputArc(j) != 0.0 ){
          V[j] =  places[k]->GetWeightOfInputArc(j);
      }
    }
      cout << rname << " : ";
    for (int j=1, l=beginofcbt; l<endofcbt; j++, l++) {
        if (V[l] != 0.0){
          ia[arindex] = r;
          ja[arindex] = j;
          ar[arindex] = V[l];
          arindex++;
          cout << j << "(" << V[l] << ") - ";
        }
      }
      cout << "0.0 <= x <= " << maxinputflow << endl;
      r++;
      }

   // constraint h
  cout << "Contraint h\n";

  for (int i=1, k=beginofbp; k<endofbp; k++, i++) {
    maxflow = ((BatchPlace*)places[k])->GetMaxSpeed() * ((BatchPlace*)places[k])->GetDensityOutputBatch();
    sprintf(rname, "h.%d", i);
    glp_set_row_name(lp, r, rname);
    if (maxflow != 0.0)
      glp_set_row_bnds(lp, r, GLP_UP, 0.0, maxflow);
    else
      glp_set_row_bnds(lp, r, GLP_FX, 0.0, 0.0);

    for (int j=0; j<NumberOfTransitions(); j++) {
      V[j] = 0.0;
    }
    for (int j=0; j<NumberOfTransitions(); j++){
      if (places[k]->GetWeightOfOutputArc(j) != 0.0 ){
          V[j] =  places[k]->GetWeightOfOutputArc(j);
      }
    }
      cout << rname << " : ";
      for (int j=1, l=beginofcbt; l<endofcbt; j++, l++) {
        if (V[l] != 0.0){
          ia[arindex] = r;
          ja[arindex] = j;
          ar[arindex] = V[l];
          arindex++;
          cout << j << "(" << V[l] << ") - ";
        }
      }
      cout << "0.0 <= x <= " << maxflow << endl;
      r++;
}

  glp_term_out(GLP_OFF);
  glp_load_matrix(lp, arindex-1, ia, ja, ar);
  glp_simplex(lp, NULL);

  for (int i=0, k=beginofcbt; k<endofcbt; k++, i++) {
    MCFF[i] = glp_get_col_prim(lp, (i+1));
    u = transitions[k]->GetFlow();  //???continuous transition class?
    u->SetTheoreticalFlow(MCFF[i]);
    u->SetCurrentFlow(MCFF[i]);

  //  cout << transitions[k]->GetName() << " : " << MCFF[i] << endl;
  }

  int col[numberofcontandbatchtransitions];
  double val[numberofcontandbatchtransitions];

//   for (int i=1; i<=glp_get_num_rows(lp); i++){
//        cout << glp_get_row_name(lp, i) << " : ";
//        for (int j=1; j<=glp_get_mat_row(lp, i, col, val); j++){
//            cout << col[j] << "(" << val[j] << ") - ";
//        }
//        cout << glp_get_row_lb(lp, i) << " - " << glp_get_row_ub(lp, i) << endl;
//  }

  glp_delete_prob(lp);
////=======================================================================
}



void BPN::ComputeMSCFF()
{
  cout << "\nCompute MCFF\n\n";

  Flow *u;
  double steadyflow;
  double maxflow;       // maximal outflow of a batch place which depends on the density of output batch and the tranfer speed.
  double maxinputflow; //maximal input flow of a batch place which is the product of density and maximal transfer speed.


  // contraints a
  int beginofcbt = numberofdiscretetransitions;
  int endofcbt   = numberofdiscretetransitions + numberofcontinuoustransitions + numberofbatchtransitions;
  int numberofcontandbatchtransitions = numberofcontinuoustransitions + numberofbatchtransitions;

  //constraints b =====>T_z
  int numberofcontandbatchtransitionsthatbelongstotz = 0;
  for (int i = beginofcbt; i<endofcbt; i++){
    if (transitions[i]->IsFiringQuantityOnState() == 0){
    numberofcontandbatchtransitionsthatbelongstotz++; //T_z
    }
  }

  if (numberofcontandbatchtransitions == 0) // no need to compute MCFF
    return;

  //constraints c    ==>  constraints c are not enabled transition set and T_ZL.
//  int numberofnotenabledcontandbatchtransitions = 0;
  int numberofnotenabledorlesssteadyandbelongstofiringquantityofcontandbatchtransitions = 0;
  for (int i=beginofcbt; i<endofcbt; i++){
  //  if (transitions[i]->GetState() == 0.0 ){
  //    numberofnotenabledcontandbatchtransitions++;
  //  }
    if ((transitions[i]->GetState() ==  0.0) || ((transitions[i]->IsFiringQuantityOnState() == 0) && (transitions[i]->IsOnState() == 0))){
      numberofnotenabledorlesssteadyandbelongstofiringquantityofcontandbatchtransitions++;   //the cardinality of T_ZL + T_N
    }
  }


   //constraints d  ==> full batch place
   int beginofbp = numberofdiscreteplaces + numberofcontinuousplaces;
   int endofbp = numberofdiscreteplaces + numberofcontinuousplaces + numberofbatchplaces;
   int numberofcontandbatchplaces = numberofcontinuousplaces + numberofbatchplaces;
   int numberoffullbatchplaces = 0;
   for (int i=beginofbp; i<endofbp; i++){
        if (((BatchPlace*)places[i])->IsFull() == 1){
        numberoffullbatchplaces++;
        }
    }

   //constraints e  ==> empty continuous places
   int beginofcp = numberofdiscreteplaces;
   int endofcp = numberofdiscreteplaces + numberofcontinuousplaces;
   int numberofemptycontinuousplaces = 0;
   for (int i=beginofcp; i<endofcp; i++){
        if (((ContinuousPlace*)places[i])->GetMarks() == 0.0 ){
        numberofemptycontinuousplaces++;
        }
    }




//constraints f ==> continuous and batch place whose marking quantity is equal to steady marking quantity (S_E)
   //                                             and its input and output transition belongs to T_Z.


//constraints f //// 09/04 last version
//  int beginofcbp = numberofdiscreteplaces;
//  int endofcbp = numberofdiscreteplaces + numberofcontinuousplaces + numberofbatchplaces;
//  int numberofequalcontandbatchplacesanditsinputoutputtransitionbelongstotz = 0;
//
//  for (int i=beginofcbp; i<endofcbp; i++){
//    int numberofinputandoutputtransitions = 0;
//    int numberofinputandoutputransitionsthatbelongstotz = 0;
//         switch (places[i] ->IsA()){
//           case Continuous_pl:
//                if (((ContinuousPlace*)places[i])->IsEqualSteadyQuantity() == 1){
//                    for (int j=0; j<NumberOfTransitions(); j++) {
//                        V[j] = 0.0;
//                    }
//                    for (int j=0; j<NumberOfTransitions(); j++){
//                        if (places[i]->GetWeightOfOutputArc(j) != 0.0 ){
//                            V[j] = - places[i]->GetWeightOfOutputArc(j);
//                        }
//                        if (places[i]->GetWeightOfInputArc(j) != 0.0 ){
//                            V[j] +=  places[i]->GetWeightOfInputArc(j);
//                        }
//                        if (V[j] !=0 ){
//                            numberofinputandoutputtransitions++;                      // no self loop net
//                        }
//                        if (V[j] !=0 && transitions[j]->IsFiringQuantityOnState() == 0){
//                            numberofinputandoutputransitionsthatbelongstotz++;
//                        }
//                    }
//                    if (numberofinputandoutputtransitions == numberofinputandoutputransitionsthatbelongstotz){
//                        numberofequalcontandbatchplacesanditsinputoutputtransitionbelongstotz++;
//                    }
//                }
//                 break;
//           case Batch_pl:
//                if (((BatchPlace*)places[i])->IsEqualSteadyQuantity() == 1){
//                    for (int j=0; j<NumberOfTransitions(); j++) {
//                        V[j] = 0.0;
//                    }
//                    for (int j=0; j<NumberOfTransitions(); j++){
//                        if (places[i]->GetWeightOfOutputArc(j) != 0.0 ){
//                            V[j] = - places[i]->GetWeightOfOutputArc(j);
//                        }
//                        if (places[i]->GetWeightOfInputArc(j) != 0.0 ){
//                            V[j] +=  places[i]->GetWeightOfInputArc(j);
//                        }
//                        if (V[j] !=0 ){
//                            numberofinputandoutputtransitions++;                      // no self loop net
//                        }
//                        if (V[j] !=0 && transitions[j]->IsFiringQuantityOnState() == 0){
//                            numberofinputandoutputransitionsthatbelongstotz++;
//                        }
//                    }
//                    if (numberofinputandoutputtransitions == numberofinputandoutputransitionsthatbelongstotz){
//                        numberofequalcontandbatchplacesanditsinputoutputtransitionbelongstotz++;
//                    }
//                }
//                 break;
//        }
//    }


//constraint f
  int beginofcbp = numberofdiscreteplaces;
  int endofcbp = numberofdiscreteplaces + numberofcontinuousplaces + numberofbatchplaces;
  int numberofequalcontandbatchplacesanditsinputoutputtransitionbelongstotz = 0;

  for (int i=beginofcbp; i<endofcbp; i++){
         switch (places[i] ->IsA()){
           case Continuous_pl:
                if (((ContinuousPlace*)places[i])->IsSteadyMarkingReachedandInputOutputTransitionBelongTz() == 1){
                        numberofequalcontandbatchplacesanditsinputoutputtransitionbelongstotz++;
                    }
                 break;
           case Batch_pl:
                if (((BatchPlace*)places[i])->IsSteadyMarkingQuantityReachedandInputOutputTransitionBelongTz() == 1){
                        numberofequalcontandbatchplacesanditsinputoutputtransitionbelongstotz++;
                    }
                 break;
        }
    }







 //constraints g and h.



  int nc = 0; // total of contraints
  int r  = 0; // current contraint
  char rname[256]; // row name

  double V[NumberOfTransitions()]; // used to compute the incidence matrix, only continuous and batch positions are used


  // variables to the linear program
  glp_prob *lp;
  double  MSCFF[numberofcontandbatchtransitions]; // to store the result of the linear program

  nc = numberofcontandbatchtransitions; // contraint a
  nc+= numberofcontandbatchtransitionsthatbelongstotz; //constraint b
  nc+= numberofnotenabledorlesssteadyandbelongstofiringquantityofcontandbatchtransitions; //constraint c
  nc+= numberoffullbatchplaces;//constraint d
  nc+= numberofemptycontinuousplaces; // constraint e
  nc+= numberofequalcontandbatchplacesanditsinputoutputtransitionbelongstotz; //constaint f
  nc+= numberofbatchplaces; //constaint g
  nc+= numberofbatchplaces; //constaint h


  // contraints matrix
  int ia[nc * numberofcontandbatchtransitions];
  int ja[nc * numberofcontandbatchtransitions];
  double ar[nc * numberofcontandbatchtransitions];
  int arindex;


  // initialising linear program
  lp = glp_create_prob();
  glp_set_prob_name(lp, "MSCFF");
  glp_set_obj_dir(lp, GLP_MAX);
  glp_add_cols(lp, numberofcontandbatchtransitions);
  glp_add_rows(lp, nc);

  // initialising transitions bound
  cout << "\nTransitions bound\n\n";
  for (int i=1, k=beginofcbt; k<endofcbt; k++, i++) {
    glp_set_obj_coef(lp, i, 1.0); // set (change) objective coefficient or constant term
    glp_set_col_name(lp, i, transitions[k]->GetName()); // name of the transition
    u = transitions[k]->GetFlow();
    if (u->GetMaximumFlow() != 0.0)
      glp_set_col_bnds(lp, i, GLP_DB, 0.0, u->GetMaximumFlow());//set (change) column bounds
    else
      glp_set_col_bnds(lp, i, GLP_FX, 0.0, 0.0);

    cout << transitions[k]->GetName() << " : 0.0 - " << u->GetMaximumFlow() << endl;
  }


  r = 1;
  arindex = 1;

  cout << "\nStart contraints\n\n";

 // constraint a
   cout << "Contraint a\n";
  for (int i=1, k=beginofcbt; k<endofcbt; k++, i++) {
    sprintf(rname, "a.%d", i);
    u = transitions[k]->GetFlow();
    glp_set_row_name(lp, r, rname);
    if (u->GetMaximumFlow() != 0.0)
      glp_set_row_bnds(lp, r, GLP_DB, 0.0, u->GetMaximumFlow());  //compare with steadyflow
    else
      glp_set_row_bnds(lp, r, GLP_FX, 0.0, 0.0);

    ia[arindex] = r;
    ja[arindex] = i;
    ar[arindex] = 1.0; // weight
    arindex++;
    r++;
    cout << rname << " : " << "0.0 <= x <= " <<  u->GetMaximumFlow() << endl;
  }


  // constraint b     T_Z
  cout << "Contraint b\n";
  for (int i=1, k=beginofcbt; k<endofcbt; k++, i++) {
    if (transitions[k]->IsFiringQuantityOnState() == 0){
        sprintf(rname, "b.%d", i);
        u = transitions[k]->GetFlow();
        glp_set_row_name(lp, r, rname);

        if (u->GetSteadyFlow() != 0.0)
            glp_set_row_bnds(lp, r, GLP_DB, 0.0, u->GetSteadyFlow());  //compare with steadyflow
        else
            glp_set_row_bnds(lp, r, GLP_FX, 0.0, 0.0);


    ia[arindex] = r;
    ja[arindex] = i;
    ar[arindex] = 1.0; // weigth
    arindex++;
    r++;

    cout << rname << " : " << "0.0 <= x <= " <<  u->GetSteadyFlow() << endl;
  }
  }

   //constraint c
  cout << "Contraint c\n";
  for (int i=1, k=beginofcbt; k<endofcbt; k++, i++){
    if (transitions[k]->GetState() == 0.0 ||
       ((transitions[k]->IsFiringQuantityOnState() == 0 ) &&
        (transitions[k]->IsOnState() ==0))){
      sprintf(rname, "c.%d", i);
      glp_set_row_name(lp, r, rname);
      glp_set_row_bnds(lp, r, GLP_FX, 0.0, 0.0);

      ia[arindex] = r;
      ja[arindex] = i;
      ar[arindex] = 1.0; // weight
      arindex++;
      r++;
      cout << rname << " : " << ja[arindex-1] << ": 0.0 = x = 0.0 " << endl;
    }
  }

  // constraint d
  cout << "Contraint d\n";
  for (int i=1, k=beginofbp; k<endofbp; k++, i++) {
      if (((BatchPlace*)places[k])->IsFull() == 1){
      sprintf(rname, "d.%d", i);
      glp_set_row_name(lp, r, rname);
      glp_set_row_bnds(lp, r, GLP_UP, 0.0, 0.0); //check after
      for (int j=0; j<NumberOfTransitions(); j++) {
        V[j] = 0.0;
      }

      for (int j=0; j<NumberOfTransitions(); j++){
        if (places[k]->GetWeightOfOutputArc(j) != 0.0 ){
          V[j] = - places[k]->GetWeightOfOutputArc(j);
          cout << " - V [" << j << "] " ;
        }
        if (places[k]->GetWeightOfInputArc(j) != 0.0 ){
          cout << " + V [" << j << "] " ;
          V[j] +=  places[k]->GetWeightOfInputArc(j);
        }
      }
      cout << endl;
      cout << rname << " : ";
      for (int j=1, l=beginofcbt; l<endofcbt; j++, l++) {
        if (V[l] != 0.0){
          ia[arindex] = r;
          ja[arindex] = j;
          ar[arindex] = V[l];
          arindex++;
          cout << j << "(" << V[l] << ") - ";
        }
      }
      cout << " <= 0.0" << endl;
      r++;
    }
    }



  // constraint e
  cout << "Contraint e\n";
  for (int i=1, k=beginofcp; k<endofcp; k++, i++) {
      if (((ContinuousPlace*)places[k])->GetMarks() == 0.0){
      sprintf(rname, "d.%d", i);
      glp_set_row_name(lp, r, rname);
      glp_set_row_bnds(lp, r, GLP_LO, 0.0, 0.0); //check after
      for (int j=0; j<NumberOfTransitions(); j++) {
        V[j] = 0.0;
      }

      for (int j=0; j<NumberOfTransitions(); j++){
        if (places[k]->GetWeightOfOutputArc(j) != 0.0 ){
          V[j] = - places[k]->GetWeightOfOutputArc(j);
          cout << " - V [" << j << "] " ;
        }
        if (places[k]->GetWeightOfInputArc(j) != 0.0 ){
          cout << " + V [" << j << "] " ;
          V[j] +=  places[k]->GetWeightOfInputArc(j);
        }
      }
      cout << endl;
      cout << rname << " : ";
      for (int j=1, l=beginofcbt; l<endofcbt; j++, l++) {
        if (V[l] != 0.0){
          ia[arindex] = r;
          ja[arindex] = j;
          ar[arindex] = V[l];
          arindex++;
          cout << j << "(" << V[l] << ") - ";
        }
      }
      cout << " >= 0.0" << endl;
      r++;
    }
    }



  //constraint f
  cout << "Contraint f\n";

    for (int i=1, k=beginofcbp; k<endofcbp; k++, i++){
         switch (places[k] ->IsA()){
            case Continuous_pl:
                if (((ContinuousPlace*)places[k])->IsSteadyMarkingReachedandInputOutputTransitionBelongTz() == 0)
                        continue;
                 break;
           case Batch_pl:
                if (((BatchPlace*)places[k])->IsSteadyMarkingQuantityReachedandInputOutputTransitionBelongTz() == 0)
                        continue;
                 break;
        }

//---------------------------------------------------------------------------
//    for (int i=1, k=beginofcbp; k<endofcbp; k++, i++){
//         int numberofinputandoutputtransitions=0;
//         int numberofinputandoutputransitionsthatbelongstotz = 0;
//         switch (places[k] ->IsA()){
//           case Continuous_pl:
//                if (((ContinuousPlace*)places[k])->IsEqualSteadyQuantity() == 1){
//                    for (int j=0; j<NumberOfTransitions(); j++) {
//                        V[j] = 0.0;
//                    }
//                    for (int j=0; j<NumberOfTransitions(); j++){
//                        if (places[k]->GetWeightOfOutputArc(j) != 0.0 ){
//                            V[j] = - places[k]->GetWeightOfOutputArc(j);
//                        }
//                        if (places[k]->GetWeightOfInputArc(j) != 0.0 ){
//                            V[j] +=  places[k]->GetWeightOfInputArc(j);
//                        }
//                        if (V[j] !=0 ){
//                            numberofinputandoutputtransitions++;                      // no self loop net
//                        }
//                        if (V[j] !=0 && transitions[j]->IsFiringQuantityOnState() == 0){
//                            numberofinputandoutputransitionsthatbelongstotz++;
//                        }
//                    }
//                    if (numberofinputandoutputtransitions == numberofinputandoutputransitionsthatbelongstotz){
//                        continue;
//                    }
//                }
//                 break;
//
//           case Batch_pl:
//                if (((BatchPlace*)places[k])->IsEqualSteadyQuantity() == 1){
//                    for (int j=0; j<NumberOfTransitions(); j++) {
//                        V[j] = 0.0;
//                    }
//                    for (int j=0; j<NumberOfTransitions(); j++){
//                        if (places[i]->GetWeightOfOutputArc(j) != 0.0 ){
//                            V[j] = - places[k]->GetWeightOfOutputArc(j);
//                        }
//                        if (places[i]->GetWeightOfInputArc(j) != 0.0 ){
//                            V[j] +=  places[k]->GetWeightOfInputArc(j);
//                        }
//                        if (V[j] !=0 ){
//                            numberofinputandoutputtransitions++;                      // no self loop net
//                        }
//                        if (V[j] !=0 && transitions[j]->IsFiringQuantityOnState() == 0){
//                            numberofinputandoutputransitionsthatbelongstotz++;
//                        }
//                    }
//                    if (numberofinputandoutputtransitions == numberofinputandoutputransitionsthatbelongstotz)
//                        continue;
//                }
//                 break;
//        }
//-------------------------------------------------------------------------------
      sprintf(rname, "f.%d", i);
      glp_set_row_name(lp, r, rname);
      glp_set_row_bnds(lp, r, GLP_FX, 0.0, 0.0);
      for (int j=0; j<NumberOfTransitions(); j++) {
        V[j] = 0.0;
      }

      for (int j=0; j<NumberOfTransitions(); j++){
        if (places[k]->GetWeightOfOutputArc(j) != 0.0 ){
          V[j] = - places[k]->GetWeightOfOutputArc(j);
          cout << " - V [" << j << "] " ;
        }
        if (places[k]->GetWeightOfInputArc(j) != 0.0 ){
          cout << " + V [" << j << "] " ;
          V[j] +=  places[k]->GetWeightOfInputArc(j);
        }
      }
      cout << endl;
      cout << rname << " : ";
      for (int j=1, l=beginofcbt; l<endofcbt; j++, l++) {
        if (V[l] != 0.0){
          ia[arindex] = r;
          ja[arindex] = j;
          ar[arindex] = V[l];
          arindex++;
          cout << j << "(" << V[l] << ") - ";
        }
      }
      cout << " = 0.0" << endl;
      r++;
    }

  //constraint g
  cout << "Contraint g\n";
  for (int i=1, k=beginofbp; k<endofbp; k++, i++){
    maxinputflow = ((BatchPlace*)places[k])->GetMaxSpeed() * ((BatchPlace*)places[k])->GetDensity();
    sprintf(rname, "g.%d", i);
    glp_set_row_name(lp, r, rname);
    glp_set_row_bnds(lp, r, GLP_UP, 0.0, maxinputflow);

    for (int j=0; j<NumberOfTransitions(); j++) {
      V[j] = 0.0;
    }
    for (int j=0; j<NumberOfTransitions(); j++){
      if (places[k]->GetWeightOfInputArc(j) != 0.0 ){
          V[j] =  places[k]->GetWeightOfInputArc(j);
      }
    }
      cout << rname << " : ";
    for (int j=1, l=beginofcbt; l<endofcbt; j++, l++) {
        if (V[l] != 0.0){
          ia[arindex] = r;
          ja[arindex] = j;
          ar[arindex] = V[l];
          arindex++;
          cout << j << "(" << V[l] << ") - ";
        }
      }
      cout << "0.0 <= x <= " << maxinputflow << endl;
      r++;
      }



   // constraint h
  cout << "Contraint h\n";

  for (int i=1, k=beginofbp; k<endofbp; k++, i++) {
    maxflow = ((BatchPlace*)places[k])->GetMaxSpeed() * ((BatchPlace*)places[k])->GetDensityOutputBatch();
    sprintf(rname, "h.%d", i);
    glp_set_row_name(lp, r, rname);
    if (maxflow != 0.0)
      glp_set_row_bnds(lp, r, GLP_UP, 0.0, maxflow);
    else
      glp_set_row_bnds(lp, r, GLP_FX, 0.0, 0.0);

    for (int j=0; j<NumberOfTransitions(); j++) {
      V[j] = 0.0;
    }
    for (int j=0; j<NumberOfTransitions(); j++){
      if (places[k]->GetWeightOfOutputArc(j) != 0.0 ){
          V[j] =  places[k]->GetWeightOfOutputArc(j);
      }
    }
      cout << rname << " : ";
      for (int j=1, l=beginofcbt; l<endofcbt; j++, l++) {
        if (V[l] != 0.0){
          ia[arindex] = r;
          ja[arindex] = j;
          ar[arindex] = V[l];
          arindex++;
          cout << j << "(" << V[l] << ") - ";
        }
      }
      cout << "0.0 <= x <= " << maxflow << endl;
      r++;
}

  glp_term_out(GLP_OFF);
  glp_load_matrix(lp, arindex-1, ia, ja, ar);
  glp_simplex(lp, NULL);

  for (int i=0, k=beginofcbt; k<endofcbt; k++, i++) {
    MSCFF[i] = glp_get_col_prim(lp, (i+1));
    u = transitions[k]->GetFlow();  //???continuous transition class?
    u->SetTheoreticalFlow(MSCFF[i]);
    u->SetCurrentFlow(MSCFF[i]);

  //  cout << transitions[k]->GetName() << " : " << MCFF[i] << endl;
  }

  int col[numberofcontandbatchtransitions];
  double val[numberofcontandbatchtransitions];

//   for (int i=1; i<=glp_get_num_rows(lp); i++){
//        cout << glp_get_row_name(lp, i) << " : ";
//        for (int j=1; j<=glp_get_mat_row(lp, i, col, val); j++){
//            cout << col[j] << "(" << val[j] << ") - ";
//        }
//        cout << glp_get_row_lb(lp, i) << " - " << glp_get_row_ub(lp, i) << endl;
//  }

  glp_delete_prob(lp);
////=======================================================================
}
