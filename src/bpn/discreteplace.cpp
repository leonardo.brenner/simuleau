//====================================================================================//
//                                                                                    //
//                     Discrete place classes inherit of place class                  //
//                                                                                    //
//====================================================================================//
//  This File:   discreteplace.cpp                Language: C++  (xlC and CC)         //
//  Software:    SIMULEAU                                                             //
//====================================================================================//
//  Creation:    22/apr/16                        by: Leonardo.Brenner@lsis.org       //
//  Last Change: 22/apr/16                        by: Leonardo.Brenner@lsis.org       //
//====================================================================================//
#include "simuleau.h"


//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
// Methods of the Discrete Place class
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Constructor
//------------------------------------------------------------------------------
DiscretePlace::DiscretePlace():Place()
{
  reservedmarks    = 0;
  nonreservedmarks = 0;
}

//------------------------------------------------------------------------------
// Named Constructor
//------------------------------------------------------------------------------
DiscretePlace::DiscretePlace(simuleau_name _name):Place(_name)
{
  reservedmarks    = 0;
  nonreservedmarks = 0;
}


//------------------------------------------------------------------------------
// Destructor
//------------------------------------------------------------------------------
DiscretePlace::~DiscretePlace()
{

}


//------------------------------------------------------------------------------
// Copy
//------------------------------------------------------------------------------
void DiscretePlace::Copy(const DiscretePlace * _place)
{
  TransferNode(_place);

  marks            = _place->marks;
  initialmarks     = _place->initialmarks;
  steadymarks      = _place->steadymarks;
  reservedmarks    = _place->reservedmarks;
  nonreservedmarks = _place->nonreservedmarks;

  conflict           = _place->conflict;
  structuralconflict = _place->structuralconflict;

}

//------------------------------------------------------------------------------
// It set the initial marking of the place
//------------------------------------------------------------------------------
void DiscretePlace::SetInitialMarking(int m)
{
  marks = initialmarks = m;
  reservedmarks    = 0;
  nonreservedmarks = m;
}

//------------------------------------------------------------------------------
// It set the steady marking of the place
//------------------------------------------------------------------------------
void DiscretePlace::SetSteadyMarking(int m)
{
  steadymarks = m;
}

//------------------------------------------------------------------------------
// It changes the place marks
//------------------------------------------------------------------------------
void DiscretePlace::ChangeMarks(int m)
{
  marks = nonreservedmarks = m;
  if (abs(m) < PRF::prf.Min_Err()){
    marks = nonreservedmarks = 0;
  }
}


//------------------------------------------------------------------------------
// It reserves the place marks
//------------------------------------------------------------------------------
void DiscretePlace::ReserveMarks(int m)
{
  reservedmarks+=m;
  nonreservedmarks = marks - reservedmarks;
}


//------------------------------------------------------------------------------
// It removes the reserved marks in the place
//------------------------------------------------------------------------------
void DiscretePlace::RemoveReservedMarks(int m)
{
  reservedmarks-=m;
  marks = nonreservedmarks + reservedmarks;
}

//------------------------------------------------------------------------------
// It adds the marks to the place
//------------------------------------------------------------------------------
void DiscretePlace::AddMarks(int m)
{
  nonreservedmarks += m;
  marks += m;
}

//------------------------------------------------------------------------------
// Compute the state of the discrete place
//------------------------------------------------------------------------------
void DiscretePlace::ComputeState()
{
  int j = 0;

  if (reservedmarks != 0){
    state = 0.5;
  }
  else{
    if (GetNumberOfOutputArcs() == 0){
      state = 1.0;
    }
    else{
      for (int i=0; i<numberofarcs; i++){
        if (outputarcs[i] != 0.0){
          if (nonreservedmarks < outputarcs[i]){
//            cout << "nonreservedmarks: " << nonreservedmarks << "  outputarcs: " << outputarcs[i]  << endl;
            j++;
          }
        }
      }
      if (j == 0)
        state = 1.0;
      else
        state = 0.0;
    }
  }
}

//------------------------------------------------------------------------------
// It verifies effectif conflits
//------------------------------------------------------------------------------
int DiscretePlace::VerifyConflict()
{
  int necessarymarks = 0;
  int numberoftransitionsinconflict = 0;
  Transition *t;

  conflict = 0;

  if (structuralconflict){
    if (GetNumberOfOutputArcs() != 0){
      for (int i=0; i<numberofarcs; i++){
        if (outputarcs[i] != 0){
          t = outputnodes[i];
          //t = simulation->sbpn->GetTransition(i);
          if ((t->GetState()==1) && (t->IsA() == Discrete_tr)) {
            necessarymarks+= outputarcs[i];
            numberoftransitionsinconflict++;
          }
        }
      }

/* Ancienne version a conserver
          represente le vrai conflit effectif
          c-a-d au moins 2 trans aval validees
	  avec marquage de la place necessaire pour les franchissements
          mais actuellement le simulateur ne peut traiter ce cas !!!!
			if (marqueNecessaire <= marqueNonReservee)
				conflitEffec =0;
			else
				conflitEffec =1;
 */

      if (numberoftransitionsinconflict > 1){
        if (necessarymarks <= nonreservedmarks)
          conflict = 0;
        else
          conflict = 1;

        return (conflict);
      }
    }
  }
  return (0);
}


//------------------------------------------------------------------------------
// Print
//------------------------------------------------------------------------------
void DiscretePlace::Print(ostream &fout)
{
  fout << "Name:                   " << name << endl;
  fout << "    Type:               " << "Discrete" << endl;
  fout << "    Marks:              " << marks << endl;
  fout << "    Initial Marks:      " << initialmarks << endl;
  if (steadymarks != 0)
    fout << "    Steady Marks:     " << steadymarks << endl;
  fout << "    Reserved Marks:     " << reservedmarks << endl;
  fout << "    Non Reserved Marks: " << nonreservedmarks << endl;
  fout << "    Arcs:               " << endl;
  PrintArcs(fout);
}

//------------------------------------------------------------------------------
// Write
//------------------------------------------------------------------------------
void DiscretePlace::Write(ostream &fout)
{
  fout << "Name:              " << name << endl;
  fout << "   Type:           " << "Discrete" << endl;
  fout << "   Marks:          " << marks << endl;
  fout << "   Reserved Marks: " << reservedmarks << endl;
//  if (steadymarks != 0)
//    fout << "    Steady Marks:     " << steadymarks << endl;
}
