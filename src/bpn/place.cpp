//====================================================================================//
//                                                                                    //
//                          Place classes inherit of node class                       //
//                                                                                    //
//====================================================================================//
//  This File:   place.cpp                        Language: C++  (xlC and CC)         //
//  Software:    SIMULEAU                                                             //
//====================================================================================//
//  Creation:    22/apr/16                        by: Leonardo.Brenner@lsis.org       //
//  Last Change: 22/apr/16                        by: Leonardo.Brenner@lsis.org       //
//====================================================================================//
#include "simuleau.h"

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
// Methods of the Place class
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Constructor
//------------------------------------------------------------------------------
Place::Place()
{
  marks        = 0;
  initialmarks = 0;
  steadymarks  = -1;

  conflict           = 0;
  structuralconflict = 0;
}

//------------------------------------------------------------------------------
// Named Constructor
//------------------------------------------------------------------------------
Place::Place(simuleau_name _name):Node(_name)
{
  marks        = 0;
  initialmarks = 0;
  steadymarks  = -1;

  conflict           = 0;
  structuralconflict = 0;
}


//------------------------------------------------------------------------------
// Destructor
//------------------------------------------------------------------------------
Place::~Place()
{
  //dtor
}

//------------------------------------------------------------------------------
// Copy
//------------------------------------------------------------------------------
void Place::Copy(const Place * _place)
{
  TransferNode(_place);

  marks        = _place->marks;
  initialmarks = _place->initialmarks;
  steadymarks  = _place->steadymarks;

  conflict           = _place->conflict;
  structuralconflict = _place->structuralconflict;
}

//------------------------------------------------------------------------------
// It set the initial marking of the place
//------------------------------------------------------------------------------
void Place::SetInitialMarking(int m)
{
  marks = initialmarks = m;
}

//------------------------------------------------------------------------------
// It set the steady marking of the place
//------------------------------------------------------------------------------
void Place::SetSteadyMarking(int m)
{
  steadymarks = m;
}


//------------------------------------------------------------------------------
// It returns the current marks
//------------------------------------------------------------------------------
int Place::GetMarks()
{
  return (marks);
}

//------------------------------------------------------------------------------
// It researches structural conflits
//------------------------------------------------------------------------------
void Place::ResearchStructuralConflict()
{
  int numberofoutputarcs = GetNumberOfOutputArcs();

  if(numberofoutputarcs <= 1)
    structuralconflict = 0;
  else
    structuralconflict = numberofoutputarcs;
}

//------------------------------------------------------------------------------
// It returns structural conflits
//------------------------------------------------------------------------------
int Place::GetStructuralConflict()
{
  return (structuralconflict);
}

//------------------------------------------------------------------------------
// It verifies effectif conflits
// returns 0 by default - Batch places don't have conflits
//------------------------------------------------------------------------------
int Place::VerifyConflict()
{
  return (0);
}



//------------------------------------------------------------------------------
// Print
//------------------------------------------------------------------------------
void Place::Print(ostream &fout)
{
  fout << "Name:              " << name << endl;
  fout << "    Arcs:          " << endl;
  PrintArcs(fout);
}

//------------------------------------------------------------------------------
// Write
//------------------------------------------------------------------------------
void Place::Write(ostream &fout)
{
  fout << "Name:              " << name << endl;
  fout << "   Marks:          " << marks << endl;
//  if (steadymarks != 0)
//    fout << "   Steady Marks: " << steadymarks << endl;
}
