//====================================================================================//
//                                                                                    //
//                                      Flow class                                    //
//                                                                                    //
//====================================================================================//
//  This File:   flow.h                           Language: C++  (xlC and CC)         //
//  Software:    SIMULEAU                                                             //
//====================================================================================//
//  Creation:    29/apr/16                        by: Leonardo.Brenner@lsis.org       //
//  Last Change: 29/apr/16                        by: Leonardo.Brenner@lsis.org       //
//====================================================================================//
#ifndef FLOW_H
#define FLOW_H

class Flow
{
  public:
    Flow();
    virtual ~Flow();

    void Copy(const Flow *_flow);

    // input functions
    void SetAllFlows(double _flow);
    void SetInitialFlow(double _flow);
    void SetCurrentFlow(double _flow);
    void SetTheoreticalFlow(double _flow);
    void SetMaximumFlow(double _flow);
    void SetSteadyFlow(double _flow);


    // output functions
    double GetCurrentFlow();
    double GetTheoreticalFlow();
    double GetMaximumFlow();
    double GetSteadyFlow();


    //print
    void Print(ostream &fout);

  protected:
    double maximumflow;
    double theoreticalflow;
    double currentflow;
    double initialflow;
    double steadyflow;

  private:
};

#endif // FLOW_H
