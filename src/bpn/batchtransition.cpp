
//====================================================================================//
//                                                                                    //
//           Batch transition classes inherit of continuous transition class          //
//                                                                                    //
//====================================================================================//
//  This File:   batchtransition.cpp              Language: C++  (xlC and CC)         //
//  Software:    SIMULEAU                                                             //
//====================================================================================//
//  Creation:    22/apr/16                        by: Leonardo.Brenner@lsis.org       //
//  Last Change: 22/apr/16                        by: Leonardo.Brenner@lsis.org       //
//====================================================================================//
#include "simuleau.h"

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
// Methods of the Batch Transition class
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Constructor
//------------------------------------------------------------------------------
BatchTransition::BatchTransition():ContinuousTransition()
{
  inputtheoreticalflow  = -1.0;
  outputtheoreticalflow = -1.0;
}

//------------------------------------------------------------------------------
// Initialized Constructor
//------------------------------------------------------------------------------
BatchTransition::BatchTransition(simuleau_name _name, double _flow, double _steadyflow):ContinuousTransition(_name, _flow, _steadyflow)
{
  inputtheoreticalflow  = -1.0;
  outputtheoreticalflow = -1.0;
}

//------------------------------------------------------------------------------
// Destructor
//------------------------------------------------------------------------------
BatchTransition::~BatchTransition()
{
 if (flow)
  delete flow;
}

//------------------------------------------------------------------------------
// Copy
//------------------------------------------------------------------------------
void BatchTransition::Copy(const BatchTransition *_transition)
{
  TransferNode(_transition);
  flow = new Flow;
  flow->Copy(_transition->flow);                 // transition flows
  firingquantity = new FiringQuantity;
  firingquantity->Copy(_transition->firingquantity);
  inputtheoreticalflow  = _transition->inputtheoreticalflow;
  outputtheoreticalflow = _transition->outputtheoreticalflow;
}

//------------------------------------------------------------------------------
// It returns the input theoretical flow
//------------------------------------------------------------------------------
double BatchTransition::GetInputTheoreticalFlow()
{
  return (inputtheoreticalflow);
}

//------------------------------------------------------------------------------
// It returns the output theoretical flow
//------------------------------------------------------------------------------
double BatchTransition::GetOutputTheoreticalFlow()
{
  return (outputtheoreticalflow);
}

//------------------------------------------------------------------------------
// It computes the input theoretical flow
//------------------------------------------------------------------------------
void BatchTransition::ComputeInputTheoreticalFlow()
{
  BatchPlace *p;
  list<Batch>::iterator b;
  list<Batch> *placemarks;

  double placespeed;
  double placestate;


  if (GetNumberOfInputArcs() == 0){
    inputtheoreticalflow = -1.0; // no input place, infinite flow
    return;
  }

  for (int i=0; i<numberofarcs; i++){
    if (inputarcs[i] != 0.0){
      p = inputnodes[i];
      switch (p->IsA()){
        case Batch_pl:
        case Triangular_pl:  placestate = p->GetState();
                        if (placestate == 1.0){ // test for output batch
                          placespeed = p->GetInstantaneousSpeed();
                          placemarks = p->GetMarking();
                          b = placemarks->begin();
                          inputtheoreticalflow = b->GetOutputFlow(placespeed);
                        }
                        else{
                          inputtheoreticalflow = 0.0;
                        }
                        i = numberofarcs;
        break;
        default: inputtheoreticalflow = -1.0;
        break;
      }
    }
  }
}

//------------------------------------------------------------------------------
// It computes the output theoretical flow
// TODO verify why only one conection is used
//------------------------------------------------------------------------------
void BatchTransition::ComputeOutputTheoreticalFlow()
{
  BatchPlace *p;
  double placespeed;
  double placedensity;

  if (GetNumberOfOutputArcs() == 0){
    outputtheoreticalflow = -1.0; // no input place, infinite flow
    return;
  }

  outputtheoreticalflow = -1.0;
  for (int i=0; i<numberofarcs; i++){
    if (outputarcs[i] != 0.0){
      p = outputnodes[i];
      switch (p->IsA()){
        case Batch_pl:
        case Triangular_pl:  placespeed   = p->GetSpeed();
                        placedensity = p->GetDensity();
                        outputtheoreticalflow = placedensity * placespeed;
        break;
      }
    }
  }
}


//------------------------------------------------------------------------------
// It computes the transition theoretical flow
//------------------------------------------------------------------------------
void BatchTransition::ComputeTheoreticalFlow()
{
  double flowvalue;

  if (state == 0.0){
    flow->SetTheoreticalFlow(0.0);
    return;
  }

  ComputeInputTheoreticalFlow();
  ComputeOutputTheoreticalFlow();
  if (state == 1.0){
    flowvalue = ComputeMinimumFlow(flow->GetMaximumFlow(), inputtheoreticalflow);
    flowvalue = ComputeMinimumFlow(flowvalue, outputtheoreticalflow);
  }
  else{
    flowvalue = ComputeMinimumFlow(flow->GetMaximumFlow(), inputtheoreticalflow);
    flowvalue = ComputeMinimumFlow(flowvalue, outputtheoreticalflow);
    flowvalue = ComputeMinimumFlow(flowvalue, ComputeTheoreticalFlowOfPlace());
  }
  flow->SetTheoreticalFlow(flowvalue);
}


//------------------------------------------------------------------------------
// Print
//------------------------------------------------------------------------------
void BatchTransition::Print(ostream &fout)
{
  fout << "Name:              " << name << endl;
  fout << "    Type:          " << "Batch" << endl;
  fout << "    Flow:          " << endl;
  flow->Print(fout);
  fout << "    Arcs:          " << endl;
  PrintArcs(fout);
}

//------------------------------------------------------------------------------
// It writes the transition flows
//------------------------------------------------------------------------------
void BatchTransition::Write(ostream &fout)
{
  fout << "Name:              " << name << endl;
  fout << "    Type:          " << "Batch" << endl;
  fout << "    State:         " << state << endl;
  fout << "    Current Flow:  " << flow->GetCurrentFlow() << endl;
  if (flow->GetSteadyFlow() != -1.0)
  fout << "    Steady Flow:   " << flow->GetSteadyFlow() << endl;
}


//========================RBF========Event=================================
// It gets the rbf event. (a new )  objective :
//===========================================================================
double BatchTransition::GetRBFEvent()
{
  double date;
  double min_date=-1.0;
  double weight;
  double transitionflow;
  Place *p;
//  Transition *t;
//  FiringQuantity *q;
  double Zs;
  double z;

 // q=t->GetFiringQuantity();
  Zs=firingquantity->GetSteadyFiringQuantity();
  z =firingquantity->GetCurrentFiringQuantity();

  if (GetNumberOfOutputArcs() == 0){
     return (-1.0);
  }
  else if(z >= Zs){
     return (-1.0);
  }
  else{
    for (int i=0; i<numberofarcs; i++){
      if (outputarcs[i] != 0){
//        p = simulation->sbpn->GetPlace(i);
        p = (Place*)outputnodes[i];

          weight =  outputarcs[i];
  //        f=t->GetFlow();
          transitionflow=flow->GetCurrentFlow();
        switch (p->IsA()){
          case Continuous_pl :
                              date = (Zs-z) / transitionflow; // FreeSteadyMarkingQuantity= 0 in P^C
                              date += simulation->stime->GetCurrentDate();

               break;
          case Batch_pl :
                              date = (Zs-z -(((BatchPlace*)p)->GetFreeSteadyQuantity() / weight)) / transitionflow;
                              if (date < 0){
                                  return (-1.0);
                              }
                              else{
                                  date += simulation->stime->GetCurrentDate();

                                  }
              break;

        }

        if (date < min_date || min_date==-1.0){
                                 min_date= date;
          }
      }


    }
    return (min_date);
    }
  return (-1.0);
}
//-----------------------------------------------------------------------------------
