//====================================================================================//
//                                                                                    //
//                                      Node class                                    //
//                                                                                    //
//====================================================================================//
//  This File:   node.h                           Language: C++  (xlC and CC)         //
//  Software:    SIMULEAU                                                             //
//====================================================================================//
//  Creation:    21/apr/16                        by: Leonardo.Brenner@lsis.org       //
//  Last Change: 21/apr/16                        by: Leonardo.Brenner@lsis.org       //
//====================================================================================//
#ifndef NODE_H
#define NODE_H

class Node
{
  public:
    Node();
    Node(simuleau_name _name);
    virtual ~Node();

    void Copy(const Node *_node);

    // input functions
    int AllocArcs(int n);       // number of input/output arcs and nodes
    int AddInputArc(int pos, double weight);  // add input arc in the position pos
    int AddOutputArc(int pos, double weight); // add output arc in the position pos
    int AddInputNodes(Node **nodes);          // add input node pointers
    int AddOutputNodes(Node **nodes);         // add output node pointers

    void SetState(double _state);

    // output functions
    char * GetName();           // get the node name
    double GetState();          // get the current node state
    int    GetNumberOfInputArcs(); // get the number of input arc different of 0
    int    GetNumberOfOutputArcs(); // get the number of output arc different of 0
    double GetWeightOfInputArc(int _arc); // Get the weight of an input arc
    double GetWeightOfOutputArc(int _arc); // Get the weight of an output arc

    // print functions
    virtual void Print(ostream &fout);       // print node informations
    void PrintArcs(ostream &fout);           // print arcs

  protected:
    int numberofarcs;           // number of arcs to this node
//    int numberofoutputarcs;     // number of output arcs from this node
    double *inputarcs;             // array of input arcs
    double *outputarcs;            // array of output arcs
    Node  **inputnodes;            // pointers to input nodes
    Node  **outputnodes;           // pointers to output nodes

    simuleau_name name;         // node name
    double        state;        // node state

    void TransferNode(const Node *_node);

  private:
};

#endif // NODE_H
