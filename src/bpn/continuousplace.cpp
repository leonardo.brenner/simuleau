//====================================================================================//
//                                                                                    //
//                    Continuous place classes inherit of place class                 //
//                                                                                    //
//====================================================================================//
//  This File:   continuousplace.cpp              Language: C++  (xlC and CC)         //
//  Software:    SIMULEAU                                                             //
//====================================================================================//
//  Creation:    22/apr/16                        by: Leonardo.Brenner@lsis.org       //
//  Last Change: 22/apr/16                        by: Leonardo.Brenner@lsis.org       //
//====================================================================================//
#include "simuleau.h"

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
// Methods of the Continuous Place class
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Constructor
//------------------------------------------------------------------------------
ContinuousPlace::ContinuousPlace()
{

}

//------------------------------------------------------------------------------
// Named Constructor
//------------------------------------------------------------------------------
ContinuousPlace::ContinuousPlace(simuleau_name _name):Place(_name)
{

}

//------------------------------------------------------------------------------
// Destructor
//------------------------------------------------------------------------------
ContinuousPlace::~ContinuousPlace()
{

}


//------------------------------------------------------------------------------
// Copy
//------------------------------------------------------------------------------
void ContinuousPlace::Copy(const ContinuousPlace * _place)
{
  TransferNode(_place);

  marks            = _place->marks;
  initialmarks     = _place->initialmarks;
  steadymarks      = _place->steadymarks;
  reservedmarks    = _place->reservedmarks;
  nonreservedmarks = _place->nonreservedmarks;

  conflict           = _place->conflict;
  structuralconflict = _place->structuralconflict;

}

//------------------------------------------------------------------------------
// It set the initial marking of the place
//------------------------------------------------------------------------------
void ContinuousPlace::SetInitialMarking(double m)
{
  marks = initialmarks = nonreservedmarks = m;
  reservedmarks = 0;
}


//-----------------------------------------------------------------------------
//It returns the initial marking of a place
//----------------------------------------------------------------------------
double ContinuousPlace::GetInitialMarks()
{
 return(initialmarks);
}


//------------------------------------------------------------------------------
// It set the steady marking of the place
//------------------------------------------------------------------------------
void ContinuousPlace::SetSteadyMarking(double m)
{
  steadymarks = m;
}

//------------------------------------------------------------------------------

// It returns the steady marking of the place
//----------------------------------------------------------------------
double ContinuousPlace::GetSteadyMarks()
{
  return (steadymarks);
}

//------------------------------------------------------------------------------
// It returns the place marks
//------------------------------------------------------------------------------
double ContinuousPlace::GetMarks()
{
  return (marks);
}

//------------------------------------------------------------------------------
// It changes the place marks
//------------------------------------------------------------------------------
void ContinuousPlace::ChangeMarks(double m)
{
  marks = nonreservedmarks = m;
  if (abs(m) < PRF::prf.Min_Err()){
    marks = nonreservedmarks = 0.0;
  }
}


//------------------------------------------------------------------------------
// It reduces the place marks
//------------------------------------------------------------------------------
void ContinuousPlace::ReduceMarks(double m)
{
  nonreservedmarks -= m;
  marks = nonreservedmarks + reservedmarks;
  if (abs(marks) < PRF::prf.Min_Err()){
    marks = nonreservedmarks = reservedmarks = 0.0;
  }
}

//------------------------------------------------------------------------------
// It reserve the place marks
//------------------------------------------------------------------------------
void ContinuousPlace::ReserveMarks(double m)
{
  reservedmarks += m;
  nonreservedmarks = marks - reservedmarks;
}

//------------------------------------------------------------------------------
// It remove the reserved marks in the place
//------------------------------------------------------------------------------
void ContinuousPlace::RemoveReservedMarks(double m)
{
  reservedmarks -= m;
  marks = nonreservedmarks + reservedmarks;
  if (abs(marks) < PRF::prf.Min_Err()){
    marks = nonreservedmarks = reservedmarks = 0.0;
  }
}

//------------------------------------------------------------------------------
// It adds the marks to the place
//------------------------------------------------------------------------------
void ContinuousPlace::AddMarks(double m)
{
  marks += m;
  nonreservedmarks = marks -reservedmarks;

  /*if (nonreservedmarks < 0.0){
		nonreservedmarks = 0.0;
		marks = nonreservedmarks + reservedmarks;
  }
	if (marks < 0.0){
		marks = 0.0;
		nonreservedmarks = 0.0;
		reservedmarks = 0.0;
  }*/
}

//------------------------------------------------------------------------------
// It compute the continuous place state
// TODO verfiy method and definition 9 in Demongodin thesis
//------------------------------------------------------------------------------
void ContinuousPlace::ComputeState()
{
  int counter = 0;
  Transition *t;

  if (GetNumberOfOutputArcs() == 0){
    state = 0.0;
  }
  else{
    for (int i=0;  i<numberofarcs; i++){
      if (outputarcs[i] != 0.0){
//        t = simulation->sbpn->GetTransition(i);
        t = outputnodes[i];
      }
    }

    switch (t->IsA()){
      case Continuous_tr:
      case Batch_tr:      if (nonreservedmarks > 0.0){
                            state = 1.0; // continuous place not empty
                          }
                          else{
                            state = PlaceIsFed(); // in case of an empty place
                          }
           break;
      case Discrete_tr: if (reservedmarks != 0.0){
                          state = 0.5;
                        }
                        else{ // there is no reserved marks
                          for (int i=0;  i<numberofarcs; i++){
                            if ((outputarcs[i] != 0.0 ) && (nonreservedmarks < (outputarcs[i] - PRF::prf.Min_Err()))){
                              counter++;
                            }
                          }
                          if (counter == 0){
                            state = 1.0;
                          }
                          else{
                            state = 0.0;
                          }
                        }
            break;
    }
  }

  //cout << "\nstate of a continuous place : " << state << endl;
}


//------------------------------------------------------------------------------
// It evolves the marks in the place
//------------------------------------------------------------------------------
void ContinuousPlace::EvolveMarks(double _date)
{
  VerifyMarks();
}

//------------------------------------------------------------------------------
// It evolves the marks in the place
//------------------------------------------------------------------------------
void ContinuousPlace::VerifyMarks()
{
  if ((nonreservedmarks < 0.0) || ((nonreservedmarks < PRF::prf.Min_Err()) && (nonreservedmarks >  - PRF::prf.Min_Err()))){
		nonreservedmarks = 0.0;
		marks = nonreservedmarks + reservedmarks;
  }
	if (((marks < PRF::prf.Min_Err()) && (marks > - PRF::prf.Min_Err())) || (marks < 0.0)){
		marks = 0.0;
		nonreservedmarks = 0.0;
		reservedmarks = 0.0;
  }
}




//------------------------------------------------------------------------------
// It verifies effectif conflits
//------------------------------------------------------------------------------
int ContinuousPlace::VerifyConflict()
{
  int    necessarymarks = 0;
  int    numberoftransitionsinconflict = 0;
  double currentflow = 0.0;

  Transition *t;
  Flow *f;

  conflict = 0;

  if (structuralconflict){
    if (GetNumberOfOutputArcs() == 0){            //why the output arcs are zero when it has a strucral conflict ----29/01/2020
      for (int i=0; i<numberofarcs; i++){
        if (outputarcs[i] != 0){
//          t = simulation->sbpn->GetTransition(i);
          t = outputnodes[i];
          if ((t->GetState()!=1)) {
            f = t->GetFlow();
            if (f->GetCurrentFlow()){
              numberoftransitionsinconflict++;
            }
          }
        }
      }

      if (numberoftransitionsinconflict > 1){
        conflict = 2;
        return (2);
      }
    }
  }
  return (0);
}


//------------------------------------------------------------------------------
// It computes if the place is fed (has an input flow)
//------------------------------------------------------------------------------
double ContinuousPlace::PlaceIsFed()
{
  double pre = 0.0;
  double transitionstate;

  Transition *t;

  if (GetNumberOfInputArcs() == 0){
    return (Not_fed);
  }
  for (int i=0; i<numberofarcs; i++){
    if (inputarcs[i] != 0.0){
//      t = simulation->sbpn->GetTransition(i);
      t = inputnodes[i];
      switch (t->IsA()){
        case Continuous_tr:
        case Batch_tr:      transitionstate = t->GetState();
                            pre = max(pre, transitionstate);
             break;
      }
    }
  }
  if (pre == 0.0){
    return (Not_fed);
  }
  else{
    return (Fed);
  }
}

//==========================WOdes================================================
//------------------------------------------------------------------------------
// It returns true if the continuous place reaches steady marking quantity
//------------------------------------------------------------------------------
int ContinuousPlace::IsEqualSteadyQuantity()
{
  if (abs(steadymarks -  marks) < PRF::prf.Min_Err())
    return 1;
  return 0;
 }

//------------------------------------------------------------------------------
// It returns true if the marking quantity of continuous place is greater than steady marking quantity
//------------------------------------------------------------------------------
int ContinuousPlace::IsGreaterThanSteadyQuantity()
{
  if ((marks - steadymarks) > PRF::prf.Min_Err())
    return 1;
  return 0;
}

//------------------------------------------------------------------------------
// It returns true if the marking quantity of continuous place is less than steady marking quantity
//------------------------------------------------------------------------------
int ContinuousPlace::IsLessThanSteadyQuantity()
{
  if ((abs(marks - steadymarks) > PRF::prf.Min_Err()) && ((marks - steadymarks) < 0.0))
    return 1;
  return 0;

}

//------------------------------------------------------------------------------
// It returns true if the marking of continuous place is equal than steady marking
//------------------------------------------------------------------------------
int ContinuousPlace::IsSteadyMarkingReached()
{
  if (abs(steadymarks -  marks) < PRF::prf.Min_Err())
    return 1;
  return 0;

}
//=============================================================================

//===================Vecos==================================================
// It returns ture if the marking of continuous place is equal to steady marking
// and all of its input and output transition belongs to T_Z.
//==========================================================================
int ContinuousPlace::IsSteadyMarkingReachedandInputOutputTransitionBelongTz()
{
    Transition *t;
    double weight;
    int placestate =0;
    int counter = 0;
    int countertz=0;


    if (abs(steadymarks -  marks) < PRF::prf.Min_Err()){
        if (GetNumberOfOutputArcs() == 0){
            return 1; // the conservative net and consistent net
        }
        else{
            for (int i=0;  i<numberofarcs; i++){
                if (outputarcs[i] != 0.0){
//        t = simulation->sbpn->GetTransition(i);
                    t=outputnodes[i];
                        counter++;
                }
                if (outputarcs[i] != 0.0 && t->IsFiringQuantityOnState()==0)
                        countertz++;

            }
            }

        if (GetNumberOfInputArcs() == 0){
            return 1; // the conservative net and consistent net
        }
        else{
            for (int i=0;  i<numberofarcs; i++){
                if (inputarcs[i] != 0.0){
//        t = simulation->sbpn->GetTransition(i);
                        t=inputnodes[i];
                        counter++;
                }
                if (inputarcs[i] != 0.0 && t->IsFiringQuantityOnState()==0)
                        countertz++;

            }
            }
        if (counter == countertz)
                return 1;
            return placestate;
    }
    return placestate;
}

//------------------------------------------------------------------------------
// It computes the input flow of a fed place
//------------------------------------------------------------------------------
double ContinuousPlace::ComputeFlowFedPlace(ContinuousTransition *ct)
{
  Transition *t;
  Flow *f = ct->GetFlow();
  double fedplaceflow = f->GetTheoreticalFlow();
  double transitionflow;
	double preflow  = 0.0;
	double postflow = 0.0;


	if (fedplaceflow < 0.0){ // infinite flow
    return (fedplaceflow);
	}

	if (GetNumberOfInputArcs() == 0){
    preflow = 0.0 ;
	}
	else{
    for (int i=0; i<numberofarcs; i++){
      if (inputarcs[i] != 0.0){
//        t = simulation->sbpn->GetTransition(i);
        t = inputnodes[i];
        switch(t->IsA()){
          case Continuous_tr:
          case Batch_tr:      f = t->GetFlow();
                              transitionflow = f->GetTheoreticalFlow();
                              if (transitionflow < 0.0){ // infinite flow
                                postflow = -1.0;
                                i = numberofarcs;
                              }
                              else{
                                postflow += (inputarcs[i] * transitionflow);
                              }
               break;
        }
      }
    }
	}

	// if the input flow isn't infinite, compute the output flow
	if (postflow < 0.0){
    return (postflow);
	}
	else{
    if (GetNumberOfOutputArcs() != 0){
      for (int i=0; i<numberofarcs; i++){
        if (outputarcs[i] != 0.0){
//          t = simulation->sbpn->GetTransition(i);
          t = outputnodes[i];
          switch(t->IsA()){
            case Continuous_tr:
            case Batch_tr:      f = t->GetFlow();
                                transitionflow = f->GetTheoreticalFlow();
                                if (transitionflow < 0.0){ // infinite flow
                                  preflow = -1.0;
                                  i = numberofarcs;
                                }
                                else{
                                  preflow += (outputarcs[i] * transitionflow);
                                }
                  break;
          }
        }
      }
    }
	}

	if (preflow < 0.0){ // infinite flow
    return (-1.0);
	}
	else{
   fedplaceflow = fedplaceflow - preflow + postflow;
   return (fedplaceflow);
	}
}



//------------------------------------------------------------------------------
// It computes the date of the next event to Continuous Place becomes Enabled type
// return -1 for no event and event date otherwise
//------------------------------------------------------------------------------
double ContinuousPlace::GetCPEEvent()
{
  Transition *t;
  Flow       *f;
  double     transitionflow = 0.0;
  double     weightflow = 0.0;
  double     eventdate = -2.0;

  if (GetNumberOfOutputArcs()){
    for (int i=0; i<numberofarcs; i++){
      if (outputarcs[i] != 0.0){
//        t = simulation->sbpn->GetTransition(i);
        t = outputnodes[i];
        switch (t->IsA()){
          case Continuous_tr:
          case Batch_tr:  f = t->GetFlow();
                          transitionflow = f->GetCurrentFlow();
                          if (transitionflow <= 0.0) { // infinit flow
                            eventdate = -1.0;
                            i = numberofarcs; // stop loop for
                          }
                          else{
                            weightflow -= (outputarcs[i] * transitionflow);
                          }
               break;
        }
      }
    }
  }

  if (eventdate ==  -1.0){
    return (eventdate);
  }
  else{
    if (GetNumberOfInputArcs()){
      for (int i=0; i<numberofarcs; i++){
        if (inputarcs[i] != 0){
//          t = simulation->sbpn->GetTransition(i);
          t = inputnodes[i];
          switch (t->IsA()){
            case Continuous_tr:
            case Batch_tr:  f = t->GetFlow();
                            transitionflow = f->GetCurrentFlow();
                            if (transitionflow > 0.0) { // non infinit flow
                              weightflow += (inputarcs[i] * transitionflow);
                            }
                 break;
          }
        }
      }
    }
  }
  if ((weightflow != 0.0)){
    eventdate = simulation->stime->GetCurrentDate() - (nonreservedmarks/weightflow);
  }
  return (eventdate);
}


//------------------------------------------------------------------------------
// It computes the date of the next event to Discrete Transistion becomes Enabled type
//------------------------------------------------------------------------------
double ContinuousPlace::GetDTEEvent()
{
  Transition *t;
  double     eventdate = -1.0;
  double     result;

  if (GetNumberOfOutputArcs()){
    for (int i=0; i<numberofarcs; i++){
      if (outputarcs[i] != 0){
//        t = simulation->sbpn->GetTransition(i);
        t = outputnodes[i];
        switch (t->IsA()){
        // cas où la transition aval est une transition discrete -----
        // ATTENTION !!!!!!
        // SUPPOSE QUE TOUTE PLACE CONTINUE A
        // UNE ET UNE SEULE TRANISTION DISCRETE AVAL
          case Discrete_tr: result = ComputeDateDTEEvent(t, outputarcs[i]);
                            if ((result <= 0.0) && (result != simulation->stime->GetCurrentDate())){ // infinit flow
                              eventdate = -1.0;
                              i =  numberofarcs;
                            }
                            else{
                              eventdate = result;
                            }
               break;
        }
      }
    }
  }
  return (eventdate);
}


//-----------------On off Code-------------------------------------------------
// It computes the date of the next event to marking quantity of a batch place
// reaches its steady marking quantity (Place reaches Steady marking Quantity).
//------------------------------------------------------------------------------
/*double ContinuousPlace::GetPSQEvent()
{
  double steadymarks; // we need input this value before the simulation. !!!
  double marks;
  double date;
  double outputtransitionflow = 0;
  Transition *t;
  Flow *u;
  InFlow *I;

    if (abs(marks - steadymarks) < PRF::prf.Min_Err()){
      return (date);
    }
    else{
        for (int i=0; i<numberofarcs; int j=0; j<numberofarcs; i++,j++){
          if (outputarcs[i] != 0.0 || inputarcs[j] != 0.0){
            t = outputnodes[i];
            m = inputnodes[j];
            switch (t->IsA()){
              case Batch_tr:  u = t->GetFlow();
                              I = m->GetFlow();
                              inputtransitionflow =  m->GetFlow();
                              outputtransitionflow = u->GetCurrentFlow();
                                date = (ContinuousPlace::GetMarks()-steadymarks) / (ContinuousPlace::GetOutflow - inputtransitionflow);
                                date += simulation->stime->GetCurrentDate();
                  break;
              case Continuous_tr: u = t->GetFlow();
                                  I = m->GetFlow();
                                  inputtransitionflow =  m->GetCurrentFlow();
                                  outputtransitionflow = u->GetCurrentFlow();
                                  date = (ContinuousPlace::GetMarks()-steadymarks) / (outputtransitionflow - inputtransitionflow);
                                  date += simulation->stime->GetCurrentDate();
                  break;
            }
}
  return (date);
}
}
}*/

//============================PSQ event=====================
//
//===========================================================

double ContinuousPlace::GetPSQEvent()
{
  double steadymarks=0.0;
  double marks=0.0;
  double     transitionflow = 0.0;
  double     weightflow = 0.0;
  double     eventdate = -2.0;
  Transition *t;
  Flow       *f;

  if (abs(GetMarks() - GetSteadyMarks()) < PRF::prf.Min_Err()){
      eventdate= simulation->stime->GetCurrentDate();
      }
  else{
  if (GetNumberOfOutputArcs()){
    for (int i=0; i<numberofarcs; i++){
      if (outputarcs[i] != 0.0){
//        t = simulation->sbpn->GetTransition(i);
        t = outputnodes[i];
        switch (t->IsA()){
          case Continuous_tr:
          case Batch_tr:  f = t->GetFlow();
                          transitionflow = f->GetCurrentFlow();
                            weightflow -= (outputarcs[i] * transitionflow);
               break;
        }
      }
    }
  }

   if (GetNumberOfInputArcs()){
      for (int i=0; i<numberofarcs; i++){
        if (inputarcs[i] != 0){
//          t = simulation->sbpn->GetTransition(i);
          t = inputnodes[i];
          switch (t->IsA()){
            case Continuous_tr:
            case Batch_tr:  f = t->GetFlow();
                            transitionflow = f->GetCurrentFlow();

                              weightflow += (inputarcs[i] * transitionflow);

                 break;
          }
        }
      }
    }
  if ((weightflow != 0.0)){
    eventdate = simulation->stime->GetCurrentDate() - ((GetMarks()-GetSteadyMarks())/weightflow);
  }
  return (eventdate);
}
}


//------------------------------------------------------------------------------
// Print
//------------------------------------------------------------------------------
void ContinuousPlace::Print(ostream &fout)
{
  fout << "Name:                  " << name << endl;
  fout << "   Type:               " << "Continuous" << endl;
  fout << "   Marks:              " << marks << endl;
  fout << "   Initial Marks:      " << initialmarks << endl;
  if (steadymarks!=-1.0)
  fout << "   Steady Marks:       " << steadymarks << endl;
  fout << "   Reserved Marks:     " << reservedmarks << endl;
  fout << "   Non Reserved Marks: " << nonreservedmarks << endl;
  fout << "   Arcs:          " << endl;
  PrintArcs(fout);
}

//------------------------------------------------------------------------------
// Write
//------------------------------------------------------------------------------
void ContinuousPlace::Write(ostream &fout)
{
  fout << "Name:              " << name << endl;
  fout << "   Type:           " << "Continuous" << endl;
  fout << "   Marks:          " << marks << endl;
  if (steadymarks!=-1.0){
    fout << "   Steady Marks:   " << steadymarks;
    if (IsSteadyMarkingReached())
      fout << " (steady marking reached)";
    fout << endl;
  }
  fout << "   Reserved Marks: " << reservedmarks << endl;
}


//------------------------------------------------------------------------------
// It computes the date of the next event DTE for a specific transition
// It return -1.0 if transtion if already enabled and eventdate otherwise
//------------------------------------------------------------------------------
double ContinuousPlace::ComputeDateDTEEvent(DiscreteTransition *t, double weight)
{
  double inputflow;
  double m;
  double eventdate;

  if (t->GetState() != 0.0){ // transition is already enabled
    return (-1.0);
  }
  else{ // compute the next validation
    inputflow = ComputeInputFlow();
    if (inputflow != 0.0){
      m = weight - nonreservedmarks;
      eventdate = m / inputflow;
      eventdate += simulation->stime->GetCurrentDate();
      return (eventdate);
    }
    else{
      return (-1.0);
    }
  }
}

//------------------------------------------------------------------------------
// It computes the input flow for the continuous place
//------------------------------------------------------------------------------
double ContinuousPlace::ComputeInputFlow()
{
  double inputflow = 0.0;
  double transitionflow;
  Flow *f;
  Transition *t;

  if (GetNumberOfInputArcs()){
    for (int i=0; i<numberofarcs; i++){
      if (inputarcs[i] != 0){
//        t = simulation->sbpn->GetTransition(i);
        t = inputnodes[i];
        switch (t->IsA()){
          case Continuous_tr:
          case Batch_tr:  if (t->GetState() != 0.0){ // test if the transition (continuous or batch) was validated
                            f = t->GetFlow();
                            transitionflow = f->GetCurrentFlow();
                            inputflow += (inputarcs[i] * transitionflow);
                          }
               break;
        }
      }
    }
  }
  return (inputflow);
}

//=============================Outputflowof continuous place=======
//
//=================================================================

/*double ContinuousPlace::ComputeOutputFlow()
{
  double outputflow = 0.0;
  double transitionflow;
  Flow *f;
  Transition *t;

  if (GetNumberOfOutputArcs()){
    for (int i=0; i<numberofarcs; i++){
      if (outputarcs[i] != 0){
//        t = simulation->sbpn->GetTransition(i);
        t = outputnodes[i];
        switch (t->IsA()){
          case Continuous_tr:
          case Batch_tr:  if (t->GetState() != 0.0){ // test if the transition (continuous or batch) was validated
                            f = t->GetFlow();
                            transitionflow = f->GetCurrentFlow();
                            outputflow += (outputarcs[i] * transitionflow);
                          }
               break;
        }
      }
    }
  }
  return (outputflow);
}

*/
