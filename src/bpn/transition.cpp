//====================================================================================//
//                                                                                    //
//                       Transition classes inherit of node class                     //
//                                                                                    //
//====================================================================================//
//  This File:   transition.cpp                   Language: C++  (xlC and CC)         //
//  Software:    SIMULEAU                                                             //
//====================================================================================//
//  Creation:    22/apr/16                        by: Leonardo.Brenner@lsis.org       //
//  Last Change: 24/jan/20                        by: Leonardo.Brenner@lsis.org       //
//====================================================================================//
#include "simuleau.h"

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
// Methods of the Transition class
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Constructor
//------------------------------------------------------------------------------
Transition::Transition()
{
  //ctor
}

//------------------------------------------------------------------------------
// Named Constructor
//------------------------------------------------------------------------------
Transition::Transition(simuleau_name _name):Node(_name)
{
  previousstate = state;
  firingquantity = new FiringQuantity;
}

//------------------------------------------------------------------------------
// Destructor
//------------------------------------------------------------------------------
Transition::~Transition()
{
   if (firingquantity)
  delete firingquantity;//dtor
}


//------------------------------------------------------------------------------
// Copy
//------------------------------------------------------------------------------
void Transition::Copy(const Transition *_transition)
{
  TransferNode(_transition);
  firingquantity = new FiringQuantity;
  firingquantity->Copy(_transition->firingquantity);
}

//------------------------------------------------------------------------------
// Set the previous state
//------------------------------------------------------------------------------
void Transition::SetPreviousState(double _state)
{
  previousstate = _state;
}


//------------------------------------------------------------------------------
// Compute states
//------------------------------------------------------------------------------
void Transition::ComputeState()
{
  double transstate = 1.0; // enabled
  double placestate;
  Place *p;

  if (GetNumberOfInputArcs() == 0){
    state = 1.0; // enabled sink transition
  }
  else{
    for (int i=0; i<numberofarcs; i++){
      if (inputarcs[i] != 0){
//        p = simulation->sbpn->GetPlace(i);
        p = inputnodes[i];
        placestate = p->GetState();
//        cout << "State of the place: " << placestate << endl;
        transstate = min(transstate, placestate);
      }
    }
    state = transstate;
//    cout << "State of the transition: " << state << endl;

  }
}

//========================Wodes20===========(modified version)====================================
//------------------------------------------------------------------------------
// Compute input place's marking quantity less than steady marking quantity states
//------------------------------------------------------------------------------
int Transition::IsOnState()
{
  int transstate = 1; // enabled
  Place *p;
  double weight;

  if (GetNumberOfInputArcs() == 0){
    return 1; // enabled     :source transition
  }
  else{
    for (int i=0; i<numberofarcs; i++){
      if (inputarcs[i] != 0){
//        p = simulation->sbpn->GetPlace(i);
        p = inputnodes[i];
        weight = -inputarcs[i];
        if (outputarcs[i] != 0 & inputarcs[i] != 0){
//        p = simulation->sbpn->GetPlace(i);
 //        for (int j=0; j<NumberOfTransitions(); j++) {
 //            V[j] = 0.0;
          weight +=  outputarcs[i];
          }


//
//        if (places[k]->GetWeightOfOutputArc(j) != 0.0 ){
//          V[j] = - places[k]->GetWeightOfOutputArc(j);
//   //       cout << " - V [" << j << "] " ;
//        }
//        if (places[k]->GetWeightOfInputArc(j) != 0.0 ){
//   //       cout << " + V [" << j << "] " ;
//          V[j] +=  places[k]->GetWeightOfInputArc(j);
//        }
//
//
//        if (outputarcs[i] != 0){
////        p = simulation->sbpn->GetPlace(i);
//        p = (Place*)outputnodes[i];
//
// //        for (int j=0; j<NumberOfTransitions(); j++) {
// //            V[j] = 0.0;
//          weight =  outputarcs[i];
//
        switch (p->IsA()){
          case Continuous_pl : if (((ContinuousPlace*)p)->IsLessThanSteadyQuantity()& weight<0)
                                  return 0;
               break;
          case Batch_pl : if (((BatchPlace*)p)->IsLessThanSteadyQuantity()& weight < 0)
                            return 0;
              break;
        }
      }
    }
 }
 return transstate;
}

//=================================Vecos20============================================


//------------------------------------------------------------------------------
FiringQuantity* Transition::GetFiringQuantity()
{
  return (firingquantity);
}

//------------------------------------------------------------------------------
// Returns firing quantity
//------------------------------------------------------------------------------


//==========================Steady firing quantity from initial marking==============
//
//   It computes the steady firing quantity of each transition from initial marking.
//=====================================================================================
void Transition::SetSteadyFiringQuantity(double _firingquantity)
{
    firingquantity->SetSteadyFiringQuantity(_firingquantity);
}
//=====================================================================================
double Transition::GetSteadyFiringQuantity()
{
    return(steadyfiringquantity);
}

//==========================Current firing quantity from initial marking==============
//
//   It computes the current firing quantity of each transition from initial marking.
//=====================================================================================
//void Transition::setCurrentFiringQuantity(double _firingquantity)
//{
//     firingquantity->SetCurrentFiringQuantity(_firingquantity);
//}

//======================================================================

double Transition::CurrentFiringQuantity(double steptime)
{
    Flow *f;
 //   FiringQuantity *firingquantity;
 //   Transition *t;
  //  double currentfiringquantity;
 //   currentfiringquantity=firingquantity->GetCurrentFiringQuantity();
    currentfiringquantity+= f->GetCurrentFlow() * steptime;

   //     firingquantity=t->GetFiringQuantity();
   //     firingquantity->SetCurrentFiringQuantity(currentfiringquantity);
    return (currentfiringquantity);
}


//===========================================================================
double Transition::GetCurrentFiringQuantity()
{
    return (currentfiringquantity);
}


//========================Vecos20======T_Z====================================
//-----------------------------------------------------------------------------------------------------------
// Compute the set of T_Z that exist one output place satisfy Post(p_i, t_j)(z_j^s-z_j(m)) <= q_i^{f,s}
//-----------------------------------------------------------------------------------------------------------
int Transition::IsFiringQuantityOnState()
{
  double weight;
  int transstate = 1; // enabled
  Place *p;
 // Transition *t;
 // FiringQuantity *q;
  double Zs;
  double z;

  //q= t->GetFiringQuantity();
  Zs=firingquantity->GetSteadyFiringQuantity();
  z =firingquantity->GetCurrentFiringQuantity();


  if (GetNumberOfOutputArcs() == 0){
    return 1; // enabled     sink transition
  }
  else{
    for (int i=0; i<numberofarcs; i++){
      if (outputarcs[i] != 0){
//        p = simulation->sbpn->GetPlace(i);
        p = (Place*)outputnodes[i];

 //        for (int j=0; j<NumberOfTransitions(); j++) {
 //            V[j] = 0.0;
          weight =  outputarcs[i];
        switch (p->IsA()){
          case Continuous_pl : if (weight * (Zs-z) <= 0)// FreeSteadyMarkingQuantity= 0 in P^C
                                  return 0;
               break;
          case Batch_pl : if (weight*(Zs-z) <= ((BatchPlace*)p)->GetFreeSteadyQuantity())
                            return 0;
              break;
        }
      }
    }
 }
 return transstate;
}


//========================RBF========Event=================================
// It gets the rbf event. (a new )  objective :
//===========================================================================
double Transition::GetRBFEvent()
{
  double date;
  double min_date=-1.0;
  double weight;
  double transitionflow;
  Flow *f;
  Place *p;
//  Transition *t;
//  FiringQuantity *q;
  double Zs;
  double z;

 // q=t->GetFiringQuantity();
  Zs=firingquantity->GetSteadyFiringQuantity();
  z =firingquantity->GetCurrentFiringQuantity();

  if (GetNumberOfOutputArcs() == 0){
     return (-1.0);
  }
  else if(z >= Zs){
     return (-1.0);
  }
  else{
    for (int i=0; i<numberofarcs; i++){
      if (outputarcs[i] != 0){
//        p = simulation->sbpn->GetPlace(i);
        p = (Place*)outputnodes[i];

          weight =  outputarcs[i];
 //         f=t->GetFlow();
          transitionflow=f->GetCurrentFlow();
        switch (p->IsA()){
          case Continuous_pl :
                              date = (Zs-z) / transitionflow; // FreeSteadyMarkingQuantity= 0 in P^C
                              date += simulation->stime->GetCurrentDate();


               break;
          case Batch_pl :
                              date = (Zs-z -(((BatchPlace*)p)->GetFreeSteadyQuantity() / weight) / transitionflow);
                              if (date < 0){
                                  return (-1.0);
                              }
                              else{
                                  date += simulation->stime->GetCurrentDate();
                                  }
              break;
        }
          if (date < min_date || min_date==-1.0){
                                 min_date= date;
          }
      }
    }
    return(min_date);
    }
  return (-1.0);
}
//-----------------------------------------------------------------------------------


//double BatchPlace::GetBOBEvent()
//{
//  double pos;
//  double date;
//  Batch b;
//
//  if (marks->empty()){
//    return (-1.0); // no event of this type
//  }
//
//  if (state != 1.0){
//    b = marks->front();
//    pos = b.GetPosition();
//    date = (length - pos) / instantaneousspeed;
//    date += simulation->stime->GetCurrentDate();
//    return (date);
//  }
//  return (-1.0);
//}





//------------------------------------------------------------------------------
// Print
//------------------------------------------------------------------------------
void Transition::Print(ostream &fout)
{
  fout << "Name:              " << name << endl;
  fout << "    Arcs:          " << endl;
  PrintArcs(fout);
}

