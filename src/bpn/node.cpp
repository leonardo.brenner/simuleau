//====================================================================================//
//                                                                                    //
//                                      Node class                                    //
//                                                                                    //
//====================================================================================//
//  This File:   node.cpp                         Language: C++  (xlC and CC)         //
//  Software:    SIMULEAU                                                             //
//====================================================================================//
//  Creation:    21/apr/16                        by: Leonardo.Brenner@lsis.org       //
//  Last Change: 21/apr/16                        by: Leonardo.Brenner@lsis.org       //
//====================================================================================//
#include "simuleau.h"

//------------------------------------------------------------------------------
// Constructor
//------------------------------------------------------------------------------
Node::Node()
{
  numberofarcs = 0;        // number of input arcs to this node
  inputarcs   = NULL;            // array of input arcs
  inputnodes  = NULL;            // array of input nodes
  outputarcs  = NULL;            // array of output arcs
  outputnodes = NULL;            // array of output nodes

  strcpy(name,"");              // node name
  state = No_state;             // node state

}

//------------------------------------------------------------------------------
// Named constructor
//------------------------------------------------------------------------------
Node::Node(simuleau_name _name)
{
  numberofarcs = 0;       // number of output arcs from this node
  inputarcs   = NULL;            // array of input arcs
  inputnodes  = NULL;            // array of input nodes
  outputarcs  = NULL;            // array of output arcs
  outputnodes = NULL;            // array of output nodes

  strcpy(name,_name);           // node name
  state = 0.0;                  // node state
}


//------------------------------------------------------------------------------
// Destructor
//------------------------------------------------------------------------------
Node::~Node()
{
  if (inputarcs)
    delete [] inputarcs;
  if (inputnodes)
    delete [] inputnodes;
  if (outputarcs)
    delete [] outputarcs;
  if (outputnodes)
    delete [] outputnodes;
}

//------------------------------------------------------------------------------
// Copy
//------------------------------------------------------------------------------
void Node::Copy(const Node *_node)
{
  strcpy(name,_node->name);           // node name
  state = _node->state;               // node state
  numberofarcs = _node->numberofarcs; // number of arcs (places or transitions)

  if (numberofarcs > 0){
    inputarcs  = new double [numberofarcs];
    inputnodes = new Node* [numberofarcs];
    for (int i=0; i<numberofarcs; i++){
      inputarcs[i]  = _node->inputarcs[i]; // 0 means this arc is not used
      inputnodes[i] = _node->inputnodes[i];
    }

    outputarcs  = new double [numberofarcs];
    outputnodes = new Node* [numberofarcs];
    for (int i=0; i<numberofarcs; i++){
      outputarcs[i]  = _node->outputarcs[i]; // 0 means this arc is not used
      outputnodes[i] = _node->outputnodes[i];
    }
  }
}


//------------------------------------------------------------------------------
// It alloc the array of input/output arcs
//------------------------------------------------------------------------------
int Node::AllocArcs(int n)
{
  numberofarcs = n;
  inputarcs  = new double [n];
  inputnodes = new Node* [n];
  for (int i=0; i<n; i++){
    inputarcs[i]  = 0; // 0 means this arc is not used
    inputnodes[i] = NULL;
  }

  outputarcs  = new double [n];
  outputnodes = new Node* [n];
  for (int i=0; i<n; i++){
    outputarcs[i]  = 0; // 0 means this arc is not used
    outputnodes[i] = 0;
  }
}

//------------------------------------------------------------------------------
// It adds an input arc in the positin pos with weight
//------------------------------------------------------------------------------
int Node::AddInputArc(int pos, double weight)
{
  inputarcs[pos] = weight;
}

//------------------------------------------------------------------------------
// It adds an output arc in the positin pos with weight
//------------------------------------------------------------------------------
int Node::AddOutputArc(int pos, double weight)
{
  outputarcs[pos] = weight;
}

//------------------------------------------------------------------------------
// set input node pointes
//------------------------------------------------------------------------------
int Node::AddInputNodes(Node **nodes)
{
  for (int i=0; i<numberofarcs; i++){
    if (inputarcs[i] != 0.0)
      inputnodes[i] = nodes[i];
    else
      inputnodes[i] = NULL;
  }
}



//------------------------------------------------------------------------------
// set output node pointes
//------------------------------------------------------------------------------
int Node::AddOutputNodes(Node **nodes)
{
  for (int i=0; i<numberofarcs; i++){
    if (outputarcs[i] != 0.0)
      outputnodes[i] = nodes[i];
    else
      outputnodes[i] = NULL;
  }
}

//------------------------------------------------------------------------------
// set node state
//------------------------------------------------------------------------------
void Node::SetState(double _state)
{
  state = _state;
}


//------------------------------------------------------------------------------
// It get the node name
//------------------------------------------------------------------------------
char* Node::GetName()
{
  return (name);
}

//------------------------------------------------------------------------------
// It get the current node state
//------------------------------------------------------------------------------
double Node::GetState()
{
  return (state);
}

//------------------------------------------------------------------------------
// get the number of input arc different of 0
//------------------------------------------------------------------------------
int Node::GetNumberOfInputArcs()
{
  int n = 0;

  for (int i=0; i<numberofarcs; i++){
    if (inputarcs[i] != 0)
      n++;
  }
  return (n);
}

//------------------------------------------------------------------------------
// get the number of Output arc different of 0
//------------------------------------------------------------------------------
int Node::GetNumberOfOutputArcs()
{
  int n = 0;

  for (int i=0; i<numberofarcs; i++){
    if (outputarcs[i] != 0)
      n++;
  }
  return (n);
}

//------------------------------------------------------------------------------
// get the weight of an input arc
//------------------------------------------------------------------------------
double Node::GetWeightOfInputArc(int _arc)
{
  return (inputarcs[_arc]);
}

//------------------------------------------------------------------------------
// get the weight of an output arc
//------------------------------------------------------------------------------
double Node::GetWeightOfOutputArc(int _arc)
{
  return (outputarcs[_arc]);
}

//------------------------------------------------------------------------------
// Print
//------------------------------------------------------------------------------
void Node::Print(ostream &fout)
{
  fout << "Name:          "  << name << endl;
  PrintArcs(fout);
}


//------------------------------------------------------------------------------
// Print arcs
//------------------------------------------------------------------------------
void Node::PrintArcs(ostream &fout)
{
  for (int i=0; i<numberofarcs; i++){
    if (outputarcs[i]){
      fout << "                   to " << outputnodes[i]->GetName() << " : " << outputarcs[i] << endl;
    }
  }

   for (int i=0; i<numberofarcs; i++){
    if (inputarcs[i])
      fout << "                   from " << inputnodes[i]->GetName() << " : " << inputarcs[i] << endl;
  }
  fout << endl;
}

//------------------------------------------------------------------------------
// Transfer node informations
//------------------------------------------------------------------------------
void Node::TransferNode(const Node *_node)
{
  strcpy(name,_node->name);           // node name
  state = _node->state;               // node state
  numberofarcs = _node->numberofarcs; // number of arcs (places or transitions)

  if (numberofarcs > 0){
    inputarcs  = new double [numberofarcs];
    inputnodes = new Node* [numberofarcs];
    for (int i=0; i<numberofarcs; i++){
      inputarcs[i]  = _node->inputarcs[i]; // 0 means this arc is not used
      inputnodes[i] = _node->inputnodes[i];
    }

    outputarcs  = new double [numberofarcs];
    outputnodes = new Node* [numberofarcs];
    for (int i=0; i<numberofarcs; i++){
      outputarcs[i]  = _node->outputarcs[i]; // 0 means this arc is not used
      outputnodes[i] = _node->outputnodes[i];
    }
  }
}
