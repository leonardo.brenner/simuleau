//====================================================================================//
//                                                                                    //
//                                      Flow class                                    //
//                                                                                    //
//====================================================================================//
//  This File:   flow.cpp                         Language: C++  (xlC and CC)         //
//  Software:    SIMULEAU                                                             //
//====================================================================================//
//  Creation:    29/apr/16                        by: Leonardo.Brenner@lsis.org       //
//  Last Change: 29/apr/16                        by: Leonardo.Brenner@lsis.org       //
//====================================================================================//
#include "simuleau.h"

//------------------------------------------------------------------------------
// Constructor
//------------------------------------------------------------------------------
Flow::Flow()
{
  maximumflow = theoreticalflow = currentflow = initialflow = steadyflow = -1.0;
}

//------------------------------------------------------------------------------
// Destructor
//------------------------------------------------------------------------------
Flow::~Flow()
{
  //dtor
}


//------------------------------------------------------------------------------
// Copy
//------------------------------------------------------------------------------
void Flow::Copy(const Flow *_flow)
{
  maximumflow     = _flow->maximumflow;
  theoreticalflow = _flow->theoreticalflow;
  currentflow     = _flow->currentflow;
  initialflow     = _flow->initialflow;
  steadyflow      = _flow->steadyflow;

}

//------------------------------------------------------------------------------
// Set up all flows
//------------------------------------------------------------------------------
void Flow::SetAllFlows(double _flow)
{
  maximumflow = theoreticalflow = currentflow = initialflow = steadyflow = _flow;
}

//------------------------------------------------------------------------------
// Set up the intial flow
//------------------------------------------------------------------------------
void Flow::SetInitialFlow(double _flow)
{
  initialflow = _flow;
}

//------------------------------------------------------------------------------
// Set up the current flow
//------------------------------------------------------------------------------
void Flow::SetCurrentFlow(double _flow)
{
  currentflow = _flow;
}

//------------------------------------------------------------------------------
// Set up the theoretical flow
//------------------------------------------------------------------------------
void Flow::SetTheoreticalFlow(double _flow)
{
  theoreticalflow = _flow;
}

//------------------------------------------------------------------------------
// Set up the theoretical flow
//------------------------------------------------------------------------------
void Flow::SetMaximumFlow(double _flow)
{
  maximumflow = _flow;
}

//------------------------------------------------------------------------------
// Set up the steady flow
//------------------------------------------------------------------------------
void Flow::SetSteadyFlow(double _flow)
{
  steadyflow = _flow;
}

//------------------------------------------------------------------------------
// Return the current flow
//------------------------------------------------------------------------------
double Flow::GetCurrentFlow()
{
  return (currentflow);
}

//------------------------------------------------------------------------------
// Return the theoretical flow
//------------------------------------------------------------------------------
double Flow::GetTheoreticalFlow()
{
  return (theoreticalflow);
}

//------------------------------------------------------------------------------
// Return the maximum flow
//------------------------------------------------------------------------------
double Flow::GetMaximumFlow()
{
  return (maximumflow);
}

//------------------------------------------------------------------------------
// Return the steady flow
//------------------------------------------------------------------------------
double Flow::GetSteadyFlow()
{
  return (steadyflow);
}

//------------------------------------------------------------------------------
// Print
//------------------------------------------------------------------------------
void Flow::Print(ostream &fout)
{
  fout << "      Maximum     : " << maximumflow << endl;
  fout << "      Theoretical : " << theoreticalflow << endl;
  fout << "      Current     : " << currentflow << endl;
  fout << "      Initial     : " << initialflow << endl;
  if (steadyflow!=-1.0)
    fout << "      Steady      : " << steadyflow << endl << endl;

}
