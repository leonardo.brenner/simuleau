//====================================================================================//
//                                                                                    //
//                                      Firingquantity class                                    //
//                                                                                    //
//====================================================================================//
//  This File:   firingquantity.h                           Language: C++  (xlC and CC)         //
//  Software:    SIMULEAU                                                             //
//====================================================================================//
//  Creation:    29/apr/16                        by: Leonardo.Brenner@lsis.org       //
//  Last Change: 29/apr/16                        by: Leonardo.Brenner@lsis.org       //
//====================================================================================//
#ifndef FIRINGQUANTITY_H
#define FIRINGQUANTITY_H

class FiringQuantity
{
  public:
    FiringQuantity();
    virtual ~FiringQuantity();

    void Copy(const FiringQuantity *_firingquantity);

    // input functions
    void SetAllFiringQuantities(double _firingquantity);
    void SetCurrentFiringQuantity(double _firingquantity);
    void SetSteadyFiringQuantity(double _firingquantity);


    // output functions
    double GetCurrentFiringQuantity();
    double GetSteadyFiringQuantity();


    //print
    void Print(ostream &fout);

  protected:
    double firingquantity;
    double currentfiringquantity;
    double steadyfiringquantity;

  private:
};

#endif // FIRINGQUANTITY_H
