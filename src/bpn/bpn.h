//====================================================================================//
//                                                                                    //
//                             Batch Petri Nets main class                            //
//                                                                                    //
//====================================================================================//
//  This File:   bpn.h                            Language: C++  (xlC and CC)         //
//  Software:    SIMULEAU                                                             //
//====================================================================================//
//  Creation:    21/apr/16                        by: Leonardo.Brenner@lsis.org       //
//  Last Change: 21/apr/16                        by: Leonardo.Brenner@lsis.org       //
//====================================================================================//
#ifndef BPN_H
#define BPN_H

class BPN
{
  public:
    BPN();
    BPN(const int _numberofplaces, const int _numberoftransitions);
    virtual ~BPN();

    void Copy(const BPN *_bpn);


    void AllocArcs();
    void AddNodeLinks();

    //input functions
    void PutName(simuleau_name _name);                // put a name on the bpn
    // place functions
    void NumberOfDiscretePlaces(const int n);         // set the number of discrete places
    void NumberOfContinuousPlaces(const int n);       // set the number of continuous places
    void NumberOfBatchPlaces(const int n);            // set the number of batch places
    void NumberOfTriangularBatchPlaces(const int n);  // set the number of triangular batch places
    int  AddPlace(int pos, Place *p);                 // add a place to the bpn in the position pos
    void SetInitialMarking(int pos, int m);           // set the initial marking of a discrete place
    void SetSteadyMarking(int pos, int m);            // set the steady marking of a discrete place
    void SetInitialMarking(int pos, double m);        // set the initial marking of a conitnuous place
    void SetSteadyMarking(int pos, double m);         // set the steady marking of a conitnuous place
    void AddBatchToInitialMarking(int pos, Batch &b); // set the initial marking of a batch place
    void AddBatchToSteadyMarking(int pos, Batch &b);  // set the steady marking of a batch place
    void AddTriangularToInitialMarking(int pos, ControllableBatch &b);
                                                      // set the initial marking of a controllable batch place
    void AddTriangularToSteadyMarking(int pos, ControllableBatch &b);
                                                      // set the steady marking of a controllable batch place
    int  AddPlaceOutputArc(int _place, int _transition, double _weight);
                                                      // Add an output arc from place to transition
    // transition functions
    void NumberOfDiscreteTransitions(const int n);    // set the number of discrete transitions
    void NumberOfContinuousTransitions(const int n);  // set the number of continuous transitions
    void NumberOfBatchTransitions(const int n);       // set the number of batch transitions
    int  AddTransition(int pos, Transition *t);       // add a transition to the bpn in the position pos
    int  AddTransitionOutputArc(int _transition, int _place, double _weight);
                                                      // Add an output arc from transition to place
    //output functions
    int   VerifyName(simuleau_name _name);  // verify if _name is already defined in the model
    char* GetName();                        // get the bpn model name
    Node* GetNode(simuleau_name _name);     // return the addresse of the node _name;
    // place functions
    int NumberOfDiscretePlaces();           // return the number of discrete places
    int NumberOfContinuousPlaces();         // return the number of continuous places
    int NumberOfBatchPlaces();              // return the number of batch places
    int NumberOfTriangularBatchPlaces();    // return the number of triangular batch places
    int NumberOfPlaces();                   // return the number of all places
    int GetPlacePosition(simuleau_name _name); // return the position of the place _name
    char* GetPlaceName(place_id _place);    // return the place name of a place position
    Place* GetPlace(place_id _place);       // return the place pointer of a place position
    place_type GetPlaceType(simuleau_name _name); // return the place type of the place _name

    // transition functions
    int NumberOfDiscreteTransitions();      // return the number of discrete transitions
    int NumberOfContinuousTransitions();    // return the number of continuous transitions
    int NumberOfBatchTransitions();         // return the number of batch transitions
    int NumberOfTransitions();              // return the number of all transitions
    int GetTransitionPosition(simuleau_name _name); // return the position of the transitions _name
    char* GetTransitionName(trans_id _trans); // return the transition name of a transition position
    Transition *GetTransition(trans_id _trans); // return the transition pointer of a transition position
    trans_type GetTransitionType(simuleau_name _name); // return the transition type of the transition _name

    // print functions
    void Print(ostream &fout);
    void WritePlaces(ostream &fout);
    void WriteFlows(ostream &fout);

    // simulation functions
    void ComputeNewMarks(double duration);
    void ReserveMarks();
    void CreateBatches();
    void DestructBatches();
    void MergeBatches();
    int  ComputeTransitionsStates();
    int  StateStability(double *p, double *t);
    void ComputeBatchesBehaviours();
    void SetBehaviourFunctions();
    void ComputeIFF();
    void ComputeCFF(); // on/off control method
    void ComputeMCFF(); // maximal on/off control method
    void ComputeMSCFF(); // near-optimal on/off control method
    void ComputeSteadyState(); //Computation of a steady state
    void ComputePeriodicSteadyState(); //Computation of a periodic steady state
    void ResearchStructuralConflict();
    int  VerifyConflict();
    int  IsSteadyStateReached();
    void ComputeCurrentFiringQuantity(double steptime); //maximal on/off control method
    void ComputeSteadyFiringQuantity();

    //process event functions
    void ComputeNextEventDTF(list<Event> * _dtf);
    void ComputeNextEventCPE(list<Event> * _cpe);
    void ComputeNextEventDTE(list<Event> * _dte);
    void ComputeNextEventBOB(list<Event> * _bob);
    void ComputeNextEventDOB(list<Event> * _dob);
    void ComputeNextEventBBD(list<Event> * _bbd);
    void ComputeNextEventTBM(list<Event> * _tbm);
    void ComputeNextEventBMOB(list<Event> * _bmob);
    void ComputeNextEventOBD(list<Event> * _obd);
    void ComputeNextEventBD(list<Event> * _bd);
    void ComputeNextEventBBF(list<Event> * _bbf);
    void ComputeNextEventPSQ(list<Event> * _psq); //on off method
    void ComputeNextEventRBF(list<Event> * _rbf); //maximal flow based on off method


    protected:
//    int numberofplaces;                   // number of all places in this model
    int numberofdiscreteplaces;           // number of discrete places
    int numberofcontinuousplaces;         // number of continuous places
    int numberofbatchplaces;              // number of batch places
    int numberoftriangularplaces;         // number of triangular batch places

//    int numberoftransitions;              // number of all transitions in this model
    int numberofdiscretetransitions;      // number of discrete transitions
    int numberofcontinuoustransitions;    // number of continuous transitions
    int numberofbatchtransitions;         // number of batch transitions

    Place      **places;                  // ensemble de places
    Transition **transitions;             // ensemble de transitions
 //   double currentfiringquantity;

  private:
    simuleau_name bpnname;                // BPN model name

};

#endif // BPN_H
