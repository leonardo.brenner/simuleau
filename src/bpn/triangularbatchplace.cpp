//====================================================================================//
//                                                                                    //
//                 Triangular batch place classes inherit of place class              //
//                                                                                    //
//====================================================================================//
//  This File:   triangularbatchplace.cpp         Language: C++  (xlC and CC)         //
//  Software:    SIMULEAU                                                             //
//====================================================================================//
//  Creation:    22/apr/16                        by: Leonardo.Brenner@lsis.org       //
//  Last Change: 22/apr/16                        by: Leonardo.Brenner@lsis.org       //
//====================================================================================//
#include "simuleau.h"

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
// Methods of the Triangular Batch  Place class
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Constructor
//------------------------------------------------------------------------------
TriangularBatchPlace::TriangularBatchPlace()
{

}

//------------------------------------------------------------------------------
// Initialized Constructor
//------------------------------------------------------------------------------
TriangularBatchPlace::TriangularBatchPlace(simuleau_name _name, double _speed, double _density, double _length,
                                           double _flow):BatchPlace(_name, _speed, _density, _length)
{

  flow    = _flow;
  maxflow = _flow;
  marks   = new list<ControllableBatch>;
  marks->clear();
  initialmarks = new list<ControllableBatch>;
  initialmarks->clear();
  steadymarks = new list<ControllableBatch>;
  steadymarks->clear();

}
//------------------------------------------------------------------------------
// Destructor
//------------------------------------------------------------------------------
TriangularBatchPlace::~TriangularBatchPlace()
{
  if (marks)
    delete[] marks;
  if (initialmarks)
    delete[] initialmarks;
  if (steadymarks)
    delete[] steadymarks;

}


//------------------------------------------------------------------------------
// Copy
//------------------------------------------------------------------------------
void TriangularBatchPlace::Copy(const TriangularBatchPlace * _place)
{
  TransferNode(_place);

  conflict           = _place->conflict;
  structuralconflict = _place->structuralconflict;

  // batch place
  behaviour     = _place->behaviour;
  formalisation = _place->formalisation;

  density  = _place->density;
  maxspeed = _place->maxspeed;
  speed    = _place->speed;
  instantaneousspeed = _place->instantaneousspeed;
  length   = _place->length;
  flow     = _place->flow;
  maxflow  = _place->maxflow;

  marks = new list<ControllableBatch>;
  marks->clear();

  for (list<ControllableBatch>::iterator it=_place->marks->begin(); it!=_place->marks->end(); ++it) {
    ControllableBatch *newbatch = new ControllableBatch;
    newbatch->Copy(*it);
    marks->push_back(*newbatch);
  }

  initialmarks = new list<ControllableBatch>;
  initialmarks->clear();

  for (list<ControllableBatch>::iterator it=_place->initialmarks->begin(); it!=_place->initialmarks->end(); ++it) {
    ControllableBatch *newbatch = new ControllableBatch;
    newbatch->Copy(*it);
    initialmarks->push_back(*newbatch);
  }


  steadymarks = new list<ControllableBatch>;
  steadymarks->clear();

  for (list<ControllableBatch>::iterator it=_place->steadymarks->begin(); it!=_place->steadymarks->end(); ++it) {
    ControllableBatch *newbatch = new ControllableBatch;
    newbatch->Copy(*it);
    steadymarks->push_back(*newbatch);
  }

 }

//------------------------------------------------------------------------------
// It set the initial marking of the place
//------------------------------------------------------------------------------
void TriangularBatchPlace::AddBatchToInitialMarking(ControllableBatch &b)
{
  initialmarks->push_back(b);
  marks->push_back(b);
}

//------------------------------------------------------------------------------
// It add a batch to the steady marking of the place
//------------------------------------------------------------------------------
void TriangularBatchPlace::AddBatchToSteadyMarking(ControllableBatch &b)
{
  steadymarks->push_back(b);
}

//------------------------------------------------------------------------------
// It add a batch to the marking of the place
//------------------------------------------------------------------------------
void TriangularBatchPlace::AddBatchToMarking(ControllableBatch &b)
{
  marks->push_back(b);
}


//------------------------------------------------------------------------------
// It set the new flow
//------------------------------------------------------------------------------
void TriangularBatchPlace::SetFlow(double _flow)
{
  flow = _flow;
}

//------------------------------------------------------------------------------
// It set the maximum flow
//------------------------------------------------------------------------------
void TriangularBatchPlace::SetMaxFlow(double _flow)
{
  maxflow = _flow;
}

//------------------------------------------------------------------------------
// It set the new flow
//------------------------------------------------------------------------------
double TriangularBatchPlace::GetFlow()
{
  return (flow);
}

//------------------------------------------------------------------------------
// It set the maximum flow
//------------------------------------------------------------------------------
double TriangularBatchPlace::GetMaxFlow()
{
  return (maxflow);
}

//------------------------------------------------------------------------------
// It returns true if the triangular batch place is full
//------------------------------------------------------------------------------
int TriangularBatchPlace::IsFull()
{
  double full = length * density;
  double quantity = 0.0;

  if (marks->size() != 0){
    for (list<ControllableBatch>::iterator cb=marks->begin(); cb!=marks->end(); cb++) {
      quantity += cb->GetDensity() * cb->GetLength();
    }
    if (abs(quantity - full) < PRF::prf.Min_Err()){
      return 1;
    }
    else{
      return 0;
    }
  }
  else {
    return 0;
  }
}


//------------------------------------------------------------------------------
// It returns true if the place has an output batch
//------------------------------------------------------------------------------
int TriangularBatchPlace::HasOutputBatch()
{
  int result;

  ControllableBatch cb;

  if (marks->size() ==  0){
    result = 0;
  }
  else{
    cb = marks->front();
    if ((length - cb.GetPosition()) <= PRF::prf.Min_Err()){
      result = 1;
    }
    else{
      result = 0;
    }
  }
  return (result);
}


//------------------------------------------------------------------------------
// It returns the output controllable batch
//------------------------------------------------------------------------------
ControllableBatch* TriangularBatchPlace::GetOutputControllableBatch()
{
  if (HasOutputBatch()){
    return (&marks->front());
  }
  else{
    return (NULL);
  }
}

//------------------------------------------------------------------------------
// It returns the critical density
//------------------------------------------------------------------------------
double TriangularBatchPlace::GetCriticalDensity()
{
  double dcri;
  dcri = ((GetPropagationSpeed() * density) / (instantaneousspeed + GetPropagationSpeed()));
  return (dcri);
}

//------------------------------------------------------------------------------
// It returns the density of the output batch
//------------------------------------------------------------------------------
double TriangularBatchPlace::GetDensityOutputBatch()
{
  double densityoutputbatch = 0.0;
  ControllableBatch *cb;

  if (HasOutputBatch()){
    cb = GetOutputControllableBatch();
    densityoutputbatch = cb->GetDensity();
  }
  return (densityoutputbatch);
}

//------------------------------------------------------------------------------
// It returns the speed of the output batch
//------------------------------------------------------------------------------
double TriangularBatchPlace::GetSpeedOutputBatch()
{
  double speedoutputbatch = 0.0;
  ControllableBatch *cb;

  if (HasOutputBatch()){
    cb = GetOutputControllableBatch();
    speedoutputbatch = cb->GetSpeed();
  }
  return (speedoutputbatch);
}

//------------------------------------------------------------------------------
// It returns the flow of the output batch
//------------------------------------------------------------------------------
double TriangularBatchPlace::GetFlowOutputBatch()
{
  return (GetDensityOutputBatch() * GetSpeedOutputBatch());
}

//------------------------------------------------------------------------------
// It returns the congested density of the place
//------------------------------------------------------------------------------
double TriangularBatchPlace::GetCongestionDensity()
{
  return (density - (GetOutputTransitonsFlow() / GetPropagationSpeed()));
}

//------------------------------------------------------------------------------
// It returns the propagation speed of congestion/decongestion
//------------------------------------------------------------------------------
double TriangularBatchPlace::GetPropagationSpeed()
{
  double w;
  w = (maxflow * maxspeed) / (density * maxspeed - maxflow);
  return (w);
}


//------------------------------------------------------------------------------
// It computes the state of the triangular batch place
//------------------------------------------------------------------------------
void TriangularBatchPlace::ComputeState()
{
  if (marks->empty()){
    state =  0.0;
  }
  else{
    if (HasOutputBatch() == 1){
      state = 1.0;
    }
    else{
      state = 0.0;
    }
  }
}


//------------------------------------------------------------------------------
// It creates a controllable batch if the place has a least an input transition (continuous or batch)
// and the input flow is non-null
// It returns a positive value if a batch was created and 0 otherwise
// TODO verify multiple input transitions
//------------------------------------------------------------------------------
int TriangularBatchPlace::CreateBatches()
{
  double inputtransflow = 0.0;
  int    result = 0;

  Transition *t;
  Flow *f;
  ControllableBatch *newcb;

  formalisation = no;

  if (GetNumberOfInputArcs() != 0){
    inputtransflow = GetInputTransitonsFlow();
    if (inputtransflow > 0.0){
      if (!marks->empty()){
        ControllableBatch &cb = marks->back();
        if ((cb.GetLength() != 0.0 ) && (cb.GetPosition() != 0.0)){
          newcb = new ControllableBatch(inputtransflow, instantaneousspeed);
          AddBatchToMarking(*newcb);
        }
      }
      else{
        newcb = new ControllableBatch(inputtransflow, instantaneousspeed);
        AddBatchToMarking(*newcb);
      }
    }
  }
}

//------------------------------------------------------------------------------
// It merges the batch that have the same speed and density and are in contact
//------------------------------------------------------------------------------
void TriangularBatchPlace::MergeBatches()
{
  int merged = 0;
  double endposition;
  double newlength;
  int numberofbatches = marks->size();
  ControllableBatch *newcb;
  list<ControllableBatch> *newmarks = new list<ControllableBatch>;
  list<ControllableBatch>::iterator cb1, cb2, cbi;

  if (numberofbatches > 1){
    cb1 = marks->begin();
    cbi = marks->begin();
    cbi++;
    for (int i=1; i<numberofbatches; i++){
      endposition = cb1->GetPosition() - cb1->GetLength();
      cb2 = cbi;

      if ((abs(cb1->GetDensity() - cb2->GetDensity()) < PRF::prf.Min_Err()) &&
          (abs(cb2->GetPosition() - endposition) < PRF::prf.Min_Err())){ // merge batches
        newlength = cb1->GetLength() + cb2->GetLength();
        cb1->SetLength(newlength);
        cbi++;
        merged = 1;
      }
      else{
        newcb = new ControllableBatch;
        newcb->Copy(*cb1);
        newmarks->push_back(*newcb);
        cb1 = cb2;
        cbi++;
        merged = 0;
      }
    }

    newcb = new ControllableBatch;
    if (merged){
      newcb->Copy(*cb1);
    }
    else{
      newcb->Copy(*cb2);
    }

    newmarks->push_back(*newcb);
    marks->clear();
    marks = newmarks;
  }
}

//------------------------------------------------------------------------------
// It destructs the Controllable batches that have a length or density equal to 0
// and are not the input batch (position equal to length)
//------------------------------------------------------------------------------
void TriangularBatchPlace::DestructBatches()
{
  int numberofbatches = marks->size();
  ControllableBatch *newcb;
  list<ControllableBatch> *newmarks = new list<ControllableBatch>;
  list<ControllableBatch>::iterator cb1;

  if (numberofbatches != 0){
    for (cb1=marks->begin(); cb1!=marks->end(); ++cb1) {
      if ((cb1->GetLength() > PRF::prf.Min_Err()) || ((cb1->GetPosition() == 0.0)  && (cb1->GetLength() == 0.0))){
        newcb = new ControllableBatch;
        newcb->Copy(*cb1);
        newmarks->push_back(*newcb);
      }
    }
    marks->clear();
    marks = newmarks;
  }
}

//------------------------------------------------------------------------------
// It computes the behaviour of a triangular batch place
// TODO all batch places have no structural conflit
//------------------------------------------------------------------------------
void TriangularBatchPlace::ComputeBehaviour()
{
  int i=0;
  double outflow;
  double trflow;
  double pcb_speed = -1.0;
  list<ControllableBatch>::iterator cbi, pcbi;
  ControllableBatch *cb, *pcb;
  ControllableBatch *ocb = NULL;

  if (HasOutputBatch()){
    ocb = GetOutputControllableBatch();
  }

  for (i=0, cbi=marks->begin(); cbi!=marks->end(); ++i, ++cbi) {
    cb = (ControllableBatch*) &(*cbi);
    if (cb == ocb){
      outflow = ocb->GetFlow();
      trflow  = GetOutputTransitonsFlow();

      if (cb->GetState(this) == Free_st){
        if ((outflow - trflow) <= PRF::prf.Min_Err()){
          cb->SetBehaviour(Free_behaviour);
        }
        else{
          cb->SetBehaviour(Congesting_behaviour);
        }
      }
      else{ // not free state
        if ((outflow - trflow) < (-PRF::prf.Min_Err())){
          cb->SetBehaviour(Uncongesting_behaviour);
        }
        else{
          if ((outflow - trflow) > PRF::prf.Min_Err()){
            cb->SetBehaviour(Congesting_behaviour);
          }
          else{
            cb->SetBehaviour(Free_behaviour);
          }
        }
      }
    }
    else{ // not the output batch
      pcbi = cbi;
      pcbi--; // previous controllable batch iterator;
      pcb = (ControllableBatch*) &(*pcbi); // previous controllable batch
      pcb_speed = -1.0;

      if ((i!=0) && (cb != pcb) && (cb->IsInContactWithByFront(*pcb))){
        pcb_speed = pcb->GetSpeed();
      }

      if (cb->GetState(this) == Free_st){
        if ((pcb_speed >= 0.0) && (pcb_speed < cb->GetSpeed())){
          cb->SetBehaviour(Congesting_behaviour);
        }
        else{
          cb->SetBehaviour(Free_behaviour);
        }
      }
      else{
        if ((pcb_speed >= 0.0) && (pcb_speed < cb->GetSpeed())){ // <=
          cb->SetBehaviour(Congesting_behaviour);
        }
        else{
          cb->SetBehaviour(Uncongesting_behaviour);
        }
      }
    }
  }
}


//------------------------------------------------------------------------------
// This function sets  the new speed and recompute the state of the controllable batches
//------------------------------------------------------------------------------
void TriangularBatchPlace::SetAndRecomputeSpeed(double newspeed)
{
  ctrl_batch_state st;
  double newcriticaldensity;

  for (list<ControllableBatch>::iterator cbi=marks->begin(); cbi!=marks->end(); ++cbi) {
    st = cbi->GetState(this);
    if (newspeed < instantaneousspeed){
      if ((st == Free_st) || (st == Congested_st) && (cbi->GetSpeed() > newspeed)){
        cbi->SetSpeed(newspeed);
      }
    }
    else{
      if (newspeed > instantaneousspeed){
        newcriticaldensity = flow / newspeed;
        if (st == Free_st) {
          if (cbi->GetDensity() <= newcriticaldensity){
            cbi->SetSpeed(newspeed);
          }
          else{
            cbi->SetSpeed(GetPropagationSpeed() * (density - cbi->GetDensity()) / cbi->GetDensity());
          }
        }
      }
    }
  }
  SetSpeed(newspeed);
}



//------------------------------------------------------------------------------
// This function sets the corresponding evolution function to each batch
//------------------------------------------------------------------------------
void TriangularBatchPlace::SetBehaviourFunctions()
{
  int i=0;
  list<ControllableBatch>::iterator cbi;

  for (i=0, cbi=marks->begin(); cbi!=marks->end(); ++i, ++cbi) {
    switch(cbi->GetBehaviour()){
      case Free_behaviour: SetFunctionFreeBatch(cbi, i);
           break;
      case Congesting_behaviour: SetFunctionCongestedBatch(cbi, i);
           break;
      case Uncongesting_behaviour: SetFunctionUncongestingBatch(cbi, i);
           break;
    }
  }
}

//------------------------------------------------------------------------------
// It sets the  evolution function to a free batch
//------------------------------------------------------------------------------
void TriangularBatchPlace::SetFunctionFreeBatch(list<ControllableBatch>::iterator cbi, int i)
{
  switch(cbi->GetState(this)){
    case Free_st : cbi->SetEvolveFunction(&ControllableBatch::EvolveInFreeBehaviour);
         break;
    case Congested_st : cbi->SetEvolveFunction(&ControllableBatch::EvolveInCongestingOutputBehaviour);
         break;
  }
}


//------------------------------------------------------------------------------
// It sets the  evolution function to a congested batch
// TODO verify cbi adn pcbi
//------------------------------------------------------------------------------
void TriangularBatchPlace::SetFunctionCongestedBatch(list<ControllableBatch>::iterator cbi, int i)
{
  double diffdensity;

  list<ControllableBatch>::iterator pcbi = prev(cbi);
  list<ControllableBatch>::iterator ncbi = next(cbi);

  int cbiisoutputbatch = cbi->IsOutputControllableBatch(this);
  int pcbiisoutputbatch = pcbi->IsOutputControllableBatch(this);

  cbi->SetEvolveFunction(&ControllableBatch::EvolveInFreeBehaviour); // default case

  if (cbiisoutputbatch) {
    diffdensity = cbi->GetDensity() - GetCongestionDensity();
    if (abs(diffdensity) <=  PRF::prf.Min_Err()){
      if (cbi->GetBehaviour() == Congesting_behaviour){
        cbi->SetEvolveFunction(&ControllableBatch::EvolveInCongestingOutputBehaviour);
      }
    }
    else{ // density != congestion density and cbi is outpubatch
      ControllableBatch *newcb = new ControllableBatch;
      newcb->Copy(*cbi); // create a copy of the current batch

      newcb->SetLength(0.0);
      newcb->SetDensity(GetCongestionDensity());
      newcb->SetSpeed(GetFlowOfOutputTransition() / GetCongestionDensity());
      newcb->SetEvolveFunction(&ControllableBatch::EvolveInFreeToCongestingBehaviourCongBatch);

      marks->insert(cbi, *newcb);

      // set function cbi
      cbi->SetEvolveFunction(&ControllableBatch::EvolveInFreeToCongestingBehaviourFreeBatch);
      cbi++;
    }
  }
  else{
    if ((!cbiisoutputbatch) && (i != 0) && (cbi->IsInContactWithByFront(*pcbi))){
      ControllableBatch *newcb = new ControllableBatch;
      newcb->Copy(*cbi); // create a copy of the current batch


      newcb->SetLength(0.0);
      newcb->SetDensity(pcbi->GetDensity());
      newcb->SetSpeed(pcbi->GetSpeed());
      newcb->SetEvolveFunction(&ControllableBatch::EvolveInFreeToCongestingBehaviourMiddlePlaceWithContactCongBatch);

      marks->insert(cbi, *newcb);
      // set function
      cbi->SetEvolveFunction(&ControllableBatch::EvolveInFreeToCongestingBehaviourMiddlePlaceWithContactFreeBatch);
      cbi++;
    }
  }
}


//------------------------------------------------------------------------------
// It sets the  evolution function to an uncongesting batch
//------------------------------------------------------------------------------
void TriangularBatchPlace::SetFunctionUncongestingBatch(list<ControllableBatch>::iterator cbi, int i)
{
  double diffdensity;
  double downflow;

  list<ControllableBatch>::iterator pcbi = prev(cbi);
  list<ControllableBatch>::iterator ncbi = next(cbi);

  int cbiisoutputbatch = cbi->IsOutputControllableBatch(this);
  int pcbiisoutputbatch = pcbi->IsOutputControllableBatch(this);

  cbi->SetEvolveFunction(&ControllableBatch::EvolveInFreeBehaviour); // default case

  if (cbiisoutputbatch){
    downflow = GetOutputTransitonsFlow();
    diffdensity = pcbi->GetDensity() - (downflow/instantaneousspeed);
    if (abs(diffdensity) <=  PRF::prf.Min_Err()){
      if (cbi->GetBehaviour() == Uncongesting_behaviour){
        cbi->SetEvolveFunction(&ControllableBatch::EvolveInUncongestingOutputBehaviour);
      }
    }
    else{
      ControllableBatch *newcb = new ControllableBatch;
      newcb->Copy(*cbi);

      newcb->SetLength(0.0);
      newcb->SetDensity(downflow/instantaneousspeed);
      newcb->SetSpeed(instantaneousspeed);
      newcb->SetEvolveFunction(&ControllableBatch::EvolveInFreeToCongestingBehaviourCongBatch);
      marks->insert(cbi, *newcb); // create a copy of the current batch

      //update batches evolve functions
      cbi->SetEvolveFunction(&ControllableBatch::EvolveInFreeToCongestingBehaviourFreeBatch);
      cbi++;
    }
  }
  else{
    if (!cbiisoutputbatch && (i == 0)){
      // create a new batch
      ControllableBatch *newcb = new ControllableBatch;
      newcb->Copy(*cbi);

      newcb->SetLength(0.0);
      newcb->SetDensity(GetCriticalDensity());
      newcb->SetSpeed(instantaneousspeed);
      newcb->SetEvolveFunction(&ControllableBatch::EvolveInUncongestingToFreeBehaviourFreeBatch);
      marks->insert(cbi, *newcb); // create a copy of the current batch

      //update batches evolve functions
      cbi->SetEvolveFunction(&ControllableBatch::EvolveInUncongestingToFreeBehaviourCongBatch);
      cbi++;
    }
    else{
      if (!cbiisoutputbatch && (i != 0) && cbi->IsInContactWithByFront(*pcbi)){
        ControllableBatch *newcb = new ControllableBatch;
        newcb->Copy(*cbi);
        if ((pcbi->GetState(this) == Congested_st) && pcbi->GetSpeed() > cbi->GetSpeed()){
          newcb->SetLength(0.0);
          newcb->SetDensity(pcbi->GetDensity());
          newcb->SetSpeed(pcbi->GetSpeed());
          //update batches evolve functions
          newcb->SetEvolveFunction(&ControllableBatch::EvolveInFreeToCongestingBehaviourMiddlePlaceWithContactCongBatch);
          marks->insert(cbi, *newcb); // create a copy of the current batch
          cbi->SetEvolveFunction(&ControllableBatch::EvolveInFreeToCongestingBehaviourMiddlePlaceWithContactFreeBatch);
        }
        else{
          newcb->SetLength(0.0);
          newcb->SetDensity(GetCriticalDensity());
          newcb->SetSpeed(instantaneousspeed);
          //update batches evolve functions
          newcb->SetEvolveFunction(&ControllableBatch::EvolveInUncongestingToFreeBehaviourFreeBatch);
          cbi->SetEvolveFunction(&ControllableBatch::EvolveInUncongestingToFreeBehaviourCongBatch);
          marks->insert(cbi, *newcb); // create a copy of the current batch
        }
        cbi++;
      }
    }
  }
}

//------------------------------------------------------------------------------
// It evolves the marks (controllable batches) of the triangular batch place
//------------------------------------------------------------------------------
void TriangularBatchPlace::EvolveMarks(double _date)
{
/*  cout << "========================\n" << "Before evolution\n" << "========================\n";
  for (list<ControllableBatch>::iterator cbi=marks->begin(); cbi!=marks->end(); ++cbi) {
    cbi->Print(ofstream &fout);
  }
*/
  for (list<ControllableBatch>::iterator cbi=marks->begin(); cbi!=marks->end(); ++cbi) {
    EvolveControllableBatch(cbi, _date);
    // fixerror?
  }
/*  cout << "========================\n" << "After evolution\n" << "========================\n";
  for (list<ControllableBatch>::iterator cbi=marks->begin(); cbi!=marks->end(); ++cbi) {
    cbi->Print(ofstream &fout);
  }
*/
}

//------------------------------------------------------------------------------
// It evolves a specific controllable batch
// TODO: verify first and last batch
//------------------------------------------------------------------------------
void TriangularBatchPlace::EvolveControllableBatch(list<ControllableBatch>::iterator cbi, double _date)
{
  list<ControllableBatch>::iterator pcbi = cbi;
  list<ControllableBatch>::iterator ncbi = cbi;
  ControllableBatch *cb, *pcb, *ncb;

  pcbi--;
  ncbi++;

  cb  = (ControllableBatch*) &(*cbi);
  pcb = (ControllableBatch*) &(*pcbi);
  ncb = (ControllableBatch*) &(*ncbi);

  cb->Evolve(simulation->stime, this, pcb, ncb);
}

//------------------------------------------------------------------------------
// It computes the date of the next event to a Controllable Batch becomes an Output Batch type
// returns -1.0 if no BOB event is possible
//------------------------------------------------------------------------------
double TriangularBatchPlace::GetBOBEvent()
{
  double cbpos;
  double cbspeed;
  double date;
  ControllableBatch cb;

  if (marks->empty()){
    return (-1.0); // no event of this type
  }

  if (state != 1.0){
    cb = marks->front();
    cbpos = cb.GetPosition();
    cbspeed = cb.GetSpeed();
    date = (length - cbpos) / cbspeed;
    date += simulation->stime->GetCurrentDate();
    return (date);
  }
  return (-1.0);
}


//------------------------------------------------------------------------------
// It computes the date of the next event to a Destruction of an Output Batch type
//------------------------------------------------------------------------------
double TriangularBatchPlace::GetDOBEvent()
{
  double date = -1.0;
  double outputflow = GetOutputTransitonsFlow();
  ControllableBatch *ocb;

  if (HasOutputBatch() == 1){
    ocb = GetOutputControllableBatch();
    if (ocb->GetState(this) == Free_st){ // free state
      if ((ocb->GetBehaviour() == Free_behaviour) && (instantaneousspeed != 0.0)){
        date = simulation->stime->GetCurrentDate() + (ocb->GetLength()/ocb->GetSpeed());
      }
    }
    else{
      if((outputflow != 0.0) && ((ocb->GetBehaviour() == Free_behaviour))) {
//          date = simulation->stime->GetCurrentDate() + cb->GetLength() * GetCongestionDensity() / outputflow;
        date = ocb->GetLength() * ocb->GetDensity() / outputflow;
	      date += simulation->stime->GetCurrentDate();
			}
    }
  }
  return (date);
}

//------------------------------------------------------------------------------
// It computes the date of the next event to a Full accumulation/congestion of any batch
// (Batch Becomes Dense) type
//------------------------------------------------------------------------------
double TriangularBatchPlace::GetBBDEvent()
{
  double date = -1.0;
  double mindate = -1.0;
  int i = 0;
  double diffdensity; // difference of densities
  double outputtransitionflow;
  double congesteddensity;
  ControllableBatch *ocb;
  list<ControllableBatch>::iterator pcb; // previous controllable batch

  i = 0;
  for (list<ControllableBatch>::iterator cb=marks->begin(); cb!=marks->end(); ++cb, i++) {
    if (cb->IsOutputControllableBatch(this)) { // congestion of the output batch
      congesteddensity = GetCongestionDensity();
      if ((cb->GetBehaviour() == Congesting_behaviour) && (abs(congesteddensity - cb->GetDensity()) > PRF::prf.Min_Err())){
        outputtransitionflow = GetOutputTransitonsFlow();
        date = cb->GetLength() * (congesteddensity - cb->GetDensity()) / (cb->GetSpeed() * congesteddensity - outputtransitionflow);
        date = date + simulation->stime->GetCurrentDate();
      }
    }
    else{
      if (i != 0){
        pcb = cb;
        pcb--;

        diffdensity = pcb->GetDensity() - cb->GetDensity();

        if (cb->IsInContactWithByFront(*pcb) && (diffdensity > PRF::prf.Min_Err())){ // in contact, compute date to full congestion
          date = (cb->GetLength() * diffdensity) / ((cb->GetSpeed() * pcb->GetDensity()) - (pcb->GetSpeed() * pcb->GetDensity())) ;
          date = date + simulation->stime->GetCurrentDate();
        }
      }
    }
        // get the minimal date
    if ((mindate > date) || (mindate == -1.0)) {
      mindate = date;
    }
  }

  return (mindate);
}

//------------------------------------------------------------------------------
// It computes the date of the next event to a Two Batches Meet
//------------------------------------------------------------------------------
double TriangularBatchPlace::GetTBMEvent()
{

  double date = -1.0;
  double mindate = -1.0;
  int i = 0;
  double diffdensity; // difference of densities
  list<ControllableBatch>::iterator pcb; // previous controllable batch

  if (marks->size() <= 1)
    return(date);

  i = 0;
  for (list<ControllableBatch>::iterator cb=marks->begin(); cb!=marks->end(); ++cb, i++) {
    if (cb->IsOutputControllableBatch(this) || i==0 ) // not previous lot
      continue;

    pcb = cb;
    pcb--;

    if (pcb->GetState() == Free_st) // Front batch is a free batch
      continue;

    if ((pcb->GetState() == Congested_st) && (pcb->IsOutputControllableBatch(this)))
      continue; // bmob event

    diffdensity = pcb->GetDensity() - cb->GetDensity();


    if (((cb->GetSpeed() - pcb->GetSpeed()) > PRF::prf.Min_Err()) && (diffdensity > PRF::prf.Min_Err())) { // compute meeting date
      date = simulation->stime->GetCurrentDate() +
             (cb->GetPosition() - pcb->GetPosition() + pcb->GetLength()) / (pcb->GetSpeed() - cb->GetSpeed()) ;
    }

    // get the minimal date
    if ((mindate > date) || (mindate == -1.0)) {
      mindate = date;
    }
  }

  return (mindate);
}

//------------------------------------------------------------------------------
// It computes the date of the next event to a Batch Meets Output Batch
// TODO: review this method
//------------------------------------------------------------------------------
double TriangularBatchPlace::GetBMOBEvent()
{
  double date = -1.0;
  double outputtransitionflow;
  double congesteddensity;
  ControllableBatch *ocb;
  list<ControllableBatch>::iterator cb;


/*  if ((HasOutputBatch() == 0) || (marks->size() <= 1)){
   return (date);
  }

  ocb = GetOutputControllableBatch();
  cb = marks->begin();
  cb++;
  congesteddensity = GetCongestionDensity();

  if (//(abs(congesteddensity - ocb->GetDensity()) < PRF::prf.Min_Err()) &&
      (ocb->GetState() == Congested_st)){ // output batch is congested
    outputtransitionflow = GetOutputTransitonsFlow();
    if ((cb->GetBehaviour() != Congesting_behaviour) && (!cb->IsInContactWithByFront(*ocb))){ // batches are not in contact
      if (((congesteddensity * cb->GetSpeed()) - outputtransitionflow) != 0){ // maybe congesteddensity is ocb->density
        date =  simulation->stime->GetCurrentDate() +
                (congesteddensity * (length - ocb->GetLength() - cb->GetPosition()) / ( (cb->GetSpeed() * congesteddensity) - outputtransitionflow));
       }
    }
    else{
      if (((congesteddensity - cb->GetDensity()) > PRF::prf.Min_Err()) && (cb->GetLength() > PRF::prf.Min_Err()) &&
          (cb->GetSpeed() > PRF::prf.Min_Err()) && (abs(congesteddensity - cb->GetDensity()) > PRF::prf.Min_Err())){
         date = simulation->stime->GetCurrentDate() +
                (((congesteddensity - cb->GetDensity()) * cb->GetLength()) / ((cb->GetSpeed() * congesteddensity) - outputtransitionflow));
      }
    }
  }
*/
  return (date);

}

//------------------------------------------------------------------------------
// It computes the date of the next event to Output Batch Decongestion
//------------------------------------------------------------------------------
double TriangularBatchPlace::GetOBDEvent()
{
  double date = -1;
  double outflow;
  double ddensity;
  ControllableBatch *ocb;

  if (HasOutputBatch() == 0)
    return (date);

  outflow = GetOutputTransitonsFlow();
  ddensity = outflow / GetInstantaneousSpeed();
  ocb = GetOutputControllableBatch();

  if ((ocb->GetBehaviour() == Uncongesting_behaviour) && (abs(ocb->GetDensity() - ddensity) > PRF::prf.Min_Err())){
    if (abs(ocb->GetSpeed() * ddensity - outflow) > PRF::prf.Min_Err()){
      date = ( simulation->stime->GetCurrentDate() +
               (ocb->GetLength() * (ddensity - ocb->GetDensity())) / ((ocb->GetSpeed() * ddensity) - outflow));
    }
  }
  return (date);
}

//------------------------------------------------------------------------------
// It computes the date of the next event to Batch Decongestion
// TODO : review this method for all batches
//------------------------------------------------------------------------------
double TriangularBatchPlace::GetBDEvent()
{
  int i = 0;
  double date = -1;
  double mindate = -1;
  double diffdensity; // difference of densities
  list<ControllableBatch>::iterator pcb; // previous controllable batch

  if (marks->size() <= 1)
    return(date);

  i = 0;
  for (list<ControllableBatch>::iterator cb=marks->begin(); cb!=marks->end(); ++cb, i++) {
    if (i == 0) // BBF event
      continue;

    if (cb->IsOutputControllableBatch(this))
      continue; // OBD event

    if (cb->GetBehaviour() != Uncongesting_behaviour)
      continue;

    pcb = cb;
    pcb--;

    if (!cb->IsInContactWithByFront(*pcb)) // BFF event
      continue;

    diffdensity = pcb->GetDensity() - cb->GetDensity();
    if (((cb->GetSpeed() - pcb->GetSpeed()) < PRF::prf.Min_Err()) && (diffdensity < PRF::prf.Min_Err())) { // verify if need diffdensity
      date = (cb->GetLength() * diffdensity) / ((cb->GetSpeed() * pcb->GetDensity()) - (pcb->GetSpeed() * pcb->GetDensity()));
      date =  date + simulation->stime->GetCurrentDate();
    }

    // get the minimal date
    if ((mindate > date) || (mindate == -1.0)) {
      mindate = date;
    }
  }
  return (date);
}

//------------------------------------------------------------------------------
// It computes the date of the next event to Batch Becomes Free
//------------------------------------------------------------------------------
double TriangularBatchPlace::GetBBFEvent()
{
  double date = -1;
  double mindate = -1;
  int i = 0;
  list<ControllableBatch>::iterator pcb; // previous controllable batch

  i = 0;
  for (list<ControllableBatch>::iterator cb=marks->begin(); cb!=marks->end(); ++cb, i++) {
    if (cb->IsOutputControllableBatch(this))
      continue;
    if (cb->GetBehaviour() != Uncongesting_behaviour)
      continue;

    pcb = cb;
    pcb--;

    if ((i == 0) || (!cb->IsInContactWithByFront(*pcb))){
      date = (cb->GetLength() * (GetCriticalDensity() - cb->GetDensity())) / ((cb->GetSpeed() * GetCriticalDensity()) - (instantaneousspeed * GetCriticalDensity()));
      date = date + simulation->stime->GetCurrentDate();

    }

    // get the minimal date
    if ((mindate > date) || (mindate == -1.0)) {
      mindate = date;
    }
  }
  return (date);
}



//------------------------------------------------------------------------------
// Print
//------------------------------------------------------------------------------
void TriangularBatchPlace::Print(ostream &fout)
{ fout << "Name:                  " << name << endl;
  fout << "   Type:               " << "Triangular Batch" << endl;
  fout << "   Behaviour:          " << behaviour << endl;
  fout << "   Density:            " << density << endl;
  fout << "   Maximum speed:      " << maxspeed << endl;
  fout << "   Speed:              " << speed << endl;
  fout << "   Length:             " << length << endl;
  fout << "   Maximum Flow:       " << maxflow << endl;
  fout << "   Flow:               "  << flow << endl;
  fout << "   Initial Tr. Batches:"  << initialmarks->size() << endl;

  for (list<ControllableBatch>::iterator it=initialmarks->begin(); it!=initialmarks->end(); ++it) {
    it->GetState(this);
    it->Print(fout);
  }

  if (!steadymarks->empty()){
    fout << "   Steady Tr. Batches: "  << steadymarks->size() << endl;

    for (list<ControllableBatch>::iterator it=steadymarks->begin(); it!=steadymarks->end(); ++it) {
      it->GetState(this);
      it->Print(fout);
    }
  }

  fout << "   Triangular Batches: "  << marks->size() << endl;

  for (list<ControllableBatch>::iterator it=marks->begin(); it!=marks->end(); ++it) {
    it->GetState(this);
    it->Print(fout);
  }
  fout << "   Arcs:          " << endl;
  PrintArcs(fout);
}


//------------------------------------------------------------------------------
// Write
//------------------------------------------------------------------------------
void TriangularBatchPlace::Write(ostream &fout)
{
  fout << "Name:             " << name << endl;
  fout << "   Type:          " << "Triangular Batch" << endl;
  fout << "   Density:       " << density << endl;
  fout << "   Speed:         " << speed << endl;
  fout << "   Length:        " << length << endl;
  fout << "   Flow:          " << flow << endl;
  fout << "   Batches:       " << marks->size() << endl;

  for (list<ControllableBatch>::iterator it=marks->begin(); it!=marks->end(); ++it) {
    it->GetState(this);
    it->Write(fout);
  }
}
