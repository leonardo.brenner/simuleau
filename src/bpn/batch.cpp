//====================================================================================//
//                                                                                    //
//                                      Batch class                                   //
//                                                                                    //
//====================================================================================//
//  This File:   batch.cpp                        Language: C++  (xlC and CC)         //
//  Software:    SIMULEAU                                                             //
//====================================================================================//
//  Creation:    28/apr/16                        by: Leonardo.Brenner@lsis.org       //
//  Last Change: 28/apr/16                        by: Leonardo.Brenner@lsis.org       //
//====================================================================================//
#include "simuleau.h"

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
// Methods of the Batch  class
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Constructor
//------------------------------------------------------------------------------
Batch::Batch()
{
  strcpy(name,"");
  length     = 0.0;
  density    = 0.0;
  position   = 0.0;
  outputflow = 0.0;
}

//------------------------------------------------------------------------------
// Constructor
//------------------------------------------------------------------------------
Batch::Batch(char *_name)
{
  strcpy(name,"_name");
  length     = 0.0;
  density    = 0.0;
  position   = 0.0;
  outputflow = 0.0;
}

//------------------------------------------------------------------------------
// Initialized constructor
//------------------------------------------------------------------------------
Batch::Batch(double _length, double _density, double _position)
{
  length     = _length;
  density    = _density;
  position   = _position;
  outputflow = 0.0;
}

//------------------------------------------------------------------------------
// Initialized constructor
//------------------------------------------------------------------------------
Batch::Batch(double _inputflow, double _speed)
{
  length     = 0.0;
  density    = _inputflow/_speed;
  position   = 0.0;
  outputflow = 0.0;
}

//------------------------------------------------------------------------------
// Destructor
//------------------------------------------------------------------------------
Batch::~Batch()
{
  //dtor
}

//------------------------------------------------------------------------------
// It returns the batch name
//------------------------------------------------------------------------------
char* Batch::GetName()
{
  return (name);
}

//------------------------------------------------------------------------------
// Copy
//------------------------------------------------------------------------------
void Batch::Copy(const Batch &_batch)
{
  strcpy(name, _batch.name);
  length     = _batch.length;
  density    = _batch.density;
  position   = _batch.position;
  outputflow = _batch.outputflow;
}

//------------------------------------------------------------------------------
// IsEqual
//------------------------------------------------------------------------------
int Batch::IsEqual(const Batch &_batch)
{
  if (abs(length -  _batch.length) > PRF::prf.Min_Err())
    return 0;
  if (abs(density - _batch.density) > PRF::prf.Min_Err())
    return 0;
  if (abs(position - _batch.position) > PRF::prf.Min_Err())
    return 0;
  return 1;
}

//------------------------------------------------------------------------------
// It sets a new density
//------------------------------------------------------------------------------
void Batch::SetDensity(double _density)
{
  density = _density;
}

//------------------------------------------------------------------------------
// It sets a new length
//------------------------------------------------------------------------------
void Batch::SetLength(double _length)
{
  length = _length;
}

//------------------------------------------------------------------------------
// It sets a new position
//------------------------------------------------------------------------------
void Batch::SetPosition(double _position)
{
  position = _position;
}

//------------------------------------------------------------------------------
// It moves the position
//------------------------------------------------------------------------------
void Batch::MovePosition(double _length)
{
  position += _length;
}

//------------------------------------------------------------------------------
// It returns the density
//------------------------------------------------------------------------------
double Batch::GetDensity()
{
  return (density);
}

//------------------------------------------------------------------------------
// It returns the length
//------------------------------------------------------------------------------
double Batch::GetLength()
{
  return (length);
}

//------------------------------------------------------------------------------
// It returns the position
//------------------------------------------------------------------------------
double Batch::GetPosition()
{
  return (position);
}

//------------------------------------------------------------------------------
// It computes and returns the output flow of a batch
//------------------------------------------------------------------------------
double Batch::GetOutputFlow(double _speed)
{
  outputflow = density * _speed;
  return (outputflow);
}

//------------------------------------------------------------------------------
// It applies the free behaviour at this batch
//------------------------------------------------------------------------------
void Batch::EvolveInFreeBehaviour(Simtime *_stime, BatchPlace *bp)
{
  double newposition;

  newposition = position + (bp->GetInstantaneousSpeed() * _stime->StepTime());
  if ((position == 0.0) && (length == 0.0)){ //new batch, input in free behaviour
    length = newposition;
    position = newposition;
  }
  else{
    if ((bp->GetLength() - position) < PRF::prf.Min_Err()){
      length -= (bp->GetInstantaneousSpeed() * _stime->StepTime());
      position = bp->GetLength();
    }
    else{
      position = newposition;
    }
  }
}

//------------------------------------------------------------------------------
// It applies the accumulated behaviour to the outpout batch
//------------------------------------------------------------------------------
void Batch::EvolveInAccumulatedOuputBehaviour(Simtime *_stime, BatchPlace *bp, double _flow)
{
  length = length - (_stime->StepTime() * _flow / bp->GetDensity());
}

//------------------------------------------------------------------------------
// It applies the free to accumulated behaviour in the accumulated batch
//------------------------------------------------------------------------------
void Batch::EvolveInFreeToAccumulatedBehaviourAccBatch(Simtime *_stime, BatchPlace *bp, double _flow)
{
  length = (((bp->GetInstantaneousSpeed() * density - _flow) / ( bp->GetDensity() - density)) * _stime->StepTime());
	density = bp->GetDensity();
}


//------------------------------------------------------------------------------
// It applies the free to accumulated behaviour in the free batch
//------------------------------------------------------------------------------
void Batch::EvolveInFreeToAccumulatedBehaviourFreeBatch(Simtime *_stime, BatchPlace *bp, double outputbatchlength, double _flow)
{
	length = length + ((_flow - bp->GetInstantaneousSpeed() * bp->GetDensity()) / (bp->GetDensity() - density)) * _stime->StepTime() ;
	position = bp->GetLength() - outputbatchlength;
}



//------------------------------------------------------------------------------
// It applies the partially accumulated behaviour in the accumulated batch
//------------------------------------------------------------------------------
void Batch::EvolveInPartiallyAccumulatedBehaviourAccBatch(Simtime *_stime, BatchPlace *bp, double outputbatchlength, double _flow)
{
	length = ((density / (bp->GetDensity() - density)) * _stime->StepTime() *(bp->GetInstantaneousSpeed() - (_flow/bp->GetDensity()))) ;
	density = bp->GetDensity();
  position = bp->GetLength() - outputbatchlength;
}

//------------------------------------------------------------------------------
// It applies the partially accumulated behaviour in the free batch
//------------------------------------------------------------------------------
void Batch::EvolveInPartiallyAccumulatedBehaviourFreeBatch(Simtime *_stime, BatchPlace *bp, double outputbatchlength, double batch2length, double _flow)
{
	length = length +((bp->GetDensity() / (density - bp->GetDensity())) * _stime->StepTime() * (bp->GetInstantaneousSpeed() - (_flow/bp->GetDensity()))) ;
	position = bp->GetLength() - outputbatchlength - batch2length;
}


//------------------------------------------------------------------------------
// Print
//------------------------------------------------------------------------------
void Batch::Print(ostream &fout)
{
  fout << "                   (" << length << ", " << density << ", " << position << ")" << endl;
}

//------------------------------------------------------------------------------
// Write
//------------------------------------------------------------------------------
void Batch::Write(ostream &fout)
{
  fout << "                   (" << length << ", " << density << ", " << position << ")" << endl;
}

