//====================================================================================//
//                                                                                    //
//                                  SIMULEAU Main                                     //
//                                                                                    //
//====================================================================================//
//  This File:   simuleau.cpp                     Language: C++  (xlC and CC)         //
//  Software:    SIMULEAU                                                             //
//====================================================================================//
//  Creation:    12/apr/16                        by: Leonardo.Brenner@lsis.org       //
//  Last Change: 12/apr/16                        by: Leonardo.Brenner@lsis.org       //
//====================================================================================//
#include "simuleau.h"


//====================================================================================//
//                                   Global Variables                                 //
//====================================================================================//
 BPN *bpn = NULL;
 Schedule *schedule = NULL;
 Simulate *simulation = NULL;
 PRF prf;



//====================================================================================//
//   BPN compilation function prototype - glc.y                                       //
//====================================================================================//
void Compile_Network(const simuleau_name simuleau_level_name, const simuleau_name str_level_name);

//====================================================================================//
void Compile_BPN()                                       //   Main compile peration   //
//====================================================================================//
{                          //     BPN model name in all directories handled by SIMULEAU
 simuleau_name model_name; // current directory (just the name, no paths, no extensions)
 simuleau_name bpn_level_name; // directory where the textual network  is      (./*.bpn)
 simuleau_name str_level_name; // directory to store the internal structure(./str/*.str)

 Timer     T;              // timer to inform total time spent

 cout << "\nCompilation of a BPN model";
 T.clear();
 Ask_an_Existing_File_Name(bpn_level_name, bpn_file);
 Get_Only_the_File_Name(bpn_level_name, model_name);
 strcpy(str_level_name, "str/");  strcat(str_level_name, model_name);
 if (Permission_to_Proceed(str_level_name, str_file)) {              // .bpn compilation
   T.start();
   cout << "Compile_Network\n";
   Compile_Network(bpn_level_name, str_level_name);
//   simulation = new Simulate(bpn, schedule); // prepare to simulation
   T.stop();
   Notify_Time_Spend(T, "compilation of a BPN model");
 }
}

//====================================================================================//
bool Start_Up_BPN()                                  //   Main simulation operation   //
//====================================================================================//
{
  bool answer = false;
  simuleau_name name;
  simuleau_name bpn_level_name; // directory where the textual network  is      (./*.bpn)
  simuleau_name dyn_level_name; // directory to store the simulation results(./dyn/*.dyn)

  if (bpn == NULL){
   cout << "There is no Batch Petri Net model (bpn) in memory.\n";
   cout << "You must compile a bpn using the option 1 in main menu.\n";
   return(false);
  }

  simulation = new Simulate(bpn, schedule); // prepare to simulation
  cout << "\nSimulation of the model '" << bpn->GetName() << "' ("
        << bpn->NumberOfPlaces() << " places - " << bpn->NumberOfTransitions()  << " transitions)\n";
   Ask_a_File_Name(bpn_level_name, dyn_file);
   Get_Only_the_File_Name(bpn_level_name, name);
   strcpy(dyn_level_name, "dyn/");  strcat(dyn_level_name, name);

   if (Permission_to_Proceed(dyn_level_name, dyn_file)) {
     simulation->Baptise(name);
     PRF::prf.Max_Time(Ask_a_Simulation_Time());
     answer = true;
   }

  return(answer);
}

//====================================================================================//
bool Start_Up_BPN_SteadyState()                 //   Compute Steady state operation   //
//====================================================================================//
{
  bool answer = false;
  simuleau_name name;
  simuleau_name bpn_level_name; // directory where the textual network  is      (./*.bpn)
  simuleau_name dyn_level_name; // directory to store the simulation results(./dyn/*.dyn)

  if (bpn == NULL){
   cout << "There is no Batch Petri Net model (bpn) in memory.\n";
   cout << "You must compile a bpn using the option 1 in main menu.\n";
   return(false);
  }

  cout << "\nComputation of the steady state for the model '" << bpn->GetName() << "' ("
        << bpn->NumberOfPlaces() << " places - " << bpn->NumberOfTransitions()  << " transitions)\n";

   if (Ask_an_Answer("This procedure will modify the steady state. Procede anyway (y/n)?")) {
     answer = true;
   }

  return(answer);
}

//====================================================================================//
//                                  SIMULEAU Main                                     //
//====================================================================================//
int main()
{
 bool          cont = true;
 simuleau_name name;

 //cout.setf(ios::fixed);
 //cout.precision(16);
 //set_new_handler(Communicate_Out_of_Memory);
 Create_Temporary_Directories();
 do {
   switch (Welcome_Get_Simuleau_Option()) {
//====================================================================================//
//                                Compiling Operations                                //
//====================================================================================//
    case compile         : Compile_BPN();                                break;
//====================================================================================//
//                               Simulation Operations                                //
//====================================================================================//
    case simulate_xct    : if(Start_Up_BPN()){
                             simulation->Start(PRF::prf.Max_Time(), simulate_xct);
                           }
    break;
    case simulate        : if(Start_Up_BPN()){
                             simulation->Start(PRF::prf.Max_Time(), simulate);
                           }
    break;
    case simulate_onoff  : if(Start_Up_BPN()){
                             simulation->Start(PRF::prf.Max_Time(), simulate_onoff);
                           }
    break;
    case compute_steadystate : if(Start_Up_BPN_SteadyState()){
                                 bpn->ComputeSteadyState();
                               }

    break;
    case compute_periodicsteadystate : if(Start_Up_BPN_SteadyState()){
                                 bpn->ComputePeriodicSteadyState();
                               }

    break;
    case simulate_mf_onoff  : if(Start_Up_BPN()){
                             simulation->Start(PRF::prf.Max_Time(), simulate_mf_onoff);
                           }
    break;

//====================================================================================//
//                                  Inspect Operations                                //
//====================================================================================//
    case see_bpn    : See_BPN();                                         break;
    case see_place  : See_place();                                       break;
    case see_trans  : See_trans();                                       break;
    case see_events : See_events();                                      break;



//====================================================================================//
//                                   Other Operations                                 //
//====================================================================================//
   case credits     : Show_Credits();                              break;
   case goodbye     : cont = false;                                break;
   default          : Programming_Error("invalid SIMULEAU option", 0000)
   }
 } while (cont);
 Say_Goodbye();
 return(EXIT_SUCCESS);
}

