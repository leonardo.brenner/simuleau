//====================================================================================//
//                                                                                    //
//                                   Schedule class                                   //
//                                                                                    //
//====================================================================================//
//  This File:   schedule.h                       Language: C++  (xlC and CC)         //
//  Software:    SIMULEAU                                                             //
//====================================================================================//
//  Creation:    08/may/16                        by: Leonardo.Brenner@lsis.org       //
//  Last Change: 24/jan/20                        by: Leonardo.Brenner@lsis.org       //
//====================================================================================//
#ifndef SCHEDULE_H
#define SCHEDULE_H

extern Simulate *simulation;

class Schedule
{
  public:
    Schedule();
    virtual ~Schedule();

    void Copy(const Schedule *_schedule);

    //input functions
    void AddEvent(Event& e, ev_type type);
    void AddControlledEvent(ControlledEvent& ce, ev_type type);
    void AddBreakpoint(Breakpoint& bp);

    //output functions
    char* GetEventDescription(ev_type type);
    char* GetBreakpointDescription(breakpoint_type type);
    int IsEmpty();
    void Clean(list<ControlledEvent> *cevl, double _currentdate);
    void CleanAll();

    //print functions
    void PrintEvents(ostream &fout);
    void PrintCE(ostream &fout);
    void PrintBP(ostream &fout);
    void Print(ostream &fout);
    void WriteEvents(ofstream & fout);
    void WriteCE(ofstream & fout);
    void WriteBP(ofstream & fout);
    void WriteNextEvents(ofstream & fout);

    //simulation functions
    void   ComputeEvents(BPN *_bpn);
    void   ComputeNextDate(BPN *_bpn, double duration);
    double ComputeNextEvent(double duration);
    double ComputeNextEventDate(list<Event> *evl, double nextdate, ev_type type);
    double ComputeNextControlledEventDate(list<ControlledEvent> *cevl, double nextdate, ev_type type);
    double ComputeNextBreakpointDate(list<Breakpoint> *bpl, double nextdate);
    void   ProcessEvents(BPN *_bpn);
    void   ProcessBreakpoints(BPN *_bpn);

  protected:
    list<Event>           *dtf;   // Discrete Transition is Fired
    list<Event>           *cpe;   // Continuous Place becomes Empty
    list<Event>           *dte;   // Discrete Transition becomes Enable
    list<Event>           *bob;   // Batch becomes an Output Batch
    list<Event>           *dob;   // Destruction of an Output Batch
    list<Event>           *bbd;   // Batch Becomes Dense
    list<Event>           *tbm;   // Two Batches Meet
    list<Event>           *bmob;  // A batch meets an output batch
    list<Event>           *obd;   // Decongestion of the output batch
    list<Event>           *bd;    // Decongestion of a batch in contact head
    list<Event>           *bbf;   // Full decongestion of an alone batch
    list<Event>           *psq;   // Place reaches Steady marking quantity
    list<Event>           *rbf;   // Remaining quantity passed by an input transition becomes equal to free steady quantity.

    list<ControlledEvent> *tfce;  // Transition maximum Flow changes
    list<ControlledEvent> *psce;  // Place maximum Speed changes

    list<Breakpoint>      *breakpoint; // breakpoint to print results

    list<Event>           *nextevents; // list of the next events to be simulated
    list<ControlledEvent> *nextcontrolledevents; // list of the next controlled events to be simulated
    list<Breakpoint>      *nextbreakpoints; // list of the next breakpoint to be simulated


  private:
};

#endif // SCHEDULE_H
