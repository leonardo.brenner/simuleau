//====================================================================================//
//                                                                                    //
//                                     Event class                                    //
//                                                                                    //
//====================================================================================//
//  This File:   event.h                          Language: C++  (xlC and CC)         //
//  Software:    SIMULEAU                                                             //
//====================================================================================//
//  Creation:    07/may/16                        by: Leonardo.Brenner@lsis.org       //
//  Last Change: 07/may/16                        by: Leonardo.Brenner@lsis.org       //
//====================================================================================//
#ifndef EVENT_H
#define EVENT_H

//====================================================================================//
//                                 Generic event class                                //
//====================================================================================//
//    Event class implements a generic event that will be used to implement all types //
// of events in the schedule class.                                                   //
//====================================================================================//
class Event
{
  public:
    Event();
    Event(Node &_node, double _date, ev_type _type);
    virtual ~Event();

    void Copy(const Event &_event);

    //output functions
    double  GetDate();
    ev_type GetType();
    Node*   GetNode();
    virtual char* GetDescription();

    // print functions
    virtual void Print(ostream &fout);

  protected:
    Node   *node;
    double  date;
    ev_type type;

  private:
};



//====================================================================================//
//                                Controlled event class                              //
//====================================================================================//
//    Controlled event class implements the storage of external controlled events     //
// readed from the input bpn file these events modify the flow or speed of transitions//
// or places.                                                                         //
//====================================================================================//
class ControlledEvent : public Event
{
  public:
    ControlledEvent();
    ControlledEvent(Node &_node, double _date, ev_type _type, double _value, simuleau_name _name);
    ControlledEvent(simuleau_name _idnode, Node& _node, double _date, ev_type _type, double _value, simuleau_name _name);

    virtual ~ControlledEvent();

    void Copy(const ControlledEvent &_event);

    //output functions
    char*  GetName();
    double GetValue();
    Node*  GetNode(BPN *_bpn);
    char*  GetNodeId();
    char*  GetDescription();

    // print functions
    void Print(ostream &fout);

  protected:
    double        value;
    simuleau_name idnode;
    simuleau_name name;

  private:
};





#endif // EVENT_H
