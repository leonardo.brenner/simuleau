//====================================================================================//
//                                                                                    //
//                                   Schedule class                                   //
//                                                                                    //
//====================================================================================//
//  This File:   schedule.cpp                     Language: C++  (xlC and CC)         //
//  Software:    SIMULEAU                                                             //
//====================================================================================//
//  Creation:    08/may/16                        by: Leonardo.Brenner@lsis.org       //
//  Last Change: 24/jan/20                        by: Leonardo.Brenner@lsis.org       //
//====================================================================================//
#include "simuleau.h"


//------------------------------------------------------------------------------
// Constructor
//------------------------------------------------------------------------------
Schedule::Schedule()
{
  dtf = new list<Event>;   // Discrete Transition is Fired
  dtf->clear();
  cpe = new list<Event>;   // Continuous Place becomes Empty
  cpe->clear();
  dte = new list<Event>;   // Discrete Transition becomes Enable
  dte->clear();
  bob = new list<Event>;   // Batch becomes an Output Batch
  bob->clear();
  dob = new list<Event>;   // Destruction of an Output Batch
  dob->clear();
  bbd = new list<Event>;   // Batch Becomes Dense
  bbd->clear();
  tbm = new list<Event>;   // Two Batches Meet
  tbm->clear();
  bmob = new list<Event>;  // A batch meets an output batch
  bmob->clear();
  obd = new list<Event>;   // Decongestion of the output batch
  obd->clear();
  bd  = new list<Event>;   // Decongestion of a batch in contact head
  bd->clear();
  bbf = new list<Event>;   // Full decongestion of a batch alone
  bbf->clear();
  psq = new list<Event>;   // Place reaches Steady marking Quantity
  psq->clear();
  rbf = new list<Event>;   // Remaining quantity entered by an input transition becomes equal to free steady marking quantity.
  rbf->clear();


  tfce = new list<ControlledEvent>;  // Transition maximum Flow changes
  tfce->clear();
  psce = new list<ControlledEvent>;  // Place maximum Speed changes
  psce->clear();

  breakpoint = new list<Breakpoint>;
  breakpoint->clear();

  nextevents = new list<Event>;
  nextevents->clear();

  nextcontrolledevents = new list<ControlledEvent>;
  nextcontrolledevents->clear();

  nextbreakpoints = new list<Breakpoint>;
  nextbreakpoints->clear();
}

//------------------------------------------------------------------------------
// Destructor
//------------------------------------------------------------------------------
Schedule::~Schedule()
{
  if (dtf)
    delete dtf;
  if (cpe)
    delete cpe;
  if (dte)
    delete dte;
  if (bob)
    delete bob;
  if (dob)
    delete dob;
  if (bbd)
    delete bbd;
  if (tbm)
    delete tbm;
  if (bmob)
    delete bmob;
  if (obd)
    delete obd;
  if (bd)
    delete bd;
  if (bbd)
    delete bbf;
  if (psq)
    delete psq;
  if (rbf)
    delete rbf;


  if (tfce)
    delete tfce;
  if (psce)
    delete psce;

  if (breakpoint)
    delete breakpoint;

  if (nextevents)
    delete nextevents;

  if (nextcontrolledevents)
    delete nextcontrolledevents;

  if (nextbreakpoints)
    delete nextbreakpoints;
}


//------------------------------------------------------------------------------
// Copy
//------------------------------------------------------------------------------
void Schedule::Copy(const Schedule *_schedule)
{
  dtf->clear();
  for (list<Event>::iterator it=_schedule->dtf->begin(); it!=_schedule->dtf->end(); ++it) {
    Event *newevent = new Event;
    newevent->Copy(*it);
    dtf->push_back(*newevent);
  }

  cpe->clear();
  for (list<Event>::iterator it=_schedule->cpe->begin(); it!=_schedule->cpe->end(); ++it) {
    Event *newevent = new Event;
    newevent->Copy(*it);
    cpe->push_back(*newevent);
  }

  dte->clear();
  for (list<Event>::iterator it=_schedule->dte->begin(); it!=_schedule->dte->end(); ++it) {
    Event *newevent = new Event;
    newevent->Copy(*it);
    dte->push_back(*newevent);
  }

  bob->clear();
  for (list<Event>::iterator it=_schedule->bob->begin(); it!=_schedule->bob->end(); ++it) {
    Event *newevent = new Event;
    newevent->Copy(*it);
    bob->push_back(*newevent);
  }

  dob->clear();
  for (list<Event>::iterator it=_schedule->dob->begin(); it!=_schedule->dob->end(); ++it) {
    Event *newevent = new Event;
    newevent->Copy(*it);
    dob->push_back(*newevent);
  }

  bbd->clear();
  for (list<Event>::iterator it=_schedule->bbd->begin(); it!=_schedule->bbd->end(); ++it) {
    Event *newevent = new Event;
    newevent->Copy(*it);
    bbd->push_back(*newevent);
  }

  tbm->clear();
  for (list<Event>::iterator it=_schedule->tbm->begin(); it!=_schedule->tbm->end(); ++it) {
    Event *newevent = new Event;
    newevent->Copy(*it);
    tbm->push_back(*newevent);
  }

  bmob->clear();
  for (list<Event>::iterator it=_schedule->bmob->begin(); it!=_schedule->bmob->end(); ++it) {
    Event *newevent = new Event;
    newevent->Copy(*it);
    bmob->push_back(*newevent);
  }

  obd->clear();
  for (list<Event>::iterator it=_schedule->obd->begin(); it!=_schedule->obd->end(); ++it) {
    Event *newevent = new Event;
    newevent->Copy(*it);
    obd->push_back(*newevent);
  }

  bd->clear();
  for (list<Event>::iterator it=_schedule->bd->begin(); it!=_schedule->bd->end(); ++it) {
    Event *newevent = new Event;
    newevent->Copy(*it);
    bd->push_back(*newevent);
  }

  bbf->clear();
  for (list<Event>::iterator it=_schedule->bbf->begin(); it!=_schedule->bbf->end(); ++it) {
    Event *newevent = new Event;
    newevent->Copy(*it);
    bbf->push_back(*newevent);
  }

  psq->clear();
  for (list<Event>::iterator it=_schedule->psq->begin(); it!=_schedule->psq->end(); ++it) {
    Event *newevent = new Event;
    newevent->Copy(*it);
    psq->push_back(*newevent);
  }

  rbf->clear();
  for (list<Event>::iterator it=_schedule->rbf->begin(); it!=_schedule->rbf->end(); ++it) {
    Event *newevent = new Event;
    newevent->Copy(*it);
    rbf->push_back(*newevent);
  }




  tfce->clear();
  for (list<ControlledEvent>::iterator it=_schedule->tfce->begin(); it!=_schedule->tfce->end(); ++it) {
    ControlledEvent *newevent = new ControlledEvent;
    newevent->Copy(*it);
    tfce->push_back(*newevent);
  }

  psce->clear();
  for (list<ControlledEvent>::iterator it=_schedule->psce->begin(); it!=_schedule->psce->end(); ++it) {
    ControlledEvent *newevent = new ControlledEvent;
    newevent->Copy(*it);
    psce->push_back(*newevent);
  }

  breakpoint->clear();
  for (list<Breakpoint>::iterator it=_schedule->breakpoint->begin(); it!=_schedule->breakpoint->end(); ++it) {
    Breakpoint *newbreakpoint = new Breakpoint;
    newbreakpoint->Copy(*it);
    breakpoint->push_back(*newbreakpoint);
  }

  nextevents->clear();
  for (list<Event>::iterator it=_schedule->nextevents->begin(); it!=_schedule->nextevents->end(); ++it) {
    Event *newevent = new Event;
    newevent->Copy(*it);
    nextevents->push_back(*newevent);
  }

  nextcontrolledevents->clear();
  for (list<ControlledEvent>::iterator it=_schedule->nextcontrolledevents->begin(); it!=_schedule->nextcontrolledevents->end(); ++it) {
    ControlledEvent *newevent = new ControlledEvent;
    newevent->Copy(*it);
    nextcontrolledevents->push_back(*newevent);
  }

  nextbreakpoints->clear();
  for (list<Breakpoint>::iterator it=_schedule->nextbreakpoints->begin(); it!=_schedule->nextbreakpoints->end(); ++it) {
    Breakpoint *newbreakpoint = new Breakpoint;
    newbreakpoint->Copy(*it);
    nextbreakpoints->push_back(*newbreakpoint);
  }
}

//------------------------------------------------------------------------------
// Add a new event
//------------------------------------------------------------------------------
void Schedule::AddEvent(Event& e, ev_type type)
{
  switch(type){
    case Discr_trans_fires:   dtf->push_back(e);
         break;
    case Cont_place_empty:    cpe->push_back(e);
         break;
    case Discr_trans_enabled: dte->push_back(e);
         break;
    case Becomes_output_batch:bob->push_back(e);
         break;
    case Destr_output_batch:  dob->push_back(e);
         break;
    case Batch_becomes_dense: bbd->push_back(e);
         break;
    case Two_batches_meet:    tbm->push_back(e);
         break;
    case Batch_meets_output_batch: bmob->push_back(e);
         break;
    case Output_batch_decongestion: obd->push_back(e);
         break;
    case Batch_decongestion:  bd->push_back(e);
         break;
    case Batch_becomes_free:  bbf->push_back(e);
         break;
    case Place_steady_quan:    psq->push_back(e); // place reaches steady marking quantity
         break;
    case Remaining_becomes_freesteadyquan: rbf->push_back(e); //Remaining marking quantity entered by an input transition becomes free steady quantity.
         break;
  }
}

//------------------------------------------------------------------------------
// Add a new controlled external event from the input bpn file
//------------------------------------------------------------------------------
void Schedule::AddControlledEvent(ControlledEvent& ce, ev_type type)
{
  switch(type){
    case Trans_flow_ce:  tfce->push_back(ce);
         break;
    case Place_speed_ce: psce->push_back(ce);
         break;
  }
}

//------------------------------------------------------------------------------
// Add a new breakpoint from the input bpn file
//------------------------------------------------------------------------------
void Schedule::AddBreakpoint(Breakpoint& bp)
{
  breakpoint->push_back(bp);
}

//------------------------------------------------------------------------------
// Add a new controlled external event from the input bpn file
//------------------------------------------------------------------------------
char* Schedule::GetEventDescription(ev_type type)
{
  switch (type)
  {
    case Discr_trans_fires:   return("Discrete transition is fired");
    case Cont_place_empty:    return("Continuous place becomes empty");
    case Discr_trans_enabled: return("Discrete transition becomes enable");
    case Becomes_output_batch:return("Batch becomes an output batch");
    case Destr_output_batch:  return("Destruction of an output batch");
    case Batch_becomes_dense: return("Batch becomes dense");
    case Two_batches_meet:    return("Two batches meet");
    case Batch_meets_output_batch: return("A batch meets an output batch");
    case Output_batch_decongestion: return("Decongestion of the output batch");
    case Batch_decongestion:  return("Decongestion of a batch in contact ahead");
    case Batch_becomes_free:  return("Full decongestion of a batch alone");
    case Place_steady_quan:   return("Place reaches steady marking quantity");
    case Trans_flow_ce:       return("Transition maximum flow changes");
    case Place_speed_ce:      return("Place maximum speed changes");
    case Remaining_becomes_freesteadyquan: return("Remaining quantity passed by an input transition becomes free steady quantity");
  }
}

//------------------------------------------------------------------------------
// Add a new controlled external event from the input bpn file
//------------------------------------------------------------------------------
char* Schedule::GetBreakpointDescription(breakpoint_type type)
{
  switch (type)
  {
    case at_bp:     return("Breakpoint at a specific date");
    case every_bp:  return("Recurent breakpoint");
  }
}

//------------------------------------------------------------------------------
// Returns true if the schedule doesn't have more events to proceed
//------------------------------------------------------------------------------
int Schedule::IsEmpty()
{
  if (nextevents->empty() && nextcontrolledevents->empty() && nextbreakpoints->empty())
    return (1);
  else
    return (0);
}

//------------------------------------------------------------------------------
// This fonction delete all controlled event that are before the current date
//------------------------------------------------------------------------------
void Schedule::Clean(list<ControlledEvent> *cevl, double _currentdate)
{
  int numerofcontrolledevents = cevl->size();

  for (int i=0; i<numerofcontrolledevents; i++){
    ControlledEvent *ce = new ControlledEvent;
    *ce = cevl->front();
    cevl->pop_front();
    double eventdate = ce->GetDate();

    if (eventdate <= _currentdate){
      continue;
    }
    else{
      cevl->push_back(*ce);
    }
  }
}

//------------------------------------------------------------------------------
// This fonction delete all controlled event
//------------------------------------------------------------------------------
void Schedule::CleanAll()
{
  int numerofcontrolledevents = tfce->size();

  for (int i=0; i<numerofcontrolledevents; i++){
    ControlledEvent *ce = new ControlledEvent;
    *ce = tfce->front();
    tfce->pop_front();
  }

  numerofcontrolledevents = psce->size();

  for (int i=0; i<numerofcontrolledevents; i++){
    ControlledEvent *ce = new ControlledEvent;
    *ce = tfce->front();
    psce->pop_front();
  }
}

//------------------------------------------------------------------------------
// Print
//------------------------------------------------------------------------------
void Schedule::PrintEvents(ostream &fout)
{
  cout <<  "Discrete transition is fired: " << dtf->size() << endl;
  for (list<Event>::iterator it=dtf->begin(); it!=dtf->end(); ++it) {
    it->Print(fout);
  }

  cout <<  "\nContinuous place becomes empty: " << cpe->size() << endl;
  for (list<Event>::iterator it=cpe->begin(); it!=cpe->end(); ++it) {
    it->Print(fout);
  }

  cout <<  "\nDiscrete transition becomes enable: " << dte->size() << endl;
  for (list<Event>::iterator it=dte->begin(); it!=dte->end(); ++it) {
    it->Print(fout);
  }

  cout <<  "\nBatch becomes an output batch: " << bob->size() << endl;
  for (list<Event>::iterator it=bob->begin(); it!=bob->end(); ++it) {
    it->Print(fout);
  }

  cout <<  "\nDestruction of an output batch: " << dob->size() << endl;
  for (list<Event>::iterator it=dob->begin(); it!=dob->end(); ++it) {
    it->Print(fout);
  }

  cout <<  "\nBatch becomes dense: " << bbd->size() << endl;
  for (list<Event>::iterator it=bbd->begin(); it!=bbd->end(); ++it) {
    it->Print(fout);
  }

  cout <<  "\nTwo batches meet: " << tbm->size() << endl;
  for (list<Event>::iterator it=tbm->begin(); it!=tbm->end(); ++it) {
    it->Print(fout);
  }

  cout <<  "\nA batch meets an output batch: " << bmob->size() << endl;
  for (list<Event>::iterator it=bmob->begin(); it!=bmob->end(); ++it) {
    it->Print(fout);
  }

  cout <<  "\nDecongestion of the output batch: " << obd->size() << endl;
  for (list<Event>::iterator it=obd->begin(); it!=obd->end(); ++it) {
    it->Print(fout);
  }

  cout <<  "\nDecongestion of a batch in contact ahead: " << bd->size() << endl;
  for (list<Event>::iterator it=bd->begin(); it!=bd->end(); ++it) {
    it->Print(fout);
  }

  cout <<  "\nFull decongestion of a batch alone: " << bbf->size() << endl;
  for (list<Event>::iterator it=bbf->begin(); it!=bbf->end(); ++it) {
    it->Print(fout);
  }

  cout <<  "\nPlace reaches steady marking quantity: " << psq->size() << endl;
  for (list<Event>::iterator it=psq->begin(); it!=psq->end(); ++it) {
    it->Print(fout);
  }

  cout <<  "\nRemain quantity passed by an input transition becomes free steady quantity : " << rbf->size() << endl;
  for (list<Event>::iterator it=rbf->begin(); it!=rbf->end(); ++it) {
    it->Print(fout);
  }

}

//------------------------------------------------------------------------------
// Print
//------------------------------------------------------------------------------
void Schedule::PrintCE(ostream &fout)
{
  cout <<  "Transition maximum flow changes: " << tfce->size() << endl;
  for (list<ControlledEvent>::iterator it=tfce->begin(); it!=tfce->end(); ++it) {
    it->Print(fout);
  }
  cout << endl;

  cout <<  "Place maximum speed changes: " << psce->size() << endl;
  for (list<ControlledEvent>::iterator it=psce->begin(); it!=psce->end(); ++it) {
    it->Print(fout);
  }
}

//------------------------------------------------------------------------------
// Print
//------------------------------------------------------------------------------
void Schedule::PrintBP(ostream &fout)
{
  for (list<Breakpoint>::iterator it=breakpoint->begin(); it!=breakpoint->end(); ++it) {
    it->Print(fout);
  }
}

//------------------------------------------------------------------------------
// Print
//------------------------------------------------------------------------------
void Schedule::Print(ostream &fout)
{
  fout << "Schedule" << endl;
  fout << "========" << endl;

  fout << "Events\n"
       << "------" << endl;
  PrintEvents(fout);

  fout << "\nControlled events\n"
       << "-----------------" << endl;
  PrintCE(fout);

  fout << "\nBreakpoints\n"
       << "-----------" << endl;
  PrintBP(fout);
}

//------------------------------------------------------------------------------
// It writes the events list in the file
//------------------------------------------------------------------------------
void Schedule::WriteEvents(ofstream &fout)
{
  if (!dtf->empty()){
    fout <<  "\bDiscrete transition is fired: " << dtf->size() << endl;
    for (list<Event>::iterator it=dtf->begin(); it!=dtf->end(); ++it) {
      fout << "Date " << it->GetDate() << "\t node " << (it->GetNode())->GetName() << endl;
    }
  }

  if (!cpe->empty()){
    fout <<  "\nContinuous place becomes empty: " << cpe->size() << endl;
    for (list<Event>::iterator it=cpe->begin(); it!=cpe->end(); ++it) {
      fout << "Date " << it->GetDate() << "\t node " << (it->GetNode())->GetName() << endl;
    }
  }

  if (!dte->empty()){
    fout <<  "\nDiscrete transition becomes enable: " << dte->size() << endl;
    for (list<Event>::iterator it=dte->begin(); it!=dte->end(); ++it) {
      fout << "Date " << it->GetDate() << "\t node " << (it->GetNode())->GetName() << endl;
    }
  }

  if (!bob->empty()){
    fout <<  "\nBatch becomes an output batch: " << bob->size() << endl;
    for (list<Event>::iterator it=bob->begin(); it!=bob->end(); ++it) {
      fout << "Date " << it->GetDate() << "\t node " << (it->GetNode())->GetName() << endl;
    }
  }

  if (!dob->empty()){
    fout <<  "\nDestruction of an output batch: " << dob->size() << endl;
    for (list<Event>::iterator it=dob->begin(); it!=dob->end(); ++it) {
      fout << "Date " << it->GetDate() << "\t node " << (it->GetNode())->GetName() << endl;
    }
  }

  if (!bbd->empty()){
    fout <<  "\nBatch becomes dense: " << bbd->size() << endl;
    for (list<Event>::iterator it=bbd->begin(); it!=bbd->end(); ++it) {
      fout << "Date " << it->GetDate() << "\t node " << (it->GetNode())->GetName() << endl;
    }
  }

  if (!tbm->empty()){
    fout <<  "\nTwo batches meet: " << tbm->size() << endl;
    for (list<Event>::iterator it=tbm->begin(); it!=tbm->end(); ++it) {
      fout << "Date " << it->GetDate() << "\t node " << (it->GetNode())->GetName() << endl;
    }
  }

  if (!bmob->empty()){
    fout <<  "\nA batch meets an output batch: " << bmob->size() << endl;
    for (list<Event>::iterator it=bmob->begin(); it!=bmob->end(); ++it) {
      fout << "Date " << it->GetDate() << "\t node " << (it->GetNode())->GetName() << endl;
    }
  }

  if (!obd->empty()){
    fout <<  "\nDecongestion of the output batch: " << obd->size() << endl;
    for (list<Event>::iterator it=obd->begin(); it!=obd->end(); ++it) {
      fout << "Date " << it->GetDate() << "\t node " << (it->GetNode())->GetName() << endl;
    }
  }

  if (!bd->empty()){
    fout <<  "\nDecongestion of a batch in contact ahead: " << bd->size() << endl;
    for (list<Event>::iterator it=bd->begin(); it!=bd->end(); ++it) {
      fout << "Date " << it->GetDate() << "\t node " << (it->GetNode())->GetName() << endl;
    }
  }

  if (!bbf->empty()){
    fout <<  "\nFull decongestion of a batch alone: " << bbf->size() << endl;
    for (list<Event>::iterator it=bbf->begin(); it!=bbf->end(); ++it) {
      fout << "Date " << it->GetDate() << "\t node " << (it->GetNode())->GetName() << endl;
    }
  }

  if (!psq->empty()){
    fout <<  "\nPlace reaches steady marking quantity: " << psq->size() << endl;
    for (list<Event>::iterator it=psq->begin(); it!=psq->end(); ++it) {
      fout << "Date " << it->GetDate() << "\t node " << (it->GetNode())->GetName() << endl;
   //   it->Print(fout);
    }
  }

  if (!rbf->empty()){
    fout <<  "\nRemain quantity passed by an input transition becomes free steady quantity: " << rbf->size() << endl;
    for (list<Event>::iterator it=rbf->begin(); it!=rbf->end(); ++it) {
      fout << "Date " << it->GetDate() << "\t node " << (it->GetNode())->GetName() << endl;
   //   it->Print(fout);
    }
  }

}

//------------------------------------------------------------------------------
// It writes the controlled events list in the file
//------------------------------------------------------------------------------
void Schedule::WriteCE(ofstream &fout)
{
  Clean(tfce, simulation->stime->GetCurrentDate());
  Clean(psce, simulation->stime->GetCurrentDate());

  if (!tfce->empty() || !psce->empty())
    fout << endl << "Controlled Events" << endl;


  if (!tfce->empty()){
    fout <<  "\nTransition maximum flow changes: " << tfce->size() << endl;
    for (list<ControlledEvent>::iterator it=tfce->begin(); it!=tfce->end(); ++it) {
      fout << "Date " << it->GetDate() << "\t node " << (it->GetNodeId()) << "\t name " << it->GetName() << endl;
    }
  }

  if (!psce->empty()){
    fout <<  "\nPlace maximum speed changes: " << psce->size() << endl;
    for (list<ControlledEvent>::iterator it=psce->begin(); it!=psce->end(); ++it) {
      fout << "Date " << it->GetDate() << "\t node " << (it->GetNodeId()) << "\t name " << it->GetName() << endl;
    }
  }

}

//------------------------------------------------------------------------------
// It writes the breakpoint list in the file
//------------------------------------------------------------------------------
void Schedule::WriteBP(ofstream &fout)
{
  if (!breakpoint->empty()){
    fout <<  "\nBreakpoints: " << breakpoint->size() << endl;
    for (list<Breakpoint>::iterator it=breakpoint->begin(); it!=breakpoint->end(); ++it) {
      fout << "Date " << it->GetDate(simulation->stime->GetCurrentDate()) << "\t node " << (it->GetNodeId()) << "\t name " << it->GetName() << endl;
    }
    fout << endl;
  }
}


//------------------------------------------------------------------------------
// It writes the events list to be proceed in the next date
//------------------------------------------------------------------------------
void Schedule::WriteNextEvents(ofstream &fout)
{
  if (!nextevents->empty()){
    for (list<Event>::iterator it=nextevents->begin(); it!=nextevents->end(); ++it) {
      fout << "Event date " << it->GetDate() << "\t in node " << (it->GetNode())->GetName() << "\t type " << it->GetDescription() << endl;
    }
  }

  if (!nextcontrolledevents->empty()){
    for (list<ControlledEvent>::iterator it=nextcontrolledevents->begin(); it!=nextcontrolledevents->end(); ++it) {
      fout << "Event date " << it->GetDate() << "\t in node " << (it->GetNodeId()) << "\t type " << it->GetDescription() << endl;
    }
  }

  if (!nextbreakpoints->empty()){
    for (list<Breakpoint>::iterator it=nextbreakpoints->begin(); it!=nextbreakpoints->end(); ++it) {
      fout << "Breakpoint date " << it->GetDate(simulation->stime->GetCurrentDate()) << "\t in node " << (it->GetNodeId()) << "\t type " << it->GetDescription() << endl;
    }
  }
}

//------------------------------------------------------------------------------
// Compute events
//------------------------------------------------------------------------------
void Schedule::ComputeEvents(BPN *_bpn)
{
  _bpn->ComputeNextEventDTF(dtf);
  _bpn->ComputeNextEventCPE(cpe);
  _bpn->ComputeNextEventDTE(dte);
  _bpn->ComputeNextEventBOB(bob);
  _bpn->ComputeNextEventDOB(dob);
  _bpn->ComputeNextEventBBD(bbd);
  _bpn->ComputeNextEventTBM(tbm);
  _bpn->ComputeNextEventBMOB(bmob);
  _bpn->ComputeNextEventOBD(obd);
  _bpn->ComputeNextEventBD(bd);
  _bpn->ComputeNextEventBBF(bbf);
  _bpn->ComputeNextEventPSQ(psq);
  _bpn->ComputeNextEventRBF(rbf);
}

//------------------------------------------------------------------------------
// Compute next date
//------------------------------------------------------------------------------
void Schedule::ComputeNextDate(BPN *_bpn, double duration)
{
  // clean all non-controlled events
  dtf->clear();
  cpe->clear();
  dte->clear();
  bob->clear();
  dob->clear();
  bbd->clear();
  tbm->clear();
  bmob->clear();
  obd->clear();
  bd->clear();
  bbf->clear();
  psq->clear();
  rbf->clear();

  ComputeEvents(_bpn);
}

//------------------------------------------------------------------------------
// Compute next event
//------------------------------------------------------------------------------
double Schedule::ComputeNextEvent(double duration)
{
  double nextdate = duration;

  if (!dtf->empty())
    nextdate = ComputeNextEventDate(dtf, nextdate, Discr_trans_fires);

  if (!cpe->empty())
    nextdate = ComputeNextEventDate(cpe, nextdate, Cont_place_empty);

  if (!dte->empty())
    nextdate = ComputeNextEventDate(dte, nextdate, Discr_trans_enabled);

  if (!bob->empty())
    nextdate = ComputeNextEventDate(bob, nextdate, Becomes_output_batch);

  if (!dob->empty())
    nextdate = ComputeNextEventDate(dob, nextdate, Destr_output_batch);

  if (!bbd->empty())
    nextdate = ComputeNextEventDate(bbd, nextdate, Batch_becomes_dense);

  if (!tbm->empty())
    nextdate = ComputeNextEventDate(tbm, nextdate, Two_batches_meet);

  if (!bmob->empty())
    nextdate = ComputeNextEventDate(bmob, nextdate, Batch_meets_output_batch);

  if (!obd->empty())
    nextdate = ComputeNextEventDate(obd, nextdate, Output_batch_decongestion);

  if (!bd->empty())
    nextdate = ComputeNextEventDate(bd, nextdate, Batch_decongestion);

  if (!bbf->empty())
    nextdate = ComputeNextEventDate(bbf, nextdate, Batch_becomes_free);

  if (!psq->empty())
    nextdate = ComputeNextEventDate(psq, nextdate, Place_steady_quan);

  if (!rbf->empty())
    nextdate = ComputeNextEventDate(rbf, nextdate, Remaining_becomes_freesteadyquan);


  // controlled events
  if (!tfce->empty())
    nextdate = ComputeNextControlledEventDate(tfce, nextdate, Trans_flow_ce);

  if (!psce->empty())
    nextdate = ComputeNextControlledEventDate(psce, nextdate, Place_speed_ce);

  // breakpoint
  if (!breakpoint->empty())
    nextdate = ComputeNextBreakpointDate(breakpoint, nextdate);

  return (nextdate);
}

//------------------------------------------------------------------------------
// Compute a date for an event list
//------------------------------------------------------------------------------
double Schedule::ComputeNextEventDate(list<Event> *evl, double nextdate, ev_type type)
{
  while (!evl->empty()){
    Event *e = new Event;
    *e = evl->front();
    evl->pop_front();
    double eventdate = e->GetDate();

    if (abs(eventdate - nextdate) < PRF::prf.Min_Err()) { // both events in the same date
      nextevents->push_back(*e);
    }

    if (((nextdate - eventdate) > PRF::prf.Min_Err()) && (eventdate >= simulation->stime->GetCurrentDate())) {
      nextdate =  eventdate;
      nextevents->clear();
      nextevents->push_back(*e);
    }
    else{
      delete e;
    }
  }

  for (list<Event>::iterator it=nextevents->begin(); it!=nextevents->end(); ++it) {
    double eventdate = it->GetDate();
    if (nextdate < eventdate)
      nextdate = eventdate;
  }
  return (nextdate);
}

//------------------------------------------------------------------------------
// Compute a date for a controlled event list
//------------------------------------------------------------------------------
double Schedule::ComputeNextControlledEventDate(list<ControlledEvent> *cevl, double nextdate, ev_type type)
{
  int numerofcontrolledevents = cevl->size();

  for (int i=0; i<numerofcontrolledevents; i++){
    ControlledEvent *ce = new ControlledEvent;
    *ce = cevl->front();
    cevl->pop_front();
    double eventdate = ce->GetDate();

    if (eventdate <= simulation->stime->GetCurrentDate()){
      continue;
    }
    else{
      cevl->push_back(*ce);
    }

    if (eventdate ==  nextdate) { // both events in the same date
      nextcontrolledevents->push_back(*ce);
    }

    if (eventdate < nextdate ) {
      nextdate = eventdate;
      nextevents->clear();
      nextcontrolledevents->clear();
      nextcontrolledevents->push_back(*ce);
    }
  }

  return (nextdate);
}

//------------------------------------------------------------------------------
// Compute a date for the next breakpoint
//------------------------------------------------------------------------------
double Schedule::ComputeNextBreakpointDate(list<Breakpoint> *bpl, double nextdate)
{
  int numberofbreakpoints = bpl->size();
  nextbreakpoints->clear();

  if (nextevents->empty() && nextcontrolledevents->empty()){
    return(nextdate);
  }

  for (int i=0; i<numberofbreakpoints; i++){
    Breakpoint *bp = new Breakpoint;
    *bp = bpl->front();
    bpl->pop_front();
    double breakpointdate = bp->GetDate(simulation->stime->GetCurrentDate());

    if (breakpointdate <= simulation->stime->GetCurrentDate()){
      continue;
    }
    else{
      bpl->push_back(*bp);
    }

    if (breakpointdate == nextdate) { // both events in the same date
      nextbreakpoints->push_back(*bp);
    }

    if (breakpointdate < nextdate) {
      nextdate = breakpointdate;
      nextevents->clear();
      nextcontrolledevents->clear();
      nextbreakpoints->clear();
      nextbreakpoints->push_back(*bp);
    }
  }
  return (nextdate);
}

//------------------------------------------------------------------------------
// Process events
// TODO - process controled events
//------------------------------------------------------------------------------
void Schedule::ProcessEvents(BPN *_bpn)
{
  int type;
  double speed;

  while (!nextcontrolledevents->empty()){
    ControlledEvent *ce = new ControlledEvent;
    *ce = nextcontrolledevents->front();
    nextcontrolledevents->pop_front();

    type = ce->GetType();

    switch(type){
        case Place_speed_ce : {
                                TriangularBatchPlace *tbp = ce->GetNode(_bpn);
                                tbp->SetAndRecomputeSpeed(ce->GetValue());
                              }
           break;
        case Trans_flow_ce : {
                               BatchTransition *bt = ce->GetNode(_bpn);
                               Flow *fl = bt->GetFlow();
                               fl->SetMaximumFlow(ce->GetValue());
                               bt->Print(cout);
                             }
           break;
    }

  }

  while (!nextevents->empty()){
    Event *e = new Event;
    *e = nextevents->front();
    nextevents->pop_front();

    switch(e->GetType()){
      case Discr_trans_fires : DiscreteTransition *dt = e->GetNode();
                               dt->FireDiscreteTransition();

           break;
    }
  }

}

//------------------------------------------------------------------------------
// Process breakpoints - this function print the bpn at the breakpoint
//------------------------------------------------------------------------------
void Schedule::ProcessBreakpoints(BPN *_bpn)
{
  while (!nextbreakpoints->empty()){
    Breakpoint *bp = new Breakpoint;
    *bp = nextbreakpoints->front();
    nextbreakpoints->pop_front();

    bp->PrintPlaceTransition(_bpn, std::cout);
  }
}
