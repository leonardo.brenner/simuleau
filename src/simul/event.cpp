//====================================================================================//
//                                                                                    //
//                                     Event class                                    //
//                                                                                    //
//====================================================================================//
//  This File:   event.cpp                        Language: C++  (xlC and CC)         //
//  Software:    SIMULEAU                                                             //
//====================================================================================//
//  Creation:    07/may/16                        by: Leonardo.Brenner@lsis.org       //
//  Last Change: 07/may/16                        by: Leonardo.Brenner@lsis.org       //
//====================================================================================//
#include "simuleau.h"

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
// Methods of the Event class
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Constructor
//------------------------------------------------------------------------------
Event::Event()
{
  //ctor
}

//------------------------------------------------------------------------------
// Initialized constructor
//------------------------------------------------------------------------------
Event::Event(Node &_node, double _date, ev_type _type)
{
  node = &_node;
  date = _date;
  type = _type;
}

//------------------------------------------------------------------------------
// Destructor
//------------------------------------------------------------------------------
Event::~Event()
{
  //dtor
}

//------------------------------------------------------------------------------
// Copy
//------------------------------------------------------------------------------
void Event::Copy(const Event &_event)
{
  date = _event.date;
  node = _event.node;
  type = _event.type;
}

//------------------------------------------------------------------------------
// Return the event date
//------------------------------------------------------------------------------
double Event::GetDate()
{
  return (date);
}

//------------------------------------------------------------------------------
// Return the event type
//------------------------------------------------------------------------------
ev_type Event::GetType()
{
  return (type);
}

//------------------------------------------------------------------------------
// Return the node related to the event
//------------------------------------------------------------------------------
Node* Event::GetNode()
{
  return (node);
}

//------------------------------------------------------------------------------
// Return the textual event description
//------------------------------------------------------------------------------
char* Event::GetDescription()
{
  switch (type)
  {
    case Discr_trans_fires:   return("Discrete transition is fired");
    case Cont_place_empty:    return("Continuous place becomes empty");
    case Discr_trans_enabled: return("Discrete transition becomes enable");
    case Becomes_output_batch:return("Batch becomes an output batch");
    case Destr_output_batch:  return("Destruction of an output batch");
    case Batch_becomes_dense: return("Batch becomes dense");
    case Two_batches_meet:    return("Two batches meet");
    case Batch_meets_output_batch: return("A batch meets an output batch");
    case Output_batch_decongestion: return("Decongestion of the output batch");
    case Batch_decongestion:  return("Decongestion of a batch in contact ahead");
    case Batch_becomes_free:  return("Full decongestion of a batch alone");
    case Place_steady_quan:  return("Place reaches steady marking quantity");
    case Remaining_becomes_freesteadyquan: return("Remaining quantity passed by an input transition becomes free steady quantity");
  }
}

//------------------------------------------------------------------------------
// Print
//------------------------------------------------------------------------------
void Event::Print(ostream &fout)
{
  fout << "Date: " << date << endl;
  node->Print(fout);
}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
// Methods of the Controlled Event class
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Constructor
//------------------------------------------------------------------------------
ControlledEvent::ControlledEvent()
{

}

//------------------------------------------------------------------------------
// Initialized constructor
//------------------------------------------------------------------------------
ControlledEvent::ControlledEvent(Node& _node, double _date, ev_type _type, double _value, simuleau_name _name):Event(_node, _date, _type)
{
  value = _value;
  strcpy(name, _name);
}

//------------------------------------------------------------------------------
// Initialized constructor
//------------------------------------------------------------------------------
ControlledEvent::ControlledEvent(simuleau_name _idnode,  Node& _node, double _date, ev_type _type, double _value, simuleau_name _name):Event(_node, _date, _type)
{
  value = _value;
  strcpy(name, _name);
  strcpy(idnode, _idnode);
}

//------------------------------------------------------------------------------
// Destructor
//------------------------------------------------------------------------------
ControlledEvent::~ControlledEvent()
{

}

//------------------------------------------------------------------------------
// Copy
//------------------------------------------------------------------------------
void ControlledEvent::Copy(const ControlledEvent &_event)
{
  date = _event.date;
  node = _event.node;
  type = _event.type;

  strcpy(name, _event.name);
  strcpy(idnode, _event.idnode);
  value = _event.value;
}

//------------------------------------------------------------------------------
// It returns controlled event name
//------------------------------------------------------------------------------
char* ControlledEvent::GetName()
{
  return (name);
}

//------------------------------------------------------------------------------
// It returns controlled event value
//------------------------------------------------------------------------------
double ControlledEvent::GetValue()
{
  return (value);
}

//------------------------------------------------------------------------------
// Return the node related to the event
//------------------------------------------------------------------------------
Node* ControlledEvent::GetNode(BPN *_bpn)
{
  node = _bpn->GetNode(idnode);
  return (node);
}

//------------------------------------------------------------------------------
// It returns controlled event node id
//------------------------------------------------------------------------------
char* ControlledEvent::GetNodeId()
{
  return (idnode);
}

//------------------------------------------------------------------------------
// Return the textual event description
//------------------------------------------------------------------------------
char* ControlledEvent::GetDescription()
{
  switch (type)
  {
    case Trans_flow_ce:       return("Transition maximum flow changes");
    case Place_speed_ce:      return("Place maximum speed changes");
  }
}

//------------------------------------------------------------------------------
// Print
//------------------------------------------------------------------------------
void ControlledEvent::Print(ostream &fout)
{
  fout << "Event Name:  " << name << endl;
  fout << "Node Name:   " << idnode << endl;
  fout << "Date:        " << date << endl;
  fout << "Type:        " << GetDescription() << endl;
  fout << "Value:       " << value << endl;
}
