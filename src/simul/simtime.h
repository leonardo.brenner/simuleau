//====================================================================================//
//                                                                                    //
//                                   Simtime class                                    //
//                                                                                    //
//====================================================================================//
//  This File:   simtime.h                        Language: C++  (xlC and CC)         //
//  Software:    SIMULEAU                                                             //
//====================================================================================//
//  Creation:    11/may/16                        by: Leonardo.Brenner@lsis.org       //
//  Last Change: 11/may/16                        by: Leonardo.Brenner@lsis.org       //
//====================================================================================//
#ifndef SIMTIME_H
#define SIMTIME_H

class Simtime
{
  public:
    Simtime();
    virtual ~Simtime();

    void   Reset();          // set all dates to zero
    void   Permute();        // permute previousdate to currentdate and currentdate to nextdate
    double StepTime();       // Compute the difference between currentdate and nextdate

    void   SetNextDate(double _nd);
    double GetPreviousDate();
    double GetCurrentDate();
    double GetNextDate();

    //print
    void Print(ofstream &fout);

  protected:
    double currentdate;
    double previousdate;
    double nextdate;

  private:
};

#endif // SIMTIME_H
