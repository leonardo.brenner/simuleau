//====================================================================================//
//                                                                                    //
//                                  Breakpoint class                                  //
//                                                                                    //
//====================================================================================//
//  This File:   breakpoint.cpp                   Language: C++  (xlC and CC)         //
//  Software:    SIMULEAU                                                             //
//====================================================================================//
//  Creation:    27/septembre/17                  by: Leonardo.Brenner@lsis.org       //
//  Last Change: 27/septembre/17                  by: Leonardo.Brenner@lsis.org       //
//====================================================================================//
#include "simuleau.h"

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
// Methods of the Breakpoint class
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Constructor
//------------------------------------------------------------------------------
Breakpoint::Breakpoint()
{
  //ctor
}

//------------------------------------------------------------------------------
// Initialized constructor
//------------------------------------------------------------------------------
Breakpoint::Breakpoint(simuleau_name _idnode, Node& _node, double _date_frequency, breakpoint_type _type, simuleau_name _name)
{
  node = &_node;
  strcpy(idnode, _idnode);
  strcpy(name, _name);
  type = _type;

  if (type == at_bp) {
    date = _date_frequency;
  }
  else{
    date = 0.0;
    frequency = _date_frequency;
  }
}

//------------------------------------------------------------------------------
// Destructor
//------------------------------------------------------------------------------
Breakpoint::~Breakpoint()
{
  //dtor
}

//------------------------------------------------------------------------------
// Copy
//------------------------------------------------------------------------------
void Breakpoint::Copy(const Breakpoint &_breakpoint)
{
  date = _breakpoint.date;
  node = _breakpoint.node;
  type = _breakpoint.type;
  frequency = _breakpoint.frequency;

  strcpy(name, _breakpoint.name);
  strcpy(idnode, _breakpoint.idnode);
}

//------------------------------------------------------------------------------
// It returns breakpoint name
//------------------------------------------------------------------------------
char* Breakpoint::GetName()
{
  return (name);
}

//------------------------------------------------------------------------------
// It returns breakpoint node id
// places represents all places  and transitions represents all transitions
//------------------------------------------------------------------------------
char* Breakpoint::GetNodeId()
{
  return (idnode);
}

//------------------------------------------------------------------------------
// Return the node related to the breakpoint
//------------------------------------------------------------------------------
Node* Breakpoint::GetNode(BPN *_bpn)
{
  node = _bpn->GetNode(idnode);
  return (node);
}

//------------------------------------------------------------------------------
// Return the breakpoint date
//------------------------------------------------------------------------------
double Breakpoint::GetDate(double currentdate)
{

  if (type == at_bp)
    return (date);
  else{
    date = (((int) currentdate/frequency) + 1) * frequency;
    return (date);
  }
}

//------------------------------------------------------------------------------
// Return the breakpoint type
// at_bp for pontual breakpoint
// every_bp for recurent breakpoint
//------------------------------------------------------------------------------
breakpoint_type Breakpoint::GetType()
{
  return (type);
}

//------------------------------------------------------------------------------
// Return the textual breakpoint description
//------------------------------------------------------------------------------
char* Breakpoint::GetDescription()
{
  switch (type)
  {
    case at_bp:     return("Breakpoint at a specific date");
    case every_bp:  return("Recurent breakpoint");
  }
}


//------------------------------------------------------------------------------
// Print
//------------------------------------------------------------------------------
void Breakpoint::Print(ostream &fout)
{
  fout << "Breakpoint Name:  " << name << endl;
  fout << "Node Name:        " << idnode << endl;
  if (type == at_bp){
    fout << "Date:             " << date << endl;
  }
  else{
    fout << "Frequency:        " << frequency << endl;
  }
  fout << "Type:             " << GetDescription() << endl;
}

//------------------------------------------------------------------------------
// Print
//------------------------------------------------------------------------------
void Breakpoint::PrintPlaceTransition(BPN *_bpn, ostream &fout)
{
  Node *bpnode;

  fout << "Breakpoint Name:  " << name << endl;

  if (!strcmp(idnode,"places")){ // print all places
    _bpn->WritePlaces(fout);
    return;
  }

  if (!strcmp(idnode,"transitions")){ // print all transitions
    _bpn->WriteFlows(fout);
    return;
  }

  //print just one place or transition
  bpnode = GetNode(_bpn);
  bpnode->Print(fout);
}
