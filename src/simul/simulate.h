//====================================================================================//
//                                                                                    //
//                                 Simulate class                                     //
//                                                                                    //
//====================================================================================//
//  This File:   simulate.h                       Language: C++  (xlC and CC)         //
//  Software:    SIMULEAU                                                             //
//====================================================================================//
//  Creation:    11/may/16                        by: Leonardo.Brenner@lsis.org       //
//  Last Change: 11/may/16                        by: Leonardo.Brenner@lsis.org       //
//====================================================================================//
#ifndef SIMULATE_H
#define SIMULATE_H
#include "interf.h"

class Simulate
{
  public:
    Simulate();
    Simulate(BPN *_bpn, Schedule *_schedule);
    virtual ~Simulate();

    void Baptise(simuleau_name file);

    BPN *      GetBpn();
    Schedule * GetSchedule();

    int IsEmpty();

    int Start(double maxtime, simuleau_options option);

    void Print(ostream &fout);
    void PrintBPN();
    void PrintSchecule();


    //friend class Schedule;
    //friend class BPN;
    //friend class ::Node;

//    friend double Schedule::ComputeNextEventDate(list<Event> *evl, double nextdate, ev_type type);

    BPN      *sbpn;
    Schedule *sschedule;
    Simtime  *stime;

  protected:
    double  simulationtime;
    int     simulationsteps;

    simuleau_name filename;  //output file name

    void WriteInfo(ofstream &fout);          // write model and simulation informations
    void WriteDate(ofstream &fout);          // write current date
    void WriteBPNMarks(ofstream &fout);      // write current BPN marks
    void WriteBPNFlows(ofstream &fout);      // write continuous and batch transition flows
    void WriteEvents(ofstream &fout);        // write the events list
    void WriteNextEvents(ofstream &fout);    // write the next events to be proceed
    void WriteEnd(ofstream &fout, int code); // write simulation step, time, ...


  private:
};

#endif // SIMULATE_H
