//====================================================================================//
//                                                                                    //
//                                  Breakpoint class                                  //
//                                                                                    //
//====================================================================================//
//  This File:   breakpoint.h                     Language: C++  (xlC and CC)         //
//  Software:    SIMULEAU                                                             //
//====================================================================================//
//  Creation:    27/septembre/17                  by: Leonardo.Brenner@lsis.org       //
//  Last Change: 27/septembre/17                  by: Leonardo.Brenner@lsis.org       //
//====================================================================================//
#ifndef BREAKPOINT_H
#define BREAKPOINT_H

//====================================================================================//
//                              Generic breakpoint class                              //
//====================================================================================//
//    Breakpoint class implements breakpoint to inspect the state of places and       //
// transitions at date where it have no events.                                       //
//====================================================================================//
class Breakpoint
{
  public:
    Breakpoint();
    Breakpoint(simuleau_name _idnode, Node& _node, double _date_frequency, breakpoint_type _type, simuleau_name _name);
    ~Breakpoint();

    void Copy(const Breakpoint &_breakpoint);

    //output functions
    char*  GetName();
    char*  GetNodeId();
    Node*  GetNode(BPN *_bpn);
    double  GetDate(double currentdate);
    breakpoint_type GetType();
    char* GetDescription();

    // print functions
    void Print(ostream &fout);
    void PrintPlaceTransition(BPN *_bpn, ostream &fout);

  protected:
    Node   *node;
    simuleau_name idnode;
    simuleau_name name;
    double date;
    double frequency;
    breakpoint_type type;

  private:
};


#endif // BREAKPOINT_H
