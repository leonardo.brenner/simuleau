//====================================================================================//
//                                                                                    //
//                                 Simulate class                                     //
//                                                                                    //
//====================================================================================//
//  This File:   simulate.cpp                     Language: C++  (xlC and CC)         //
//  Software:    SIMULEAU                                                             //
//====================================================================================//
//  Creation:    11/may/16                        by: Leonardo.Brenner@lsis.org       //
//  Last Change: 11/may/16                        by: Leonardo.Brenner@lsis.org       //
//====================================================================================//
#include "simuleau.h"

//------------------------------------------------------------------------------
// Empty contructor
//------------------------------------------------------------------------------
Simulate::Simulate()
{
  simulationtime  = 0.0;
  simulationsteps = 0;

  sbpn      = NULL;
  sschedule = NULL;
  stime     = NULL;
}

//------------------------------------------------------------------------------
// Its simulate the bpn model with controlled events in schedule for maxtime
//------------------------------------------------------------------------------
Simulate::Simulate(BPN *_bpn, Schedule *_schedule)
{
  simulationtime  = 0.0;
  simulationsteps = 0;

  sbpn = new BPN;
  sbpn->Copy(_bpn);

  sschedule = new Schedule;
  sschedule->Copy(_schedule);

  stime = new Simtime;
  stime->Reset();
}

//------------------------------------------------------------------------------
// Destructor
//------------------------------------------------------------------------------
Simulate::~Simulate()
{
  if (sbpn)
    delete  sbpn;
  if (sschedule)
    delete  sschedule;
  if (stime)
    delete  stime;
}


//------------------------------------------------------------------------------
// It assigns a file name
//-----------------------------------------------------------------------------
void Simulate::Baptise(simuleau_name file)
{
  strcpy(filename, file);
}


//------------------------------------------------------------------------------
// return the sbpn pointer
//-----------------------------------------------------------------------------
BPN * Simulate::GetBpn()
{
  return sbpn;
}

//------------------------------------------------------------------------------
// return the sschedule pointer
//-----------------------------------------------------------------------------
Schedule * Simulate::GetSchedule()
{
  return sschedule;
}

//------------------------------------------------------------------------------
// Return true if there are a bpn model in memory
//------------------------------------------------------------------------------
int Simulate::IsEmpty()
{
  if (sbpn != NULL && sschedule != NULL)
    return false;
  else
    return true;
}

//------------------------------------------------------------------------------
// Start the model simulation
//------------------------------------------------------------------------------
int Simulate::Start(double maxtime, simuleau_options option)
{
  simuleau_name dyn_level_name;
  ofstream fout;
  Timer     T;

  int    result = 0;
  int    conflict = 0;
  double precision = PRF::prf.Min_Err();
  double steptime = maxtime;

  strcpy(dyn_level_name, "dyn/");  strcat(dyn_level_name, filename);
  Open_File(dyn_level_name, dyn_file, fout);

  //verify conflit
  sbpn->ResearchStructuralConflict();

  if (option == simulate_xct){ // no external events
    sschedule->CleanAll();
  }

  // start simulation
  simulationtime  = maxtime;
  simulationsteps = 0;

  WriteInfo(fout);

  stime->Reset();
  T.clear();
  T.start();

  //sbpn->ComputeSteadyState();
  switch(option){
      case simulate_onoff :
      case simulate_mf_onoff : sbpn->ComputeSteadyFiringQuantity();
      break;
  }



  do{
    stime->Permute();
    WriteDate(fout);

    cout << "================================================\n" ;
    cout << "Current date: " << stime->GetCurrentDate() << endl;
    cout << "================================================\n" ;

    steptime = stime->StepTime(); // compute the difference betwen current and previous date
      switch(option){
      case simulate_onoff :
      case simulate_mf_onoff : sbpn->ComputeCurrentFiringQuantity(steptime); // compute current firing quantity from initial marking  //currentFiringquantity is not in sbpn.
      break;
  }


/*    fout << "================================================\n" ;
    fout << "At the end of the step\n" ;
    fout << "================================================\n" ;
    WriteBPNMarks(fout);
*/
    sbpn->ComputeNewMarks(steptime);

    sschedule->ProcessEvents(sbpn);

    sbpn->DestructBatches();

    sbpn->MergeBatches();

    sbpn->DestructBatches();



    fout << "================================================\n" ;
    fout << "At the begin of the step\n" ;
    fout << "================================================\n" ;


    WriteBPNMarks(fout);
//    sbpn->Print(ofstream &fout);

    sbpn->ComputeTransitionsStates();

    switch(option){
      case simulate :
      case simulate_xct : sbpn->ComputeIFF();
           break;
      case simulate_onoff :  sbpn->ComputeCFF(); // on/off control method
           break;
      case simulate_mf_onoff :  sbpn->ComputeMCFF(); // on/off control method
           break;
    }


    WriteBPNFlows(fout);
    if (sbpn->IsSteadyStateReached() && (sbpn->NumberOfDiscretePlaces()==0)){
      result = 5;
      cout << " Steady state reached\n"
           << " stopping simulation at " << stime->GetCurrentDate()  << endl;
      break;
    }

    sschedule->ProcessBreakpoints(sbpn);

    // Create Batches
		sbpn->CreateBatches();

		sbpn->ComputeBatchesBehaviours();

		conflict = sbpn->VerifyConflict();
		if (conflict){
      result = conflict + 2;
      cout << " conflict found\n"
           << " stopping simulation at " << stime->GetCurrentDate()  << endl;
      break;
		}


    sbpn->ReserveMarks();

    sschedule->ComputeNextDate(sbpn, simulationtime);
    WriteEvents(fout);

    stime->SetNextDate(sschedule->ComputeNextEvent(simulationtime));
    WriteNextEvents(fout);

    simulationsteps++;
    if (sschedule->IsEmpty()){
      result = 2;
    }
    if (stime->GetNextDate() > simulationtime){
      result = 1;
    }


    sbpn->SetBehaviourFunctions();
    cout << endl << endl;


  } while ((result == 0) && ( conflict == 0));
  T.stop();

  WriteEnd(fout, result);
  Close_File(dyn_level_name, dyn_file, fout);

  cout << "\nFinal simulation time:      " << stime->GetCurrentDate() << endl;
  cout << "Number of simulation steps: " << simulationsteps;
  Notify_Time_Spend(T, "simulation of a BPN model");

}

//------------------------------------------------------------------------------
// Print
//------------------------------------------------------------------------------
void Simulate::Print(ostream &fout)
{
  if (stime != NULL){
    fout << "Simulated time: " << stime->GetCurrentDate() << endl;
    fout << "Simulated step: " << simulationsteps << endl << endl;
  }

  sbpn->Print(fout);
  sschedule->Print(fout);
}

//------------------------------------------------------------------------------
// It writes model and simulation informations
//------------------------------------------------------------------------------
void Simulate::WriteInfo(ofstream &fout)
{
  fout << "Model name:      " << sbpn->GetName() << endl;
  fout << "Simulation time: " << simulationtime << endl;
  fout << "Precision:       " << PRF::prf.Min_Err()  << endl << endl;

  fout << "Number of places:      " << sbpn->NumberOfPlaces()  << endl; // specify discr, .....
  fout << "Number of transitions: " << sbpn->NumberOfTransitions() << endl;
}

//------------------------------------------------------------------------------
// It writes current date
//------------------------------------------------------------------------------
void Simulate::WriteDate(ofstream &fout)
{
  fout << "==================================================="<< endl;
  fout << "Current date : " << stime->GetCurrentDate() << endl;
  fout << "==================================================="<< endl << endl;
}

//------------------------------------------------------------------------------
// It writes current BPN marks
//------------------------------------------------------------------------------
void Simulate::WriteBPNMarks(ofstream &fout)
{
  sbpn->WritePlaces(fout);
}


//------------------------------------------------------------------------------
// It writes continuous and batch transition flows
//------------------------------------------------------------------------------
void Simulate::WriteBPNFlows(ofstream &fout)
{
  sbpn->WriteFlows(fout);
}

//------------------------------------------------------------------------------
// It writes the events list
//------------------------------------------------------------------------------
void Simulate::WriteEvents(ofstream &fout)
{
  fout << endl << "Events" << endl;
  sschedule->WriteEvents(fout);
  fout << endl;

  sschedule->WriteCE(fout);

  sschedule->WriteBP(fout);



}

//------------------------------------------------------------------------------
// It writes the next events to be proceed
//------------------------------------------------------------------------------
void Simulate::WriteNextEvents(ofstream &fout)
{
  fout << "Next events to proceed" << endl;

  sschedule->WriteNextEvents(fout);
  fout << endl;
}

//------------------------------------------------------------------------------
// It writes simulation steps, time, ..
//------------------------------------------------------------------------------
void Simulate::WriteEnd(ofstream &fout, int code)
{
  fout << endl;
  fout << "Final simulation time:      " << stime->GetCurrentDate() << endl;
  fout << "Number of simulation steps: " << simulationsteps << endl;

  switch (code){
    case 1 : fout << "Maximal simulation time reached" << endl;
         break;
    case 2 : fout << "No more events in the schedule" << endl;
         break;
    case 3 : fout << "Conflit in discrete place - Simulation was stopped" << endl;
         break;
    case 4 : fout << "Conflit in continuous place - Simulation was stopped" << endl;
         break;
    case 5 : fout << "Steady state was reached" << endl;
         break;
  }
}
