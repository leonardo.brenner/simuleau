//====================================================================================//
//                                                                                    //
//                           SIMULEAU Global Declarations                             //
//                                                                                    //
//====================================================================================//
//  This File:   simuleau.h                       Language: C++  (xlC and CC)         //
//  Software:    SIMULEAU                                                             //
//====================================================================================//
//  Creation:    12/apr/16                        by: Leonardo.Brenner@lsis.org       //
//  Last Change: 28/apr/16                        by: Leonardo.Brenner@lsis.org       //
//====================================================================================//
#ifndef SIMULEAU_H
#define SIMULEAU_H

#include <fstream>               // file streams (i/o)
#include <iostream>              // std i/o with streams (cin, cout, cerr)
#include <iomanip>               // i/o manipulators (setiosflags-scientific)
#include <string.h>              // string manipulation (strcpy, strcat)
#include <math.h>                // absolute values (abs), square roots (sqrt)
#include <cmath>                 // std::abs
#include <limits.h>              // maximum and minimum values to int, float, etc
#include <new>                   // handler for new commands (test for allocation)
#include <stdio.h>               // i/o standard of C language (used for the parser)
#include <stdlib.h>              // standard library of C language
#include <unistd.h>              //
#include <sys/stat.h>            // system file manipulation functions
#include <sys/types.h>           //
#include <list>                  // list manipulation
#include <stack>                 // stack manipulation
#include <glpk.h>                // linear program solver



using namespace std;

//====================================================================================//
//   Release Date ******************** CHANGE THIS DATE FOR NEW RELEASES OF SIMULEAU  //
//====================================================================================//

#define __RELEASE_DATE__   "Apr 30 2016"


//====================================================================================//
//   BPN classes                                                                      //
//====================================================================================//
class BPN;                   // Main BPN class                  (main structure)  1100
class Node;                  // Node class                 (node main structure)  1200
class Place;                 // Place class               (place main structure)  1300
class DiscretePlace;         // DiscretePlace class   (discrete place structure)  1300
class ContinuousPlace;       // ContinuousPlace class (continuous place structure)1300
class BatchPlace;            // BatchPlace class         (batch place structure)  1300
class TriangularBatchPlace;  // TriangularBatchPlace class
                             //               (traingular batch place structure)  1300

class Transition;            // Transition class     (transition main structure)  1400
class DiscreteTransition;    // DiscreteTransition class
                             //                  (discrete transition structure)  1400
class ContinuousTransition;  // ContinuousTransition class
                             //                (continuous transition structure)  1400
class BatchTransition;       // BatchTransition clas (batch transition structure) 1400
class Arc;                   // Arc class                   (arc main structure)  1500
class Batch;                 // Batch class               (batch main structure)  1600
class ControllableBatch;     // ControllableBatch class
                             //                    (controllable batch structure) 1600
class Flow;                  // Flow class                 (flow main structure)  1700

class Event;                 // Event class               (event main structure)  1800
class ControlledEvent;       // ControlledEvent class(Controlled event structure) 1800
class Breakpoint;            // Breakpoint class           (Breakpoint structure) 1850
class Schedule;              // Schedule class         (Schedule main structure)  1900
class Simtime;               // Simtime class                (simtime structure)  2000
class Simulate;              // Simulate class              (simulate structure)  2100



//====================================================================================//
//   User Interface                                                                   //
//====================================================================================//
class PRF;                   // User Preferences                (main structure)  6100

//====================================================================================//
//   Miscelaneous                                                                     //
//====================================================================================//
class Timer;                 // general chronometers from pac++

//====================================================================================//
//   Types of Files Handled by Simuleau                                               //
//====================================================================================//

enum file_types  // all files handled by Simuleau
     { bpn_file ,   // textual description of BPN
       str_file ,   // internal representation of BPN STRucture
       xct_file ,   // textual description of eXTernal Controlled events
       dyn_file ,   // messages of DYNamics of BPN
       tim_file ,   // messages of TIMes spend in Simuleau software
       dbg_file ,   // textual output (Readable) with DeBuGging information
       prf_file ,   // user PReFerences for parameters (numerical precision, etc)
       dot_file ,   // names without extension
       err_file };  // unrecognized extension

//====================================================================================//
//   Basic Definitions                                                                //
//====================================================================================//
const   int         no   = 0;                  // used in formalisation variable
const   int         yes  = 1;                  // used in formalisation variable
const   double      zero = 0.0;                // double sized constants to avoid
const   double      one  = 1.0;                //   problems passing parameters
const   double      two  = 2.0;
const   double      epsilon = 1e-5;            // a small double valeu
const   double      close_to_zero = 1e-16;     // a very small double valeu
const   double      round_zero = 1e-12;        // a rather small double valeu
const   double      visual_zero = 1e-5;        // smallest value displayed as decimal
const   int         simuleau_precision = 10;   // precision to handle (i/o) double values
const   int         max_integer = INT_MAX;     // maximum integer (signed)
const   int         min_integer = INT_MIN;     // minimum integer (signed)
const   double      max_float =  1e+50;        // max. float (actually just a huge one)
const   double      min_float = -1e+50;        // min. float (actually just a tiny one)
#ifdef _CC_COMPILER_
  typedef int         bool;                    // boolean variables
  const   bool        true  = 1;               //   gcc and other non-standart c++
  const   bool        false = 0;               //   compilers have built-in bool types
#endif
const   int         mem_unit = 1024;           // unit of memory space (Kbytes)

//====================================================================================//
//   Simuleau Name Definitions                                                        //
//====================================================================================//

const int max_simuleau_name = 128;      // Maximum size (actually the last 4 are ".ext")

typedef char simuleau_name[max_simuleau_name];
                                        // User Definided Names for any Simuleau Entity

//====================================================================================//
//   Identifiers Definitions                                                          //
//====================================================================================//
typedef int any_id;        // for identifiers of any kind
const any_id               no_id = -1;

typedef int node_id;       // for node (anykind)
const node_id              no_node = -1;
const node_id              fst_node = 0;

typedef int place_id;      // for place (anykind)
const place_id             no_place = -1;
const place_id             fst_place = 0;

typedef int trans_id;      // for transtion (anykind)
const trans_id             no_trans = -1;
const trans_id             fst_trans = 0;

typedef int batch_id;       // for batch in a place
const batch_id              no_batch = -1;
const batch_id              fst_batch = 0;

typedef int ev_id;         // for events
const ev_id                no_ev = -1;
const ev_id                fst_ev = 0;

typedef int m_coord;       // for pre/post matrices coordinates
const m_coord              no_coord = -1;
const m_coord              fst_m_coord = 0;  // the first coordinate (in matrices)

typedef double pl_cont_fed;
const pl_cont_fed          Not_fed = 0.0;
const pl_cont_fed          Fed = 0.5;                   // The place has an input flow


//====================================================================================//
//   Pre-defined Enumerations                                                         //
//====================================================================================//
enum node_type       { Place_nd , Transition_nd };           // Place or Transition node
enum place_type      { Discrete_pl , Continuous_pl , Batch_pl, Triangular_pl };
                              // Discrete, Continuous, Batch and Triangular Batch places
enum trans_type      { Discrete_tr , Continuous_tr , Batch_tr };
                                           // Discrete, Continuous and Batch transitions
enum batch_type      { Batch_bt , Controllable_bt };                      // Batches types
enum node_state      { No_state };                                      // No state, ...
enum Batch_place_behaviour  { Accumulating_behaviour=2 }; // No behaviour, ...
enum ctrl_batch_state{ No_st , Free_st , Congested_st };
                                                       // States of a Controllable batch
enum ctrl_batch_behaviour { No_behaviour ,
                            Free_behaviour ,
                            Congesting_behaviour ,
                            Uncongesting_behaviour };
                                                   // Behaviours of a controllable batch

enum ev_class        { Internal_ev, External_ev, Controlled_ev };
                               // Internal, External and Controlled event classification
enum ev_type         { No_event,  // No event
                       Discr_trans_fires,   // Discrete transition is fired
                       Cont_place_empty,    // Continuous place becomes empty
                       Discr_trans_enabled, // Discrete transition becomes enable
                       Becomes_output_batch,// A batch becomes an output batch
                       Destr_output_batch,  // Destruction of an output batch
                       Batch_becomes_dense, // Full accumulation/congestion of a batch
                       Two_batches_meet,    // Two batches meet
                       Batch_meets_output_batch, // A  batch meet an output batch
                       Output_batch_decongestion, // Decongestion of the output batch
                       Batch_decongestion,  // Decongestion of a batch in contact head
                       Batch_becomes_free,  // Full decongestion of an alone batch
                       Place_steady_quan,   // Place reaches Steady marking Quantity
                       Remaining_becomes_freesteadyquan, //Remaining quantity becomes free steady quantity


                       Trans_flow_ce,       // A controled event changes the maximum
                                            // flow of a transition
                       Place_speed_ce       // A controlled event changes the maximum
                                            // speed of a batch place
                     };
enum ce_type         { Flow_ce, Speed_ce };          // Flow and Speed controlled events

enum breakpoint_type { at_bp, every_bp };          // Flow and Speed controlled events


//====================================================================================//
//   User Preferences Defaults                                                        //
//====================================================================================//

const int           def_verbose      = 1;             // Verbose mode
const double        def_time         = 1.0 ;          // Maximum simulation time
const double        def_err          = 1e-5;          // Minimum error
const double        def_thresh       = close_to_zero; // Threshold
const simuleau_name def_length_unity = "km";          // Length unity
const simuleau_name def_time_unity   = "h";           // Time unity

//====================================================================================//
//   Includes Simuleau Classes                                            Error Codes //
//====================================================================================//
#include "bpn/bpn.h"          // main BPN class (class BPN     )                  1100
#include "bpn/node.h"         // node class (class Node     )                     1200
#include "bpn/place.h"        // place classes (class Place, DiscretePlace,
                              //                ContinuousPlace, BatchPlace,
                              //                TriangularBatchPlace      )       1300
#include "bpn/transition.h"   // transition classes (class Transition,
                              //                     DiscreteTransition,
                              //                     ContinuousTransition,
                              //                     BatchTransition      )       1400
#include "bpn/arc.h"          // arc class (class Arc     )                       1500
#include "bpn/batch.h"        // batch classes (class Batch, TriangularBatch)     1600
#include "bpn/flow.h"         // flow class   (class Flow     )                   1700
#include "bpn/firingquantity.h" // firingquantity class

#include "simul/event.h"      // event classes (class Event, ExternalEvent    )   1800
#include "simul/breakpoint.h" // event classes (class Event, ExternalEvent    )   1850
#include "simul/schedule.h"   // schedule class (class Schedule     )             1900
#include "simul/simtime.h"    // simtime class (class Simtime     )               2000
#include "simul/simulate.h"   // simulate class (class Simulate     )             2100


//====================================================================================//
//   Includes Simuleau Packages                                           Error Codes //
//====================================================================================//
#include "interf/interf.h" // user interface package for UNIX version         6200
#include "interf/prf.h"    // user preferences (class PRF)                    6100
#include "interf/timer.h"  // timer definitions package (timer.C from pac++)


#endif // SIMULEAU_H
