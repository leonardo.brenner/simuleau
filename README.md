# Simuleau

The Simuleau tool has been developed to provide an academic tool for modeling and simulating systems described by the Petri Nets (PN) formalism, with a specific focus on Batches Petri Nets (BPN) models. 

BPN models were initially used to model the Perrier water bottling manufacturing system, in which conveyors were modeled using batch places. Since then, BPN models have been developed and applied in various areas such as high-speed communications, traffic and crowd modeling.

The first version of Simuleau was proposed in 1993 to implement the concepts of the "original" BPN class. In 1998, the concepts of sensors and control were integrated into the tool.

Since 2016, the current version of Simuleau has been under development. The goal of this new version is to rewrite the old code using modern programming languages, integrate the various extensions of BPN that have been developed in recent years, and restructure the tool to make it easier to develop future extensions.

Simuleau is a tool written in C++ that runs on Linux systems. It enables the modeling and simulation of systems described using Petri Net (PN) formalism, specifically focusing on different classes of Batch Petri Nets (BPN) models. The goal of Simuleau is to provide an academic tool for modeling and simulating BPNs.

Currently, Simuleau is compatible only with Linux systems that have the GLPK library installed.
 
## Interface

To make the tool user-friendly, we have incorporated a textual menu interface that displays different options to the user. The main menu of Simuleau includes in option "1", which enables the user to compile the input file and verify the model using Simuleau.
````
    +--------------------------------------------------------+
    |             This is Simuleau - the BPN tool            |
    +--------------------------------------------------------+

  1) Compile a BPN model             4) Inspect data structures
  2) Simulate a compiled BPN model   5) Dynamics facilities
  3) Preferences                     6) About this version

    0) Exit Simuleau (Option 0 always exits the current menu)
````

The option "2" in the main menu leads the user to the simulation menu. In this menu, the user can select from different simulation algorithms. One of the options, option "2", simulates the system's evolution without control (autonomous). The remaining options simulate the system's evolution using external controlled events (option "1") or controlled methods that drive the system's evolution to a steady state.

````
              ******* Simulating a BPN model *******

 1) With controlled events       3) SF-On/Off control method
 2) Without controlled events    4) MF-On/Off control method

````


## Textual model description

Another important development in this new version of Simuleau is the input model description. The objectives for this syntax are to have a human-readable input file and to use well-known tools like flex and bison to make the development easy and extendable.

Here is a brief example of the input syntax used in Simuleau. The Petri net model is described in a textual file, which is structured in three blocks.

The first block of the input file gives a name to the model and sets the length and time units.

````
// double "/" for comments 
model example; 
length unity=km; 
time unity=h; 
````

The second block is dedicated to the model description. The keyword "network description" starts this block. The block is organized into two sub-parts. The first one describes the places of the model, while the second one is dedicated to the transitions. Different types of places (discrete, continuous, batch, and triangular) and transitions (discrete, continuous, batch) can be described. Each one has specific parameters, and some are optional.

An example of a model description is given below.

````
network description
  places
    place DPlace (discrete) 
      initial marking {2} // integer value 
      steady marking {1} // optional parameter
      output arc DTransition (2) // 2 is the weight of the arc

    place CPlace (continuous) 
      initial marking {2.3} // real value 
      steady marking {1.1} // optional parameter
      output arc CTransition // if no specified weight is 1
        
    place BPlace (batch)
      function (110, 200, 10) // speed, max density, and length 
      initial marking {(1.2, 150, 9.0)} 
      // list of initial batches 
      // {(length, density, position), ..}
      steady marking {} // optional parameter
      output arc BTransition

    place TBPlace (triangular)
      function (110, 200, 10, 3600) 
      // speed, max density, length, and max flow
      initial marking {(1.2, 150, 9.0, 35)} 
      // list of initial batches 
      // {(length, density, position, speed), ..}  
      steady marking {} // optional parameter
      output arc BTransition

  transitions
    transition DTransition (discrete) 
      timing (4) // real value
      output arc CPlace (3)

    transition CTransition (continuous) 
      flow (2.0) // real value    
      steady flow (2) // option parameter
      output arc BPlace
      output arc TBPlace
    
    transition BTransition (batch) 
      flow (200.0) // real value         
      steady flow (150) // option parameter
````

The last block of the input file concerns controlled events. Following the formalism definitions, it is possible to change the maximum flow of a transition and the maximum speed of a place. These controlled events will be included in the scheduler, and the changes will be made at the specified time.

````
// the section of controlled events are option 
controlled events
  max_flow_change=(flow, BTransition, 3000, 1.15); 
  // event type, concerned transition, new flow, and time
  max_speed_change=(speed, TBPlace, 100, 0.4); 
  // event type, concerned place, new speed, and time
````


## Textual simulation output

When a compiled model is simulated, an output file is generated. For each date that drives the system's evolution, Simuleau writes the current date, the state of each place and transition, and a list of all upcoming events with their respective dates. The upcoming events that will be processed are listed at the end of each step.

The following listing gives an example of a step in the output file for the example above starting from the initial date.

````
================================================
Current date : 0
================================================
================================================
At the begin of the step
================================================
Name:              DPlace
   Type:           Discrete
   Marks:          2
   Reserved Marks: 0
Name:              CPlace
   Type:           Continuous
   Marks:          5
   Reserved Marks: 0
Name:             BPlace
   Type:          Batch
   Density:       200
   Speed:         110
   Length:        10
   Batches:       1
                   (1.2, 150, 2)
Name:              CTransition
    Type:          Continuous
    Current Flow:  2
Name:              BTransition
    Type:          Batch
    Current Flow:  0

Events
Discrete transition is fired: 1
Date 1	 node DTransition
Continuous place becomes empty: 1
Date 2.5	 node CPlace
Batch becomes an output batch: 1
Date 0.07272727273	 node BPlace

Controlled Events
Transition maximum flow changes: 1
Date 1.15	 node BTransition	 name max_flow_change

Next events to proceed
Event date 0.07272727273	 in node BPlace	 type Batch becomes an output batch
````